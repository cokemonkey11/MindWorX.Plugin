$wex = 
    "MindWorX.GenericAddressManager",
    "MindWorX.GenericLibraryInjectionSystem",
    "MindWorX.SimpleAddressManager",
    "MindWorX.SimpleLibraryInjectionSystem",
    "MindWorX.War3Editor.ExtendedSettings",
    "MindWorX.War3Editor.Grimex",
    "MindWorX.War3Editor.Hooks",
    "MindWorX.War3Editor.JassHelper",
    "MindWorX.War3Editor.MenuInjectionSystem",
    "MindWorX.War3Editor.ProcessingInjectionSystem",
    "MindWorX.War3Editor.TriggerEditorSyntaxHighlight"
$rtc =
    "MindWorX.GenericAddressManager",
    "MindWorX.GenericLibraryInjectionSystem",
    "MindWorX.SimpleAddressManager",
    "MindWorX.SimpleLibraryInjectionSystem",
    "MindWorX.War3Game.ExtendedSettings",
    "MindWorX.War3Game.InfoInjection",
    "MindWorX.War3Game.JassAPI",
    "MindWorX.War3Game.WarAPI"

$root = Split-Path $MyInvocation.MyCommand.Path


# =============================
# === World Editor Extended ===
# =============================
$plugins = $wex
$folder = $root + "\WEX"
$name = "World Editor Extended"

"Bundling " + $name + " . . ."
del $folder -recurse | Out-Null
md -Force $folder | Out-Null
for ($i=0; $i -lt $plugins.Count; $i++) {
    $path = $root + "\" + $plugins[$i] + "\bin\x86\Debug"
    md -Force ($folder + "\" + $plugins[$i])
    cp ($path + "\*") ($folder + "\" + $plugins[$i]) -recurse
    #del ($folder + "\" + $plugins[$i] + "\*.pdb")
}
"Done!"
""

# =============================
# === Reinventing the Craft ===
# =============================
$plugins = $rtc
$folder = $root + "\RtC"
$name = "Reinventing the Craft"

"Bundling " + $name + " . . ."
del $folder -recurse | Out-Null
md -Force $folder | Out-Null
for ($i=0; $i -lt $plugins.Count; $i++) {
    $path = $root + "\" + $plugins[$i] + "\bin\x86\Debug"
    md -Force ($folder + "\" + $plugins[$i])
    cp ($path + "\*") ($folder + "\" + $plugins[$i]) -recurse
    #del ($folder + "\" + $plugins[$i] + "\*.pdb")
}
"Done!"
""