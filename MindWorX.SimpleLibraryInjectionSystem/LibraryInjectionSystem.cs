﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Runtime.InteropServices;
using EasyHook;
using MindWorX.GenericLibraryInjectionSystem;
using MindWorX.Unmanaged.Windows;
using Serilog;
using SharpCraft;

namespace MindWorX.SimpleLibraryInjectionSystem
{
    [Export(typeof(ILibraryInjectionSystem))]
    internal unsafe class LibraryInjectionSystem : IPlugin, ILibraryInjectionSystem
    {
        private LocalHook loadLibraryAHook;

        private LocalHook loadLibraryWHook;

        private readonly HashSet<string> loadedFiles = new HashSet<string>();

        private readonly List<EventHandler<LibraryLoadingEventArgs>> genericLoadingHandlers = new List<EventHandler<LibraryLoadingEventArgs>>();

        private readonly Dictionary<string, List<EventHandler<LibraryLoadingEventArgs>>> specificLoadingHandlers = new Dictionary<string, List<EventHandler<LibraryLoadingEventArgs>>>();

        private readonly List<EventHandler<LibraryLoadedEventArgs>> genericLoadedHandlers = new List<EventHandler<LibraryLoadedEventArgs>>();

        private readonly Dictionary<string, List<EventHandler<LibraryLoadedEventArgs>>> specificLoadedHandlers = new Dictionary<string, List<EventHandler<LibraryLoadedEventArgs>>>();

        public event PluginReadyEventHandler Ready;

        public void Initialize()
        {
            this.loadLibraryAHook = LocalHook.Create(Kernel32.GetProcAddress(Kernel32.Handle, nameof(Kernel32.LoadLibraryA)), new Kernel32.LoadLibraryAPrototype(this.LoadLibraryAHook), null);
            this.loadLibraryAHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            this.loadLibraryWHook = LocalHook.Create(Kernel32.GetProcAddress(Kernel32.Handle, nameof(Kernel32.LoadLibraryW)), new Kernel32.LoadLibraryWPrototype(this.LoadLibraryWHook), null);
            this.loadLibraryWHook.ThreadACL.SetExclusiveACL(new[] { 0 });

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        public PluginInfo Info { get; } = new PluginInfo(
            "Library Injection System", 
            "A system for intercepting library loading and injecting changes before they are used.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        private IntPtr LoadLibraryWHook(string fileName) => this.LoadLibraryHook(fileName, Kernel32.LoadLibraryW);

        private IntPtr LoadLibraryAHook(string fileName) => this.LoadLibraryHook(fileName, Kernel32.LoadLibraryA);

        private IntPtr LoadLibraryHook(string fileName, Kernel32.LoadLibraryPrototype loader)
        {
            var loading = new LibraryLoadingEventArgs(fileName);

            List<EventHandler<LibraryLoadingEventArgs>> specificLoadingHandlers;
            if (this.specificLoadingHandlers.TryGetValue(fileName, out specificLoadingHandlers))
            {
                foreach (var handler in specificLoadingHandlers)
                {
                    handler?.Invoke(this, loading);
                }
            }
            foreach (var handler in this.genericLoadingHandlers)
            {
                handler?.Invoke(this, loading);
            }

            var loaded = new LibraryLoadedEventArgs(
                loading.FileName,
                String.IsNullOrEmpty(loading.FileName) ? IntPtr.Zero : loader(loading.FileName),
                loading.FileName != fileName);

            if (!this.loadedFiles.Contains(loaded.FileName))
            {
                this.loadedFiles.Add(loaded.FileName);
                if (loaded.Handle != IntPtr.Zero)
                {
                    var module = new { ModuleFileName = loaded.FileName, ModuleHandle = ((uint)loaded.Handle).ToString("X8") };
                    Log.Logger?.Information("Loaded {@module}", module);
                    try
                    {
                        var header = (DosHeader*)loaded.Handle;

                        var ntHeader = (ImageNTHeader*)(loaded.Handle + header->e_lfanew);
                        for (var i = 0; i < ntHeader->FileHeader.NumberOfSections; i++)
                        {
                            var section = (ImageSectionHeader*)((byte*)ntHeader + ntHeader->Size + i * sizeof(ImageSectionHeader));

                            Log.Logger?.Verbose(" - {@section}", new
                            {
                                SectionName = Marshal.PtrToStringAnsi((IntPtr)section->Name, 8).Replace("\0", ""),
                                SectionHandle = ((uint)loaded.Handle + section->VirtualAddress).ToString("X8"),
                                SectionSize = section->VirtualSize,
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Logger?.Error(e.ToString());
                    }
                }
                else
                {
                    Log.Logger?.Error("Failed {@module}", new { ModuleFileName = loaded.FileName, Exception = new Win32Exception(Marshal.GetLastWin32Error()) });
                }
            }

            if (loaded.Handle != IntPtr.Zero)
            {
                List<EventHandler<LibraryLoadedEventArgs>> specificLoadedHandlers;
                if (this.specificLoadedHandlers.TryGetValue(fileName, out specificLoadedHandlers))
                {
                    foreach (var handler in specificLoadedHandlers)
                    {
                        handler?.Invoke(this, loaded);
                    }
                }
                foreach (var handler in this.genericLoadedHandlers)
                {
                    handler?.Invoke(this, loaded);
                }
            }

            return loaded.Handle;
        }

        public void AddLibraryLoadingEventHandler(EventHandler<LibraryLoadingEventArgs> handler)
        {
            this.genericLoadingHandlers.Add(handler);
        }

        public bool RemoveLibraryLoadingEventHandler(EventHandler<LibraryLoadingEventArgs> handler)
        {
            return this.genericLoadingHandlers.Remove(handler);
        }

        public void AddLibraryLoadingEventHandler(string fileName, EventHandler<LibraryLoadingEventArgs> handler)
        {
            List<EventHandler<LibraryLoadingEventArgs>> handlers;
            if (!this.specificLoadingHandlers.TryGetValue(fileName, out handlers))
                this.specificLoadingHandlers[fileName] = handlers = new List<EventHandler<LibraryLoadingEventArgs>>();
            handlers.Add(handler);
        }

        public bool RemoveLibraryLoadingEventHandler(string fileName, EventHandler<LibraryLoadingEventArgs> handler)
        {
            List<EventHandler<LibraryLoadingEventArgs>> handlers;
            if (!this.specificLoadingHandlers.TryGetValue(fileName, out handlers))
                return false;
            return handlers.Remove(handler);
        }

        public void AddLibraryLoadedEventHandler(EventHandler<LibraryLoadedEventArgs> handler)
        {
            this.genericLoadedHandlers.Add(handler);
        }

        public bool RemoveLibraryLoadedEventHandler(EventHandler<LibraryLoadedEventArgs> handler)
        {
            return this.genericLoadedHandlers.Remove(handler);
        }

        public void AddLibraryLoadedEventHandler(string fileName, EventHandler<LibraryLoadedEventArgs> handler)
        {
            List<EventHandler<LibraryLoadedEventArgs>> handlers;
            if (!this.specificLoadedHandlers.TryGetValue(fileName, out handlers))
                this.specificLoadedHandlers[fileName] = handlers = new List<EventHandler<LibraryLoadedEventArgs>>();
            handlers.Add(handler);
        }

        public bool RemoveLibraryLoadedEventHandler(string fileName, EventHandler<LibraryLoadedEventArgs> handler)
        {
            List<EventHandler<LibraryLoadedEventArgs>> handlers;
            if (!this.specificLoadedHandlers.TryGetValue(fileName, out handlers))
                return false;
            return handlers.Remove(handler);
        }
    }
}
