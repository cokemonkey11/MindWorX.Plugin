﻿using System;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using MindWorX.Blizzard.Storm;
using MindWorX.War3Editor.ProcessingInjectionSystem;

namespace MindWorX.War3Editor.JassHelper
{
    [Export]
    public class JassHelperCompiler : ICompiler
    {
        private readonly string jassHelper;

        private readonly string commonJ;

        private readonly string blizzardJ;

        public string Name { get; } = "JassHelper (Vexorian)";

        public string Group { get; } = "JassHelper";

        public JassHelperCompiler()
        {
            var root = Path.GetDirectoryName(typeof(JassHelperCompiler).Assembly.Location);
            if (String.IsNullOrEmpty(root))
                throw new InvalidOperationException("Assembly is missing root location.");

            this.jassHelper = Path.Combine(root.ToLower(), @"executable\jasshelper.exe");
            if (!File.Exists(this.jassHelper))
                throw new FileNotFoundException("Could not find JassHelper.", "jasshelper.exe");

            if (!Directory.Exists(Path.Combine(root.ToLower(), @"temp")))
                Directory.CreateDirectory(Path.Combine(root.ToLower(), @"temp"));
            this.commonJ = Path.Combine(root.ToLower(), @"temp\common.j");
            this.blizzardJ = Path.Combine(root.ToLower(), @"temp\blizzard.j");
        }

        /// <summary>
        /// This method is called both when saving the map and when running 
        /// </summary>
        /// <param name="scriptPath"></param>
        /// <param name="savingMap"></param>
        /// <returns></returns>
        public bool CompileScript(string scriptPath, bool savingMap)
        {
            if (savingMap)
                return true; // if we are saving the map, we don't need to check here, we can wait.

            // in here we can't open the map, as it doesn't exist, any special common.j/blizzard.j will not be included
            using (var commonJIn = SFile.Open(@"scripts\common.j"))
            using (var commonJOut = File.Open(this.commonJ, FileMode.Create, FileAccess.Write, FileShare.Write))
            using (var blizzardJIn = SFile.Open(@"scripts\blizzard.j"))
            using (var blizzardJOut = File.Open(this.blizzardJ, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                // clone the files to our output
                commonJIn.CopyTo(commonJOut);
                blizzardJIn.CopyTo(blizzardJOut);
            }

            var process = Process.Start(this.jassHelper, $@"--scriptonly ""{this.commonJ}"" ""{this.blizzardJ}"" ""{scriptPath}"" ""{scriptPath}""");
            process.WaitForExit();
            return process.ExitCode == 0;
        }

        public bool CompileMap(string mapPath)
        {
            // open the map with high priority
            using (SFile.OpenArchive(mapPath, 15))
            // open the files without a specific archive, this will load the first file it finds
            // this means it'll load from map if the map has the file, but from patch.mpq
            // if the map doesn't have the file.
            using (var commonJIn = SFile.Open(@"scripts\common.j"))
            using (var commonJOut = File.Open(this.commonJ, FileMode.Create, FileAccess.Write, FileShare.Write))
            using (var blizzardJIn = SFile.Open(@"scripts\blizzard.j"))
            using (var blizzardJOut = File.Open(this.blizzardJ, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                // clone the files to our output
                commonJIn.CopyTo(commonJOut);
                blizzardJIn.CopyTo(blizzardJOut);
            }

            var process = Process.Start(this.jassHelper, $@"""{this.commonJ}"" ""{this.blizzardJ}"" ""{mapPath}""");
            process.WaitForExit();
            return process.ExitCode == 0;
        }
    }
}
