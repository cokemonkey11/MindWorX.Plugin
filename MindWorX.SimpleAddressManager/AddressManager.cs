﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SharpCraft;
using MindWorX.GenericAddressManager;
using System.ComponentModel.Composition;
using Serilog;

namespace MindWorX.SimpleAddressManager
{
    [Export(typeof(IAddressManager))]
    internal class AddressManager : IPlugin, IAddressManager
    {
        [Import]
        private Profile profile;

        private readonly Dictionary<string, Dictionary<string, int>> addresses = new Dictionary<string, Dictionary<string, int>>();

        public Dictionary<string, int> this[string key]
        {
            get
            {
                Dictionary<string, int> address;
                if (!this.addresses.TryGetValue(key, out address))
                    this.addresses[key] = address = new Dictionary<string, int>();
                return address;
            }
            set { this.addresses[key] = value; }
        }

        public Dictionary<string, int> Current { get; private set; }

        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo(
            "Basic Address Manager",
            "A basic INI based address manager",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        private void Initialize(string version)
        {
            var root = Path.GetDirectoryName(typeof(AddressManager).Assembly.Location);
            if (root == null)
                throw new InvalidOperationException("Assembly is missing root location.");

            var addressesIni = Path.Combine(root.ToLower(), @"addresses.ini");
            if (!File.Exists(addressesIni))
                throw new FileNotFoundException("Could not find addresses.", "addresses.ini");

            this.Current = this[version];
            Log.Logger?.Information("Current version: " + version);
            Log.Logger?.Information("Loading addresses from: {fileName} . .", addressesIni);
            var sw = Stopwatch.StartNew();
            var lineNumber = 0;
            using (var stream = File.Open(addressesIni, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var reader = new StreamReader(stream))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    lineNumber++;
                    line = line.Trim();

                    // empty
                    if (String.IsNullOrWhiteSpace(line))
                        continue;

                    // comment
                    if (line.StartsWith(";") || line.StartsWith("#"))
                        continue;

                    // version
                    if (line.StartsWith("["))
                    {
                        version = line.Substring(1, line.IndexOf("]", StringComparison.Ordinal) - 1);
                        continue;
                    }

                    // address
                    var parts = line.Split('=');
                    if (parts.Length == 2)
                    {
                        var key = parts[0].Trim();
                        var value = parts[1].Trim();
                        Log.Logger?.Information("[{version}].{key} = {value}", version, key, value);
                        this[version][key] = int.Parse(value, System.Globalization.NumberStyles.AllowHexSpecifier);
                        continue;
                    }

                    Log.Logger?.Warning("Invalid line({lineNumber}): {line}", lineNumber, line);
                }
            }
            Log.Logger?.Information(". . done ({time:0.00}ms)!", sw.Elapsed.TotalMilliseconds);
        }

        public void Initialize()
        {
            string version;

            var fileName = this.profile.FileName.ToLower();

            if (String.Equals(Path.GetFileName(fileName), "worldedit.exe", StringComparison.CurrentCultureIgnoreCase))
            {
                Log.Logger?.Information("Detected worldedit.exe, switching to war3.exe for version lookup.");
                version = FileVersionInfo.GetVersionInfo(fileName.Replace("worldedit.exe", "war3.exe")).ProductVersion;
            }
            else
            {
                version = FileVersionInfo.GetVersionInfo(fileName).ProductVersion;
            }


            if (version != null)
                this.Initialize(version);
            else
                throw new InvalidOperationException("Unabled to locate version info for addresses");


            this.IsReady = true;
            this.Ready?.Invoke(this);
        }
    }
}
