﻿using System;

namespace MindWorX.War3Editor.Hooks
{
    public class ScriptCompiledEventArgs : EventArgs
    {
        public ScriptCompiledEventArgs(object o, string fileName, bool savingMap, bool result)
        {
            this.Object = o;
            this.FileName = fileName;
            this.SavingMap = savingMap;
            this.Result = result;
        }

        public object Object { get; }

        /// <summary>
        /// The path to the file being compiled.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// Determines whether the map is being saved or just validated.
        /// </summary>
        public bool SavingMap { get; }

        /// <summary>
        /// The result of the vanilla compiler. Can be overriden.
        /// </summary>
        public bool Result { get; set; }
    }
}
