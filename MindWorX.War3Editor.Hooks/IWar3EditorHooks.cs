﻿using System;

namespace MindWorX.War3Editor.Hooks
{
    public interface IWar3EditorHooks
    {
        event EventHandler<MapSavingEventArgs> MapSaving;

        event EventHandler<MapSavedEventArgs> MapSaved;

        event EventHandler<ObjectIdEventArgs> GetNextObjectId;

        event EventHandler<ScriptCompilingEventArgs> ScriptCompiling;

        event EventHandler<ScriptCompiledEventArgs> ScriptCompiled;

        event EventHandler<CreatingProcessEventArgs> CreatingProcess;
    }
}
