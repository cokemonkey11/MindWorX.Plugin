﻿using System;
using System.Runtime.InteropServices;
using EasyHook;
using MindWorX.Unmanaged.Windows;
using Serilog;
using SharpCraft;
using System.ComponentModel.Composition;
using MindWorX.Blizzard;
using MindWorX.GenericAddressManager;

namespace MindWorX.War3Editor.Hooks
{
    [Export(typeof(IWar3EditorHooks))]
    internal class War3EditorHooks : IPlugin, IWar3EditorHooks
    {
        [Import]
        private Profile profile;

        [Import]
        private IAddressManager addressManager;

        public event PluginReadyEventHandler Ready;

        public event EventHandler<MapSavingEventArgs> MapSaving;

        public event EventHandler<MapSavedEventArgs> MapSaved;

        public event EventHandler<ObjectIdEventArgs> GetNextObjectId;

        public event EventHandler<ScriptCompilingEventArgs> ScriptCompiling;

        public event EventHandler<ScriptCompiledEventArgs> ScriptCompiled;

        public event EventHandler<CreatingProcessEventArgs> CreatingProcess;

        // ReSharper disable InconsistentNaming
        #region bool __thiscall SaveMap(int this, char* pFileName)

        private LocalHook saveMapLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        private delegate bool SaveMapPrototype(IntPtr @this, IntPtr pFileName);

        private SaveMapPrototype saveMap;

        #endregion

        #region int __thiscall GetNextObjectId(int this, int oldObjectId)

        private LocalHook getNextObjectIdLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        private delegate ObjectIdB GetNextObjectIdPrototype(IntPtr @this, ObjectIdB oldObjectId);

        private GetNextObjectIdPrototype getNextObjectId;

        #endregion

        #region bool __thiscall CompileScript(void *this, char* pFileName, bool saveMap)

        private LocalHook compileScriptLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        private delegate bool CompileScriptPrototype(IntPtr @this, IntPtr pFileName, bool saveMap);

        private CompileScriptPrototype compileScript;

        #endregion

        #region CreateProcessA

        private LocalHook createProcessALocalHook;

        private Kernel32.CreateProcessA_SafePrototype createProcessA;

        #endregion
        // ReSharper restore InconsistentNaming

        public PluginInfo Info { get; } = new PluginInfo(
            typeof(War3EditorHooks).FullName,
            "A collection of hooks to enhance the world editor.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIEditor)
                return;

            var createProcessAPtr = Kernel32.GetProcAddress(Kernel32.Handle, "CreateProcessA");
            Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.CreateProcessAHook), (uint)createProcessAPtr);
            this.createProcessA = Marshal.GetDelegateForFunctionPointer<Kernel32.CreateProcessA_SafePrototype>(createProcessAPtr);
            this.createProcessALocalHook = LocalHook.Create(createProcessAPtr, new Kernel32.CreateProcessA_SafePrototype(this.CreateProcessAHook), null);
            this.createProcessALocalHook.ThreadACL.SetExclusiveACL(new[] { 0 });

            var handle = Kernel32.GetModuleHandle("worldedit.exe");

            if (this.addressManager.Current["WE_SaveMap"] != 0)
            {
                var saveMapPtr = handle + this.addressManager.Current["WE_SaveMap"];
                Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.SaveMapHook), (uint)saveMapPtr);
                this.saveMap = Marshal.GetDelegateForFunctionPointer<SaveMapPrototype>(saveMapPtr);
                this.saveMapLocalHook = LocalHook.Create(saveMapPtr, new SaveMapPrototype(this.SaveMapHook), null);
                this.saveMapLocalHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WE_SaveMap");
            }

            if (this.addressManager.Current["WE_GetNextObjectId"] != 0)
            {
                var getNextObjectIdPtr = handle + this.addressManager.Current["WE_GetNextObjectId"];
                Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.GetNextObjectIdHook), (uint)getNextObjectIdPtr);
                this.getNextObjectId = Marshal.GetDelegateForFunctionPointer<GetNextObjectIdPrototype>(getNextObjectIdPtr);
                this.getNextObjectIdLocalHook = LocalHook.Create(getNextObjectIdPtr, new GetNextObjectIdPrototype(this.GetNextObjectIdHook), null);
                this.getNextObjectIdLocalHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WE_GetNextObjectId");
            }

            if (this.addressManager.Current["WE_CompileScript"] != 0)
            {
                var compileScriptPtr = handle + this.addressManager.Current["WE_CompileScript"];
                Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.CompileScriptHook), (uint)compileScriptPtr);
                this.compileScript = Marshal.GetDelegateForFunctionPointer<CompileScriptPrototype>(compileScriptPtr);
                this.compileScriptLocalHook = LocalHook.Create(compileScriptPtr, new CompileScriptPrototype(this.CompileScriptHook), null);
                this.compileScriptLocalHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WE_CompileScript");
            }

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        private bool CreateProcessAHook(
            IntPtr lpApplicationName, IntPtr lpCommandLine, IntPtr lpProcessAttributes, IntPtr lpThreadAttributes,
            bool bInheritHandles, uint dwCreationFlags, IntPtr lpEnvironment, IntPtr lpCurrentDirectory, IntPtr lpStartupInfo, IntPtr lpProcessInformation)
        {
            var creatingProcess = new CreatingProcessEventArgs(Marshal.PtrToStringAnsi(lpApplicationName), Marshal.PtrToStringAnsi(lpCommandLine));
            this.OnCreatingProcess(creatingProcess);

            if (creatingProcess.Cancel)
                return creatingProcess.CancelResult;

            return this.createProcessA(
                lpApplicationName, lpCommandLine, lpProcessAttributes, lpThreadAttributes,
                bInheritHandles, dwCreationFlags, lpEnvironment, lpCurrentDirectory, lpStartupInfo, lpProcessInformation);
        }

        private bool SaveMapHook(IntPtr @this, IntPtr pFileName)
        {
            var fileName = Marshal.PtrToStringAnsi(pFileName);

            var mapSaving = new MapSavingEventArgs(@this, fileName);
            Log.Logger?.Information("{@mapSaving}", mapSaving);
            this.OnMapSaving(mapSaving);

            if (mapSaving.Cancel)
                return mapSaving.CancelResult;

            var mapSaved = new MapSavedEventArgs(@this, fileName, this.saveMap(@this, pFileName));
            Log.Logger?.Information("{@mapSaved}", mapSaved);
            this.OnMapSaved(mapSaved);

            return mapSaved.Result;
        }

        private ObjectIdB GetNextObjectIdHook(IntPtr @this, ObjectIdB oldObjectId)
        {
            Log.Logger?.Information("+GetNextObjectId({this:X8}, {a2})", (uint)@this, oldObjectId);

            var objectId = new ObjectIdEventArgs(this.getNextObjectId(@this, oldObjectId));
            this.OnGetNextObjectId(objectId);

            Log.Logger?.Information("-GetNextObjectId({this:X8}, {a2}) = {result}", (uint)@this, oldObjectId, objectId.ObjectId);

            return objectId.ObjectId;
        }

        private bool CompileScriptHook(IntPtr @this, IntPtr pFileName, bool saveMap)
        {
            var fileName = Marshal.PtrToStringAnsi(pFileName);

            var scriptCompiling = new ScriptCompilingEventArgs(@this, fileName, saveMap);
            Log.Logger?.Information("{@scriptCompiling}", scriptCompiling);
            this.OnScriptCompiling(scriptCompiling);

            if (scriptCompiling.Cancel)
                return scriptCompiling.CancelResult;

            var scriptCompiled = new ScriptCompiledEventArgs(@this, fileName, saveMap, this.compileScript(@this, pFileName, saveMap));
            Log.Logger?.Information("{@scriptCompiled}", scriptCompiled);
            this.OnScriptCompiled(scriptCompiled);

            return scriptCompiled.Result;
        }

        protected void OnMapSaving(MapSavingEventArgs e) => this.MapSaving?.Invoke(this, e);

        protected void OnMapSaved(MapSavedEventArgs e) => this.MapSaved?.Invoke(this, e);

        protected void OnGetNextObjectId(ObjectIdEventArgs e) => this.GetNextObjectId?.Invoke(this, e);

        protected void OnScriptCompiling(ScriptCompilingEventArgs e) => this.ScriptCompiling?.Invoke(this, e);

        protected void OnScriptCompiled(ScriptCompiledEventArgs e) => this.ScriptCompiled?.Invoke(this, e);

        protected void OnCreatingProcess(CreatingProcessEventArgs e) => this.CreatingProcess?.Invoke(this, e);
    }
}
