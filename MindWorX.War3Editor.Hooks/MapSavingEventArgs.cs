﻿using System.ComponentModel;

namespace MindWorX.War3Editor.Hooks
{
    public class MapSavingEventArgs : CancelEventArgs
    {
        public MapSavingEventArgs(object o, string fileName)
        {
            this.Object = o;
            this.FileName = fileName;
        }

        public object Object { get; }

        /// <summary>
        /// The path to the map being saved.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// The result to use if saving is canceled.
        /// </summary>
        public bool CancelResult { get; set; }
    }
}
