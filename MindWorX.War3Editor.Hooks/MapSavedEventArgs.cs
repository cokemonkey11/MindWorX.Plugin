﻿using System;

namespace MindWorX.War3Editor.Hooks
{
    public class MapSavedEventArgs : EventArgs
    {
        public MapSavedEventArgs(object o, string fileName, bool result)
        {
            this.Object = o;
            this.FileName = fileName;
            this.Result = result;
        }

        public object Object { get; }

        /// <summary>
        /// The path to the map being saved.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// The result of the vanilla map saver. Can be overriden.
        /// </summary>
        public bool Result { get; set; }
    }
}
