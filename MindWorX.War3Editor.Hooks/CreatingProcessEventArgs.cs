﻿using System.ComponentModel;

namespace MindWorX.War3Editor.Hooks
{
    public class CreatingProcessEventArgs : CancelEventArgs
    {
        public CreatingProcessEventArgs(string applicationName, string commandLine)
        {
            this.ApplicationName = applicationName;
            this.CommandLine = commandLine;
        }

        public string ApplicationName { get;  }

        public string CommandLine { get; }

        public bool CancelResult { get; set; }
    }
}
