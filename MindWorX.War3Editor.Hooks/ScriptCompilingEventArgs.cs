﻿using System.ComponentModel;

namespace MindWorX.War3Editor.Hooks
{
    public class ScriptCompilingEventArgs : CancelEventArgs
    {
        public ScriptCompilingEventArgs(object o, string fileName, bool savingMap)
        {
            this.Object = o;
            this.FileName = fileName;
            this.SavingMap = savingMap;
        }

        public object Object { get; }

        /// <summary>
        /// The path to the file being compiled.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// Determines whether the map is being saved or just validated.
        /// </summary>
        public bool SavingMap { get; }

        /// <summary>
        /// The result to use if compiling is canceled.
        /// </summary>
        public bool CancelResult { get; set; }
    }
}
