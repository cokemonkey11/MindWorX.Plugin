﻿using System;
using MindWorX.Blizzard;

namespace MindWorX.War3Editor.Hooks
{
    public class ObjectIdEventArgs : EventArgs
    {
        public ObjectIdEventArgs(ObjectIdB objectId)
        {
            this.ObjectId = objectId;
        }

        public ObjectIdB ObjectId { get; set; }
    }
}
