﻿using System.ComponentModel.Composition;

namespace MindWorX.War3Editor.ProcessingInjectionSystem
{
    [InheritedExport(typeof(ICompiler))]
    public interface ICompiler
    {
        string Name { get; }

        string Group { get; }

        bool CompileScript(string scriptPath, bool savingMap);

        bool CompileMap(string mapPath);
    }
}
