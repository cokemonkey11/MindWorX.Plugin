﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Windows.Forms;
using MindWorX.War3Editor.Hooks;
using MindWorX.War3Editor.MenuInjectionSystem;
using Serilog;
using SharpCraft;

namespace MindWorX.War3Editor.ProcessingInjectionSystem
{
    public class War3EditorProcessingInjectionSystem : IPlugin
    {
        [Import]
        private Profile profile;

        [ImportMany(typeof(ICompiler))]
        private IEnumerable<ICompiler> compilers;

        [Import]
        private IWar3EditorMenuInjectionSystem worldEditMenus;

        [Import]
        private IWar3EditorHooks worldEditHooksPlugin;

        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo("World Edit Compiler Injector", "A system for injecting custom compilers/preprocessors in the World Editor.", "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIEditor)
                return;

            var root = Path.GetDirectoryName(typeof(War3EditorProcessingInjectionSystem).Assembly.Location);

            Log.Logger?.Information("{@compilers}", this.compilers);

            this.worldEditMenus.WorldEditor.MenuReady += (sender, eventArgs) =>
            {
                var compilerMenu = this.worldEditMenus.WorldEditor.AppendMenu("Compilers");

                var configurationMenu = compilerMenu.AppendMenuItem("Configuration");
                //configurationMenu.Click += (s, e) => this.configurationWindow.Show();

                var aboutMenu = compilerMenu.AppendMenuItem("About World Edit Compiler Injector...");
                aboutMenu.Click += (s, e) => MessageBox.Show("Version " + this.Info.Version);
            };

            this.worldEditHooksPlugin.MapSaved += this.WorldEditHooksPluginOnMapSaved;
            this.worldEditHooksPlugin.ScriptCompiling += this.WorldEditHooksPluginOnScriptCompiling;

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        private void WorldEditHooksPluginOnScriptCompiling(object sender, ScriptCompilingEventArgs e)
        {
            foreach (var compiler in this.compilers)
            {
                try
                {
                    compiler.CompileScript(e.FileName, e.SavingMap);
                }
                catch (Exception ex) { Log.Logger?.Error(ex, "Exception in compiler({name}).", compiler.Name); }
            }

            //if (this.configuration.DisableVanillaCompiler)
            //{
            //    e.Cancel = true;
            //    e.CancelResult = true;
            //}
        }

        private void WorldEditHooksPluginOnMapSaved(object sender, MapSavedEventArgs e)
        {
            foreach (var compiler in this.compilers)
            {
                try
                {
                    compiler.CompileMap(e.FileName);
                }
                catch (Exception ex) { Log.Logger?.Error(ex, "Exception in compiler({name}).", compiler.Name); }
            }
        }
    }
}
