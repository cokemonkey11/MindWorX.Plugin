﻿using System.Collections.Generic;

namespace MindWorX.GenericAddressManager
{
    public interface IAddressManager
    {
        /// <summary>
        /// The collection of addresses for the currently detected version.
        /// </summary>
        Dictionary<string, int> Current { get; }

        /// <summary>
        /// All the addresses detected.
        /// </summary>
        /// <param name="key">The version the addresses are for.</param>
        /// <returns></returns>
        Dictionary<string, int> this[string key] { get; set; }
    }
}
