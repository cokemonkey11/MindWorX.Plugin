﻿using MindWorX.War3Game.WarAPI.Types;

namespace MindWorX.War3Game.WarAPI
{
    public unsafe interface IGame
    {
        ThreadLocalStorage* GetThreadLocalStorage();
    }
}
