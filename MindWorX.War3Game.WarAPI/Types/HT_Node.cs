﻿using System;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x1C)]
    public unsafe struct HT_Node
    {
        //  typedef struct HT_Node {
        //      int hash;                   // 0x00
        //      struct HT_Bucket *parent;	// 0x04
        //      struct HT_Node *next;		// 0x08
        //      int xC;                     // 0x0C
        //      int x10;                    // 0x10
        //      const char* key;            // 0x14
        //      void* value;                // 0x18
        //  } HT_Node;

        public int Hash;

        public HT_Bucket* Parent;

        public HT_Node* Next;

        public IntPtr field000C;

        public IntPtr field0010;

        public IntPtr KeyPtr;

        public void* Value;

        public string Key => Memory.ReadString(this.KeyPtr);
    }
}
