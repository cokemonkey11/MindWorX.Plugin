﻿using System.Runtime.InteropServices;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Explicit, CharSet = CharSet.Ansi, Pack = 1, Size = 8)]
    public unsafe struct OpCode
    {
        //  typedef struct opcode {
        //      union {
        //          unsigned char r1;
        //          unsigned char nfarg;
        //      };
        //      union {
        //          unsigned char r2;
        //          unsigned char usereturn;
        //      };
        //      union {
        //          unsigned char r3;
        //          unsigned char rettype;
        //      };
        //      unsigned char optype;
        //      union {
        //          int arg;
        //          opcode* dest;
        //      };
        //  } opcode;

        /* union { */
        [FieldOffset(0x00)]
        [MarshalAs(UnmanagedType.U1)]
        public byte R1;
        [FieldOffset(0x00)]
        [MarshalAs(UnmanagedType.U1)]
        public byte NFArg;
        /* }; */

        /* union { */
        [FieldOffset(0x01)]
        [MarshalAs(UnmanagedType.U1)]
        public byte R2;
        [FieldOffset(0x01)]
        [MarshalAs(UnmanagedType.U1)] // should be I1 and the type should be bool, but it will cause silent fails.
        public byte UseReturn;
        /* }; */

        /* union { */
        [FieldOffset(0x02)]
        [MarshalAs(UnmanagedType.U1)]
        public byte R3;
        [FieldOffset(0x02)]
        [MarshalAs(UnmanagedType.U1)]
        public JassType ReturnType;
        /* }; */

        [FieldOffset(0x03)]
        [MarshalAs(UnmanagedType.U1)]
        public OpCodeType OpType;

        /* union { */
        [FieldOffset(0x04)]
        [MarshalAs(UnmanagedType.U4)]
        public int Argument;
        [FieldOffset(0x04)]
        //[MarshalAs(UnmanagedType.LPStruct)], this causes a silent fail.
        public OpCode* Destination;
        /* }; */
    }
}
