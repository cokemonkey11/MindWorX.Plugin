﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x0C)]
    public unsafe struct StringPool
    {
        public IntPtr field0000;

        public IntPtr field0004;

        public StringNode** Nodes;
    }
}
