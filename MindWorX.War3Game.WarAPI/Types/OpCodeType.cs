﻿namespace MindWorX.War3Game.WarAPI.Types
{
    public enum OpCodeType : byte
    {
        EndProgram = 0x1,
        Function = 0x3, // _ _ rettype funcname
        EndFunction = 0x4,
        Local = 0x5, // _ _ type name
        Global = 0x6, Constant = 0x7,
        PopFuncArg = 0x8, // _ srcargi type destvar
        CleanStack = 0xB, // _ _ nargs _
        Literal = 0xC, // _ type destreg srcvalue
        SetRet = 0xD, // _ srcreg _ _
        GetVar = 0xE, // _ type destreg srcvar
        Code = 0xF,
        GetArray = 0x10,
        SetVar = 0x11,   // _ _ srcreg destvar
        SetArray = 0x12,
        Push = 0x13, // _ _ srcreg _
        SetRight = 0x14,
        Native = 0x15, // _ _ _ fn
        JassCall = 0x16, // _ _ _ fn
        I2R = 0x17,
        And = 0x18,
        Or = 0x19,
        Equal = 0x1A,
        NotEqual = 0x1B,     // check
        LesserEqual = 0x1C, GreaterEqual = 0x1D,
        Lesser = 0x1E, Greater = 0x1F,
        Add = 0x20, Sub = 0x21, Mul = 0x22, Div = 0x23,
        Modulo = 0x24,
        Negate = 0x25,
        Not = 0x26,
        Return = 0x27,   // _ _ _ _
        JumpTarget = 0x28,
        JumpIfTrue = 0x29, JumpIfFalse = 0x2A,
        Jump = 0x2B,
    }
}
