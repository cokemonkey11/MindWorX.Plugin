﻿using System;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x28)]
    public unsafe struct Variable
    {
        //  typedef struct Variable {
        //      int hash;                   // 0x00
        //      struct HT_Bucket *parent;	// 0x04
        //      struct Variable *next;		// 0x08
        //      int xC;                     // 0x0C
        //      int x10;                    // 0x10
        //      const char* name;           // 0x14
        //      int vartype;                // 0x18
        //      int vartype2;               // 0x1C
        //      int value;                  // 0x20
        //      int is_func_arg;            // 0x24
        //  } Variable;

        public int Hash;

        public HT_Bucket* Parent;

        public Variable* Next;

        public IntPtr field000C;

        public IntPtr field0010;

        public IntPtr NamePtr;

        public JassType Type;

        public JassType Type2;

        public IntPtr Value;

        public bool IsFunctionArgument;

        public string Name => Memory.ReadString(this.NamePtr);
    }
}
