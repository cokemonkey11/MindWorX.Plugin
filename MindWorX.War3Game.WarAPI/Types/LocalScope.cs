﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0xBC)]
    public unsafe struct LocalScope
    {
        //  typedef struct LocalScope {
        //      struct LocalScope *ret;
        //      //struct LocalScope *prev;
        //      struct LocalScope *parent;	//x04   //has got to be a parent pointer
        //      int nalloc;                 //x08   //This must be a stack limit of 32 function arguments ?
        //      Variable* stack[0x20];      //x0C
        //      int nstack;                 //x8C
        //      Hashtable locals;           //x90
        //      int xB8;                    //xB8
        //  } LocalScope;

        public LocalScope* Ret;

        public LocalScope* Parent;

        public int NAlloc;

        private fixed byte Variables[0x20 * 0x28]; // This can't be done in a graceful way. Needs helper methods.

        public int NStack;

        public Hashtable Locals;

        public IntPtr field00B8;
    }
}
