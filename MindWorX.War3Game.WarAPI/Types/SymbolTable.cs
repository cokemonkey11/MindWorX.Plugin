﻿using System.Runtime.InteropServices;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x0C)]
    public unsafe struct SymbolTable
    {
        public OpCode* FirstOperation;

        public int ProgramLength;

        public StringPool* StringPool;
    }
}
