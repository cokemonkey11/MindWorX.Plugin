﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential/*, Size = unknown*/)]
    public unsafe struct VirtualMachine
    {
        public IntPtr field0000;
        public IntPtr field0004;
        public IntPtr field0008;
        public IntPtr field000C;
        public IntPtr field0010;
        public IntPtr field0014;
        public IntPtr field0018;
        public IntPtr field001C;
        public IntPtr CurrentOpcode;
        public IntPtr field0024;
        public IntPtr field0028;
        public IntPtr field002C;
        public IntPtr field0030;
        public IntPtr field0034;
        public IntPtr field0038;
        public IntPtr field003C;
        public IntPtr field0040;
        public int MaxOperations;
        public IntPtr field0048;
        private fixed byte Variables[0x100 * 0x28]; // This can't be done in a graceful way. Needs helper methods.
        public IntPtr field284C;
        public IntPtr field2850;
        public IntPtr field2854;
        public SymbolTable* SymbolTable;
        public Hashtable* GlobalTable;
        public IntPtr field2860;
        public IntPtr field2864;
        public LocalScope* LocalTable;
        public IntPtr field286C;
        public IntPtr JumpTable;
        public IntPtr StringTable;
        public IntPtr field2878;
        public IntPtr field287C;
        public IntPtr field2880;
        public Hashtable* FunctionTable;
        public Hashtable* field2888;
        public IntPtr field288C;
        public IntPtr field2890;
        public IntPtr field2894;
        public IntPtr field2898;
        public IntPtr field289C;
        public IntPtr DecrementHandleFunction;
        public IntPtr HandleTable;
        public IntPtr table28A8;
        public IntPtr table28AC;
        public IntPtr table28B0;

        /// <summary>
        /// Gets the function name of the function which contains the opcode.
        /// </summary>
        /// <param name="op">The op to look from.</param>
        /// <param name="name">The out parameter with the name, empty if nothing was found.</param>
        /// <returns>Whether the name was found or not.</returns>
        public bool TryGetOpCodeFunctionName(OpCode* op, out string name)
        {
            while (op->OpType != OpCodeType.Function)
            {
                op = &op[-1];
                if (op < this.SymbolTable->FirstOperation)
                {
                    name = String.Empty;
                    return false;
                }
            }
            name = this.SymbolTable->StringPool->Nodes[op->Argument]->Value;
            return true;
        }

        public Variable*[] GetAllLocals()
        {
            var nodes = (&this.LocalTable->Locals)->GetAllValues();

            var array = new Variable*[nodes.Length];
            for (var i = 0; i < nodes.Length; i++)
                array[i] = (Variable*)nodes[i];
            return array;
        }

        public Variable*[] GetAllGlobals()
        {
            var nodes = this.GlobalTable->GetAllValues();

            var array = new Variable*[nodes.Length];
            for (var i = 0; i < nodes.Length; i++)
                array[i] = (Variable*)nodes[i];
            return array;
        }
    }
}
