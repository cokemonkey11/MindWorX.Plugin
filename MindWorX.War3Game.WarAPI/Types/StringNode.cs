﻿using System;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x18)]
    public struct StringNode
    {
        public IntPtr field0000;

        public IntPtr field0004;

        public IntPtr field0008;

        public IntPtr field0010;

        public IntPtr field0014;

        public IntPtr ValuePtr;

        public string Value => Memory.ReadString(this.ValuePtr);
    }
}
