﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using MindWorX.Blizzard.Storm;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x34)]
    public unsafe struct Hashtable
    {
        //  typedef struct Hashtable {
        //      int (__fastcall** list_destroy)(Hashtable*, HT_Node*);  // 0x00
        //	    int node_offset;                                        // 0x04
        //      int field_8;                                            // 0x08
        //      HT_Node* nodelist;                                      // 0x0C
        //      int field_16;                                           // 0x10
        //      int field_20;                                           // 0x14
        //      int nbuckets;                                           // 0x18
        //      HT_Bucket* buckets;                                     // 0x1C
        //      int unkl;                                               // 0x20
        //      int bitmask;                                            //0x24 //determines number of elements
        //      HT_Bucket bucket_arr;                                   // 0x28 .. 0x30
        //  } Hashtable;

        public void* list_destroy_method;

        public int NodeOffset;

        public int field0008;

        public HT_Node* NodeList;

        public int field0010;

        public int field0014;

        public int BucketCount;

        public HT_Bucket* Buckets;

        public int field0020;

        public int Bitmask;

        public HT_Bucket BucketArray;

        public HT_Node* Lookup(string key)
        {
            if (String.IsNullOrEmpty(key))
                return null;

            if (this.Bitmask == -1)
                return null;

            var hash = SStr.HashHT(key);
            var i = this.Bitmask & hash;
            var n = this.Buckets[i].List;

            while ((int)n > 0)
            {
                if (hash == n->Hash && (key == n->Key))
                    return n;

                n = n->Next;
            }

            return null;
        }

        public void* LookupValue(string key)
        {
            var n = this.Lookup(key);
            if (n == null)
                return null;
            return n->Value;
        }

        public bool ContainsKey(string key)
        {
            if (String.IsNullOrEmpty(key))
                return false;

            if (this.Bitmask == -1)
                return false;

            var hash = SStr.HashHT(key);
            var i = this.Bitmask & hash;
            var n = this.Buckets[i].List;

            while ((int)n > 0)
            {
                if (hash == n->Hash && (key == n->Key))
                    return true;

                n = n->Next;
            }

            return false;
        }

        public HT_Node*[] GetAllValues()
        {
            var list = new List<IntPtr>();

            var size = this.BucketCount;

            for (var i = 0; i < size; i++)
            {
                var node = this.Buckets[i].List;
                while ((int)node > 0)
                {
                    list.Add((IntPtr)node);
                    node = node->Next;
                }
            }

            var array = new HT_Node*[list.Count];
            for (var i = 0; i < array.Length; i++)
                array[i] = (HT_Node*)list[i];
            return array;
        }
    }
}
