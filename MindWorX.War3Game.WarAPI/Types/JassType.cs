﻿namespace MindWorX.War3Game.WarAPI.Types
{
    public enum JassType : byte
    {
        Nothing,
        /// <summary>
        /// Type01 is used internally for saving the cur op.
        /// </summary>
        Type01,
        Null,
        Code,
        Integer,
        Real,
        String,
        Handle,
        Boolean,
        IntegerArray,
        RealArray,
        StringArray,
        HandleArray,
        BooleanArray,
    }
}
