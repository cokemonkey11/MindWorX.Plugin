﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x50)]
    public struct ThreadLocalStorage
    {
        public IntPtr field0000;
        public IntPtr field0004;
        public IntPtr field0008;
        public IntPtr field000C;

        public IntPtr field0010;
        public IntPtr Jass;
        public IntPtr field0018;
        public IntPtr field001C;

        public IntPtr field0020;
        public IntPtr field0024;
        public IntPtr field0028;
        public IntPtr field002C;

        public IntPtr field0030;
        public IntPtr field0034;
        public IntPtr NetProvider; /* NetProviderLOOP (Single Player), NetProviderBNET (Battle.net) or NetProviderLTCP (Local Area Network) */
        public IntPtr field003C;

        public IntPtr field0040;
        public IntPtr field0044;
        public IntPtr field0048;
        public IntPtr field004C;
    }
}
