﻿using System;
using System.Runtime.InteropServices;

namespace MindWorX.War3Game.WarAPI.Types
{
    [StructLayout(LayoutKind.Sequential, Size = 0x0C)]
    public unsafe struct HT_Bucket
    {
        //  typedef struct HT_Bucket {
        //      int offset;     // 0x00
        //      int unk;        // 0x04
        //      HT_Node* list;  // 0x08
        //  } HT_Bucket;

        public int Offset;

        public IntPtr field0004;

        public HT_Node* List;
    }
}
