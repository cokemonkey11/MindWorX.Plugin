﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged.Invokers;
using MindWorX.War3Game.WarAPI.Types;

namespace MindWorX.War3Game.WarAPI
{
    public unsafe class Game : IGame
    {
        public static Game Instance { get; } = new Game();


        public event EventHandler Ready;

        private Game() { }

        public bool IsReady { get; private set; }

        private DynamicMethod getThreadLocalStorage;

        private DynamicMethod jassStringHandleToString;

        private DynamicMethod stringToJassStringIndex;

        public void Initialize(IntPtr module, Dictionary<string, int> addresses)
        {
            this.getThreadLocalStorage = StdCall.Create(module + addresses["WC3_GetThreadLocalStorage"], typeof(IntPtr));
            this.jassStringHandleToString = FastCall.Create(module + addresses["WC3_JassStringHandleToString"], typeof(IntPtr), typeof(IntPtr));
            this.stringToJassStringIndex = FastCall.Create(module + addresses["WC3_StringToJassStringIndex"], typeof(int), typeof(IntPtr));

            this.IsReady = true;
            this.Ready?.Invoke(this, EventArgs.Empty);
        }

        public ThreadLocalStorage* GetThreadLocalStorage() => (ThreadLocalStorage*)(IntPtr)this.getThreadLocalStorage.Invoke(null, new object[] { });

        public string JassStringHandleToString(IntPtr jassHandle) => Marshal.PtrToStringAnsi((IntPtr)this.jassStringHandleToString.Invoke(null, new object[] { jassHandle }));

        public int StringToJassStringIndex(string str) => (int)this.stringToJassStringIndex.Invoke(null, new object[] { Marshal.StringToHGlobalAnsi(str) });

        public IntPtr JassStringIndexToJassStringHandle(int jassStringIndex)
        {
            return (IntPtr)((int)Marshal.ReadIntPtr(Marshal.ReadIntPtr(Marshal.ReadIntPtr(Marshal.ReadIntPtr(this.GetThreadLocalStorage()->Jass, 0x0C)), 0x2874), 0x0008) + 0x10 * jassStringIndex);
            // the above code may be a bit confusing, but we're essentially doing the following, without needing to
            // find the function every patch, and avoid the convoluted class hierarchy.
            // return Jass->VirtualMachine->StringManager->Table[jassStringIndex];
            // sub_6F6B1CA0 in 1.21b
            // sub_6F459640 in 1.26.0.6401
            // TODO: Future proof this.
        }
    }
}
