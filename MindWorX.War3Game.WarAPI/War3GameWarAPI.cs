﻿using System.ComponentModel.Composition;
using System.Diagnostics;
using MindWorX.GenericLibraryInjectionSystem;
using SharpCraft;
using MindWorX.GenericAddressManager;

namespace MindWorX.War3Game.WarAPI
{
    internal class War3GameWarAPI : IPlugin
    {
        [Import]
        private Profile profile;

        [Import]
        private IAddressManager addressManager;

        [Import]
        private ILibraryInjectionSystem libraryInjectionSystem;

        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo(
            typeof(War3GameWarAPI).FullName,
            "An api to interact with the core features of Warcraft III.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIGame)
                return;

            this.libraryInjectionSystem.AddLibraryLoadedEventHandler("Game.dll", (sender, args) =>
            {
                Game.Instance.Initialize(args.Handle, this.addressManager.Current);

                this.IsReady = true;
                this.Ready?.Invoke(this);
            });

        }
    }
}
