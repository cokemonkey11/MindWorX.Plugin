﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using MindWorX.Unmanaged.Windows;
using SharpCraft;

namespace MindWorX.War3Editor.TriggerEditorSyntaxHighlight
{
    internal class War3EditorTriggerEditorSyntaxHighlight : IPlugin
    {
        [Import]
        private Profile profile;

        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo(
            "TESH", 
            "A new Trigger Editor Syntax Highlighter for the Warcraft 3 World Editor", 
            "0.7.1.1", "SFilip", "Zoxc", "Van Damm", "Artificial");

        public bool IsReady { get; private set; }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIEditor)
                return;

            var root = Path.GetDirectoryName(typeof(War3EditorTriggerEditorSyntaxHighlight).Assembly.Location);
            if (root == null)
                throw new InvalidOperationException("Assembly is missing root location.");

            var tesh = Path.Combine(root.ToLower(), @"executable\tesh.dll");
            if (!File.Exists(tesh))
                throw new FileNotFoundException("Could not find TESH.", "tesh.dll");

            Kernel32.LoadLibrary(tesh);

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }
    }
}
