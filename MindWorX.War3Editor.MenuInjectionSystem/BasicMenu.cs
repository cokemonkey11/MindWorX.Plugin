﻿using System;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.War3Editor.MenuInjectionSystem
{
    public class BasicMenu
    {
        private readonly War3EditorMenuInjectionSystem war3EditorMenuInjectionSystem;

        internal BasicMenu(IntPtr handle, string text, War3EditorMenuInjectionSystem war3EditorMenuInjectionSystem)
        {
            this.Handle = handle;
            this.Text = text;
            this.war3EditorMenuInjectionSystem = war3EditorMenuInjectionSystem;
        }

        public IntPtr Handle { get; }

        public string Text { get; }

        public void AppendSeparator()
        {
            User32.AppendMenu(this.Handle, MenuFlags.Separator, (uint)this.war3EditorMenuInjectionSystem.NextMenuId(), null);
        }

        public BasicMenuItem AppendMenuItem(string text)
        {
            var basicMenuItem = new BasicMenuItem((IntPtr)this.war3EditorMenuInjectionSystem.NextMenuId(), text, this.war3EditorMenuInjectionSystem);
            this.war3EditorMenuInjectionSystem.RegisterCommandCallback((int)basicMenuItem.Handle, basicMenuItem);
            User32.AppendMenu(this.Handle, MenuFlags.String, (uint)basicMenuItem.Handle, text);
            return basicMenuItem;
        }
    }
}
