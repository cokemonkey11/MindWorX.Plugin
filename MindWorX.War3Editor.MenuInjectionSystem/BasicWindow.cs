﻿using System;
using MindWorX.Unmanaged.Windows;

namespace MindWorX.War3Editor.MenuInjectionSystem
{
    public class BasicWindow
    {
        private readonly War3EditorMenuInjectionSystem war3EditorMenuInjectionSystem;

        private IntPtr handle;

        private IntPtr menuHandle;

        public event EventHandler Ready;

        public event EventHandler MenuReady;

        internal BasicWindow(War3EditorMenuInjectionSystem war3EditorMenuInjectionSystem)
        {
            this.war3EditorMenuInjectionSystem = war3EditorMenuInjectionSystem;
        }

        public IntPtr Handle
        {
            get { return this.handle; }
            set
            {
                if (value == this.handle)
                    return;
                this.handle = value;
                this.OnReady();
            }
        }

        public IntPtr MenuHandle
        {
            get { return this.menuHandle; }
            set
            {
                if (value == this.menuHandle)
                    return;
                this.menuHandle = value;
                this.OnMenuReady();
            }
        }

        protected void OnReady() => this.Ready?.Invoke(this, EventArgs.Empty);

        protected void OnMenuReady() => this.MenuReady?.Invoke(this, EventArgs.Empty);

        public BasicMenu AppendMenu(string text)
        {
            var basicMenu = new BasicMenu(User32.CreateMenu(), text, this.war3EditorMenuInjectionSystem);
            User32.AppendMenu(this.MenuHandle, MenuFlags.Popup, (uint)basicMenu.Handle, text);
            return basicMenu;
        }
    }
}
