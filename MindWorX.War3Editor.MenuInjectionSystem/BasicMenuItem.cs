﻿using System;

namespace MindWorX.War3Editor.MenuInjectionSystem
{
    public class BasicMenuItem
    {
        private readonly War3EditorMenuInjectionSystem war3EditorMenuInjectionSystem;

        public event EventHandler Click;

        internal BasicMenuItem(IntPtr handle, string text, War3EditorMenuInjectionSystem war3EditorMenuInjectionSystem)
        {
            this.Handle = handle;
            this.Text = text;
            this.war3EditorMenuInjectionSystem = war3EditorMenuInjectionSystem;
        }

        public IntPtr Handle { get; }

        public string Text { get; }

        protected void OnClick() => this.Click?.Invoke(this, EventArgs.Empty);

        public void NotifyOnClick() => this.OnClick();
    }
}
