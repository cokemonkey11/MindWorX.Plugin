﻿namespace MindWorX.War3Editor.MenuInjectionSystem
{
    public interface IWar3EditorMenuInjectionSystem
    {
        BasicWindow WorldEditor { get; }

        BasicWindow TriggerEditor { get; }

        BasicWindow ObjectEditor { get; }
    }
}