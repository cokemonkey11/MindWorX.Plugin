﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using EasyHook;
using MindWorX.Unmanaged.Windows;
using Serilog;
using SharpCraft;
using System.ComponentModel.Composition;

namespace MindWorX.War3Editor.MenuInjectionSystem
{
    [Export(typeof(IWar3EditorMenuInjectionSystem))]
    internal class War3EditorMenuInjectionSystem : IPlugin, IWar3EditorMenuInjectionSystem
    {
        [Import]
        private Profile profile;

        private LocalHook createWindowExHook;

        private LocalHook setMenuHook;

        private int menuId = 0xBEEF;

        private readonly Dictionary<int, BasicMenuItem> menuItems = new Dictionary<int, BasicMenuItem>();

        private WndProcDelegate oldWndProc;

        private WndProcDelegate newWndProc;

        public event PluginReadyEventHandler Ready;

        public War3EditorMenuInjectionSystem()
        {
            this.WorldEditor = new BasicWindow(this);
            this.TriggerEditor = new BasicWindow(this);
            this.ObjectEditor = new BasicWindow(this);
        }

        public BasicWindow WorldEditor { get; }

        public BasicWindow TriggerEditor { get; }

        public BasicWindow ObjectEditor { get; }

        public PluginInfo Info { get; } = new PluginInfo(
            typeof(War3EditorMenuInjectionSystem).FullName,
            "A system for injecting custom menus in the World Editor.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIEditor)
                return;

            this.createWindowExHook = LocalHook.Create(Kernel32.GetProcAddress(User32.Handle, nameof(User32.CreateWindowExA)), new User32.CreateWindowExA_SafePrototype(this.CreateWindowExHook), null);
            this.createWindowExHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            this.setMenuHook = LocalHook.Create(Kernel32.GetProcAddress(User32.Handle, nameof(User32.SetMenu)), new User32.SetMenuPrototype(this.SetMenuHook), null);
            this.setMenuHook.ThreadACL.SetExclusiveACL(new[] { 0 });

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        private IntPtr CreateWindowExHook(
            WindowStylesEx dwExStyle, IntPtr lpClassName, IntPtr lpWindowName, WindowStyles dwStyle,
            int x, int y, int nWidth, int nHeight,
            IntPtr hWndParent, IntPtr hMenu, IntPtr hInstance, IntPtr lpParam)
        {
            var window = User32.CreateWindowExA_Safe(dwExStyle, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam);

            if (Marshal.PtrToStringAnsi(lpClassName) == "Warcraft III")
            {
                this.WorldEditor.Handle = window;
                this.oldWndProc = Marshal.GetDelegateForFunctionPointer<WndProcDelegate>((IntPtr)User32.GetWindowLong(this.WorldEditor.Handle, (int)GWL.WndProc));
                this.newWndProc = this.WndProcHook; // pin the object so it isn't garbage collected
                User32.SetWindowLong(this.WorldEditor.Handle, (int)GWL.WndProc, (int)Marshal.GetFunctionPointerForDelegate(this.newWndProc));
                Log.Logger?.Information("Found World Editor window(0x{handle:X8}).", (int)window);
            }
            else
            {
                var parent = User32.GetParent(window);
                var title = new StringBuilder(512);
                User32.GetWindowText(parent, title, 511);
                switch (title.ToString())
                {
                    case "Trigger Editor":
                        if (parent != this.TriggerEditor.Handle)
                        {
                            this.TriggerEditor.Handle = parent;
                            Log.Logger?.Information("Found Trigger Editor window(0x{handle:X8}).", (int)parent);
                        }
                        break;
                    case "Object Editor":
                        if (parent != this.ObjectEditor.Handle)
                        {
                            this.ObjectEditor.Handle = parent;
                            Log.Logger?.Information("Found Object Editor window(0x{handle:X8}).", (int)parent);
                        }
                        break;
                }
            }

            return window;
        }

        private IntPtr WndProcHook(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam)
        {
            switch ((WindowsMessage)msg)
            {
                case WindowsMessage.Command:
                    BasicMenuItem menuItem;
                    if (this.menuItems.TryGetValue((int)wParam, out menuItem))
                    {
                        try
                        {
                            menuItem.NotifyOnClick();
                            return IntPtr.Zero;
                        }
                        catch (Exception e)
                        {
                            Log.Logger.Error(e, "Exception in MenuItem({name})", menuItem.Text);
                        }
                    }
                    break;
            }

            return this.oldWndProc(hWnd, msg, wParam, lParam);
        }

        private bool SetMenuHook(IntPtr hWnd, IntPtr hMenu)
        {
            var result = User32.SetMenu(hWnd, hMenu);

            if (hWnd != this.WorldEditor.Handle)
                return result;

            Log.Logger?.Information("Found Menu for World Editor(0x{handle:X8}).", (int)hMenu);
            this.WorldEditor.MenuHandle = hMenu;

            return result;
        }

        internal int NextMenuId() => this.menuId++;

        internal void RegisterCommandCallback(int id, BasicMenuItem basicMenuItem) => this.menuItems[id] = basicMenuItem;
    }
}
