﻿using SharpCraft;

namespace MindWorX.War3Game.ExtendedSettings
{
    public interface IWar3GameExtendedSettings : IPlugin
    {
        bool IsNoInstanceLimitEnabled { get; set; }

        bool IsNoCheatEnabled { get; set; }

        bool IsNoPauseEnabled { get; set; }
    }
}
