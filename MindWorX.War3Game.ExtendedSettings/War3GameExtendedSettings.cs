﻿using System.ComponentModel.Composition;
using MindWorX.GenericAddressManager;
using MindWorX.GenericLibraryInjectionSystem;
using MindWorX.Unmanaged;
using Serilog;
using SharpCraft;

namespace MindWorX.War3Game.ExtendedSettings
{
    [Export(typeof(IWar3GameExtendedSettings))]
    internal class War3GameExtendedSettings : IWar3GameExtendedSettings
    {
        [Import]
        private Profile profile;

        [Import]
        private ILibraryInjectionSystem libraryInjectionSystem;

        [Import]
        private IAddressManager addressManager;

        private MemoryPatch noInstanceLimitPatch;

        private MemoryPatch noCheatPatch;

        private MemoryPatch noPausePatch;

        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo(
            typeof(War3GameExtendedSettings).FullName,
            "A plugin that adds some additional game settings, like nocheat, nopause, etc.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public bool IsNoInstanceLimitEnabled
        {
            get { return this.noInstanceLimitPatch.IsEnabled; }
            set { this.noInstanceLimitPatch.IsEnabled = value; }
        }

        public bool IsNoCheatEnabled
        {
            get { return this.noCheatPatch.IsEnabled; }
            set { this.noCheatPatch.IsEnabled = value; }
        }

        public bool IsNoPauseEnabled
        {
            get { return this.noPausePatch.IsEnabled; }
            set { this.noPausePatch.IsEnabled = value; }
        }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIGame)
                return;

            this.libraryInjectionSystem.AddLibraryLoadedEventHandler("Game.dll", this.OnGameLoad);

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        private void OnGameLoad(object sender, LibraryLoadedEventArgs e)
        {
            if (this.addressManager.Current["WC3_NoInstanceLimit"] != default(int))
            {
                Log.Logger?.Information("Injecting no instance limit patch . . .");
                this.noInstanceLimitPatch = new MemoryPatch(e.Handle + this.addressManager.Current["WC3_NoInstanceLimit"], true, 0xEB);
                this.IsNoInstanceLimitEnabled = true;
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_NoInstanceLimit");
            }

            if (this.addressManager.Current["WC3_NoCheat"] != default(int))
            {
                Log.Logger?.Information("Injecting no cheat patch . . .");
                this.noCheatPatch = new MemoryPatch(e.Handle + this.addressManager.Current["WC3_NoCheat"], true, 0x90, 0x90, 0x90, 0x90, 0x90);
                this.IsNoCheatEnabled = true;
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_NoCheat");
            }

            if (this.addressManager.Current["WC3_NoPause"] != default(int))
            {
                Log.Logger?.Information("Injecting no pause patch . . .");
                this.noPausePatch = new MemoryPatch(e.Handle + this.addressManager.Current["WC3_NoPause"], true, 0xEB);
                this.IsNoPauseEnabled = true;
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_NoPause");
            }
        }
    }
}
