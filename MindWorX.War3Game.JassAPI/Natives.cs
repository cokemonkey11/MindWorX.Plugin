﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using EasyHook;
using MindWorX.GenericAddressManager;
using MindWorX.GenericLibraryInjectionSystem;
using MindWorX.Unmanaged;
using MindWorX.Unmanaged.Invokers;
using MindWorX.War3Game.JassAPI.Primitives;
using MindWorX.War3Game.WarAPI.Types;
using Serilog;
using SharpCraft;

namespace MindWorX.War3Game.JassAPI
{
    public unsafe partial class Natives : IPlugin
    {
        [Import]
        private Profile profile;

        [Import]
        private ILibraryInjectionSystem libraryInjectionSystem;

        [Import]
        private IAddressManager addressManager;

        private IntPtr bindNativePtr;

        private LocalHook initNativesLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate int InitNativesPrototype();

        private InitNativesPrototype initNatives;

        #region VirtualMachine__CodeToFunction

        private LocalHook virtualMachine__GetOpCodeForHandleLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        delegate OpCode* VirtualMachine__CodeToFunctionPrototype(VirtualMachine* vm, JassCode a2);

        private VirtualMachine__CodeToFunctionPrototype virtualMachine__GetOpCodeForHandle;

        #endregion

        #region VirtualMachine__RunCode

        private LocalHook virtualMachine__RunCodeLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        delegate CodeResult VirtualMachine__RunCodePrototype(VirtualMachine* vm, OpCode* opCode, int* a3, uint opLimit, IntPtr a5);

        private VirtualMachine__RunCodePrototype virtualMachine__RunCode;

        #endregion

        #region VirtualMachine__RunFunction

        private LocalHook virtualMachine__RunFunctionLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        delegate CodeResult VirtualMachine__RunFunctionPrototype(VirtualMachine* @this, IntPtr functionNamePtr, IntPtr a3, IntPtr a4, IntPtr a5, uint opLimit);

        private VirtualMachine__RunFunctionPrototype virtualMachine__RunFunction;

        #endregion

        #region signed int __thiscall sub_6F296DE0(void *this)

        private LocalHook sub_6F296DE0LocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        delegate IntPtr sub_6F296DE0Prototype(IntPtr @this);

        private sub_6F296DE0Prototype sub_6F296DE0;
        #endregion

        #region int __thiscall sub_6F9174A0(int this, int a1)

        private LocalHook sub_6F9174A0LocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        delegate IntPtr sub_6F9174A0Prototype(IntPtr @this, IntPtr a1);

        private sub_6F9174A0Prototype sub_6F9174A0;

        #endregion

        #region void *__thiscall sub_6F91C510(int this, int a1, int a2)

        private LocalHook sub_6F91C510LocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        delegate IntPtr sub_6F91C510Prototype(IntPtr @this, IntPtr a1, IntPtr a2, IntPtr a3, IntPtr a4, IntPtr a5, IntPtr a6);

        private sub_6F91C510Prototype sub_6F91C510;

        #endregion

        private readonly Dictionary<string, NativeInfo> vanillaNatives;

        private readonly Dictionary<string, NativeInfo> customNatives;

        public event PluginReadyEventHandler Ready;

        public Natives()
        {
            this.vanillaNatives = new Dictionary<string, NativeInfo>();
            this.customNatives = new Dictionary<string, NativeInfo>();
            this.VanillaNatives = new ReadOnlyDictionary<string, NativeInfo>(this.vanillaNatives);
            this.CustomNatives = new ReadOnlyDictionary<string, NativeInfo>(this.customNatives);
        }

        public PluginInfo Info { get; } = new PluginInfo(
            "Natives",
            "An api to interact with the natives of Warcraft III.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public ReadOnlyDictionary<string, NativeInfo> VanillaNatives { get; }

        public ReadOnlyDictionary<string, NativeInfo> CustomNatives { get; }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIGame)
                return;

            this.libraryInjectionSystem.AddLibraryLoadedEventHandler("Game.dll", this.OnGameLoad);
        }

        private void OnGameLoad(object sender, LibraryLoadedEventArgs libraryLoadedEventArgs)
        {
            var handle = libraryLoadedEventArgs.Handle;

            if (this.addressManager.Current["WC3_BindNative"] != 0)
            {
                this.bindNativePtr = handle + this.addressManager.Current["WC3_BindNative"];
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_BindNative");
                return;
            }

            if (this.addressManager.Current["WC3_InitNatives"] != 0)
            {
                var initNativesPtr = handle + this.addressManager.Current["WC3_InitNatives"];
                Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.InitNativesHook), (uint)initNativesPtr);
                this.initNatives = Marshal.GetDelegateForFunctionPointer<InitNativesPrototype>(initNativesPtr);
                this.initNativesLocalHook = LocalHook.Create(initNativesPtr, new InitNativesPrototype(this.InitNativesHook), null);
                this.initNativesLocalHook.ThreadACL.SetExclusiveACL(new int[0]);

                Log.Logger?.Information("Scanning vanilla natives . . .");
                var offset = 0x05;
                while (Memory.Read<byte>(initNativesPtr + offset) == 0x68)
                {
                    var native = new NativeInfo(initNativesPtr + offset);
                    this.vanillaNatives.Add(native.Name, native);
                    offset += 0x14;
                }
                this.InitializeVanillaNatives();
                Log.Logger?.Information("Found {nativeCount} natives", this.vanillaNatives.Count);

            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_InitNatives");
                return;
            }

            //if (this.addressManager.Current["WC3_VirtualMachine__CodeToFunction"] != 0)
            //{
            //    var virtualMachine__CodeToFunctionPtr = handle + this.addressManager.Current["WC3_VirtualMachine__CodeToFunction"];
            //    Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.VirtualMachine__GetOpCodeForHandleHook), (uint)virtualMachine__CodeToFunctionPtr);
            //    this.virtualMachine__GetOpCodeForHandle = Marshal.GetDelegateForFunctionPointer<VirtualMachine__CodeToFunctionPrototype>(virtualMachine__CodeToFunctionPtr);
            //    this.virtualMachine__GetOpCodeForHandleLocalHook = LocalHook.Create(virtualMachine__CodeToFunctionPtr, new VirtualMachine__CodeToFunctionPrototype(this.VirtualMachine__GetOpCodeForHandleHook), null);
            //    this.virtualMachine__GetOpCodeForHandleLocalHook.ThreadACL.SetExclusiveACL(new int[0]);
            //}
            //else
            //{
            //    Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_VirtualMachine__CodeToFunction");
            //}

            //if (this.addressManager.Current["WC3_VirtualMachine__RunCode"] != 0)
            //{
            //    var virtualMachine__RunCodePtr = handle + this.addressManager.Current["WC3_VirtualMachine__RunCode"];
            //    Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.VirtualMachine__RunCodeHook), (uint)virtualMachine__RunCodePtr);
            //    this.virtualMachine__RunCode = Marshal.GetDelegateForFunctionPointer<VirtualMachine__RunCodePrototype>(virtualMachine__RunCodePtr);
            //    this.virtualMachine__RunCodeLocalHook = LocalHook.Create(virtualMachine__RunCodePtr, new VirtualMachine__RunCodePrototype(this.VirtualMachine__RunCodeHook), null);
            //    this.virtualMachine__RunCodeLocalHook.ThreadACL.SetExclusiveACL(new int[0]);
            //}
            //else
            //{
            //    Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_VirtualMachine__RunCode");
            //}

            //if (this.addressManager.Current["WC3_VirtualMachine__RunFunction"] != 0)
            //{
            //    var virtualMachine__RunFunctionPtr = handle + this.addressManager.Current["WC3_VirtualMachine__RunFunction"];
            //    Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.VirtualMachine__RunFunctionHook), (uint)virtualMachine__RunFunctionPtr);
            //    this.virtualMachine__RunFunction = Marshal.GetDelegateForFunctionPointer<VirtualMachine__RunFunctionPrototype>(virtualMachine__RunFunctionPtr);
            //    this.virtualMachine__RunFunctionLocalHook = LocalHook.Create(virtualMachine__RunFunctionPtr, new VirtualMachine__RunFunctionPrototype(this.VirtualMachine__RunFunctionHook), null);
            //    this.virtualMachine__RunFunctionLocalHook.ThreadACL.SetExclusiveACL(new int[0]);
            //}
            //else
            //{
            //    Log.Logger?.Error("Unable to locate address for {addressName}", "WC3_VirtualMachine__RunFunction");
            //}

            //var ptr = handle + 0x296DE0;
            //this.sub_6F296DE0 = Marshal.GetDelegateForFunctionPointer<sub_6F296DE0Prototype>(ptr);
            //this.sub_6F296DE0LocalHook = LocalHook.Create(ptr, new sub_6F296DE0Prototype(this.sub_6F296DE0Hook), null);
            //this.sub_6F296DE0LocalHook.ThreadACL.SetExclusiveACL(new int[0]);

            //ptr = handle + 0x9174A0;
            //this.sub_6F9174A0 = Marshal.GetDelegateForFunctionPointer<sub_6F9174A0Prototype>(ptr);
            //this.sub_6F9174A0LocalHook = LocalHook.Create(ptr, new sub_6F9174A0Prototype(this.sub_6F9174A0Hook), null);
            //this.sub_6F9174A0LocalHook.ThreadACL.SetExclusiveACL(new int[0]);

            //ptr = handle + 0x91C510;
            //this.sub_6F91C510 = Marshal.GetDelegateForFunctionPointer<sub_6F91C510Prototype>(ptr);
            //this.sub_6F91C510LocalHook = LocalHook.Create(ptr, new sub_6F91C510Prototype(this.sub_6F91C510Hook), null);
            //this.sub_6F91C510LocalHook.ThreadACL.SetExclusiveACL(new int[0]);


            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        private int InitNativesHook()
        {
            Log.Logger?.Information("InitNativesHook");
            var result = this.initNatives();

            foreach (var native in this.customNatives.Values)
            {
                FastCall.Invoke<IntPtr>(this.bindNativePtr, native.FunctionPtr, native.NamePtr, native.PrototypePtr);
            }

            return result;
        }

        private OpCode* VirtualMachine__GetOpCodeForHandleHook(VirtualMachine* vm, JassCode code)
        {
            var result = this.virtualMachine__GetOpCodeForHandle(vm, code);
            //string name;
            //vm->TryGetOpCodeFunctionName(result, out name);
            //Log.Logger?.Information("Detected {function} handle: {handle:X8}", name, (int)code.Handle);

            //if ((int)a2.Handle < 0)
            //{
            //    var instantReturnFunction = (OpCode*)SMem.Alloc(1 * Marshal.SizeOf(typeof(OpCode)));
            //    instantReturnFunction[0].OpType = OpCodeType.Return;
            //    instantReturnFunction[0].Argument = (int)a2.Handle;
            //    Log.Logger?.Information("VirtualMachine__GetOpCodeForHandle: C# function[{n}] detected.", (int)a2.Handle);
            //    return instantReturnFunction;
            //}

            //var from = HookRuntimeInfo.ReturnAddress - (int)Kernel32.GetModuleHandle("game.dll") + 0x6F000000;
            //Log.Logger?.Information("+" + new string(' ', 2 * this.indent) + "VM__GetOpCodeForHandle({from:X8}): {vm}, {code}", (int)from, ((int)vm).ToString("X8"), code.Handle);
            //this.indent++;
            //var result = this.virtualMachine__GetOpCodeForHandle(vm, code);
            //this.indent--;
            //Log.Logger?.Information("-" + new string(' ', 2 * this.indent) + "VM__GetOpCodeForHandle: {result}", ((int)result).ToString("X8"));

            //string name;
            //vm->TryGetOpCodeFunctionName(result, out name);
            //Log.Logger?.Information("VirtualMachine__GetOpCodeForHandle: {function}", new
            //{
            //    VirtualMachine = ((int)vm).ToString("X8"),
            //    Address = ((int)result).ToString("X8"),
            //    Handle = a2.Handle,
            //    Name = name
            //});

            return result;
        }

        private CodeResult VirtualMachine__RunCodeHook(VirtualMachine* vm, OpCode* opCode, int* a3, uint opLimit, IntPtr a5)
        {
            //if (opCode->OpType == OpCodeType.Return && opCode->Argument != 0)
            //{
            //    Log.Logger?.Information("VirtualMachine__RunCode: C# function[{n}] detected.", opCode->Argument);
            //    return CodeResult.Success;
            //}

            //var from = HookRuntimeInfo.ReturnAddress - (int)Kernel32.GetModuleHandle("game.dll") + 0x6F000000;
            //Log.Logger?.Information("+" + new string(' ', 2 * this.indent) + "VM__RunCode({from:X8}): {vm}, {opCode}, {a3}, {opLimit}, {a5}", (int)from, ((int)vm).ToString("X8"), ((int)opCode).ToString("X8"), ((int)a3).ToString("X8"), opLimit, a5);
            //this.indent++;
            ////var result = this.virtualMachine__RunCode(vm, opCode, a3, opLimit, a5);
            //this.indent--;
            //Log.Logger?.Information("-" + new string(' ', 2 * this.indent) + "VM__RunCode: {result}", result);

            //Log.Logger?.Information("VirtualMachine__RunCodeHook({a1:X8}, ...) = {result}", (int)vm, result);

            //int? aa3 = null;
            //if (a3 != null)
            //    aa3 = *a3;
            //string name;
            //if (vm->TryGetOpCodeFunctionName(opCode, out name))
            //    Log.Logger?.Information("VirtualMachine__RunCode({a1:X8}, {functionName}({opCode:X8}), {a3}, {opLimit}, {a5}) = {result}", (int)vm, name, (int)opCode, aa3, opLimit, a5, result);
            //else
            //    Log.Logger?.Information("VirtualMachine__RunCode({a1:X8}, {opCode:X8}, {a3}, {opLimit}, {a5}) = {result}", (int)vm, (int)opCode, aa3, opLimit, a5, result);

            return this.virtualMachine__RunCode(vm, opCode, a3, opLimit, a5);
        }

        private CodeResult VirtualMachine__RunFunctionHook(VirtualMachine* vm, IntPtr functionNamePtr, IntPtr a3, IntPtr a4, IntPtr a5, uint opLimit)
        {
            //var result = this.virtualMachine__RunFunction(vm, functionNamePtr, a3, a4, a5, opLimit);


            //Log.Logger?.Information("VirtualMachine__RunFunction({a1:X8}, {functionName}, {a3}, {a4}, {a5}, {opLimit}) = {result}", (int)vm, functionName, a3, a4, a5, opLimit, result);

            //if (functionName == "config")
            //{
            //    var functions = vm->FunctionTable->GetAllValues();
            //    Log.Logger?.Information("Functions");
            //    foreach (var function in functions)
            //    {
            //        var opCode = (OpCode*)function->Value;
            //        var n = 0;
            //        for (var i = -1; opCode[i].OpType != OpCodeType.EndFunction; i++)
            //        {
            //            n++;
            //            //output += "0x" + (i + 1).ToString("X4") + ByteCodeToString(&op[i], "\t") + Environment.NewLine;
            //        }
            //        Log.Logger?.Information(" * {name}[{n}]", vm->SymbolTable->StringPool->Nodes[opCode[-1].Argument]->Value, n);
            //    }
            //}

            //if (functionName == "main")
            //{
            //    var trigger = this.CreateTrigger();
            //    this.TriggerRegisterTimerEvent(trigger, 1.00f, true);
            //    //this.TriggerAddCondition(trigger, new JassBooleanExpression(new IntPtr(-1)));
            //    this.TriggerAddAction(trigger, new JassCode(new IntPtr(-2)));
            //}

            //var name = Memory.ReadString(functionNamePtr);

            //if (name == "config")
            //{
            //    Log.Logger?.Information("Detected {function}", name);

            //    var action = vm->FunctionTable->Lookup("SharpCraft_Action");
            //    if (action != null)
            //        Log.Logger?.Information("Detected {function}", "SharpCraft_Action");

            //    var condition = vm->FunctionTable->Lookup("SharpCraft_Condition");
            //    if (condition != null)
            //        Log.Logger?.Information("Detected {function}", "SharpCraft_Condition");
            //}

            return this.virtualMachine__RunFunction(vm, functionNamePtr, a3, a4, a5, opLimit);
        }

        private IntPtr sub_6F296DE0Hook(IntPtr @this)
        {
            //Log.Logger?.Information("+" + new string(' ', 2 * this.indent) + "sub_6F296DE0");
            //this.indent++;
            //var result = this.sub_6F296DE0(@this);
            //this.indent--;
            //Log.Logger?.Information("-" + new string(' ', 2 * this.indent) + "sub_6F296DE0");

            return this.sub_6F296DE0(@this);
        }

        private IntPtr sub_6F9174A0Hook(IntPtr @this, IntPtr a1) => this.sub_6F9174A0(@this, a1);

        private IntPtr sub_6F91C510Hook(IntPtr @this, IntPtr a1, IntPtr a2, IntPtr a3, IntPtr a4, IntPtr a5, IntPtr a6)
        {
            //Log.Logger?.Information("+" + new string(' ', 2 * this.indent) + "sub_6F91C510");
            //this.indent++;
            //var result = this.sub_6F91C510(@this, a1, a2, a3, a4, a5, a6);
            //this.indent--;
            //Log.Logger?.Information("-" + new string(' ', 2 * this.indent) + "sub_6F91C510");

            return this.sub_6F91C510(@this, a1, a2, a3, a4, a5, a6);
        }

        public void Add(NativeInfo native)
        {
            //Trace.WriteLine("Native added: " + native.Name + native.Prototype);
            this.customNatives.Add(native.Name, native);
        }

        public NativeInfo Add(Delegate function, string name, string prototype)
        {
            var declaration = new NativeInfo(function, name, prototype);
            this.Add(declaration);
            return declaration;
        }

        public NativeInfo Add(Delegate function, string name)
        {
            var prototype = new StringBuilder("(");

            foreach (var parameter in function.Method.GetParameters())
            {
                var attribute = (JassTypeAttribute)parameter.ParameterType.GetCustomAttributes(typeof(JassTypeAttribute), true).Single();
                prototype.Append(attribute.TypeString);
            }

            prototype.Append(")");

            if (function.Method.ReturnType == typeof(void))
            {
                prototype.Append("V");
            }
            else
            {
                var attribute = (JassTypeAttribute)function.Method.ReturnType.GetCustomAttributes(typeof(JassTypeAttribute), true).Single();
                prototype.Append(attribute.TypeString);
            }

            return this.Add(function, name, prototype.ToString());
        }

        public NativeInfo Add(Delegate function)
        {
            return this.Add(function, function.Method.Name);
        }

        public NativeInfo Get(string name)
        {
            NativeInfo native;
            if (this.vanillaNatives.TryGetValue(name, out native))
                return native;
            return null;
        }

        public bool Remove(NativeInfo native)
        {
            //Trace.WriteLine("Native removed: " + native.Name + native.Prototype);
            return this.customNatives.Remove(native.Name);

        }
    }
}
