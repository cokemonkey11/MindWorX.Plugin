using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hleaderboard;")]
    [Serializable]
    public struct JassLeaderboard
    {
        public readonly IntPtr Handle;
        
        public JassLeaderboard(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
