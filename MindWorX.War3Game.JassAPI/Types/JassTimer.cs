using System;
using System.ComponentModel.Composition;
using MindWorX.War3Game.JassAPI.Primitives;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Htimer;")]
    [Serializable]
    public struct JassTimer
    {
        [Import]
        private static Natives Natives;

        public static JassTimer GetExpired() => Natives.GetExpiredTimer();

        public static JassTimer Create() => Natives.CreateTimer();


        public readonly IntPtr Handle;

        public JassTimer(IntPtr handle)
        {
            this.Handle = handle;
        }

        public void Destroy() => Natives.DestroyTimer(this);

        public void Start(float timeout, bool periodic, JassCode function)
            => Natives.TimerStart(this, timeout, periodic, function);

        public void Start(float timeout, bool periodic)
            => this.Start(timeout, periodic, JassCode.None);

        public void Start(TimeSpan timeout, bool periodic, JassCode function)
            => this.Start((float)timeout.TotalSeconds, periodic, function);

        public void Start(TimeSpan timeout, bool periodic)
            => this.Start((float)timeout.TotalSeconds, periodic);

        public void Pause() => Natives.PauseTimer(this);

        public void Resume() => Natives.ResumeTimer(this);


        public float GetElapsedSeconds() => Natives.TimerGetElapsed(this);

        public float ElapsedSeconds => this.GetElapsedSeconds();

        public TimeSpan Elapsed => TimeSpan.FromSeconds(this.ElapsedSeconds);


        public float GetRemainingSeconds() => Natives.TimerGetRemaining(this);

        public float RemainingSeconds => this.GetRemainingSeconds();

        public TimeSpan Remaining => TimeSpan.FromSeconds(this.RemainingSeconds);


        public float GetTimeoutSeconds() => Natives.TimerGetTimeout(this);

        public float TimeoutSeconds => this.GetTimeoutSeconds();

        public TimeSpan Timeout => TimeSpan.FromSeconds(this.TimeoutSeconds);
    }
}
