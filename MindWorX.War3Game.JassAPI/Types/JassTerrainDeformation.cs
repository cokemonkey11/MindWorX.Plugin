using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hterraindeformation;")]
    [Serializable]
    public struct JassTerrainDeformation
    {
        public readonly IntPtr Handle;
        
        public JassTerrainDeformation(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
