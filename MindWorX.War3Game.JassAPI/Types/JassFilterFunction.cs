using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hfilterfunc;")]
    [Serializable]
    public struct JassFilterFunction
    {
        public readonly IntPtr Handle;
        
        public JassFilterFunction(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
