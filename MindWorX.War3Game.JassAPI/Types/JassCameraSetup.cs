using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hcamerasetup;")]
    [Serializable]
    public struct JassCameraSetup
    {
        public readonly IntPtr Handle;
        
        public JassCameraSetup(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
