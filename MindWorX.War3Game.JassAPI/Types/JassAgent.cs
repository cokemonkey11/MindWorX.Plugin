using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hagent;")]
    [Serializable]
    public struct JassAgent
    {
        public readonly IntPtr Handle;
        
        public JassAgent(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
