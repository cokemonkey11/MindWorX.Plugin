using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hdefeatcondition;")]
    [Serializable]
    public struct JassDefeatCondition
    {
        public readonly IntPtr Handle;
        
        public JassDefeatCondition(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
