using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hboolexpr;")]
    [Serializable]
    public struct JassBooleanExpression
    {
        public static JassBooleanExpression None = new JassBooleanExpression(IntPtr.Zero);


        public readonly IntPtr Handle;

        public JassBooleanExpression(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
