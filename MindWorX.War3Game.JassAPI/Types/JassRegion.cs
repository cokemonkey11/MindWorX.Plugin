using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hregion;")]
    [Serializable]
    public struct JassRegion
    {
        public readonly IntPtr Handle;
        
        public JassRegion(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
