using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hforce;")]
    [Serializable]
    public struct JassForce
    {
        public readonly IntPtr Handle;
        
        public JassForce(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
