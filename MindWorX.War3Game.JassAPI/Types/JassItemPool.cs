using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hitempool;")]
    [Serializable]
    public struct JassItemPool
    {
        public readonly IntPtr Handle;
        
        public JassItemPool(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
