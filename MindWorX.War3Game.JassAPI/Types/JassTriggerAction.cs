using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Htriggeraction;")]
    [Serializable]
    public struct JassTriggerAction
    {
        public readonly IntPtr Handle;
        
        public JassTriggerAction(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
