﻿using System;
using System.ComponentModel.Composition;
using MindWorX.War3Game.JassAPI.Primitives;
using MindWorX.War3Game.JassAPI.Types.Enums;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hitem;")]
    [Serializable]
    public struct JassItem
    {
        [Import]
        private static Natives Natives;

        public static JassItem Create(JassObjectId itemid, float x, float y) => Natives.CreateItem(itemid, x, y);


        public readonly IntPtr Handle;

        public JassItem(IntPtr handle)
        {
            this.Handle = handle;
        }

        //public CItemPtr ToCItem()
        //{
        //    return CItemPtr.FromHandle(this.Handle);
        //}

        public void Remove() => Natives.RemoveItem(this);

        public JassObjectId GetTypeId() => Natives.GetItemTypeId(this);

        public JassObjectId TypeId => this.GetTypeId();


        public JassPlayer GetPlayer() => Natives.GetItemPlayer(this);

        public void SetPlayer(JassPlayer player, bool changeColor) => Natives.SetItemPlayer(this, player, changeColor);

        public void SetPlayer(JassPlayer player) => Natives.SetItemPlayer(this, player, false);

        public JassPlayer Player
        {
            get { return this.GetPlayer(); }
            set { this.SetPlayer(value); }
        }

        public bool IsOwned() => Natives.IsItemOwned(this);

        public bool Owned => this.IsOwned();


        public float GetX() => Natives.GetItemX(this);

        public float GetY() => Natives.GetItemY(this);

        public void SetX(float x) => this.SetPosition(x, this.GetY());

        public void SetY(float y) => this.SetPosition(this.GetX(), y);

        public void SetPosition(float x, float y) => Natives.SetItemPosition(this, x, y);

        public float X
        {
            get { return this.GetX(); }
            set { this.SetX(value); }
        }

        public float Y
        {
            get { return this.GetY(); }
            set { this.SetY(value); }
        }


        public bool IsInvulnerable() => Natives.IsItemInvulnerable(this);

        public void SetInvulnerable(bool flag) => Natives.SetItemInvulnerable(this, flag);

        public bool Invulnerable
        {
            get { return this.IsInvulnerable(); }
            set { this.SetInvulnerable(value); }
        }


        public bool IsPawnable() => Natives.IsItemPawnable(this);

        public void SetPawnable(bool flag) => Natives.SetItemPawnable(this, flag);

        public bool Pawnable
        {
            get { return this.IsPawnable(); }
            set { this.SetPawnable(value); }
        }


        public bool IsVisible() => Natives.IsItemVisible(this);

        public void SetVisible(bool flag) => Natives.SetItemVisible(this, flag);

        public bool Visible
        {
            get { return this.IsVisible(); }
            set { this.SetVisible(value); }
        }


        public int GetCharges() => Natives.GetItemCharges(this);

        public void SetCharges(int charges) => Natives.SetItemCharges(this, charges);

        public int Charges
        {
            get { return this.GetCharges(); }
            set { this.SetCharges(value); }
        }


        public int GetUserData() => Natives.GetItemUserData(this);

        public void SetUserData(int data) => Natives.SetItemUserData(this, data);

        public int UserData
        {
            get { return this.GetUserData(); }
            set { this.SetUserData(value); }
        }


        public string GetName() => Natives.GetItemName(this);

        public string Name => this.GetName();


        public int GetLevel() => Natives.GetItemLevel(this);

        public int Level => this.GetLevel();


        public JassItemType GetItemType() => Natives.GetItemType(this);

        public JassItemType ItemType => this.GetItemType();


        public bool IsPowerup() => Natives.IsItemPowerup(this);

        public bool Powerup => this.IsSellable();


        public bool IsSellable() => Natives.IsItemSellable(this);

        public bool Sellable => this.IsSellable();


        public void SetDropOnDeath(bool flag) => Natives.SetItemDropOnDeath(this, flag);

        public void SetDroppable(bool flag) => Natives.SetItemDroppable(this, flag);

        public void SetDropID(JassObjectId unitId) => Natives.SetItemDropID(this, unitId);

        /*
            native          IsItemIdPowerup takes integer itemId returns boolean
            native          IsItemIdSellable takes integer itemId returns boolean
            native          IsItemIdPawnable takes integer itemId returns boolean
            native          EnumItemsInRect     takes rect r, boolexpr filter, code actionFunc returns nothing
         */
    }
}
