using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hquestitem;")]
    [Serializable]
    public struct JassQuestItem
    {
        public readonly IntPtr Handle;
        
        public JassQuestItem(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
