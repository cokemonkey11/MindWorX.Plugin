using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hhashtable;")]
    [Serializable]
    public struct JassHashTable
    {
        public readonly IntPtr Handle;
        
        public JassHashTable(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
