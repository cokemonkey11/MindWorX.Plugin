using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hubersplat;")]
    [Serializable]
    public struct JassUberSplat
    {
        public readonly IntPtr Handle;
        
        public JassUberSplat(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
