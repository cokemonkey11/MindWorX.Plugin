using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hmultiboard;")]
    [Serializable]
    public struct JassMultiboard
    {
        public readonly IntPtr Handle;
        
        public JassMultiboard(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
