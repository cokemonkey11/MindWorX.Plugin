using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hmultiboarditem;")]
    [Serializable]
    public struct JassMultiboardItem
    {
        public readonly IntPtr Handle;
        
        public JassMultiboardItem(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
