﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hunitstate;")]
    [Serializable]
    public enum JassUnitState
    {
        Life = 0,
        MaxLife = 1,
        Mana = 2,
        MaxMana = 3,
    }
}
