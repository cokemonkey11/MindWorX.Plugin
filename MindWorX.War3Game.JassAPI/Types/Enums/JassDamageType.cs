using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hdamagetype;")]
    [Serializable]
    public enum JassDamageType
    {
        Unknown = 0,
        //Unknown = 1,
        //Unknown = 2,
        //Unknown = 3,
        Normal = 4,
        Enhanced = 5,
        //Unknown = 6,
        //Unknown = 7,
        Fire = 8,
        Cold = 9,
        Lightning = 10,
        Poison = 11,
        Disease = 12,
        Divine = 13,
        Magic = 14,
        Sonic = 15,
        Acid = 16,
        Force = 17,
        Death = 18,
        Mind = 19,
        Plant = 20,
        Defensive = 21,
        Demolition = 22,
        SlowPoison = 23,
        SpiritLink = 24,
        ShadowStrike = 25,
        Universal = 26,
    }
}
