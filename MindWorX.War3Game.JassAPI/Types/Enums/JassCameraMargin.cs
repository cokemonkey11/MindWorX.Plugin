﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("I")]
    [Serializable]
    public enum JassCameraMargin
    {
        Left = 0,
        Right = 1,
        Top = 2,
        Bottom = 3,
    }
}
