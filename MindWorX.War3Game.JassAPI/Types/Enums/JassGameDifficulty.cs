using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hgamedifficulty;")]
    [Serializable]
    public enum JassGameDifficulty
    {
        Easy = 0,
        Normal = 1,
        Hard = 2,
        Insane = 3,
    }
}
