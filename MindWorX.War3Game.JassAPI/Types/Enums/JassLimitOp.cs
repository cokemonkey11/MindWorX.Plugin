using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hlimitop;")]
    [Serializable]
    public enum JassLimitOp
    {
        LessThan = 0,
        LessThanOrEqual = 1,
        Equal = 2,
        GreaterThanOrEqual = 3,
        GreaterThan = 4,
        NotEqual = 5,
    }
}
