using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hpathingtype;")]
    [Serializable]
    public enum JassPathingType
    {
        Any = 0,
        Walkability = 1,
        Flyability = 2,
        Buildability = 3,
        PeonHarvestPathing = 4,
        BlightPathing = 5,
        Floatability = 6,
        AmphibiousPathing = 7,
    }
}
