using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Htexmapflags;")]
    [Serializable]
    public enum JassTextureMapFlags
    {
        None,
        WrapU,
        WrapV,
        WrapUV,
    }
}
