using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplayerunitevent;")]
    [Serializable]
    public enum JassPlayerUnitEvent
    {
        UnitAttacked = 18,
        UnitRescued = 19,

        UnitDeath = 20,
        UnitDecay = 21,

        UnitDetected = 22,
        UnitHidden = 23,

        UnitSelected = 24,
        UnitDeselected = 25,

        UnitConstructStart = 26,
        UnitConstructCancel = 27,
        UnitConstructFinish = 28,

        UnitUpgradeStart = 29,
        UnitUpgradeCancel = 30,
        UnitUpgradeFinish = 31,

        UnitTrainStart = 32,
        UnitTrainCancel = 33,
        UnitTrainFinish = 34,

        UnitResearchStart = 35,
        UnitResearchCancel = 36,
        UnitResearchFinish = 37,
        UnitIssuedOrder = 38,
        UnitIssuedPointOrder = 39,
        UnitIssuedTargetOrder = 40,

        HeroLevel = 41,
        HeroSkill = 42,

        HeroRevivable = 43,

        HeroReviveStart = 44,
        HeroReviveCancel = 45,
        HeroReviveFinish = 46,
        UnitSummon = 47,
        UnitDropItem = 48,
        UnitPickupItem = 49,
        UnitUseItem = 50,

        UnitLoaded = 51,

        UnitSell = 269,
        UnitChangeOwner = 270,
        UnitSellItem = 271,
        UnitSpellChannel = 272,
        UnitSpellCast = 273,
        UnitSpellEffect = 274,
        UnitSpellFinish = 275,
        UnitSpellEndCast = 276,
        UnitPawnItem = 277,
    }
}
