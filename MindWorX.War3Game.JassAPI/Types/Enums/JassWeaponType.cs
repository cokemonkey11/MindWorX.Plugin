using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hweapontype;")]
    [Serializable]
    public enum JassWeaponType
    {
        WhoKnows = 0,        MetalLightChop = 1,
        MetalMediumChop = 2,
        MetalHeavyChop = 3,
        MetalLightSlice = 4,
        MetalMediumSlice = 5,
        MetalHeavySlice = 6,
        MetalMediumBash = 7,
        MetalHeavyBash = 8,
        MetalMediumStab = 9,
        MetalHeavyStab = 10,
        WoodLightSlice = 11,
        WoodMediumSlice = 12,
        WoodHeavySlice = 13,
        WoodLightBash = 14,
        WoodMediumBash = 15,
        WoodHeavyBash = 16,
        WoodLightStab = 17,
        WoodMediumStab = 18,
        ClawLightSlice = 19,
        ClawMediumSlice = 20,
        ClawHeavySlice = 21,
        AxeMediumChop = 22,
        RockHeavyBash = 23,
    }
}
