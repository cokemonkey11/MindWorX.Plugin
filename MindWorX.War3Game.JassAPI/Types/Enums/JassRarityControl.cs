using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hraritycontrol;")]
    [Serializable]
    public enum JassRarityControl
    {
        Frequent = 0,
        Rare = 1,
    }
}
