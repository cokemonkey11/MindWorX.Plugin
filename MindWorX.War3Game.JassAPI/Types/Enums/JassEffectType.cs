using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Heffecttype;")]
    [Serializable]
    public enum JassEffectType
    {
        Effect = 0,
        Target = 1,
        Caster = 2,
        Special = 3,
        AreaEffect = 4,
        Missile = 5,
        Lightning = 6,
    }
}
