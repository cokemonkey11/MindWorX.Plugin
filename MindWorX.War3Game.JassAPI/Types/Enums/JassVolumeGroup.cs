using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hvolumegroup;")]
    [Serializable]
    public enum JassVolumeGroup
    {
        UnitMovement = 0,
        UnitSounds = 1,
        Combat = 2,
        Spells = 3,
        UserInterface = 4,
        Music = 5,
        AmbientSounds = 6,
        Fire = 7
    }
}
