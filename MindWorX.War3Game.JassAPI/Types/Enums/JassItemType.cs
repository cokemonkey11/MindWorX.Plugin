﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hitemtype;")]
    [Serializable]
    public enum JassItemType
    {
        Permanent = 0,
        Charged = 1,
        PowerUp = 2,
        Artifact = 3,
        Purchasable = 4,
        Campaign = 5,
        Miscellaneous = 6,
        Unknown = 7,
        Any = 8,
    }
}
