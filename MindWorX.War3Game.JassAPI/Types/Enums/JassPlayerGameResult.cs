using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplayergameresult;")]
    [Serializable]
    public enum JassPlayerGameResult
    {
        Victory = 0,
        Defeat = 1,
        Tie = 2,
        Neutral = 3
    }
}
