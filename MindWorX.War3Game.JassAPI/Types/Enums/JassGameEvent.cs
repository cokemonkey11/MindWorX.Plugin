using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hgameevent;")]
    [Serializable]
    public enum JassGameEvent
    {
        Victory = 0,
        EndLevel = 1,
        VariableLimit = 2,
        StateLimit = 3,
        TimerExpired = 4,
        EnterRegion = 5,
        LeaveRegion = 6,
        TrackableHit = 7,
        TrackableTrack = 8,
        ShowSkill = 9,
        BuildSubmenu = 10,

        Loaded = 256,
        TournamentFinishSoon = 257,
        TournamentFinishNow = 258,
        Save = 259,
    }
}
