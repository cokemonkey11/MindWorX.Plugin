﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hracepreference;")]
    [Serializable]
    [Flags]
    public enum JassRacePreference
    {
        None = 0,
        Human = (1 << 0),
        Orc = (1 << 1),
        NightElf = (1 << 2),
        Undead = (1 << 3),
        Demon = (1 << 4),
        Random = (1 << 5),
        UserSelectable = (1 << 6),
        All = None | Human | Orc | NightElf | Undead | Demon | Random | UserSelectable
    }
}
