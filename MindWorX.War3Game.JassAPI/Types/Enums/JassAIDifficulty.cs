using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Haidifficulty;")]
    [Serializable]
    public enum JassAIDifficulty
    {
        Newbie = 0,
        Normal = 1,
        Insane = 2
    }
}
