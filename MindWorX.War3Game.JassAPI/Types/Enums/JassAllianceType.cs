using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Halliancetype;")]
    [Serializable]
    public enum JassAllianceType
    {
        Passive = 0,
        HelpRequest = 1,
        HelpResponse = 2,
        SharedExperience = 3,
        SharedSpells = 4,
        SharedVision = 5,
        SharedControl = 6,
        SharedAdvancedControl = 7,
        Rescuable = 8,
        SharedVisionForced = 9
    }
}
