using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplayerslotstate;")]
    [Serializable]
    public enum JassPlayerSlotState
    {
        Empty = 0,
        Playing = 1,
        Left = 2,
    }
}
