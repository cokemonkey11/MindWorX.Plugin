using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hdialogevent;")]
    [Serializable]
    public enum JassDialogEvent
    {
        ButtonClick = 90,
        Click = 91
    }
}
