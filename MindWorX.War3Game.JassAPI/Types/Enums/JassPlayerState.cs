﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplayerstate;")]
    [Serializable]
    public enum JassPlayerState
    {
        GameResult = 0,
        ResourceGold = 1,
        ResourceLumber = 2,
        ResourceHeroTokens = 3,
        ResourceFoodCap = 4,
        ResourceFoodUsed = 5,
        FoodCapCeiling = 6,
        GivesBounty = 7,
        AlliedVictory = 8,
        Placed = 9,
        ObserverOnDeath = 10,
        Observer = 11,
        Unfollowable = 12,
        GoldUpkeepRate = 13,
        LumberUpkeepRate = 14,
        GoldGathered = 15,
        LumberGathered = 16,

        NoCreepSleep = 25,
    }
}
