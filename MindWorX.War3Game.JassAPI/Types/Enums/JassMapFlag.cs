using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hmapflag;")]
    [Serializable]
    [Flags]
    public enum JassMapFlag
    {
        None = 0,

        FogHideTerrain = 1 << 0,
        FogMapExplored = 1 << 1,
        FogAlwaysVisible = 1 << 2,

        UseHandicaps = 1 << 3,
        Observers = 1 << 4,
        ObserversOnDeath = 1 << 5,

        FixedColors = 1 << 7,

        LockResourceTrading = 1 << 8,
        ResourceTradingAlliesOnly = 1 << 9,

        LockAllianceChanges = 1 << 10,
        AllianceChangesHidden = 1 << 11,

        Cheats = 1 << 12,
        CheatsHidden = 1 << 13,

        LockSpeed = 1 << 14,
        LockRandomSeed = 1 << 15,
        SharedAdvancedControl = 1 << 16,
        RandomHero = 1 << 17,
        RandomRaces = 1 << 18,
        Reloaded = 1 << 19,
    }
}
