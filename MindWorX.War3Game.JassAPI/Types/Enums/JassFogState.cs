﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hfogstate;")]
    [Serializable]
    [Flags]
    public enum JassFogState
    {
        Masked = 1,
        Fogged = 2,
        Visible = 4,
    }
}
