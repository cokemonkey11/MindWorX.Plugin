using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplayerevent;")]
    [Serializable]
    public enum JassPlayerEvent
    {
        StateLimit = 11,
        AllianceChanged = 12,

        Defeat = 13,
        Victory = 14,
        Leave = 15,
        Chat = 16,
        EndCinematic = 17,

        ArrowLeftDown = 261,
        ArrowLeftUp = 262,
        ArrowRightDown = 263,
        ArrowRightUp = 264,
        ArrowDownDown = 265,
        ArrowDownUp = 266,
        ArrowUpDown = 267,
        ArrowUpUp = 268,
    }
}
