using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hunittype;")]
    [Serializable]
    public enum JassUnitType
    {
        Hero = 0,
        Dead = 1,
        Structure = 2,

        Flying = 3,
        Ground = 4,

        AttacksFlying = 5,
        AttacksGround = 6,

        MeleeAttacker = 7,
        RangedAttacker = 8,

        Giant = 9,
        Summoned = 10,
        Stunned = 11,
        Plagued = 12,
        Snared = 13,

        Undead = 14,
        Mechanical = 15,
        Peon = 16,
        Sapper = 17,
        Townhall = 18,
        Ancient = 19,

        Tauren = 20,
        Poisoned = 21,
        Polymorphed = 22,
        Sleeping = 23,
        Resistant = 24,
        Ethereal = 25,
        MagicImmune = 26,
    }
}
