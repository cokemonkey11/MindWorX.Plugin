using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hstartlocprio;")]
    [Serializable]
    public enum JassStartLocationPriority
    {
        Low = 0,
        High = 1,
        Not = 2
    }
}
