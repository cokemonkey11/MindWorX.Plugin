using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hmapdensity;")]
    [Serializable]
    public enum JassMapDensity
    {
        None = 0,
        Light = 1,
        Medium = 2,
        Heavy = 3
    }
}
