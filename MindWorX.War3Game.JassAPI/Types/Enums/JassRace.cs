using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hrace;")]
    [Serializable]
    public enum JassRace
    {
        None = 0,
        Human = 1,
        Orc = 2,
        Undead = 3,
        NightElf = 4,
        Demon = 5,
        //Unknown = 6,
        Other = 7,
    }
}
