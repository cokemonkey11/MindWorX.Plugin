using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hblendmode;")]
    [Serializable]
    public enum JassBlendMode
    {
        None = 0,
        KeyAlpha = 1,
        Blend = 2,
        Additive = 3,
        Modulate = 4,
        Modulate2X = 5
    }
}
