using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Higamestate;")]
    [Serializable]
    public enum JassIGameState
    {
        DivineIntervention = 0,
        Disconnected = 1,
    }
}
