using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hversion;")]
    [Serializable]
    public enum JassVersion
    {
        ReignOfChaos = 0,
        FrozenThrone = 1,
    }
}
