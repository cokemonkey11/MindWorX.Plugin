﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hmapcontrol;")]
    [Serializable]
    public enum JassMapControl
    {
        User = 0,
        Computer = 1,
        Rescuable = 2,
        Neutral = 3,
        Creep = 4,
        None = 5,
    }
}
