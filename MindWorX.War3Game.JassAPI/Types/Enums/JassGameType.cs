using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hgametype;")]
    [Serializable]
    [Flags]
    public enum JassGameType
    {
        None = 0,
        Melee = 1 << 0,
        FreeForAll = 1 << 1,
        UseMapSettings = 1 << 2,
        Blizzard = 1 << 3,
        OneOnOne = 1 << 4,
        TwoTeamPlay = 1 << 5,
        ThreeTeamPlay = 1 << 6,
        FourTeamPlay = 1 << 7,
    }
}
