﻿using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplayercolor;")]
    [Serializable]
    public enum JassPlayerColor
    {
        Red = 0,
        Blue = 1,
        Cyan = 2,
        Purple = 3,
        Yellow = 4,
        Orange = 5,
        Green = 6,
        Pink = 7,
        LightGray = 8,
        LightBlue = 9,
        Aqua = 10,
        Brown = 11,
    }
}
