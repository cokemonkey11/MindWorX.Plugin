using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplayerscore;")]
    [Serializable]
    public enum JassPlayerScore
    {
        UnitsTrained = 0,
        UnitsKilled = 1,
        StructuresBuilt = 2,
        StructuresRazed = 3,
        TechPercent = 4,
        FoodMaxProduced = 5,
        FoodMaxUsed = 6,
        HeroesKilled = 7,
        ItemsGained = 8,
        MercenariesHired = 9,
        GoldMinedTotal = 10,
        GoldMinedUpkeep = 11,
        GoldLostUpkeep = 12,
        GoldLostTax = 13,
        GoldGiven = 14,
        GoldReceived = 15,
        LumberTotal = 16,
        LumberLostUpkeep = 17,
        LumberLostTax = 18,
        LumberGiven = 19,
        LumberReceived = 20,
        UnitTotal = 21,
        HeroTotal = 22,
        ResourceTotal = 23,
        Total = 24,
    }
}
