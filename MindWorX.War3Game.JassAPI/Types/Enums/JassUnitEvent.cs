using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hunitevent;")]
    [Serializable]
    public enum JassUnitEvent
    {
        Damaged = 52,
        Death = 53,
        Decay = 54,
        Detected = 55,
        Hidden = 56,
        Selected = 57,
        Deselected = 58,

        StateLimit = 59,

        AcquiredTarget = 60,
        TargetInRange = 61,
        Attacked = 62,
        Rescued = 63,

        ConstructCancel = 64,
        ConstructFinish = 65,

        UpgradeStart = 66,
        UpgradeCancel = 67,
        UpgradeFinish = 68,

        TrainStart = 69,
        TrainCancel = 70,
        TrainFinish = 71,

        ResearchStart = 72,
        ResearchCancel = 73,
        ResearchFinish = 74,

        IssuedOrder = 75,
        IssuedPointOrder = 76,
        IssuedTargetOrder = 77,

        HeroLevel = 78,
        HeroSkill = 79,

        HeroRevivable = 80,
        HeroReviveStart = 81,
        HeroReviveCancel = 82,
        HeroReviveFinish = 83,

        Summon = 84,

        DropItem = 85,
        PickupItem = 86,
        UseItem = 87,

        Loaded = 88,

        Sell = 286,
        ChangeOwner = 287,
        SellItem = 288,
        SpellChannel = 289,
        SpellCast = 290,
        SpellEffect = 291,
        SpellFinish = 292,
        SpellEndCast = 293,
        PawnItem = 294,
    }
}
