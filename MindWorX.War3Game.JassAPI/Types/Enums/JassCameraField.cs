using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hcamerafield;")]
    [Serializable]
    public enum JassCameraField
    {
        TargetDistance = 0,
        FarZ = 1,
        AngleOfAttack = 2,
        FieldOfView = 3,
        Roll = 4,
        Rotation = 5,
        ZOffset = 6,
    }
}
