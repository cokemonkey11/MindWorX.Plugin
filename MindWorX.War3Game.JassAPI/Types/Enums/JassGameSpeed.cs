using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hgamespeed;")]
    [Serializable]
    public enum JassGameSpeed
    {
        Slowest = 0,
        Slow = 1,
        Normal = 2,
        Fast = 3,
        Fastest = 4,
    }
}
