using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hsoundtype;")]
    [Serializable]
    public enum JassSoundType
    {
        Effect = 0,
        EffectLooped = 1,
    }
}
