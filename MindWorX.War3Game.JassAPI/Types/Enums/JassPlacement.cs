using System;

namespace MindWorX.War3Game.JassAPI.Types.Enums
{
    [JassType("Hplacement;")]
    [Serializable]
    public enum JassPlacement
    {
        /// <summary>
        /// random among all slots
        /// </summary>
        Random = 0,

        /// <summary>
        /// player 0 in start loc 0...
        /// </summary>
        Fixed = 1,

        /// <summary>
        /// whatever was specified by the script
        /// </summary>
        UseMapSettings = 2,

        /// <summary>
        /// random with allies next to each other
        /// </summary>
        TeamsTogether = 3
    }
}
