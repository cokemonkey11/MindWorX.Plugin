using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hbutton;")]
    [Serializable]
    public struct JassButton
    {
        public readonly IntPtr Handle;
        
        public JassButton(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
