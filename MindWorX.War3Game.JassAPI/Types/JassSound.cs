using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hsound;")]
    [Serializable]
    public struct JassSound
    {
        public readonly IntPtr Handle;
        
        public JassSound(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
