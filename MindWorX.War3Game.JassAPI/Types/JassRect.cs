using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hrect;")]
    [Serializable]
    public struct JassRect
    {
        public readonly IntPtr Handle;
        
        public JassRect(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
