using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hattacktype;")]
    [Serializable]
    public struct JassAttackType
    {
        public readonly IntPtr Handle;
        
        public JassAttackType(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
