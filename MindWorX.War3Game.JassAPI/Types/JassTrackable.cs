using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Htrackable;")]
    [Serializable]
    public struct JassTrackable
    {
        public readonly IntPtr Handle;
        
        public JassTrackable(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
