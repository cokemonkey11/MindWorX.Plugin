using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Himage;")]
    [Serializable]
    public struct JassImage
    {
        public readonly IntPtr Handle;
        
        public JassImage(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
