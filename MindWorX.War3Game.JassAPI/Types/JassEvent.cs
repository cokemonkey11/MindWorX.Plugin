using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hevent;")]
    [Serializable]
    public struct JassEvent
    {
        public readonly IntPtr Handle;
        
        public JassEvent(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
