using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hwidget;")]
    [Serializable]
    public struct JassWidget
    {
        public readonly IntPtr Handle;
        
        public JassWidget(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
