using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Htexttag;")]
    [Serializable]
    public struct JassTextTag
    {
        public readonly IntPtr Handle;
        
        public JassTextTag(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
