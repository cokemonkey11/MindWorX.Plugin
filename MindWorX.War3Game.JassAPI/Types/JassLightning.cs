using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hlightning;")]
    [Serializable]
    public struct JassLightning
    {
        public readonly IntPtr Handle;
        
        public JassLightning(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
