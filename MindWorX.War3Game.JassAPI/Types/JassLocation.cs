using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hlocation;")]
    [Serializable]
    public struct JassLocation
    {
        public readonly IntPtr Handle;
        
        public JassLocation(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
