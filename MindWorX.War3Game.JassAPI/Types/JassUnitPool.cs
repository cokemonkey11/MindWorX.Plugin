using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hunitpool;")]
    [Serializable]
    public struct JassUnitPool
    {
        public readonly IntPtr Handle;
        
        public JassUnitPool(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
