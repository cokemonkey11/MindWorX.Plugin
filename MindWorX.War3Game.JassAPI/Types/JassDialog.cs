using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hdialog;")]
    [Serializable]
    public struct JassDialog
    {
        public readonly IntPtr Handle;
        
        public JassDialog(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
