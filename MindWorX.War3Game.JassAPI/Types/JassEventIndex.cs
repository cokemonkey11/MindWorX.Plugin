using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Heventid;")]
    [Serializable]
    public struct JassEventIndex
    {
        public readonly IntPtr Handle;
        
        public JassEventIndex(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
