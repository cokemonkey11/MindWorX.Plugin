﻿using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hgamecache;")]
    [Serializable]
    public struct JassGameCache : IEquatable<JassGameCache>
    {
        public static bool operator ==(JassGameCache left, JassGameCache right) => left.Equals(right);

        public static bool operator !=(JassGameCache left, JassGameCache right) => !left.Equals(right);


        public readonly IntPtr Handle;

        public JassGameCache(IntPtr handle)
        {
            this.Handle = handle;
        }

        public bool Equals(JassGameCache other) => this.Handle == other.Handle;

        public override bool Equals(object other) => other is JassGameCache && this.Equals((JassGameCache)other);

        public override int GetHashCode() => this.Handle.ToInt32();
    }
}
