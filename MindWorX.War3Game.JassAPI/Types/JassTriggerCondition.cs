using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Htriggercondition;")]
    [Serializable]
    public struct JassTriggerCondition
    {
        public readonly IntPtr Handle;
        
        public JassTriggerCondition(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
