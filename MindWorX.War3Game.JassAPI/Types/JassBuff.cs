using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hbuff;")]
    [Serializable]
    public struct JassBuff
    {
        public readonly IntPtr Handle;
        
        public JassBuff(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
