using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hability;")]
    [Serializable]
    public struct JassAbility
    {
        public readonly IntPtr Handle;
        
        public JassAbility(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
