using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Heffect;")]
    [Serializable]
    public struct JassEffect
    {
        public readonly IntPtr Handle;
        
        public JassEffect(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
