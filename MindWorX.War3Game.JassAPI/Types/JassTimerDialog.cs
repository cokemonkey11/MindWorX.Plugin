using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Htimerdialog;")]
    [Serializable]
    public struct JassTimerDialog
    {
        public readonly IntPtr Handle;
        
        public JassTimerDialog(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
