using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hgamestate;")]
    [Serializable]
    public struct JassGameState
    {
        public readonly IntPtr Handle;
        
        public JassGameState(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
