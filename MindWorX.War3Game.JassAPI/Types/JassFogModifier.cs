using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hfogmodifier;")]
    [Serializable]
    public struct JassFogModifier
    {
        public readonly IntPtr Handle;
        
        public JassFogModifier(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
