using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hgroup;")]
    [Serializable]
    public struct JassGroup
    {
        public readonly IntPtr Handle;
        
        public JassGroup(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
