using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hdestructable;")]
    [Serializable]
    public struct JassDestructable
    {
        public readonly IntPtr Handle;
        
        public JassDestructable(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
