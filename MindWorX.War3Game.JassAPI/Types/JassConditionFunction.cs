using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hconditionfunc;")]
    [Serializable]
    public struct JassConditionFunction
    {
        public readonly IntPtr Handle;
        
        public JassConditionFunction(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
