using System;

namespace MindWorX.War3Game.JassAPI.Types
{
    [JassType("Hquest;")]
    [Serializable]
    public struct JassQuest
    {
        public readonly IntPtr Handle;
        
        public JassQuest(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
