using MindWorX.War3Game.JassAPI.Primitives;
using MindWorX.War3Game.JassAPI.Types;
using MindWorX.War3Game.JassAPI.Types.Enums;

namespace MindWorX.War3Game.JassAPI
{
    public partial class Natives
    {
        //native ConvertRace takes integer i returns race
        public delegate JassRace ConvertRacePrototype(JassInteger i);
        private ConvertRacePrototype _ConvertRace;
        public JassRace ConvertRace(JassInteger i)
        {
            return this._ConvertRace(i);
        }
        
        //native ConvertAllianceType takes integer i returns alliancetype
        public delegate JassAllianceType ConvertAllianceTypePrototype(JassInteger i);
        private ConvertAllianceTypePrototype _ConvertAllianceType;
        public JassAllianceType ConvertAllianceType(JassInteger i)
        {
            return this._ConvertAllianceType(i);
        }
        
        //native ConvertRacePref takes integer i returns racepreference
        public delegate JassRacePreference ConvertRacePrefPrototype(JassInteger i);
        private ConvertRacePrefPrototype _ConvertRacePref;
        public JassRacePreference ConvertRacePref(JassInteger i)
        {
            return this._ConvertRacePref(i);
        }
        
        //native ConvertIGameState takes integer i returns igamestate
        public delegate JassIGameState ConvertIGameStatePrototype(JassInteger i);
        private ConvertIGameStatePrototype _ConvertIGameState;
        public JassIGameState ConvertIGameState(JassInteger i)
        {
            return this._ConvertIGameState(i);
        }
        
        //native ConvertFGameState takes integer i returns fgamestate
        public delegate JassFGameState ConvertFGameStatePrototype(JassInteger i);
        private ConvertFGameStatePrototype _ConvertFGameState;
        public JassFGameState ConvertFGameState(JassInteger i)
        {
            return this._ConvertFGameState(i);
        }
        
        //native ConvertPlayerState takes integer i returns playerstate
        public delegate JassPlayerState ConvertPlayerStatePrototype(JassInteger i);
        private ConvertPlayerStatePrototype _ConvertPlayerState;
        public JassPlayerState ConvertPlayerState(JassInteger i)
        {
            return this._ConvertPlayerState(i);
        }
        
        //native ConvertPlayerScore takes integer i returns playerscore
        public delegate JassPlayerScore ConvertPlayerScorePrototype(JassInteger i);
        private ConvertPlayerScorePrototype _ConvertPlayerScore;
        public JassPlayerScore ConvertPlayerScore(JassInteger i)
        {
            return this._ConvertPlayerScore(i);
        }
        
        //native ConvertPlayerGameResult takes integer i returns playergameresult
        public delegate JassPlayerGameResult ConvertPlayerGameResultPrototype(JassInteger i);
        private ConvertPlayerGameResultPrototype _ConvertPlayerGameResult;
        public JassPlayerGameResult ConvertPlayerGameResult(JassInteger i)
        {
            return this._ConvertPlayerGameResult(i);
        }
        
        //native ConvertUnitState takes integer i returns unitstate
        public delegate JassUnitState ConvertUnitStatePrototype(JassInteger i);
        private ConvertUnitStatePrototype _ConvertUnitState;
        public JassUnitState ConvertUnitState(JassInteger i)
        {
            return this._ConvertUnitState(i);
        }
        
        //native ConvertAIDifficulty takes integer i returns aidifficulty
        public delegate JassAIDifficulty ConvertAIDifficultyPrototype(JassInteger i);
        private ConvertAIDifficultyPrototype _ConvertAIDifficulty;
        public JassAIDifficulty ConvertAIDifficulty(JassInteger i)
        {
            return this._ConvertAIDifficulty(i);
        }
        
        //native ConvertGameEvent takes integer i returns gameevent
        public delegate JassGameEvent ConvertGameEventPrototype(JassInteger i);
        private ConvertGameEventPrototype _ConvertGameEvent;
        public JassGameEvent ConvertGameEvent(JassInteger i)
        {
            return this._ConvertGameEvent(i);
        }
        
        //native ConvertPlayerEvent takes integer i returns playerevent
        public delegate JassPlayerEvent ConvertPlayerEventPrototype(JassInteger i);
        private ConvertPlayerEventPrototype _ConvertPlayerEvent;
        public JassPlayerEvent ConvertPlayerEvent(JassInteger i)
        {
            return this._ConvertPlayerEvent(i);
        }
        
        //native ConvertPlayerUnitEvent takes integer i returns playerunitevent
        public delegate JassPlayerUnitEvent ConvertPlayerUnitEventPrototype(JassInteger i);
        private ConvertPlayerUnitEventPrototype _ConvertPlayerUnitEvent;
        public JassPlayerUnitEvent ConvertPlayerUnitEvent(JassInteger i)
        {
            return this._ConvertPlayerUnitEvent(i);
        }
        
        //native ConvertWidgetEvent takes integer i returns widgetevent
        public delegate JassWidgetEvent ConvertWidgetEventPrototype(JassInteger i);
        private ConvertWidgetEventPrototype _ConvertWidgetEvent;
        public JassWidgetEvent ConvertWidgetEvent(JassInteger i)
        {
            return this._ConvertWidgetEvent(i);
        }
        
        //native ConvertDialogEvent takes integer i returns dialogevent
        public delegate JassDialogEvent ConvertDialogEventPrototype(JassInteger i);
        private ConvertDialogEventPrototype _ConvertDialogEvent;
        public JassDialogEvent ConvertDialogEvent(JassInteger i)
        {
            return this._ConvertDialogEvent(i);
        }
        
        //native ConvertUnitEvent takes integer i returns unitevent
        public delegate JassUnitEvent ConvertUnitEventPrototype(JassInteger i);
        private ConvertUnitEventPrototype _ConvertUnitEvent;
        public JassUnitEvent ConvertUnitEvent(JassInteger i)
        {
            return this._ConvertUnitEvent(i);
        }
        
        //native ConvertLimitOp takes integer i returns limitop
        public delegate JassLimitOp ConvertLimitOpPrototype(JassInteger i);
        private ConvertLimitOpPrototype _ConvertLimitOp;
        public JassLimitOp ConvertLimitOp(JassInteger i)
        {
            return this._ConvertLimitOp(i);
        }
        
        //native ConvertUnitType takes integer i returns unittype
        public delegate JassUnitType ConvertUnitTypePrototype(JassInteger i);
        private ConvertUnitTypePrototype _ConvertUnitType;
        public JassUnitType ConvertUnitType(JassInteger i)
        {
            return this._ConvertUnitType(i);
        }
        
        //native ConvertGameSpeed takes integer i returns gamespeed
        public delegate JassGameSpeed ConvertGameSpeedPrototype(JassInteger i);
        private ConvertGameSpeedPrototype _ConvertGameSpeed;
        public JassGameSpeed ConvertGameSpeed(JassInteger i)
        {
            return this._ConvertGameSpeed(i);
        }
        
        //native ConvertPlacement takes integer i returns placement
        public delegate JassPlacement ConvertPlacementPrototype(JassInteger i);
        private ConvertPlacementPrototype _ConvertPlacement;
        public JassPlacement ConvertPlacement(JassInteger i)
        {
            return this._ConvertPlacement(i);
        }
        
        //native ConvertStartLocPrio takes integer i returns startlocprio
        public delegate JassStartLocationPriority ConvertStartLocPrioPrototype(JassInteger i);
        private ConvertStartLocPrioPrototype _ConvertStartLocPrio;
        public JassStartLocationPriority ConvertStartLocPrio(JassInteger i)
        {
            return this._ConvertStartLocPrio(i);
        }
        
        //native ConvertGameDifficulty takes integer i returns gamedifficulty
        public delegate JassGameDifficulty ConvertGameDifficultyPrototype(JassInteger i);
        private ConvertGameDifficultyPrototype _ConvertGameDifficulty;
        public JassGameDifficulty ConvertGameDifficulty(JassInteger i)
        {
            return this._ConvertGameDifficulty(i);
        }
        
        //native ConvertGameType takes integer i returns gametype
        public delegate JassGameType ConvertGameTypePrototype(JassInteger i);
        private ConvertGameTypePrototype _ConvertGameType;
        public JassGameType ConvertGameType(JassInteger i)
        {
            return this._ConvertGameType(i);
        }
        
        //native ConvertMapFlag takes integer i returns mapflag
        public delegate JassMapFlag ConvertMapFlagPrototype(JassInteger i);
        private ConvertMapFlagPrototype _ConvertMapFlag;
        public JassMapFlag ConvertMapFlag(JassInteger i)
        {
            return this._ConvertMapFlag(i);
        }
        
        //native ConvertMapVisibility takes integer i returns mapvisibility
        public delegate JassMapVisibility ConvertMapVisibilityPrototype(JassInteger i);
        private ConvertMapVisibilityPrototype _ConvertMapVisibility;
        public JassMapVisibility ConvertMapVisibility(JassInteger i)
        {
            return this._ConvertMapVisibility(i);
        }
        
        //native ConvertMapSetting takes integer i returns mapsetting
        public delegate JassMapSetting ConvertMapSettingPrototype(JassInteger i);
        private ConvertMapSettingPrototype _ConvertMapSetting;
        public JassMapSetting ConvertMapSetting(JassInteger i)
        {
            return this._ConvertMapSetting(i);
        }
        
        //native ConvertMapDensity takes integer i returns mapdensity
        public delegate JassMapDensity ConvertMapDensityPrototype(JassInteger i);
        private ConvertMapDensityPrototype _ConvertMapDensity;
        public JassMapDensity ConvertMapDensity(JassInteger i)
        {
            return this._ConvertMapDensity(i);
        }
        
        //native ConvertMapControl takes integer i returns mapcontrol
        public delegate JassMapControl ConvertMapControlPrototype(JassInteger i);
        private ConvertMapControlPrototype _ConvertMapControl;
        public JassMapControl ConvertMapControl(JassInteger i)
        {
            return this._ConvertMapControl(i);
        }
        
        //native ConvertPlayerColor takes integer i returns playercolor
        public delegate JassPlayerColor ConvertPlayerColorPrototype(JassInteger i);
        private ConvertPlayerColorPrototype _ConvertPlayerColor;
        public JassPlayerColor ConvertPlayerColor(JassInteger i)
        {
            return this._ConvertPlayerColor(i);
        }
        
        //native ConvertPlayerSlotState takes integer i returns playerslotstate
        public delegate JassPlayerSlotState ConvertPlayerSlotStatePrototype(JassInteger i);
        private ConvertPlayerSlotStatePrototype _ConvertPlayerSlotState;
        public JassPlayerSlotState ConvertPlayerSlotState(JassInteger i)
        {
            return this._ConvertPlayerSlotState(i);
        }
        
        //native ConvertVolumeGroup takes integer i returns volumegroup
        public delegate JassVolumeGroup ConvertVolumeGroupPrototype(JassInteger i);
        private ConvertVolumeGroupPrototype _ConvertVolumeGroup;
        public JassVolumeGroup ConvertVolumeGroup(JassInteger i)
        {
            return this._ConvertVolumeGroup(i);
        }
        
        //native ConvertCameraField takes integer i returns camerafield
        public delegate JassCameraField ConvertCameraFieldPrototype(JassInteger i);
        private ConvertCameraFieldPrototype _ConvertCameraField;
        public JassCameraField ConvertCameraField(JassInteger i)
        {
            return this._ConvertCameraField(i);
        }
        
        //native ConvertBlendMode takes integer i returns blendmode
        public delegate JassBlendMode ConvertBlendModePrototype(JassInteger i);
        private ConvertBlendModePrototype _ConvertBlendMode;
        public JassBlendMode ConvertBlendMode(JassInteger i)
        {
            return this._ConvertBlendMode(i);
        }
        
        //native ConvertRarityControl takes integer i returns raritycontrol
        public delegate JassRarityControl ConvertRarityControlPrototype(JassInteger i);
        private ConvertRarityControlPrototype _ConvertRarityControl;
        public JassRarityControl ConvertRarityControl(JassInteger i)
        {
            return this._ConvertRarityControl(i);
        }
        
        //native ConvertTexMapFlags takes integer i returns texmapflags
        public delegate JassTextureMapFlags ConvertTexMapFlagsPrototype(JassInteger i);
        private ConvertTexMapFlagsPrototype _ConvertTexMapFlags;
        public JassTextureMapFlags ConvertTexMapFlags(JassInteger i)
        {
            return this._ConvertTexMapFlags(i);
        }
        
        //native ConvertFogState takes integer i returns fogstate
        public delegate JassFogState ConvertFogStatePrototype(JassInteger i);
        private ConvertFogStatePrototype _ConvertFogState;
        public JassFogState ConvertFogState(JassInteger i)
        {
            return this._ConvertFogState(i);
        }
        
        //native ConvertEffectType takes integer i returns effecttype
        public delegate JassEffectType ConvertEffectTypePrototype(JassInteger i);
        private ConvertEffectTypePrototype _ConvertEffectType;
        public JassEffectType ConvertEffectType(JassInteger i)
        {
            return this._ConvertEffectType(i);
        }
        
        //native ConvertVersion takes integer i returns version
        public delegate JassVersion ConvertVersionPrototype(JassInteger i);
        private ConvertVersionPrototype _ConvertVersion;
        public JassVersion ConvertVersion(JassInteger i)
        {
            return this._ConvertVersion(i);
        }
        
        //native ConvertItemType takes integer i returns itemtype
        public delegate JassItemType ConvertItemTypePrototype(JassInteger i);
        private ConvertItemTypePrototype _ConvertItemType;
        public JassItemType ConvertItemType(JassInteger i)
        {
            return this._ConvertItemType(i);
        }
        
        //native ConvertAttackType takes integer i returns attacktype
        public delegate JassAttackType ConvertAttackTypePrototype(JassInteger i);
        private ConvertAttackTypePrototype _ConvertAttackType;
        public JassAttackType ConvertAttackType(JassInteger i)
        {
            return this._ConvertAttackType(i);
        }
        
        //native ConvertDamageType takes integer i returns damagetype
        public delegate JassDamageType ConvertDamageTypePrototype(JassInteger i);
        private ConvertDamageTypePrototype _ConvertDamageType;
        public JassDamageType ConvertDamageType(JassInteger i)
        {
            return this._ConvertDamageType(i);
        }
        
        //native ConvertWeaponType takes integer i returns weapontype
        public delegate JassWeaponType ConvertWeaponTypePrototype(JassInteger i);
        private ConvertWeaponTypePrototype _ConvertWeaponType;
        public JassWeaponType ConvertWeaponType(JassInteger i)
        {
            return this._ConvertWeaponType(i);
        }
        
        //native ConvertSoundType takes integer i returns soundtype
        public delegate JassSoundType ConvertSoundTypePrototype(JassInteger i);
        private ConvertSoundTypePrototype _ConvertSoundType;
        public JassSoundType ConvertSoundType(JassInteger i)
        {
            return this._ConvertSoundType(i);
        }
        
        //native ConvertPathingType takes integer i returns pathingtype
        public delegate JassPathingType ConvertPathingTypePrototype(JassInteger i);
        private ConvertPathingTypePrototype _ConvertPathingType;
        public JassPathingType ConvertPathingType(JassInteger i)
        {
            return this._ConvertPathingType(i);
        }
        
        //native OrderId takes string orderIdString returns integer
        public delegate JassOrder OrderIdPrototype(JassStringArg orderIdString);
        private OrderIdPrototype _OrderId;
        public JassOrder OrderId(string orderIdString)
        {
            return this._OrderId(orderIdString);
        }
        
        //native OrderId2String takes integer orderId returns string
        public delegate JassStringRet OrderId2StringPrototype(JassOrder orderId);
        private OrderId2StringPrototype _OrderId2String;
        public string OrderId2String(JassOrder orderId)
        {
            return this._OrderId2String(orderId);
        }
        
        //native UnitId takes string unitIdString returns integer
        public delegate JassObjectId UnitIdPrototype(JassStringArg unitIdString);
        private UnitIdPrototype _UnitId;
        public JassObjectId UnitId(string unitIdString)
        {
            return this._UnitId(unitIdString);
        }
        
        //native UnitId2String takes integer unitId returns string
        public delegate JassStringRet UnitId2StringPrototype(JassObjectId unitId);
        private UnitId2StringPrototype _UnitId2String;
        public string UnitId2String(JassObjectId unitId)
        {
            return this._UnitId2String(unitId);
        }
        
        //native AbilityId takes string abilityIdString returns integer
        public delegate JassObjectId AbilityIdPrototype(JassStringArg abilityIdString);
        private AbilityIdPrototype _AbilityId;
        public JassObjectId AbilityId(string abilityIdString)
        {
            return this._AbilityId(abilityIdString);
        }
        
        //native AbilityId2String takes integer abilityId returns string
        public delegate JassStringRet AbilityId2StringPrototype(JassObjectId abilityId);
        private AbilityId2StringPrototype _AbilityId2String;
        public string AbilityId2String(JassObjectId abilityId)
        {
            return this._AbilityId2String(abilityId);
        }
        
        //native GetObjectName takes integer objectId returns string
        public delegate JassStringRet GetObjectNamePrototype(JassObjectId objectId);
        private GetObjectNamePrototype _GetObjectName;
        public string GetObjectName(JassObjectId objectId)
        {
            return this._GetObjectName(objectId);
        }
        
        //native Deg2Rad takes real degrees returns real
        public delegate JassRealRet Deg2RadPrototype(JassRealArg degrees);
        private Deg2RadPrototype _Deg2Rad;
        public float Deg2Rad(float degrees)
        {
            return this._Deg2Rad(degrees);
        }
        
        //native Rad2Deg takes real radians returns real
        public delegate JassRealRet Rad2DegPrototype(JassRealArg radians);
        private Rad2DegPrototype _Rad2Deg;
        public float Rad2Deg(float radians)
        {
            return this._Rad2Deg(radians);
        }
        
        //native Sin takes real radians returns real
        public delegate JassRealRet SinPrototype(JassRealArg radians);
        private SinPrototype _Sin;
        public float Sin(float radians)
        {
            return this._Sin(radians);
        }
        
        //native Cos takes real radians returns real
        public delegate JassRealRet CosPrototype(JassRealArg radians);
        private CosPrototype _Cos;
        public float Cos(float radians)
        {
            return this._Cos(radians);
        }
        
        //native Tan takes real radians returns real
        public delegate JassRealRet TanPrototype(JassRealArg radians);
        private TanPrototype _Tan;
        public float Tan(float radians)
        {
            return this._Tan(radians);
        }
        
        //native Asin takes real y returns real
        public delegate JassRealRet AsinPrototype(JassRealArg y);
        private AsinPrototype _Asin;
        public float Asin(float y)
        {
            return this._Asin(y);
        }
        
        //native Acos takes real x returns real
        public delegate JassRealRet AcosPrototype(JassRealArg x);
        private AcosPrototype _Acos;
        public float Acos(float x)
        {
            return this._Acos(x);
        }
        
        //native Atan takes real x returns real
        public delegate JassRealRet AtanPrototype(JassRealArg x);
        private AtanPrototype _Atan;
        public float Atan(float x)
        {
            return this._Atan(x);
        }
        
        //native Atan2 takes real y, real x returns real
        public delegate JassRealRet Atan2Prototype(JassRealArg y, JassRealArg x);
        private Atan2Prototype _Atan2;
        public float Atan2(float y, float x)
        {
            return this._Atan2(y, x);
        }
        
        //native SquareRoot takes real x returns real
        public delegate JassRealRet SquareRootPrototype(JassRealArg x);
        private SquareRootPrototype _SquareRoot;
        public float SquareRoot(float x)
        {
            return this._SquareRoot(x);
        }
        
        //native Pow takes real x, real power returns real
        public delegate JassRealRet PowPrototype(JassRealArg x, JassRealArg power);
        private PowPrototype _Pow;
        public float Pow(float x, float power)
        {
            return this._Pow(x, power);
        }
        
        //native I2R takes integer i returns real
        public delegate JassRealRet I2RPrototype(JassInteger i);
        private I2RPrototype _I2R;
        public float I2R(JassInteger i)
        {
            return this._I2R(i);
        }
        
        //native R2I takes real r returns integer
        public delegate JassInteger R2IPrototype(JassRealArg r);
        private R2IPrototype _R2I;
        public JassInteger R2I(float r)
        {
            return this._R2I(r);
        }
        
        //native I2S takes integer i returns string
        public delegate JassStringRet I2SPrototype(JassInteger i);
        private I2SPrototype _I2S;
        public string I2S(JassInteger i)
        {
            return this._I2S(i);
        }
        
        //native R2S takes real r returns string
        public delegate JassStringRet R2SPrototype(JassRealArg r);
        private R2SPrototype _R2S;
        public string R2S(float r)
        {
            return this._R2S(r);
        }
        
        //native R2SW takes real r, integer width, integer precision returns string
        public delegate JassStringRet R2SWPrototype(JassRealArg r, JassInteger width, JassInteger precision);
        private R2SWPrototype _R2SW;
        public string R2SW(float r, JassInteger width, JassInteger precision)
        {
            return this._R2SW(r, width, precision);
        }
        
        //native S2I takes string s returns integer
        public delegate JassInteger S2IPrototype(JassStringArg s);
        private S2IPrototype _S2I;
        public JassInteger S2I(string s)
        {
            return this._S2I(s);
        }
        
        //native S2R takes string s returns real
        public delegate JassRealRet S2RPrototype(JassStringArg s);
        private S2RPrototype _S2R;
        public float S2R(string s)
        {
            return this._S2R(s);
        }
        
        //native GetHandleId takes handle h returns integer
        public delegate JassInteger GetHandleIdPrototype(JassHandle h);
        private GetHandleIdPrototype _GetHandleId;
        public JassInteger GetHandleId(JassHandle h)
        {
            return this._GetHandleId(h);
        }
        
        //native SubString takes string source, integer start, integer end returns string
        public delegate JassStringRet SubStringPrototype(JassStringArg source, JassInteger start, JassInteger end);
        private SubStringPrototype _SubString;
        public string SubString(string source, JassInteger start, JassInteger end)
        {
            return this._SubString(source, start, end);
        }
        
        //native StringLength takes string s returns integer
        public delegate JassInteger StringLengthPrototype(JassStringArg s);
        private StringLengthPrototype _StringLength;
        public JassInteger StringLength(string s)
        {
            return this._StringLength(s);
        }
        
        //native StringCase takes string source, boolean upper returns string
        public delegate JassStringRet StringCasePrototype(JassStringArg source, JassBoolean upper);
        private StringCasePrototype _StringCase;
        public string StringCase(string source, bool upper)
        {
            return this._StringCase(source, upper);
        }
        
        //native StringHash takes string s returns integer
        public delegate JassInteger StringHashPrototype(JassStringArg s);
        private StringHashPrototype _StringHash;
        public JassInteger StringHash(string s)
        {
            return this._StringHash(s);
        }
        
        //native GetLocalizedString takes string source returns string
        public delegate JassStringRet GetLocalizedStringPrototype(JassStringArg source);
        private GetLocalizedStringPrototype _GetLocalizedString;
        public string GetLocalizedString(string source)
        {
            return this._GetLocalizedString(source);
        }
        
        //native GetLocalizedHotkey takes string source returns integer
        public delegate JassInteger GetLocalizedHotkeyPrototype(JassStringArg source);
        private GetLocalizedHotkeyPrototype _GetLocalizedHotkey;
        public JassInteger GetLocalizedHotkey(string source)
        {
            return this._GetLocalizedHotkey(source);
        }
        
        //native SetMapName takes string name returns nothing
        public delegate void SetMapNamePrototype(JassStringArg name);
        private SetMapNamePrototype _SetMapName;
        public void SetMapName(string name)
        {
            this._SetMapName(name);
        }
        
        //native SetMapDescription takes string description returns nothing
        public delegate void SetMapDescriptionPrototype(JassStringArg description);
        private SetMapDescriptionPrototype _SetMapDescription;
        public void SetMapDescription(string description)
        {
            this._SetMapDescription(description);
        }
        
        //native SetTeams takes integer teamcount returns nothing
        public delegate void SetTeamsPrototype(JassInteger teamcount);
        private SetTeamsPrototype _SetTeams;
        public void SetTeams(JassInteger teamcount)
        {
            this._SetTeams(teamcount);
        }
        
        //native SetPlayers takes integer playercount returns nothing
        public delegate void SetPlayersPrototype(JassInteger playercount);
        private SetPlayersPrototype _SetPlayers;
        public void SetPlayers(JassInteger playercount)
        {
            this._SetPlayers(playercount);
        }
        
        //native DefineStartLocation takes integer whichStartLoc, real x, real y returns nothing
        public delegate void DefineStartLocationPrototype(JassInteger whichStartLoc, JassRealArg x, JassRealArg y);
        private DefineStartLocationPrototype _DefineStartLocation;
        public void DefineStartLocation(JassInteger whichStartLoc, float x, float y)
        {
            this._DefineStartLocation(whichStartLoc, x, y);
        }
        
        //native DefineStartLocationLoc takes integer whichStartLoc, location whichLocation returns nothing
        public delegate void DefineStartLocationLocPrototype(JassInteger whichStartLoc, JassLocation whichLocation);
        private DefineStartLocationLocPrototype _DefineStartLocationLoc;
        public void DefineStartLocationLoc(JassInteger whichStartLoc, JassLocation whichLocation)
        {
            this._DefineStartLocationLoc(whichStartLoc, whichLocation);
        }
        
        //native SetStartLocPrioCount takes integer whichStartLoc, integer prioSlotCount returns nothing
        public delegate void SetStartLocPrioCountPrototype(JassInteger whichStartLoc, JassInteger prioSlotCount);
        private SetStartLocPrioCountPrototype _SetStartLocPrioCount;
        public void SetStartLocPrioCount(JassInteger whichStartLoc, JassInteger prioSlotCount)
        {
            this._SetStartLocPrioCount(whichStartLoc, prioSlotCount);
        }
        
        //native SetStartLocPrio takes integer whichStartLoc, integer prioSlotIndex, integer otherStartLocIndex, startlocprio priority returns nothing
        public delegate void SetStartLocPrioPrototype(JassInteger whichStartLoc, JassInteger prioSlotIndex, JassInteger otherStartLocIndex, JassStartLocationPriority priority);
        private SetStartLocPrioPrototype _SetStartLocPrio;
        public void SetStartLocPrio(JassInteger whichStartLoc, JassInteger prioSlotIndex, JassInteger otherStartLocIndex, JassStartLocationPriority priority)
        {
            this._SetStartLocPrio(whichStartLoc, prioSlotIndex, otherStartLocIndex, priority);
        }
        
        //native GetStartLocPrioSlot takes integer whichStartLoc, integer prioSlotIndex returns integer
        public delegate JassInteger GetStartLocPrioSlotPrototype(JassInteger whichStartLoc, JassInteger prioSlotIndex);
        private GetStartLocPrioSlotPrototype _GetStartLocPrioSlot;
        public JassInteger GetStartLocPrioSlot(JassInteger whichStartLoc, JassInteger prioSlotIndex)
        {
            return this._GetStartLocPrioSlot(whichStartLoc, prioSlotIndex);
        }
        
        //native GetStartLocPrio takes integer whichStartLoc, integer prioSlotIndex returns startlocprio
        public delegate JassStartLocationPriority GetStartLocPrioPrototype(JassInteger whichStartLoc, JassInteger prioSlotIndex);
        private GetStartLocPrioPrototype _GetStartLocPrio;
        public JassStartLocationPriority GetStartLocPrio(JassInteger whichStartLoc, JassInteger prioSlotIndex)
        {
            return this._GetStartLocPrio(whichStartLoc, prioSlotIndex);
        }
        
        //native SetGameTypeSupported takes gametype whichGameType, boolean value returns nothing
        public delegate void SetGameTypeSupportedPrototype(JassGameType whichGameType, JassBoolean value);
        private SetGameTypeSupportedPrototype _SetGameTypeSupported;
        public void SetGameTypeSupported(JassGameType whichGameType, bool value)
        {
            this._SetGameTypeSupported(whichGameType, value);
        }
        
        //native SetMapFlag takes mapflag whichMapFlag, boolean value returns nothing
        public delegate void SetMapFlagPrototype(JassMapFlag whichMapFlag, JassBoolean value);
        private SetMapFlagPrototype _SetMapFlag;
        public void SetMapFlag(JassMapFlag whichMapFlag, bool value)
        {
            this._SetMapFlag(whichMapFlag, value);
        }
        
        //native SetGamePlacement takes placement whichPlacementType returns nothing
        public delegate void SetGamePlacementPrototype(JassPlacement whichPlacementType);
        private SetGamePlacementPrototype _SetGamePlacement;
        public void SetGamePlacement(JassPlacement whichPlacementType)
        {
            this._SetGamePlacement(whichPlacementType);
        }
        
        //native SetGameSpeed takes gamespeed whichspeed returns nothing
        public delegate void SetGameSpeedPrototype(JassGameSpeed whichspeed);
        private SetGameSpeedPrototype _SetGameSpeed;
        public void SetGameSpeed(JassGameSpeed whichspeed)
        {
            this._SetGameSpeed(whichspeed);
        }
        
        //native SetGameDifficulty takes gamedifficulty whichdifficulty returns nothing
        public delegate void SetGameDifficultyPrototype(JassGameDifficulty whichdifficulty);
        private SetGameDifficultyPrototype _SetGameDifficulty;
        public void SetGameDifficulty(JassGameDifficulty whichdifficulty)
        {
            this._SetGameDifficulty(whichdifficulty);
        }
        
        //native SetResourceDensity takes mapdensity whichdensity returns nothing
        public delegate void SetResourceDensityPrototype(JassMapDensity whichdensity);
        private SetResourceDensityPrototype _SetResourceDensity;
        public void SetResourceDensity(JassMapDensity whichdensity)
        {
            this._SetResourceDensity(whichdensity);
        }
        
        //native SetCreatureDensity takes mapdensity whichdensity returns nothing
        public delegate void SetCreatureDensityPrototype(JassMapDensity whichdensity);
        private SetCreatureDensityPrototype _SetCreatureDensity;
        public void SetCreatureDensity(JassMapDensity whichdensity)
        {
            this._SetCreatureDensity(whichdensity);
        }
        
        //native GetTeams takes nothing returns integer
        public delegate JassInteger GetTeamsPrototype();
        private GetTeamsPrototype _GetTeams;
        public JassInteger GetTeams()
        {
            return this._GetTeams();
        }
        
        //native GetPlayers takes nothing returns integer
        public delegate JassInteger GetPlayersPrototype();
        private GetPlayersPrototype _GetPlayers;
        public JassInteger GetPlayers()
        {
            return this._GetPlayers();
        }
        
        //native IsGameTypeSupported takes gametype whichGameType returns boolean
        public delegate JassBoolean IsGameTypeSupportedPrototype(JassGameType whichGameType);
        private IsGameTypeSupportedPrototype _IsGameTypeSupported;
        public bool IsGameTypeSupported(JassGameType whichGameType)
        {
            return this._IsGameTypeSupported(whichGameType);
        }
        
        //native GetGameTypeSelected takes nothing returns gametype
        public delegate JassGameType GetGameTypeSelectedPrototype();
        private GetGameTypeSelectedPrototype _GetGameTypeSelected;
        public JassGameType GetGameTypeSelected()
        {
            return this._GetGameTypeSelected();
        }
        
        //native IsMapFlagSet takes mapflag whichMapFlag returns boolean
        public delegate JassBoolean IsMapFlagSetPrototype(JassMapFlag whichMapFlag);
        private IsMapFlagSetPrototype _IsMapFlagSet;
        public bool IsMapFlagSet(JassMapFlag whichMapFlag)
        {
            return this._IsMapFlagSet(whichMapFlag);
        }
        
        //native GetGamePlacement takes nothing returns placement
        public delegate JassPlacement GetGamePlacementPrototype();
        private GetGamePlacementPrototype _GetGamePlacement;
        public JassPlacement GetGamePlacement()
        {
            return this._GetGamePlacement();
        }
        
        //native GetGameSpeed takes nothing returns gamespeed
        public delegate JassGameSpeed GetGameSpeedPrototype();
        private GetGameSpeedPrototype _GetGameSpeed;
        public JassGameSpeed GetGameSpeed()
        {
            return this._GetGameSpeed();
        }
        
        //native GetGameDifficulty takes nothing returns gamedifficulty
        public delegate JassGameDifficulty GetGameDifficultyPrototype();
        private GetGameDifficultyPrototype _GetGameDifficulty;
        public JassGameDifficulty GetGameDifficulty()
        {
            return this._GetGameDifficulty();
        }
        
        //native GetResourceDensity takes nothing returns mapdensity
        public delegate JassMapDensity GetResourceDensityPrototype();
        private GetResourceDensityPrototype _GetResourceDensity;
        public JassMapDensity GetResourceDensity()
        {
            return this._GetResourceDensity();
        }
        
        //native GetCreatureDensity takes nothing returns mapdensity
        public delegate JassMapDensity GetCreatureDensityPrototype();
        private GetCreatureDensityPrototype _GetCreatureDensity;
        public JassMapDensity GetCreatureDensity()
        {
            return this._GetCreatureDensity();
        }
        
        //native GetStartLocationX takes integer whichStartLocation returns real
        public delegate JassRealRet GetStartLocationXPrototype(JassInteger whichStartLocation);
        private GetStartLocationXPrototype _GetStartLocationX;
        public float GetStartLocationX(JassInteger whichStartLocation)
        {
            return this._GetStartLocationX(whichStartLocation);
        }
        
        //native GetStartLocationY takes integer whichStartLocation returns real
        public delegate JassRealRet GetStartLocationYPrototype(JassInteger whichStartLocation);
        private GetStartLocationYPrototype _GetStartLocationY;
        public float GetStartLocationY(JassInteger whichStartLocation)
        {
            return this._GetStartLocationY(whichStartLocation);
        }
        
        //native GetStartLocationLoc takes integer whichStartLocation returns location
        public delegate JassLocation GetStartLocationLocPrototype(JassInteger whichStartLocation);
        private GetStartLocationLocPrototype _GetStartLocationLoc;
        public JassLocation GetStartLocationLoc(JassInteger whichStartLocation)
        {
            return this._GetStartLocationLoc(whichStartLocation);
        }
        
        //native SetPlayerTeam takes player whichPlayer, integer whichTeam returns nothing
        public delegate void SetPlayerTeamPrototype(JassPlayer whichPlayer, JassInteger whichTeam);
        private SetPlayerTeamPrototype _SetPlayerTeam;
        public void SetPlayerTeam(JassPlayer whichPlayer, JassInteger whichTeam)
        {
            this._SetPlayerTeam(whichPlayer, whichTeam);
        }
        
        //native SetPlayerStartLocation takes player whichPlayer, integer startLocIndex returns nothing
        public delegate void SetPlayerStartLocationPrototype(JassPlayer whichPlayer, JassInteger startLocIndex);
        private SetPlayerStartLocationPrototype _SetPlayerStartLocation;
        public void SetPlayerStartLocation(JassPlayer whichPlayer, JassInteger startLocIndex)
        {
            this._SetPlayerStartLocation(whichPlayer, startLocIndex);
        }
        
        //native ForcePlayerStartLocation takes player whichPlayer, integer startLocIndex returns nothing
        public delegate void ForcePlayerStartLocationPrototype(JassPlayer whichPlayer, JassInteger startLocIndex);
        private ForcePlayerStartLocationPrototype _ForcePlayerStartLocation;
        public void ForcePlayerStartLocation(JassPlayer whichPlayer, JassInteger startLocIndex)
        {
            this._ForcePlayerStartLocation(whichPlayer, startLocIndex);
        }
        
        //native SetPlayerColor takes player whichPlayer, playercolor color returns nothing
        public delegate void SetPlayerColorPrototype(JassPlayer whichPlayer, JassPlayerColor color);
        private SetPlayerColorPrototype _SetPlayerColor;
        public void SetPlayerColor(JassPlayer whichPlayer, JassPlayerColor color)
        {
            this._SetPlayerColor(whichPlayer, color);
        }
        
        //native SetPlayerAlliance takes player sourcePlayer, player otherPlayer, alliancetype whichAllianceSetting, boolean value returns nothing
        public delegate void SetPlayerAlliancePrototype(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassAllianceType whichAllianceSetting, JassBoolean value);
        private SetPlayerAlliancePrototype _SetPlayerAlliance;
        public void SetPlayerAlliance(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassAllianceType whichAllianceSetting, bool value)
        {
            this._SetPlayerAlliance(sourcePlayer, otherPlayer, whichAllianceSetting, value);
        }
        
        //native SetPlayerTaxRate takes player sourcePlayer, player otherPlayer, playerstate whichResource, integer rate returns nothing
        public delegate void SetPlayerTaxRatePrototype(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassPlayerState whichResource, JassInteger rate);
        private SetPlayerTaxRatePrototype _SetPlayerTaxRate;
        public void SetPlayerTaxRate(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassPlayerState whichResource, JassInteger rate)
        {
            this._SetPlayerTaxRate(sourcePlayer, otherPlayer, whichResource, rate);
        }
        
        //native SetPlayerRacePreference takes player whichPlayer, racepreference whichRacePreference returns nothing
        public delegate void SetPlayerRacePreferencePrototype(JassPlayer whichPlayer, JassRacePreference whichRacePreference);
        private SetPlayerRacePreferencePrototype _SetPlayerRacePreference;
        public void SetPlayerRacePreference(JassPlayer whichPlayer, JassRacePreference whichRacePreference)
        {
            this._SetPlayerRacePreference(whichPlayer, whichRacePreference);
        }
        
        //native SetPlayerRaceSelectable takes player whichPlayer, boolean value returns nothing
        public delegate void SetPlayerRaceSelectablePrototype(JassPlayer whichPlayer, JassBoolean value);
        private SetPlayerRaceSelectablePrototype _SetPlayerRaceSelectable;
        public void SetPlayerRaceSelectable(JassPlayer whichPlayer, bool value)
        {
            this._SetPlayerRaceSelectable(whichPlayer, value);
        }
        
        //native SetPlayerController takes player whichPlayer, mapcontrol controlType returns nothing
        public delegate void SetPlayerControllerPrototype(JassPlayer whichPlayer, JassMapControl controlType);
        private SetPlayerControllerPrototype _SetPlayerController;
        public void SetPlayerController(JassPlayer whichPlayer, JassMapControl controlType)
        {
            this._SetPlayerController(whichPlayer, controlType);
        }
        
        //native SetPlayerName takes player whichPlayer, string name returns nothing
        public delegate void SetPlayerNamePrototype(JassPlayer whichPlayer, JassStringArg name);
        private SetPlayerNamePrototype _SetPlayerName;
        public void SetPlayerName(JassPlayer whichPlayer, string name)
        {
            this._SetPlayerName(whichPlayer, name);
        }
        
        //native SetPlayerOnScoreScreen takes player whichPlayer, boolean flag returns nothing
        public delegate void SetPlayerOnScoreScreenPrototype(JassPlayer whichPlayer, JassBoolean flag);
        private SetPlayerOnScoreScreenPrototype _SetPlayerOnScoreScreen;
        public void SetPlayerOnScoreScreen(JassPlayer whichPlayer, bool flag)
        {
            this._SetPlayerOnScoreScreen(whichPlayer, flag);
        }
        
        //native GetPlayerTeam takes player whichPlayer returns integer
        public delegate JassInteger GetPlayerTeamPrototype(JassPlayer whichPlayer);
        private GetPlayerTeamPrototype _GetPlayerTeam;
        public JassInteger GetPlayerTeam(JassPlayer whichPlayer)
        {
            return this._GetPlayerTeam(whichPlayer);
        }
        
        //native GetPlayerStartLocation takes player whichPlayer returns integer
        public delegate JassInteger GetPlayerStartLocationPrototype(JassPlayer whichPlayer);
        private GetPlayerStartLocationPrototype _GetPlayerStartLocation;
        public JassInteger GetPlayerStartLocation(JassPlayer whichPlayer)
        {
            return this._GetPlayerStartLocation(whichPlayer);
        }
        
        //native GetPlayerColor takes player whichPlayer returns playercolor
        public delegate JassPlayerColor GetPlayerColorPrototype(JassPlayer whichPlayer);
        private GetPlayerColorPrototype _GetPlayerColor;
        public JassPlayerColor GetPlayerColor(JassPlayer whichPlayer)
        {
            return this._GetPlayerColor(whichPlayer);
        }
        
        //native GetPlayerSelectable takes player whichPlayer returns boolean
        public delegate JassBoolean GetPlayerSelectablePrototype(JassPlayer whichPlayer);
        private GetPlayerSelectablePrototype _GetPlayerSelectable;
        public bool GetPlayerSelectable(JassPlayer whichPlayer)
        {
            return this._GetPlayerSelectable(whichPlayer);
        }
        
        //native GetPlayerController takes player whichPlayer returns mapcontrol
        public delegate JassMapControl GetPlayerControllerPrototype(JassPlayer whichPlayer);
        private GetPlayerControllerPrototype _GetPlayerController;
        public JassMapControl GetPlayerController(JassPlayer whichPlayer)
        {
            return this._GetPlayerController(whichPlayer);
        }
        
        //native GetPlayerSlotState takes player whichPlayer returns playerslotstate
        public delegate JassPlayerSlotState GetPlayerSlotStatePrototype(JassPlayer whichPlayer);
        private GetPlayerSlotStatePrototype _GetPlayerSlotState;
        public JassPlayerSlotState GetPlayerSlotState(JassPlayer whichPlayer)
        {
            return this._GetPlayerSlotState(whichPlayer);
        }
        
        //native GetPlayerTaxRate takes player sourcePlayer, player otherPlayer, playerstate whichResource returns integer
        public delegate JassInteger GetPlayerTaxRatePrototype(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassPlayerState whichResource);
        private GetPlayerTaxRatePrototype _GetPlayerTaxRate;
        public JassInteger GetPlayerTaxRate(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassPlayerState whichResource)
        {
            return this._GetPlayerTaxRate(sourcePlayer, otherPlayer, whichResource);
        }
        
        //native IsPlayerRacePrefSet takes player whichPlayer, racepreference pref returns boolean
        public delegate JassBoolean IsPlayerRacePrefSetPrototype(JassPlayer whichPlayer, JassRacePreference pref);
        private IsPlayerRacePrefSetPrototype _IsPlayerRacePrefSet;
        public bool IsPlayerRacePrefSet(JassPlayer whichPlayer, JassRacePreference pref)
        {
            return this._IsPlayerRacePrefSet(whichPlayer, pref);
        }
        
        //native GetPlayerName takes player whichPlayer returns string
        public delegate JassStringRet GetPlayerNamePrototype(JassPlayer whichPlayer);
        private GetPlayerNamePrototype _GetPlayerName;
        public string GetPlayerName(JassPlayer whichPlayer)
        {
            return this._GetPlayerName(whichPlayer);
        }
        
        //native CreateTimer takes nothing returns timer
        public delegate JassTimer CreateTimerPrototype();
        private CreateTimerPrototype _CreateTimer;
        public JassTimer CreateTimer()
        {
            return this._CreateTimer();
        }
        
        //native DestroyTimer takes timer whichTimer returns nothing
        public delegate void DestroyTimerPrototype(JassTimer whichTimer);
        private DestroyTimerPrototype _DestroyTimer;
        public void DestroyTimer(JassTimer whichTimer)
        {
            this._DestroyTimer(whichTimer);
        }
        
        //native TimerStart takes timer whichTimer, real timeout, boolean periodic, code handlerFunc returns nothing
        public delegate void TimerStartPrototype(JassTimer whichTimer, JassRealArg timeout, JassBoolean periodic, JassCode handlerFunc);
        private TimerStartPrototype _TimerStart;
        public void TimerStart(JassTimer whichTimer, float timeout, bool periodic, JassCode handlerFunc)
        {
            this._TimerStart(whichTimer, timeout, periodic, handlerFunc);
        }
        
        //native TimerGetElapsed takes timer whichTimer returns real
        public delegate JassRealRet TimerGetElapsedPrototype(JassTimer whichTimer);
        private TimerGetElapsedPrototype _TimerGetElapsed;
        public float TimerGetElapsed(JassTimer whichTimer)
        {
            return this._TimerGetElapsed(whichTimer);
        }
        
        //native TimerGetRemaining takes timer whichTimer returns real
        public delegate JassRealRet TimerGetRemainingPrototype(JassTimer whichTimer);
        private TimerGetRemainingPrototype _TimerGetRemaining;
        public float TimerGetRemaining(JassTimer whichTimer)
        {
            return this._TimerGetRemaining(whichTimer);
        }
        
        //native TimerGetTimeout takes timer whichTimer returns real
        public delegate JassRealRet TimerGetTimeoutPrototype(JassTimer whichTimer);
        private TimerGetTimeoutPrototype _TimerGetTimeout;
        public float TimerGetTimeout(JassTimer whichTimer)
        {
            return this._TimerGetTimeout(whichTimer);
        }
        
        //native PauseTimer takes timer whichTimer returns nothing
        public delegate void PauseTimerPrototype(JassTimer whichTimer);
        private PauseTimerPrototype _PauseTimer;
        public void PauseTimer(JassTimer whichTimer)
        {
            this._PauseTimer(whichTimer);
        }
        
        //native ResumeTimer takes timer whichTimer returns nothing
        public delegate void ResumeTimerPrototype(JassTimer whichTimer);
        private ResumeTimerPrototype _ResumeTimer;
        public void ResumeTimer(JassTimer whichTimer)
        {
            this._ResumeTimer(whichTimer);
        }
        
        //native GetExpiredTimer takes nothing returns timer
        public delegate JassTimer GetExpiredTimerPrototype();
        private GetExpiredTimerPrototype _GetExpiredTimer;
        public JassTimer GetExpiredTimer()
        {
            return this._GetExpiredTimer();
        }
        
        //native CreateGroup takes nothing returns group
        public delegate JassGroup CreateGroupPrototype();
        private CreateGroupPrototype _CreateGroup;
        public JassGroup CreateGroup()
        {
            return this._CreateGroup();
        }
        
        //native DestroyGroup takes group whichGroup returns nothing
        public delegate void DestroyGroupPrototype(JassGroup whichGroup);
        private DestroyGroupPrototype _DestroyGroup;
        public void DestroyGroup(JassGroup whichGroup)
        {
            this._DestroyGroup(whichGroup);
        }
        
        //native GroupAddUnit takes group whichGroup, unit whichUnit returns nothing
        public delegate void GroupAddUnitPrototype(JassGroup whichGroup, JassUnit whichUnit);
        private GroupAddUnitPrototype _GroupAddUnit;
        public void GroupAddUnit(JassGroup whichGroup, JassUnit whichUnit)
        {
            this._GroupAddUnit(whichGroup, whichUnit);
        }
        
        //native GroupRemoveUnit takes group whichGroup, unit whichUnit returns nothing
        public delegate void GroupRemoveUnitPrototype(JassGroup whichGroup, JassUnit whichUnit);
        private GroupRemoveUnitPrototype _GroupRemoveUnit;
        public void GroupRemoveUnit(JassGroup whichGroup, JassUnit whichUnit)
        {
            this._GroupRemoveUnit(whichGroup, whichUnit);
        }
        
        //native GroupClear takes group whichGroup returns nothing
        public delegate void GroupClearPrototype(JassGroup whichGroup);
        private GroupClearPrototype _GroupClear;
        public void GroupClear(JassGroup whichGroup)
        {
            this._GroupClear(whichGroup);
        }
        
        //native GroupEnumUnitsOfType takes group whichGroup, string unitname, boolexpr filter returns nothing
        public delegate void GroupEnumUnitsOfTypePrototype(JassGroup whichGroup, JassStringArg unitname, JassBooleanExpression filter);
        private GroupEnumUnitsOfTypePrototype _GroupEnumUnitsOfType;
        public void GroupEnumUnitsOfType(JassGroup whichGroup, string unitname, JassBooleanExpression filter)
        {
            this._GroupEnumUnitsOfType(whichGroup, unitname, filter);
        }
        
        //native GroupEnumUnitsOfPlayer takes group whichGroup, player whichPlayer, boolexpr filter returns nothing
        public delegate void GroupEnumUnitsOfPlayerPrototype(JassGroup whichGroup, JassPlayer whichPlayer, JassBooleanExpression filter);
        private GroupEnumUnitsOfPlayerPrototype _GroupEnumUnitsOfPlayer;
        public void GroupEnumUnitsOfPlayer(JassGroup whichGroup, JassPlayer whichPlayer, JassBooleanExpression filter)
        {
            this._GroupEnumUnitsOfPlayer(whichGroup, whichPlayer, filter);
        }
        
        //native GroupEnumUnitsOfTypeCounted takes group whichGroup, string unitname, boolexpr filter, integer countLimit returns nothing
        public delegate void GroupEnumUnitsOfTypeCountedPrototype(JassGroup whichGroup, JassStringArg unitname, JassBooleanExpression filter, JassInteger countLimit);
        private GroupEnumUnitsOfTypeCountedPrototype _GroupEnumUnitsOfTypeCounted;
        public void GroupEnumUnitsOfTypeCounted(JassGroup whichGroup, string unitname, JassBooleanExpression filter, JassInteger countLimit)
        {
            this._GroupEnumUnitsOfTypeCounted(whichGroup, unitname, filter, countLimit);
        }
        
        //native GroupEnumUnitsInRect takes group whichGroup, rect r, boolexpr filter returns nothing
        public delegate void GroupEnumUnitsInRectPrototype(JassGroup whichGroup, JassRect r, JassBooleanExpression filter);
        private GroupEnumUnitsInRectPrototype _GroupEnumUnitsInRect;
        public void GroupEnumUnitsInRect(JassGroup whichGroup, JassRect r, JassBooleanExpression filter)
        {
            this._GroupEnumUnitsInRect(whichGroup, r, filter);
        }
        
        //native GroupEnumUnitsInRectCounted takes group whichGroup, rect r, boolexpr filter, integer countLimit returns nothing
        public delegate void GroupEnumUnitsInRectCountedPrototype(JassGroup whichGroup, JassRect r, JassBooleanExpression filter, JassInteger countLimit);
        private GroupEnumUnitsInRectCountedPrototype _GroupEnumUnitsInRectCounted;
        public void GroupEnumUnitsInRectCounted(JassGroup whichGroup, JassRect r, JassBooleanExpression filter, JassInteger countLimit)
        {
            this._GroupEnumUnitsInRectCounted(whichGroup, r, filter, countLimit);
        }
        
        //native GroupEnumUnitsInRange takes group whichGroup, real x, real y, real radius, boolexpr filter returns nothing
        public delegate void GroupEnumUnitsInRangePrototype(JassGroup whichGroup, JassRealArg x, JassRealArg y, JassRealArg radius, JassBooleanExpression filter);
        private GroupEnumUnitsInRangePrototype _GroupEnumUnitsInRange;
        public void GroupEnumUnitsInRange(JassGroup whichGroup, float x, float y, float radius, JassBooleanExpression filter)
        {
            this._GroupEnumUnitsInRange(whichGroup, x, y, radius, filter);
        }
        
        //native GroupEnumUnitsInRangeOfLoc takes group whichGroup, location whichLocation, real radius, boolexpr filter returns nothing
        public delegate void GroupEnumUnitsInRangeOfLocPrototype(JassGroup whichGroup, JassLocation whichLocation, JassRealArg radius, JassBooleanExpression filter);
        private GroupEnumUnitsInRangeOfLocPrototype _GroupEnumUnitsInRangeOfLoc;
        public void GroupEnumUnitsInRangeOfLoc(JassGroup whichGroup, JassLocation whichLocation, float radius, JassBooleanExpression filter)
        {
            this._GroupEnumUnitsInRangeOfLoc(whichGroup, whichLocation, radius, filter);
        }
        
        //native GroupEnumUnitsInRangeCounted takes group whichGroup, real x, real y, real radius, boolexpr filter, integer countLimit returns nothing
        public delegate void GroupEnumUnitsInRangeCountedPrototype(JassGroup whichGroup, JassRealArg x, JassRealArg y, JassRealArg radius, JassBooleanExpression filter, JassInteger countLimit);
        private GroupEnumUnitsInRangeCountedPrototype _GroupEnumUnitsInRangeCounted;
        public void GroupEnumUnitsInRangeCounted(JassGroup whichGroup, float x, float y, float radius, JassBooleanExpression filter, JassInteger countLimit)
        {
            this._GroupEnumUnitsInRangeCounted(whichGroup, x, y, radius, filter, countLimit);
        }
        
        //native GroupEnumUnitsInRangeOfLocCounted takes group whichGroup, location whichLocation, real radius, boolexpr filter, integer countLimit returns nothing
        public delegate void GroupEnumUnitsInRangeOfLocCountedPrototype(JassGroup whichGroup, JassLocation whichLocation, JassRealArg radius, JassBooleanExpression filter, JassInteger countLimit);
        private GroupEnumUnitsInRangeOfLocCountedPrototype _GroupEnumUnitsInRangeOfLocCounted;
        public void GroupEnumUnitsInRangeOfLocCounted(JassGroup whichGroup, JassLocation whichLocation, float radius, JassBooleanExpression filter, JassInteger countLimit)
        {
            this._GroupEnumUnitsInRangeOfLocCounted(whichGroup, whichLocation, radius, filter, countLimit);
        }
        
        //native GroupEnumUnitsSelected takes group whichGroup, player whichPlayer, boolexpr filter returns nothing
        public delegate void GroupEnumUnitsSelectedPrototype(JassGroup whichGroup, JassPlayer whichPlayer, JassBooleanExpression filter);
        private GroupEnumUnitsSelectedPrototype _GroupEnumUnitsSelected;
        public void GroupEnumUnitsSelected(JassGroup whichGroup, JassPlayer whichPlayer, JassBooleanExpression filter)
        {
            this._GroupEnumUnitsSelected(whichGroup, whichPlayer, filter);
        }
        
        //native GroupImmediateOrder takes group whichGroup, string order returns boolean
        public delegate JassBoolean GroupImmediateOrderPrototype(JassGroup whichGroup, JassStringArg order);
        private GroupImmediateOrderPrototype _GroupImmediateOrder;
        public bool GroupImmediateOrder(JassGroup whichGroup, string order)
        {
            return this._GroupImmediateOrder(whichGroup, order);
        }
        
        //native GroupImmediateOrderById takes group whichGroup, integer order returns boolean
        public delegate JassBoolean GroupImmediateOrderByIdPrototype(JassGroup whichGroup, JassOrder order);
        private GroupImmediateOrderByIdPrototype _GroupImmediateOrderById;
        public bool GroupImmediateOrderById(JassGroup whichGroup, JassOrder order)
        {
            return this._GroupImmediateOrderById(whichGroup, order);
        }
        
        //native GroupPointOrder takes group whichGroup, string order, real x, real y returns boolean
        public delegate JassBoolean GroupPointOrderPrototype(JassGroup whichGroup, JassStringArg order, JassRealArg x, JassRealArg y);
        private GroupPointOrderPrototype _GroupPointOrder;
        public bool GroupPointOrder(JassGroup whichGroup, string order, float x, float y)
        {
            return this._GroupPointOrder(whichGroup, order, x, y);
        }
        
        //native GroupPointOrderLoc takes group whichGroup, string order, location whichLocation returns boolean
        public delegate JassBoolean GroupPointOrderLocPrototype(JassGroup whichGroup, JassStringArg order, JassLocation whichLocation);
        private GroupPointOrderLocPrototype _GroupPointOrderLoc;
        public bool GroupPointOrderLoc(JassGroup whichGroup, string order, JassLocation whichLocation)
        {
            return this._GroupPointOrderLoc(whichGroup, order, whichLocation);
        }
        
        //native GroupPointOrderById takes group whichGroup, integer order, real x, real y returns boolean
        public delegate JassBoolean GroupPointOrderByIdPrototype(JassGroup whichGroup, JassOrder order, JassRealArg x, JassRealArg y);
        private GroupPointOrderByIdPrototype _GroupPointOrderById;
        public bool GroupPointOrderById(JassGroup whichGroup, JassOrder order, float x, float y)
        {
            return this._GroupPointOrderById(whichGroup, order, x, y);
        }
        
        //native GroupPointOrderByIdLoc takes group whichGroup, integer order, location whichLocation returns boolean
        public delegate JassBoolean GroupPointOrderByIdLocPrototype(JassGroup whichGroup, JassOrder order, JassLocation whichLocation);
        private GroupPointOrderByIdLocPrototype _GroupPointOrderByIdLoc;
        public bool GroupPointOrderByIdLoc(JassGroup whichGroup, JassOrder order, JassLocation whichLocation)
        {
            return this._GroupPointOrderByIdLoc(whichGroup, order, whichLocation);
        }
        
        //native GroupTargetOrder takes group whichGroup, string order, widget targetWidget returns boolean
        public delegate JassBoolean GroupTargetOrderPrototype(JassGroup whichGroup, JassStringArg order, JassWidget targetWidget);
        private GroupTargetOrderPrototype _GroupTargetOrder;
        public bool GroupTargetOrder(JassGroup whichGroup, string order, JassWidget targetWidget)
        {
            return this._GroupTargetOrder(whichGroup, order, targetWidget);
        }
        
        //native GroupTargetOrderById takes group whichGroup, integer order, widget targetWidget returns boolean
        public delegate JassBoolean GroupTargetOrderByIdPrototype(JassGroup whichGroup, JassOrder order, JassWidget targetWidget);
        private GroupTargetOrderByIdPrototype _GroupTargetOrderById;
        public bool GroupTargetOrderById(JassGroup whichGroup, JassOrder order, JassWidget targetWidget)
        {
            return this._GroupTargetOrderById(whichGroup, order, targetWidget);
        }
        
        //native ForGroup takes group whichGroup, code callback returns nothing
        public delegate void ForGroupPrototype(JassGroup whichGroup, JassCode callback);
        private ForGroupPrototype _ForGroup;
        public void ForGroup(JassGroup whichGroup, JassCode callback)
        {
            this._ForGroup(whichGroup, callback);
        }
        
        //native FirstOfGroup takes group whichGroup returns unit
        public delegate JassUnit FirstOfGroupPrototype(JassGroup whichGroup);
        private FirstOfGroupPrototype _FirstOfGroup;
        public JassUnit FirstOfGroup(JassGroup whichGroup)
        {
            return this._FirstOfGroup(whichGroup);
        }
        
        //native CreateForce takes nothing returns force
        public delegate JassForce CreateForcePrototype();
        private CreateForcePrototype _CreateForce;
        public JassForce CreateForce()
        {
            return this._CreateForce();
        }
        
        //native DestroyForce takes force whichForce returns nothing
        public delegate void DestroyForcePrototype(JassForce whichForce);
        private DestroyForcePrototype _DestroyForce;
        public void DestroyForce(JassForce whichForce)
        {
            this._DestroyForce(whichForce);
        }
        
        //native ForceAddPlayer takes force whichForce, player whichPlayer returns nothing
        public delegate void ForceAddPlayerPrototype(JassForce whichForce, JassPlayer whichPlayer);
        private ForceAddPlayerPrototype _ForceAddPlayer;
        public void ForceAddPlayer(JassForce whichForce, JassPlayer whichPlayer)
        {
            this._ForceAddPlayer(whichForce, whichPlayer);
        }
        
        //native ForceRemovePlayer takes force whichForce, player whichPlayer returns nothing
        public delegate void ForceRemovePlayerPrototype(JassForce whichForce, JassPlayer whichPlayer);
        private ForceRemovePlayerPrototype _ForceRemovePlayer;
        public void ForceRemovePlayer(JassForce whichForce, JassPlayer whichPlayer)
        {
            this._ForceRemovePlayer(whichForce, whichPlayer);
        }
        
        //native ForceClear takes force whichForce returns nothing
        public delegate void ForceClearPrototype(JassForce whichForce);
        private ForceClearPrototype _ForceClear;
        public void ForceClear(JassForce whichForce)
        {
            this._ForceClear(whichForce);
        }
        
        //native ForceEnumPlayers takes force whichForce, boolexpr filter returns nothing
        public delegate void ForceEnumPlayersPrototype(JassForce whichForce, JassBooleanExpression filter);
        private ForceEnumPlayersPrototype _ForceEnumPlayers;
        public void ForceEnumPlayers(JassForce whichForce, JassBooleanExpression filter)
        {
            this._ForceEnumPlayers(whichForce, filter);
        }
        
        //native ForceEnumPlayersCounted takes force whichForce, boolexpr filter, integer countLimit returns nothing
        public delegate void ForceEnumPlayersCountedPrototype(JassForce whichForce, JassBooleanExpression filter, JassInteger countLimit);
        private ForceEnumPlayersCountedPrototype _ForceEnumPlayersCounted;
        public void ForceEnumPlayersCounted(JassForce whichForce, JassBooleanExpression filter, JassInteger countLimit)
        {
            this._ForceEnumPlayersCounted(whichForce, filter, countLimit);
        }
        
        //native ForceEnumAllies takes force whichForce, player whichPlayer, boolexpr filter returns nothing
        public delegate void ForceEnumAlliesPrototype(JassForce whichForce, JassPlayer whichPlayer, JassBooleanExpression filter);
        private ForceEnumAlliesPrototype _ForceEnumAllies;
        public void ForceEnumAllies(JassForce whichForce, JassPlayer whichPlayer, JassBooleanExpression filter)
        {
            this._ForceEnumAllies(whichForce, whichPlayer, filter);
        }
        
        //native ForceEnumEnemies takes force whichForce, player whichPlayer, boolexpr filter returns nothing
        public delegate void ForceEnumEnemiesPrototype(JassForce whichForce, JassPlayer whichPlayer, JassBooleanExpression filter);
        private ForceEnumEnemiesPrototype _ForceEnumEnemies;
        public void ForceEnumEnemies(JassForce whichForce, JassPlayer whichPlayer, JassBooleanExpression filter)
        {
            this._ForceEnumEnemies(whichForce, whichPlayer, filter);
        }
        
        //native ForForce takes force whichForce, code callback returns nothing
        public delegate void ForForcePrototype(JassForce whichForce, JassCode callback);
        private ForForcePrototype _ForForce;
        public void ForForce(JassForce whichForce, JassCode callback)
        {
            this._ForForce(whichForce, callback);
        }
        
        //native Rect takes real minx, real miny, real maxx, real maxy returns rect
        public delegate JassRect RectPrototype(JassRealArg minx, JassRealArg miny, JassRealArg maxx, JassRealArg maxy);
        private RectPrototype _Rect;
        public JassRect Rect(float minx, float miny, float maxx, float maxy)
        {
            return this._Rect(minx, miny, maxx, maxy);
        }
        
        //native RectFromLoc takes location min, location max returns rect
        public delegate JassRect RectFromLocPrototype(JassLocation min, JassLocation max);
        private RectFromLocPrototype _RectFromLoc;
        public JassRect RectFromLoc(JassLocation min, JassLocation max)
        {
            return this._RectFromLoc(min, max);
        }
        
        //native RemoveRect takes rect whichRect returns nothing
        public delegate void RemoveRectPrototype(JassRect whichRect);
        private RemoveRectPrototype _RemoveRect;
        public void RemoveRect(JassRect whichRect)
        {
            this._RemoveRect(whichRect);
        }
        
        //native SetRect takes rect whichRect, real minx, real miny, real maxx, real maxy returns nothing
        public delegate void SetRectPrototype(JassRect whichRect, JassRealArg minx, JassRealArg miny, JassRealArg maxx, JassRealArg maxy);
        private SetRectPrototype _SetRect;
        public void SetRect(JassRect whichRect, float minx, float miny, float maxx, float maxy)
        {
            this._SetRect(whichRect, minx, miny, maxx, maxy);
        }
        
        //native SetRectFromLoc takes rect whichRect, location min, location max returns nothing
        public delegate void SetRectFromLocPrototype(JassRect whichRect, JassLocation min, JassLocation max);
        private SetRectFromLocPrototype _SetRectFromLoc;
        public void SetRectFromLoc(JassRect whichRect, JassLocation min, JassLocation max)
        {
            this._SetRectFromLoc(whichRect, min, max);
        }
        
        //native MoveRectTo takes rect whichRect, real newCenterX, real newCenterY returns nothing
        public delegate void MoveRectToPrototype(JassRect whichRect, JassRealArg newCenterX, JassRealArg newCenterY);
        private MoveRectToPrototype _MoveRectTo;
        public void MoveRectTo(JassRect whichRect, float newCenterX, float newCenterY)
        {
            this._MoveRectTo(whichRect, newCenterX, newCenterY);
        }
        
        //native MoveRectToLoc takes rect whichRect, location newCenterLoc returns nothing
        public delegate void MoveRectToLocPrototype(JassRect whichRect, JassLocation newCenterLoc);
        private MoveRectToLocPrototype _MoveRectToLoc;
        public void MoveRectToLoc(JassRect whichRect, JassLocation newCenterLoc)
        {
            this._MoveRectToLoc(whichRect, newCenterLoc);
        }
        
        //native GetRectCenterX takes rect whichRect returns real
        public delegate JassRealRet GetRectCenterXPrototype(JassRect whichRect);
        private GetRectCenterXPrototype _GetRectCenterX;
        public float GetRectCenterX(JassRect whichRect)
        {
            return this._GetRectCenterX(whichRect);
        }
        
        //native GetRectCenterY takes rect whichRect returns real
        public delegate JassRealRet GetRectCenterYPrototype(JassRect whichRect);
        private GetRectCenterYPrototype _GetRectCenterY;
        public float GetRectCenterY(JassRect whichRect)
        {
            return this._GetRectCenterY(whichRect);
        }
        
        //native GetRectMinX takes rect whichRect returns real
        public delegate JassRealRet GetRectMinXPrototype(JassRect whichRect);
        private GetRectMinXPrototype _GetRectMinX;
        public float GetRectMinX(JassRect whichRect)
        {
            return this._GetRectMinX(whichRect);
        }
        
        //native GetRectMinY takes rect whichRect returns real
        public delegate JassRealRet GetRectMinYPrototype(JassRect whichRect);
        private GetRectMinYPrototype _GetRectMinY;
        public float GetRectMinY(JassRect whichRect)
        {
            return this._GetRectMinY(whichRect);
        }
        
        //native GetRectMaxX takes rect whichRect returns real
        public delegate JassRealRet GetRectMaxXPrototype(JassRect whichRect);
        private GetRectMaxXPrototype _GetRectMaxX;
        public float GetRectMaxX(JassRect whichRect)
        {
            return this._GetRectMaxX(whichRect);
        }
        
        //native GetRectMaxY takes rect whichRect returns real
        public delegate JassRealRet GetRectMaxYPrototype(JassRect whichRect);
        private GetRectMaxYPrototype _GetRectMaxY;
        public float GetRectMaxY(JassRect whichRect)
        {
            return this._GetRectMaxY(whichRect);
        }
        
        //native CreateRegion takes nothing returns region
        public delegate JassRegion CreateRegionPrototype();
        private CreateRegionPrototype _CreateRegion;
        public JassRegion CreateRegion()
        {
            return this._CreateRegion();
        }
        
        //native RemoveRegion takes region whichRegion returns nothing
        public delegate void RemoveRegionPrototype(JassRegion whichRegion);
        private RemoveRegionPrototype _RemoveRegion;
        public void RemoveRegion(JassRegion whichRegion)
        {
            this._RemoveRegion(whichRegion);
        }
        
        //native RegionAddRect takes region whichRegion, rect r returns nothing
        public delegate void RegionAddRectPrototype(JassRegion whichRegion, JassRect r);
        private RegionAddRectPrototype _RegionAddRect;
        public void RegionAddRect(JassRegion whichRegion, JassRect r)
        {
            this._RegionAddRect(whichRegion, r);
        }
        
        //native RegionClearRect takes region whichRegion, rect r returns nothing
        public delegate void RegionClearRectPrototype(JassRegion whichRegion, JassRect r);
        private RegionClearRectPrototype _RegionClearRect;
        public void RegionClearRect(JassRegion whichRegion, JassRect r)
        {
            this._RegionClearRect(whichRegion, r);
        }
        
        //native RegionAddCell takes region whichRegion, real x, real y returns nothing
        public delegate void RegionAddCellPrototype(JassRegion whichRegion, JassRealArg x, JassRealArg y);
        private RegionAddCellPrototype _RegionAddCell;
        public void RegionAddCell(JassRegion whichRegion, float x, float y)
        {
            this._RegionAddCell(whichRegion, x, y);
        }
        
        //native RegionAddCellAtLoc takes region whichRegion, location whichLocation returns nothing
        public delegate void RegionAddCellAtLocPrototype(JassRegion whichRegion, JassLocation whichLocation);
        private RegionAddCellAtLocPrototype _RegionAddCellAtLoc;
        public void RegionAddCellAtLoc(JassRegion whichRegion, JassLocation whichLocation)
        {
            this._RegionAddCellAtLoc(whichRegion, whichLocation);
        }
        
        //native RegionClearCell takes region whichRegion, real x, real y returns nothing
        public delegate void RegionClearCellPrototype(JassRegion whichRegion, JassRealArg x, JassRealArg y);
        private RegionClearCellPrototype _RegionClearCell;
        public void RegionClearCell(JassRegion whichRegion, float x, float y)
        {
            this._RegionClearCell(whichRegion, x, y);
        }
        
        //native RegionClearCellAtLoc takes region whichRegion, location whichLocation returns nothing
        public delegate void RegionClearCellAtLocPrototype(JassRegion whichRegion, JassLocation whichLocation);
        private RegionClearCellAtLocPrototype _RegionClearCellAtLoc;
        public void RegionClearCellAtLoc(JassRegion whichRegion, JassLocation whichLocation)
        {
            this._RegionClearCellAtLoc(whichRegion, whichLocation);
        }
        
        //native Location takes real x, real y returns location
        public delegate JassLocation LocationPrototype(JassRealArg x, JassRealArg y);
        private LocationPrototype _Location;
        public JassLocation Location(float x, float y)
        {
            return this._Location(x, y);
        }
        
        //native RemoveLocation takes location whichLocation returns nothing
        public delegate void RemoveLocationPrototype(JassLocation whichLocation);
        private RemoveLocationPrototype _RemoveLocation;
        public void RemoveLocation(JassLocation whichLocation)
        {
            this._RemoveLocation(whichLocation);
        }
        
        //native MoveLocation takes location whichLocation, real newX, real newY returns nothing
        public delegate void MoveLocationPrototype(JassLocation whichLocation, JassRealArg newX, JassRealArg newY);
        private MoveLocationPrototype _MoveLocation;
        public void MoveLocation(JassLocation whichLocation, float newX, float newY)
        {
            this._MoveLocation(whichLocation, newX, newY);
        }
        
        //native GetLocationX takes location whichLocation returns real
        public delegate JassRealRet GetLocationXPrototype(JassLocation whichLocation);
        private GetLocationXPrototype _GetLocationX;
        public float GetLocationX(JassLocation whichLocation)
        {
            return this._GetLocationX(whichLocation);
        }
        
        //native GetLocationY takes location whichLocation returns real
        public delegate JassRealRet GetLocationYPrototype(JassLocation whichLocation);
        private GetLocationYPrototype _GetLocationY;
        public float GetLocationY(JassLocation whichLocation)
        {
            return this._GetLocationY(whichLocation);
        }
        
        //native GetLocationZ takes location whichLocation returns real
        public delegate JassRealRet GetLocationZPrototype(JassLocation whichLocation);
        private GetLocationZPrototype _GetLocationZ;
        public float GetLocationZ(JassLocation whichLocation)
        {
            return this._GetLocationZ(whichLocation);
        }
        
        //native IsUnitInRegion takes region whichRegion, unit whichUnit returns boolean
        public delegate JassBoolean IsUnitInRegionPrototype(JassRegion whichRegion, JassUnit whichUnit);
        private IsUnitInRegionPrototype _IsUnitInRegion;
        public bool IsUnitInRegion(JassRegion whichRegion, JassUnit whichUnit)
        {
            return this._IsUnitInRegion(whichRegion, whichUnit);
        }
        
        //native IsPointInRegion takes region whichRegion, real x, real y returns boolean
        public delegate JassBoolean IsPointInRegionPrototype(JassRegion whichRegion, JassRealArg x, JassRealArg y);
        private IsPointInRegionPrototype _IsPointInRegion;
        public bool IsPointInRegion(JassRegion whichRegion, float x, float y)
        {
            return this._IsPointInRegion(whichRegion, x, y);
        }
        
        //native IsLocationInRegion takes region whichRegion, location whichLocation returns boolean
        public delegate JassBoolean IsLocationInRegionPrototype(JassRegion whichRegion, JassLocation whichLocation);
        private IsLocationInRegionPrototype _IsLocationInRegion;
        public bool IsLocationInRegion(JassRegion whichRegion, JassLocation whichLocation)
        {
            return this._IsLocationInRegion(whichRegion, whichLocation);
        }
        
        //native GetWorldBounds takes nothing returns rect
        public delegate JassRect GetWorldBoundsPrototype();
        private GetWorldBoundsPrototype _GetWorldBounds;
        public JassRect GetWorldBounds()
        {
            return this._GetWorldBounds();
        }
        
        //native CreateTrigger takes nothing returns trigger
        public delegate JassTrigger CreateTriggerPrototype();
        private CreateTriggerPrototype _CreateTrigger;
        public JassTrigger CreateTrigger()
        {
            return this._CreateTrigger();
        }
        
        //native DestroyTrigger takes trigger whichTrigger returns nothing
        public delegate void DestroyTriggerPrototype(JassTrigger whichTrigger);
        private DestroyTriggerPrototype _DestroyTrigger;
        public void DestroyTrigger(JassTrigger whichTrigger)
        {
            this._DestroyTrigger(whichTrigger);
        }
        
        //native ResetTrigger takes trigger whichTrigger returns nothing
        public delegate void ResetTriggerPrototype(JassTrigger whichTrigger);
        private ResetTriggerPrototype _ResetTrigger;
        public void ResetTrigger(JassTrigger whichTrigger)
        {
            this._ResetTrigger(whichTrigger);
        }
        
        //native EnableTrigger takes trigger whichTrigger returns nothing
        public delegate void EnableTriggerPrototype(JassTrigger whichTrigger);
        private EnableTriggerPrototype _EnableTrigger;
        public void EnableTrigger(JassTrigger whichTrigger)
        {
            this._EnableTrigger(whichTrigger);
        }
        
        //native DisableTrigger takes trigger whichTrigger returns nothing
        public delegate void DisableTriggerPrototype(JassTrigger whichTrigger);
        private DisableTriggerPrototype _DisableTrigger;
        public void DisableTrigger(JassTrigger whichTrigger)
        {
            this._DisableTrigger(whichTrigger);
        }
        
        //native IsTriggerEnabled takes trigger whichTrigger returns boolean
        public delegate JassBoolean IsTriggerEnabledPrototype(JassTrigger whichTrigger);
        private IsTriggerEnabledPrototype _IsTriggerEnabled;
        public bool IsTriggerEnabled(JassTrigger whichTrigger)
        {
            return this._IsTriggerEnabled(whichTrigger);
        }
        
        //native TriggerWaitOnSleeps takes trigger whichTrigger, boolean flag returns nothing
        public delegate void TriggerWaitOnSleepsPrototype(JassTrigger whichTrigger, JassBoolean flag);
        private TriggerWaitOnSleepsPrototype _TriggerWaitOnSleeps;
        public void TriggerWaitOnSleeps(JassTrigger whichTrigger, bool flag)
        {
            this._TriggerWaitOnSleeps(whichTrigger, flag);
        }
        
        //native IsTriggerWaitOnSleeps takes trigger whichTrigger returns boolean
        public delegate JassBoolean IsTriggerWaitOnSleepsPrototype(JassTrigger whichTrigger);
        private IsTriggerWaitOnSleepsPrototype _IsTriggerWaitOnSleeps;
        public bool IsTriggerWaitOnSleeps(JassTrigger whichTrigger)
        {
            return this._IsTriggerWaitOnSleeps(whichTrigger);
        }
        
        //native GetFilterUnit takes nothing returns unit
        public delegate JassUnit GetFilterUnitPrototype();
        private GetFilterUnitPrototype _GetFilterUnit;
        public JassUnit GetFilterUnit()
        {
            return this._GetFilterUnit();
        }
        
        //native GetEnumUnit takes nothing returns unit
        public delegate JassUnit GetEnumUnitPrototype();
        private GetEnumUnitPrototype _GetEnumUnit;
        public JassUnit GetEnumUnit()
        {
            return this._GetEnumUnit();
        }
        
        //native GetFilterDestructable takes nothing returns destructable
        public delegate JassDestructable GetFilterDestructablePrototype();
        private GetFilterDestructablePrototype _GetFilterDestructable;
        public JassDestructable GetFilterDestructable()
        {
            return this._GetFilterDestructable();
        }
        
        //native GetEnumDestructable takes nothing returns destructable
        public delegate JassDestructable GetEnumDestructablePrototype();
        private GetEnumDestructablePrototype _GetEnumDestructable;
        public JassDestructable GetEnumDestructable()
        {
            return this._GetEnumDestructable();
        }
        
        //native GetFilterItem takes nothing returns item
        public delegate JassItem GetFilterItemPrototype();
        private GetFilterItemPrototype _GetFilterItem;
        public JassItem GetFilterItem()
        {
            return this._GetFilterItem();
        }
        
        //native GetEnumItem takes nothing returns item
        public delegate JassItem GetEnumItemPrototype();
        private GetEnumItemPrototype _GetEnumItem;
        public JassItem GetEnumItem()
        {
            return this._GetEnumItem();
        }
        
        //native GetFilterPlayer takes nothing returns player
        public delegate JassPlayer GetFilterPlayerPrototype();
        private GetFilterPlayerPrototype _GetFilterPlayer;
        public JassPlayer GetFilterPlayer()
        {
            return this._GetFilterPlayer();
        }
        
        //native GetEnumPlayer takes nothing returns player
        public delegate JassPlayer GetEnumPlayerPrototype();
        private GetEnumPlayerPrototype _GetEnumPlayer;
        public JassPlayer GetEnumPlayer()
        {
            return this._GetEnumPlayer();
        }
        
        //native GetTriggeringTrigger takes nothing returns trigger
        public delegate JassTrigger GetTriggeringTriggerPrototype();
        private GetTriggeringTriggerPrototype _GetTriggeringTrigger;
        public JassTrigger GetTriggeringTrigger()
        {
            return this._GetTriggeringTrigger();
        }
        
        //native GetTriggerEventId takes nothing returns eventid
        public delegate JassEventIndex GetTriggerEventIdPrototype();
        private GetTriggerEventIdPrototype _GetTriggerEventId;
        public JassEventIndex GetTriggerEventId()
        {
            return this._GetTriggerEventId();
        }
        
        //native GetTriggerEvalCount takes trigger whichTrigger returns integer
        public delegate JassInteger GetTriggerEvalCountPrototype(JassTrigger whichTrigger);
        private GetTriggerEvalCountPrototype _GetTriggerEvalCount;
        public JassInteger GetTriggerEvalCount(JassTrigger whichTrigger)
        {
            return this._GetTriggerEvalCount(whichTrigger);
        }
        
        //native GetTriggerExecCount takes trigger whichTrigger returns integer
        public delegate JassInteger GetTriggerExecCountPrototype(JassTrigger whichTrigger);
        private GetTriggerExecCountPrototype _GetTriggerExecCount;
        public JassInteger GetTriggerExecCount(JassTrigger whichTrigger)
        {
            return this._GetTriggerExecCount(whichTrigger);
        }
        
        //native ExecuteFunc takes string funcName returns nothing
        public delegate void ExecuteFuncPrototype(JassStringArg funcName);
        private ExecuteFuncPrototype _ExecuteFunc;
        public void ExecuteFunc(string funcName)
        {
            this._ExecuteFunc(funcName);
        }
        
        //native And takes boolexpr operandA, boolexpr operandB returns boolexpr
        public delegate JassBooleanExpression AndPrototype(JassBooleanExpression operandA, JassBooleanExpression operandB);
        private AndPrototype _And;
        public JassBooleanExpression And(JassBooleanExpression operandA, JassBooleanExpression operandB)
        {
            return this._And(operandA, operandB);
        }
        
        //native Or takes boolexpr operandA, boolexpr operandB returns boolexpr
        public delegate JassBooleanExpression OrPrototype(JassBooleanExpression operandA, JassBooleanExpression operandB);
        private OrPrototype _Or;
        public JassBooleanExpression Or(JassBooleanExpression operandA, JassBooleanExpression operandB)
        {
            return this._Or(operandA, operandB);
        }
        
        //native Not takes boolexpr operand returns boolexpr
        public delegate JassBooleanExpression NotPrototype(JassBooleanExpression operand);
        private NotPrototype _Not;
        public JassBooleanExpression Not(JassBooleanExpression operand)
        {
            return this._Not(operand);
        }
        
        //native Condition takes code func returns conditionfunc
        public delegate JassConditionFunction ConditionPrototype(JassCode func);
        private ConditionPrototype _Condition;
        public JassConditionFunction Condition(JassCode func)
        {
            return this._Condition(func);
        }
        
        //native DestroyCondition takes conditionfunc c returns nothing
        public delegate void DestroyConditionPrototype(JassConditionFunction c);
        private DestroyConditionPrototype _DestroyCondition;
        public void DestroyCondition(JassConditionFunction c)
        {
            this._DestroyCondition(c);
        }
        
        //native Filter takes code func returns filterfunc
        public delegate JassFilterFunction FilterPrototype(JassCode func);
        private FilterPrototype _Filter;
        public JassFilterFunction Filter(JassCode func)
        {
            return this._Filter(func);
        }
        
        //native DestroyFilter takes filterfunc f returns nothing
        public delegate void DestroyFilterPrototype(JassFilterFunction f);
        private DestroyFilterPrototype _DestroyFilter;
        public void DestroyFilter(JassFilterFunction f)
        {
            this._DestroyFilter(f);
        }
        
        //native DestroyBoolExpr takes boolexpr e returns nothing
        public delegate void DestroyBoolExprPrototype(JassBooleanExpression e);
        private DestroyBoolExprPrototype _DestroyBoolExpr;
        public void DestroyBoolExpr(JassBooleanExpression e)
        {
            this._DestroyBoolExpr(e);
        }
        
        //native TriggerRegisterVariableEvent takes trigger whichTrigger, string varName, limitop opcode, real limitval returns event
        public delegate JassEvent TriggerRegisterVariableEventPrototype(JassTrigger whichTrigger, JassStringArg varName, JassLimitOp opcode, JassRealArg limitval);
        private TriggerRegisterVariableEventPrototype _TriggerRegisterVariableEvent;
        public JassEvent TriggerRegisterVariableEvent(JassTrigger whichTrigger, string varName, JassLimitOp opcode, float limitval)
        {
            return this._TriggerRegisterVariableEvent(whichTrigger, varName, opcode, limitval);
        }
        
        //native TriggerRegisterTimerEvent takes trigger whichTrigger, real timeout, boolean periodic returns event
        public delegate JassEvent TriggerRegisterTimerEventPrototype(JassTrigger whichTrigger, JassRealArg timeout, JassBoolean periodic);
        private TriggerRegisterTimerEventPrototype _TriggerRegisterTimerEvent;
        public JassEvent TriggerRegisterTimerEvent(JassTrigger whichTrigger, float timeout, bool periodic)
        {
            return this._TriggerRegisterTimerEvent(whichTrigger, timeout, periodic);
        }
        
        //native TriggerRegisterTimerExpireEvent takes trigger whichTrigger, timer t returns event
        public delegate JassEvent TriggerRegisterTimerExpireEventPrototype(JassTrigger whichTrigger, JassTimer t);
        private TriggerRegisterTimerExpireEventPrototype _TriggerRegisterTimerExpireEvent;
        public JassEvent TriggerRegisterTimerExpireEvent(JassTrigger whichTrigger, JassTimer t)
        {
            return this._TriggerRegisterTimerExpireEvent(whichTrigger, t);
        }
        
        //native TriggerRegisterGameStateEvent takes trigger whichTrigger, gamestate whichState, limitop opcode, real limitval returns event
        public delegate JassEvent TriggerRegisterGameStateEventPrototype(JassTrigger whichTrigger, JassGameState whichState, JassLimitOp opcode, JassRealArg limitval);
        private TriggerRegisterGameStateEventPrototype _TriggerRegisterGameStateEvent;
        public JassEvent TriggerRegisterGameStateEvent(JassTrigger whichTrigger, JassGameState whichState, JassLimitOp opcode, float limitval)
        {
            return this._TriggerRegisterGameStateEvent(whichTrigger, whichState, opcode, limitval);
        }
        
        //native TriggerRegisterDialogEvent takes trigger whichTrigger, dialog whichDialog returns event
        public delegate JassEvent TriggerRegisterDialogEventPrototype(JassTrigger whichTrigger, JassDialog whichDialog);
        private TriggerRegisterDialogEventPrototype _TriggerRegisterDialogEvent;
        public JassEvent TriggerRegisterDialogEvent(JassTrigger whichTrigger, JassDialog whichDialog)
        {
            return this._TriggerRegisterDialogEvent(whichTrigger, whichDialog);
        }
        
        //native TriggerRegisterDialogButtonEvent takes trigger whichTrigger, button whichButton returns event
        public delegate JassEvent TriggerRegisterDialogButtonEventPrototype(JassTrigger whichTrigger, JassButton whichButton);
        private TriggerRegisterDialogButtonEventPrototype _TriggerRegisterDialogButtonEvent;
        public JassEvent TriggerRegisterDialogButtonEvent(JassTrigger whichTrigger, JassButton whichButton)
        {
            return this._TriggerRegisterDialogButtonEvent(whichTrigger, whichButton);
        }
        
        //native GetEventGameState takes nothing returns gamestate
        public delegate JassGameState GetEventGameStatePrototype();
        private GetEventGameStatePrototype _GetEventGameState;
        public JassGameState GetEventGameState()
        {
            return this._GetEventGameState();
        }
        
        //native TriggerRegisterGameEvent takes trigger whichTrigger, gameevent whichGameEvent returns event
        public delegate JassEvent TriggerRegisterGameEventPrototype(JassTrigger whichTrigger, JassGameEvent whichGameEvent);
        private TriggerRegisterGameEventPrototype _TriggerRegisterGameEvent;
        public JassEvent TriggerRegisterGameEvent(JassTrigger whichTrigger, JassGameEvent whichGameEvent)
        {
            return this._TriggerRegisterGameEvent(whichTrigger, whichGameEvent);
        }
        
        //native GetWinningPlayer takes nothing returns player
        public delegate JassPlayer GetWinningPlayerPrototype();
        private GetWinningPlayerPrototype _GetWinningPlayer;
        public JassPlayer GetWinningPlayer()
        {
            return this._GetWinningPlayer();
        }
        
        //native TriggerRegisterEnterRegion takes trigger whichTrigger, region whichRegion, boolexpr filter returns event
        public delegate JassEvent TriggerRegisterEnterRegionPrototype(JassTrigger whichTrigger, JassRegion whichRegion, JassBooleanExpression filter);
        private TriggerRegisterEnterRegionPrototype _TriggerRegisterEnterRegion;
        public JassEvent TriggerRegisterEnterRegion(JassTrigger whichTrigger, JassRegion whichRegion, JassBooleanExpression filter)
        {
            return this._TriggerRegisterEnterRegion(whichTrigger, whichRegion, filter);
        }
        
        //native GetTriggeringRegion takes nothing returns region
        public delegate JassRegion GetTriggeringRegionPrototype();
        private GetTriggeringRegionPrototype _GetTriggeringRegion;
        public JassRegion GetTriggeringRegion()
        {
            return this._GetTriggeringRegion();
        }
        
        //native GetEnteringUnit takes nothing returns unit
        public delegate JassUnit GetEnteringUnitPrototype();
        private GetEnteringUnitPrototype _GetEnteringUnit;
        public JassUnit GetEnteringUnit()
        {
            return this._GetEnteringUnit();
        }
        
        //native TriggerRegisterLeaveRegion takes trigger whichTrigger, region whichRegion, boolexpr filter returns event
        public delegate JassEvent TriggerRegisterLeaveRegionPrototype(JassTrigger whichTrigger, JassRegion whichRegion, JassBooleanExpression filter);
        private TriggerRegisterLeaveRegionPrototype _TriggerRegisterLeaveRegion;
        public JassEvent TriggerRegisterLeaveRegion(JassTrigger whichTrigger, JassRegion whichRegion, JassBooleanExpression filter)
        {
            return this._TriggerRegisterLeaveRegion(whichTrigger, whichRegion, filter);
        }
        
        //native GetLeavingUnit takes nothing returns unit
        public delegate JassUnit GetLeavingUnitPrototype();
        private GetLeavingUnitPrototype _GetLeavingUnit;
        public JassUnit GetLeavingUnit()
        {
            return this._GetLeavingUnit();
        }
        
        //native TriggerRegisterTrackableHitEvent takes trigger whichTrigger, trackable t returns event
        public delegate JassEvent TriggerRegisterTrackableHitEventPrototype(JassTrigger whichTrigger, JassTrackable t);
        private TriggerRegisterTrackableHitEventPrototype _TriggerRegisterTrackableHitEvent;
        public JassEvent TriggerRegisterTrackableHitEvent(JassTrigger whichTrigger, JassTrackable t)
        {
            return this._TriggerRegisterTrackableHitEvent(whichTrigger, t);
        }
        
        //native TriggerRegisterTrackableTrackEvent takes trigger whichTrigger, trackable t returns event
        public delegate JassEvent TriggerRegisterTrackableTrackEventPrototype(JassTrigger whichTrigger, JassTrackable t);
        private TriggerRegisterTrackableTrackEventPrototype _TriggerRegisterTrackableTrackEvent;
        public JassEvent TriggerRegisterTrackableTrackEvent(JassTrigger whichTrigger, JassTrackable t)
        {
            return this._TriggerRegisterTrackableTrackEvent(whichTrigger, t);
        }
        
        //native GetTriggeringTrackable takes nothing returns trackable
        public delegate JassTrackable GetTriggeringTrackablePrototype();
        private GetTriggeringTrackablePrototype _GetTriggeringTrackable;
        public JassTrackable GetTriggeringTrackable()
        {
            return this._GetTriggeringTrackable();
        }
        
        //native GetClickedButton takes nothing returns button
        public delegate JassButton GetClickedButtonPrototype();
        private GetClickedButtonPrototype _GetClickedButton;
        public JassButton GetClickedButton()
        {
            return this._GetClickedButton();
        }
        
        //native GetClickedDialog takes nothing returns dialog
        public delegate JassDialog GetClickedDialogPrototype();
        private GetClickedDialogPrototype _GetClickedDialog;
        public JassDialog GetClickedDialog()
        {
            return this._GetClickedDialog();
        }
        
        //native GetTournamentFinishSoonTimeRemaining takes nothing returns real
        public delegate JassRealRet GetTournamentFinishSoonTimeRemainingPrototype();
        private GetTournamentFinishSoonTimeRemainingPrototype _GetTournamentFinishSoonTimeRemaining;
        public float GetTournamentFinishSoonTimeRemaining()
        {
            return this._GetTournamentFinishSoonTimeRemaining();
        }
        
        //native GetTournamentFinishNowRule takes nothing returns integer
        public delegate JassInteger GetTournamentFinishNowRulePrototype();
        private GetTournamentFinishNowRulePrototype _GetTournamentFinishNowRule;
        public JassInteger GetTournamentFinishNowRule()
        {
            return this._GetTournamentFinishNowRule();
        }
        
        //native GetTournamentFinishNowPlayer takes nothing returns player
        public delegate JassPlayer GetTournamentFinishNowPlayerPrototype();
        private GetTournamentFinishNowPlayerPrototype _GetTournamentFinishNowPlayer;
        public JassPlayer GetTournamentFinishNowPlayer()
        {
            return this._GetTournamentFinishNowPlayer();
        }
        
        //native GetTournamentScore takes player whichPlayer returns integer
        public delegate JassInteger GetTournamentScorePrototype(JassPlayer whichPlayer);
        private GetTournamentScorePrototype _GetTournamentScore;
        public JassInteger GetTournamentScore(JassPlayer whichPlayer)
        {
            return this._GetTournamentScore(whichPlayer);
        }
        
        //native GetSaveBasicFilename takes nothing returns string
        public delegate JassStringRet GetSaveBasicFilenamePrototype();
        private GetSaveBasicFilenamePrototype _GetSaveBasicFilename;
        public string GetSaveBasicFilename()
        {
            return this._GetSaveBasicFilename();
        }
        
        //native TriggerRegisterPlayerEvent takes trigger whichTrigger, player whichPlayer, playerevent whichPlayerEvent returns event
        public delegate JassEvent TriggerRegisterPlayerEventPrototype(JassTrigger whichTrigger, JassPlayer whichPlayer, JassPlayerEvent whichPlayerEvent);
        private TriggerRegisterPlayerEventPrototype _TriggerRegisterPlayerEvent;
        public JassEvent TriggerRegisterPlayerEvent(JassTrigger whichTrigger, JassPlayer whichPlayer, JassPlayerEvent whichPlayerEvent)
        {
            return this._TriggerRegisterPlayerEvent(whichTrigger, whichPlayer, whichPlayerEvent);
        }
        
        //native GetTriggerPlayer takes nothing returns player
        public delegate JassPlayer GetTriggerPlayerPrototype();
        private GetTriggerPlayerPrototype _GetTriggerPlayer;
        public JassPlayer GetTriggerPlayer()
        {
            return this._GetTriggerPlayer();
        }
        
        //native TriggerRegisterPlayerUnitEvent takes trigger whichTrigger, player whichPlayer, playerunitevent whichPlayerUnitEvent, boolexpr filter returns event
        public delegate JassEvent TriggerRegisterPlayerUnitEventPrototype(JassTrigger whichTrigger, JassPlayer whichPlayer, JassPlayerUnitEvent whichPlayerUnitEvent, JassBooleanExpression filter);
        private TriggerRegisterPlayerUnitEventPrototype _TriggerRegisterPlayerUnitEvent;
        public JassEvent TriggerRegisterPlayerUnitEvent(JassTrigger whichTrigger, JassPlayer whichPlayer, JassPlayerUnitEvent whichPlayerUnitEvent, JassBooleanExpression filter)
        {
            return this._TriggerRegisterPlayerUnitEvent(whichTrigger, whichPlayer, whichPlayerUnitEvent, filter);
        }
        
        //native GetLevelingUnit takes nothing returns unit
        public delegate JassUnit GetLevelingUnitPrototype();
        private GetLevelingUnitPrototype _GetLevelingUnit;
        public JassUnit GetLevelingUnit()
        {
            return this._GetLevelingUnit();
        }
        
        //native GetLearningUnit takes nothing returns unit
        public delegate JassUnit GetLearningUnitPrototype();
        private GetLearningUnitPrototype _GetLearningUnit;
        public JassUnit GetLearningUnit()
        {
            return this._GetLearningUnit();
        }
        
        //native GetLearnedSkill takes nothing returns integer
        public delegate JassInteger GetLearnedSkillPrototype();
        private GetLearnedSkillPrototype _GetLearnedSkill;
        public JassInteger GetLearnedSkill()
        {
            return this._GetLearnedSkill();
        }
        
        //native GetLearnedSkillLevel takes nothing returns integer
        public delegate JassInteger GetLearnedSkillLevelPrototype();
        private GetLearnedSkillLevelPrototype _GetLearnedSkillLevel;
        public JassInteger GetLearnedSkillLevel()
        {
            return this._GetLearnedSkillLevel();
        }
        
        //native GetRevivableUnit takes nothing returns unit
        public delegate JassUnit GetRevivableUnitPrototype();
        private GetRevivableUnitPrototype _GetRevivableUnit;
        public JassUnit GetRevivableUnit()
        {
            return this._GetRevivableUnit();
        }
        
        //native GetRevivingUnit takes nothing returns unit
        public delegate JassUnit GetRevivingUnitPrototype();
        private GetRevivingUnitPrototype _GetRevivingUnit;
        public JassUnit GetRevivingUnit()
        {
            return this._GetRevivingUnit();
        }
        
        //native GetAttacker takes nothing returns unit
        public delegate JassUnit GetAttackerPrototype();
        private GetAttackerPrototype _GetAttacker;
        public JassUnit GetAttacker()
        {
            return this._GetAttacker();
        }
        
        //native GetRescuer takes nothing returns unit
        public delegate JassUnit GetRescuerPrototype();
        private GetRescuerPrototype _GetRescuer;
        public JassUnit GetRescuer()
        {
            return this._GetRescuer();
        }
        
        //native GetDyingUnit takes nothing returns unit
        public delegate JassUnit GetDyingUnitPrototype();
        private GetDyingUnitPrototype _GetDyingUnit;
        public JassUnit GetDyingUnit()
        {
            return this._GetDyingUnit();
        }
        
        //native GetKillingUnit takes nothing returns unit
        public delegate JassUnit GetKillingUnitPrototype();
        private GetKillingUnitPrototype _GetKillingUnit;
        public JassUnit GetKillingUnit()
        {
            return this._GetKillingUnit();
        }
        
        //native GetDecayingUnit takes nothing returns unit
        public delegate JassUnit GetDecayingUnitPrototype();
        private GetDecayingUnitPrototype _GetDecayingUnit;
        public JassUnit GetDecayingUnit()
        {
            return this._GetDecayingUnit();
        }
        
        //native GetConstructingStructure takes nothing returns unit
        public delegate JassUnit GetConstructingStructurePrototype();
        private GetConstructingStructurePrototype _GetConstructingStructure;
        public JassUnit GetConstructingStructure()
        {
            return this._GetConstructingStructure();
        }
        
        //native GetCancelledStructure takes nothing returns unit
        public delegate JassUnit GetCancelledStructurePrototype();
        private GetCancelledStructurePrototype _GetCancelledStructure;
        public JassUnit GetCancelledStructure()
        {
            return this._GetCancelledStructure();
        }
        
        //native GetConstructedStructure takes nothing returns unit
        public delegate JassUnit GetConstructedStructurePrototype();
        private GetConstructedStructurePrototype _GetConstructedStructure;
        public JassUnit GetConstructedStructure()
        {
            return this._GetConstructedStructure();
        }
        
        //native GetResearchingUnit takes nothing returns unit
        public delegate JassUnit GetResearchingUnitPrototype();
        private GetResearchingUnitPrototype _GetResearchingUnit;
        public JassUnit GetResearchingUnit()
        {
            return this._GetResearchingUnit();
        }
        
        //native GetResearched takes nothing returns integer
        public delegate JassInteger GetResearchedPrototype();
        private GetResearchedPrototype _GetResearched;
        public JassInteger GetResearched()
        {
            return this._GetResearched();
        }
        
        //native GetTrainedUnitType takes nothing returns integer
        public delegate JassInteger GetTrainedUnitTypePrototype();
        private GetTrainedUnitTypePrototype _GetTrainedUnitType;
        public JassInteger GetTrainedUnitType()
        {
            return this._GetTrainedUnitType();
        }
        
        //native GetTrainedUnit takes nothing returns unit
        public delegate JassUnit GetTrainedUnitPrototype();
        private GetTrainedUnitPrototype _GetTrainedUnit;
        public JassUnit GetTrainedUnit()
        {
            return this._GetTrainedUnit();
        }
        
        //native GetDetectedUnit takes nothing returns unit
        public delegate JassUnit GetDetectedUnitPrototype();
        private GetDetectedUnitPrototype _GetDetectedUnit;
        public JassUnit GetDetectedUnit()
        {
            return this._GetDetectedUnit();
        }
        
        //native GetSummoningUnit takes nothing returns unit
        public delegate JassUnit GetSummoningUnitPrototype();
        private GetSummoningUnitPrototype _GetSummoningUnit;
        public JassUnit GetSummoningUnit()
        {
            return this._GetSummoningUnit();
        }
        
        //native GetSummonedUnit takes nothing returns unit
        public delegate JassUnit GetSummonedUnitPrototype();
        private GetSummonedUnitPrototype _GetSummonedUnit;
        public JassUnit GetSummonedUnit()
        {
            return this._GetSummonedUnit();
        }
        
        //native GetTransportUnit takes nothing returns unit
        public delegate JassUnit GetTransportUnitPrototype();
        private GetTransportUnitPrototype _GetTransportUnit;
        public JassUnit GetTransportUnit()
        {
            return this._GetTransportUnit();
        }
        
        //native GetLoadedUnit takes nothing returns unit
        public delegate JassUnit GetLoadedUnitPrototype();
        private GetLoadedUnitPrototype _GetLoadedUnit;
        public JassUnit GetLoadedUnit()
        {
            return this._GetLoadedUnit();
        }
        
        //native GetSellingUnit takes nothing returns unit
        public delegate JassUnit GetSellingUnitPrototype();
        private GetSellingUnitPrototype _GetSellingUnit;
        public JassUnit GetSellingUnit()
        {
            return this._GetSellingUnit();
        }
        
        //native GetSoldUnit takes nothing returns unit
        public delegate JassUnit GetSoldUnitPrototype();
        private GetSoldUnitPrototype _GetSoldUnit;
        public JassUnit GetSoldUnit()
        {
            return this._GetSoldUnit();
        }
        
        //native GetBuyingUnit takes nothing returns unit
        public delegate JassUnit GetBuyingUnitPrototype();
        private GetBuyingUnitPrototype _GetBuyingUnit;
        public JassUnit GetBuyingUnit()
        {
            return this._GetBuyingUnit();
        }
        
        //native GetSoldItem takes nothing returns item
        public delegate JassItem GetSoldItemPrototype();
        private GetSoldItemPrototype _GetSoldItem;
        public JassItem GetSoldItem()
        {
            return this._GetSoldItem();
        }
        
        //native GetChangingUnit takes nothing returns unit
        public delegate JassUnit GetChangingUnitPrototype();
        private GetChangingUnitPrototype _GetChangingUnit;
        public JassUnit GetChangingUnit()
        {
            return this._GetChangingUnit();
        }
        
        //native GetChangingUnitPrevOwner takes nothing returns player
        public delegate JassPlayer GetChangingUnitPrevOwnerPrototype();
        private GetChangingUnitPrevOwnerPrototype _GetChangingUnitPrevOwner;
        public JassPlayer GetChangingUnitPrevOwner()
        {
            return this._GetChangingUnitPrevOwner();
        }
        
        //native GetManipulatingUnit takes nothing returns unit
        public delegate JassUnit GetManipulatingUnitPrototype();
        private GetManipulatingUnitPrototype _GetManipulatingUnit;
        public JassUnit GetManipulatingUnit()
        {
            return this._GetManipulatingUnit();
        }
        
        //native GetManipulatedItem takes nothing returns item
        public delegate JassItem GetManipulatedItemPrototype();
        private GetManipulatedItemPrototype _GetManipulatedItem;
        public JassItem GetManipulatedItem()
        {
            return this._GetManipulatedItem();
        }
        
        //native GetOrderedUnit takes nothing returns unit
        public delegate JassUnit GetOrderedUnitPrototype();
        private GetOrderedUnitPrototype _GetOrderedUnit;
        public JassUnit GetOrderedUnit()
        {
            return this._GetOrderedUnit();
        }
        
        //native GetIssuedOrderId takes nothing returns integer
        public delegate JassOrder GetIssuedOrderIdPrototype();
        private GetIssuedOrderIdPrototype _GetIssuedOrderId;
        public JassOrder GetIssuedOrderId()
        {
            return this._GetIssuedOrderId();
        }
        
        //native GetOrderPointX takes nothing returns real
        public delegate JassRealRet GetOrderPointXPrototype();
        private GetOrderPointXPrototype _GetOrderPointX;
        public float GetOrderPointX()
        {
            return this._GetOrderPointX();
        }
        
        //native GetOrderPointY takes nothing returns real
        public delegate JassRealRet GetOrderPointYPrototype();
        private GetOrderPointYPrototype _GetOrderPointY;
        public float GetOrderPointY()
        {
            return this._GetOrderPointY();
        }
        
        //native GetOrderPointLoc takes nothing returns location
        public delegate JassLocation GetOrderPointLocPrototype();
        private GetOrderPointLocPrototype _GetOrderPointLoc;
        public JassLocation GetOrderPointLoc()
        {
            return this._GetOrderPointLoc();
        }
        
        //native GetOrderTarget takes nothing returns widget
        public delegate JassWidget GetOrderTargetPrototype();
        private GetOrderTargetPrototype _GetOrderTarget;
        public JassWidget GetOrderTarget()
        {
            return this._GetOrderTarget();
        }
        
        //native GetOrderTargetDestructable takes nothing returns destructable
        public delegate JassDestructable GetOrderTargetDestructablePrototype();
        private GetOrderTargetDestructablePrototype _GetOrderTargetDestructable;
        public JassDestructable GetOrderTargetDestructable()
        {
            return this._GetOrderTargetDestructable();
        }
        
        //native GetOrderTargetItem takes nothing returns item
        public delegate JassItem GetOrderTargetItemPrototype();
        private GetOrderTargetItemPrototype _GetOrderTargetItem;
        public JassItem GetOrderTargetItem()
        {
            return this._GetOrderTargetItem();
        }
        
        //native GetOrderTargetUnit takes nothing returns unit
        public delegate JassUnit GetOrderTargetUnitPrototype();
        private GetOrderTargetUnitPrototype _GetOrderTargetUnit;
        public JassUnit GetOrderTargetUnit()
        {
            return this._GetOrderTargetUnit();
        }
        
        //native GetSpellAbilityUnit takes nothing returns unit
        public delegate JassUnit GetSpellAbilityUnitPrototype();
        private GetSpellAbilityUnitPrototype _GetSpellAbilityUnit;
        public JassUnit GetSpellAbilityUnit()
        {
            return this._GetSpellAbilityUnit();
        }
        
        //native GetSpellAbilityId takes nothing returns integer
        public delegate JassObjectId GetSpellAbilityIdPrototype();
        private GetSpellAbilityIdPrototype _GetSpellAbilityId;
        public JassObjectId GetSpellAbilityId()
        {
            return this._GetSpellAbilityId();
        }
        
        //native GetSpellAbility takes nothing returns ability
        public delegate JassAbility GetSpellAbilityPrototype();
        private GetSpellAbilityPrototype _GetSpellAbility;
        public JassAbility GetSpellAbility()
        {
            return this._GetSpellAbility();
        }
        
        //native GetSpellTargetLoc takes nothing returns location
        public delegate JassLocation GetSpellTargetLocPrototype();
        private GetSpellTargetLocPrototype _GetSpellTargetLoc;
        public JassLocation GetSpellTargetLoc()
        {
            return this._GetSpellTargetLoc();
        }
        
        //native GetSpellTargetX takes nothing returns real
        public delegate JassRealRet GetSpellTargetXPrototype();
        private GetSpellTargetXPrototype _GetSpellTargetX;
        public float GetSpellTargetX()
        {
            return this._GetSpellTargetX();
        }
        
        //native GetSpellTargetY takes nothing returns real
        public delegate JassRealRet GetSpellTargetYPrototype();
        private GetSpellTargetYPrototype _GetSpellTargetY;
        public float GetSpellTargetY()
        {
            return this._GetSpellTargetY();
        }
        
        //native GetSpellTargetDestructable takes nothing returns destructable
        public delegate JassDestructable GetSpellTargetDestructablePrototype();
        private GetSpellTargetDestructablePrototype _GetSpellTargetDestructable;
        public JassDestructable GetSpellTargetDestructable()
        {
            return this._GetSpellTargetDestructable();
        }
        
        //native GetSpellTargetItem takes nothing returns item
        public delegate JassItem GetSpellTargetItemPrototype();
        private GetSpellTargetItemPrototype _GetSpellTargetItem;
        public JassItem GetSpellTargetItem()
        {
            return this._GetSpellTargetItem();
        }
        
        //native GetSpellTargetUnit takes nothing returns unit
        public delegate JassUnit GetSpellTargetUnitPrototype();
        private GetSpellTargetUnitPrototype _GetSpellTargetUnit;
        public JassUnit GetSpellTargetUnit()
        {
            return this._GetSpellTargetUnit();
        }
        
        //native TriggerRegisterPlayerAllianceChange takes trigger whichTrigger, player whichPlayer, alliancetype whichAlliance returns event
        public delegate JassEvent TriggerRegisterPlayerAllianceChangePrototype(JassTrigger whichTrigger, JassPlayer whichPlayer, JassAllianceType whichAlliance);
        private TriggerRegisterPlayerAllianceChangePrototype _TriggerRegisterPlayerAllianceChange;
        public JassEvent TriggerRegisterPlayerAllianceChange(JassTrigger whichTrigger, JassPlayer whichPlayer, JassAllianceType whichAlliance)
        {
            return this._TriggerRegisterPlayerAllianceChange(whichTrigger, whichPlayer, whichAlliance);
        }
        
        //native TriggerRegisterPlayerStateEvent takes trigger whichTrigger, player whichPlayer, playerstate whichState, limitop opcode, real limitval returns event
        public delegate JassEvent TriggerRegisterPlayerStateEventPrototype(JassTrigger whichTrigger, JassPlayer whichPlayer, JassPlayerState whichState, JassLimitOp opcode, JassRealArg limitval);
        private TriggerRegisterPlayerStateEventPrototype _TriggerRegisterPlayerStateEvent;
        public JassEvent TriggerRegisterPlayerStateEvent(JassTrigger whichTrigger, JassPlayer whichPlayer, JassPlayerState whichState, JassLimitOp opcode, float limitval)
        {
            return this._TriggerRegisterPlayerStateEvent(whichTrigger, whichPlayer, whichState, opcode, limitval);
        }
        
        //native GetEventPlayerState takes nothing returns playerstate
        public delegate JassPlayerState GetEventPlayerStatePrototype();
        private GetEventPlayerStatePrototype _GetEventPlayerState;
        public JassPlayerState GetEventPlayerState()
        {
            return this._GetEventPlayerState();
        }
        
        //native TriggerRegisterPlayerChatEvent takes trigger whichTrigger, player whichPlayer, string chatMessageToDetect, boolean exactMatchOnly returns event
        public delegate JassEvent TriggerRegisterPlayerChatEventPrototype(JassTrigger whichTrigger, JassPlayer whichPlayer, JassStringArg chatMessageToDetect, JassBoolean exactMatchOnly);
        private TriggerRegisterPlayerChatEventPrototype _TriggerRegisterPlayerChatEvent;
        public JassEvent TriggerRegisterPlayerChatEvent(JassTrigger whichTrigger, JassPlayer whichPlayer, string chatMessageToDetect, bool exactMatchOnly)
        {
            return this._TriggerRegisterPlayerChatEvent(whichTrigger, whichPlayer, chatMessageToDetect, exactMatchOnly);
        }
        
        //native GetEventPlayerChatString takes nothing returns string
        public delegate JassStringRet GetEventPlayerChatStringPrototype();
        private GetEventPlayerChatStringPrototype _GetEventPlayerChatString;
        public string GetEventPlayerChatString()
        {
            return this._GetEventPlayerChatString();
        }
        
        //native GetEventPlayerChatStringMatched takes nothing returns string
        public delegate JassStringRet GetEventPlayerChatStringMatchedPrototype();
        private GetEventPlayerChatStringMatchedPrototype _GetEventPlayerChatStringMatched;
        public string GetEventPlayerChatStringMatched()
        {
            return this._GetEventPlayerChatStringMatched();
        }
        
        //native TriggerRegisterDeathEvent takes trigger whichTrigger, widget whichWidget returns event
        public delegate JassEvent TriggerRegisterDeathEventPrototype(JassTrigger whichTrigger, JassWidget whichWidget);
        private TriggerRegisterDeathEventPrototype _TriggerRegisterDeathEvent;
        public JassEvent TriggerRegisterDeathEvent(JassTrigger whichTrigger, JassWidget whichWidget)
        {
            return this._TriggerRegisterDeathEvent(whichTrigger, whichWidget);
        }
        
        //native GetTriggerUnit takes nothing returns unit
        public delegate JassUnit GetTriggerUnitPrototype();
        private GetTriggerUnitPrototype _GetTriggerUnit;
        public JassUnit GetTriggerUnit()
        {
            return this._GetTriggerUnit();
        }
        
        //native TriggerRegisterUnitStateEvent takes trigger whichTrigger, unit whichUnit, unitstate whichState, limitop opcode, real limitval returns event
        public delegate JassEvent TriggerRegisterUnitStateEventPrototype(JassTrigger whichTrigger, JassUnit whichUnit, JassUnitState whichState, JassLimitOp opcode, JassRealArg limitval);
        private TriggerRegisterUnitStateEventPrototype _TriggerRegisterUnitStateEvent;
        public JassEvent TriggerRegisterUnitStateEvent(JassTrigger whichTrigger, JassUnit whichUnit, JassUnitState whichState, JassLimitOp opcode, float limitval)
        {
            return this._TriggerRegisterUnitStateEvent(whichTrigger, whichUnit, whichState, opcode, limitval);
        }
        
        //native GetEventUnitState takes nothing returns unitstate
        public delegate JassUnitState GetEventUnitStatePrototype();
        private GetEventUnitStatePrototype _GetEventUnitState;
        public JassUnitState GetEventUnitState()
        {
            return this._GetEventUnitState();
        }
        
        //native TriggerRegisterUnitEvent takes trigger whichTrigger, unit whichUnit, unitevent whichEvent returns event
        public delegate JassEvent TriggerRegisterUnitEventPrototype(JassTrigger whichTrigger, JassUnit whichUnit, JassUnitEvent whichEvent);
        private TriggerRegisterUnitEventPrototype _TriggerRegisterUnitEvent;
        public JassEvent TriggerRegisterUnitEvent(JassTrigger whichTrigger, JassUnit whichUnit, JassUnitEvent whichEvent)
        {
            return this._TriggerRegisterUnitEvent(whichTrigger, whichUnit, whichEvent);
        }
        
        //native GetEventDamage takes nothing returns real
        public delegate JassRealRet GetEventDamagePrototype();
        private GetEventDamagePrototype _GetEventDamage;
        public float GetEventDamage()
        {
            return this._GetEventDamage();
        }
        
        //native GetEventDamageSource takes nothing returns unit
        public delegate JassUnit GetEventDamageSourcePrototype();
        private GetEventDamageSourcePrototype _GetEventDamageSource;
        public JassUnit GetEventDamageSource()
        {
            return this._GetEventDamageSource();
        }
        
        //native GetEventDetectingPlayer takes nothing returns player
        public delegate JassPlayer GetEventDetectingPlayerPrototype();
        private GetEventDetectingPlayerPrototype _GetEventDetectingPlayer;
        public JassPlayer GetEventDetectingPlayer()
        {
            return this._GetEventDetectingPlayer();
        }
        
        //native TriggerRegisterFilterUnitEvent takes trigger whichTrigger, unit whichUnit, unitevent whichEvent, boolexpr filter returns event
        public delegate JassEvent TriggerRegisterFilterUnitEventPrototype(JassTrigger whichTrigger, JassUnit whichUnit, JassUnitEvent whichEvent, JassBooleanExpression filter);
        private TriggerRegisterFilterUnitEventPrototype _TriggerRegisterFilterUnitEvent;
        public JassEvent TriggerRegisterFilterUnitEvent(JassTrigger whichTrigger, JassUnit whichUnit, JassUnitEvent whichEvent, JassBooleanExpression filter)
        {
            return this._TriggerRegisterFilterUnitEvent(whichTrigger, whichUnit, whichEvent, filter);
        }
        
        //native GetEventTargetUnit takes nothing returns unit
        public delegate JassUnit GetEventTargetUnitPrototype();
        private GetEventTargetUnitPrototype _GetEventTargetUnit;
        public JassUnit GetEventTargetUnit()
        {
            return this._GetEventTargetUnit();
        }
        
        //native TriggerRegisterUnitInRange takes trigger whichTrigger, unit whichUnit, real range, boolexpr filter returns event
        public delegate JassEvent TriggerRegisterUnitInRangePrototype(JassTrigger whichTrigger, JassUnit whichUnit, JassRealArg range, JassBooleanExpression filter);
        private TriggerRegisterUnitInRangePrototype _TriggerRegisterUnitInRange;
        public JassEvent TriggerRegisterUnitInRange(JassTrigger whichTrigger, JassUnit whichUnit, float range, JassBooleanExpression filter)
        {
            return this._TriggerRegisterUnitInRange(whichTrigger, whichUnit, range, filter);
        }
        
        //native TriggerAddCondition takes trigger whichTrigger, boolexpr condition returns triggercondition
        public delegate JassTriggerCondition TriggerAddConditionPrototype(JassTrigger whichTrigger, JassBooleanExpression condition);
        private TriggerAddConditionPrototype _TriggerAddCondition;
        public JassTriggerCondition TriggerAddCondition(JassTrigger whichTrigger, JassBooleanExpression condition)
        {
            return this._TriggerAddCondition(whichTrigger, condition);
        }
        
        //native TriggerRemoveCondition takes trigger whichTrigger, triggercondition whichCondition returns nothing
        public delegate void TriggerRemoveConditionPrototype(JassTrigger whichTrigger, JassTriggerCondition whichCondition);
        private TriggerRemoveConditionPrototype _TriggerRemoveCondition;
        public void TriggerRemoveCondition(JassTrigger whichTrigger, JassTriggerCondition whichCondition)
        {
            this._TriggerRemoveCondition(whichTrigger, whichCondition);
        }
        
        //native TriggerClearConditions takes trigger whichTrigger returns nothing
        public delegate void TriggerClearConditionsPrototype(JassTrigger whichTrigger);
        private TriggerClearConditionsPrototype _TriggerClearConditions;
        public void TriggerClearConditions(JassTrigger whichTrigger)
        {
            this._TriggerClearConditions(whichTrigger);
        }
        
        //native TriggerAddAction takes trigger whichTrigger, code actionFunc returns triggeraction
        public delegate JassTriggerAction TriggerAddActionPrototype(JassTrigger whichTrigger, JassCode actionFunc);
        private TriggerAddActionPrototype _TriggerAddAction;
        public JassTriggerAction TriggerAddAction(JassTrigger whichTrigger, JassCode actionFunc)
        {
            return this._TriggerAddAction(whichTrigger, actionFunc);
        }
        
        //native TriggerRemoveAction takes trigger whichTrigger, triggeraction whichAction returns nothing
        public delegate void TriggerRemoveActionPrototype(JassTrigger whichTrigger, JassTriggerAction whichAction);
        private TriggerRemoveActionPrototype _TriggerRemoveAction;
        public void TriggerRemoveAction(JassTrigger whichTrigger, JassTriggerAction whichAction)
        {
            this._TriggerRemoveAction(whichTrigger, whichAction);
        }
        
        //native TriggerClearActions takes trigger whichTrigger returns nothing
        public delegate void TriggerClearActionsPrototype(JassTrigger whichTrigger);
        private TriggerClearActionsPrototype _TriggerClearActions;
        public void TriggerClearActions(JassTrigger whichTrigger)
        {
            this._TriggerClearActions(whichTrigger);
        }
        
        //native TriggerSleepAction takes real timeout returns nothing
        public delegate void TriggerSleepActionPrototype(JassRealArg timeout);
        private TriggerSleepActionPrototype _TriggerSleepAction;
        public void TriggerSleepAction(float timeout)
        {
            this._TriggerSleepAction(timeout);
        }
        
        //native TriggerWaitForSound takes sound s, real offset returns nothing
        public delegate void TriggerWaitForSoundPrototype(JassSound s, JassRealArg offset);
        private TriggerWaitForSoundPrototype _TriggerWaitForSound;
        public void TriggerWaitForSound(JassSound s, float offset)
        {
            this._TriggerWaitForSound(s, offset);
        }
        
        //native TriggerEvaluate takes trigger whichTrigger returns boolean
        public delegate JassBoolean TriggerEvaluatePrototype(JassTrigger whichTrigger);
        private TriggerEvaluatePrototype _TriggerEvaluate;
        public bool TriggerEvaluate(JassTrigger whichTrigger)
        {
            return this._TriggerEvaluate(whichTrigger);
        }
        
        //native TriggerExecute takes trigger whichTrigger returns nothing
        public delegate void TriggerExecutePrototype(JassTrigger whichTrigger);
        private TriggerExecutePrototype _TriggerExecute;
        public void TriggerExecute(JassTrigger whichTrigger)
        {
            this._TriggerExecute(whichTrigger);
        }
        
        //native TriggerExecuteWait takes trigger whichTrigger returns nothing
        public delegate void TriggerExecuteWaitPrototype(JassTrigger whichTrigger);
        private TriggerExecuteWaitPrototype _TriggerExecuteWait;
        public void TriggerExecuteWait(JassTrigger whichTrigger)
        {
            this._TriggerExecuteWait(whichTrigger);
        }
        
        //native TriggerSyncStart takes nothing returns nothing
        public delegate void TriggerSyncStartPrototype();
        private TriggerSyncStartPrototype _TriggerSyncStart;
        public void TriggerSyncStart()
        {
            this._TriggerSyncStart();
        }
        
        //native TriggerSyncReady takes nothing returns nothing
        public delegate void TriggerSyncReadyPrototype();
        private TriggerSyncReadyPrototype _TriggerSyncReady;
        public void TriggerSyncReady()
        {
            this._TriggerSyncReady();
        }
        
        //native GetWidgetLife takes widget whichWidget returns real
        public delegate JassRealRet GetWidgetLifePrototype(JassWidget whichWidget);
        private GetWidgetLifePrototype _GetWidgetLife;
        public float GetWidgetLife(JassWidget whichWidget)
        {
            return this._GetWidgetLife(whichWidget);
        }
        
        //native SetWidgetLife takes widget whichWidget, real newLife returns nothing
        public delegate void SetWidgetLifePrototype(JassWidget whichWidget, JassRealArg newLife);
        private SetWidgetLifePrototype _SetWidgetLife;
        public void SetWidgetLife(JassWidget whichWidget, float newLife)
        {
            this._SetWidgetLife(whichWidget, newLife);
        }
        
        //native GetWidgetX takes widget whichWidget returns real
        public delegate JassRealRet GetWidgetXPrototype(JassWidget whichWidget);
        private GetWidgetXPrototype _GetWidgetX;
        public float GetWidgetX(JassWidget whichWidget)
        {
            return this._GetWidgetX(whichWidget);
        }
        
        //native GetWidgetY takes widget whichWidget returns real
        public delegate JassRealRet GetWidgetYPrototype(JassWidget whichWidget);
        private GetWidgetYPrototype _GetWidgetY;
        public float GetWidgetY(JassWidget whichWidget)
        {
            return this._GetWidgetY(whichWidget);
        }
        
        //native GetTriggerWidget takes nothing returns widget
        public delegate JassWidget GetTriggerWidgetPrototype();
        private GetTriggerWidgetPrototype _GetTriggerWidget;
        public JassWidget GetTriggerWidget()
        {
            return this._GetTriggerWidget();
        }
        
        //native CreateDestructable takes integer objectid, real x, real y, real face, real scale, integer variation returns destructable
        public delegate JassDestructable CreateDestructablePrototype(JassObjectId objectid, JassRealArg x, JassRealArg y, JassRealArg face, JassRealArg scale, JassInteger variation);
        private CreateDestructablePrototype _CreateDestructable;
        public JassDestructable CreateDestructable(JassObjectId objectid, float x, float y, float face, float scale, JassInteger variation)
        {
            return this._CreateDestructable(objectid, x, y, face, scale, variation);
        }
        
        //native CreateDestructableZ takes integer objectid, real x, real y, real z, real face, real scale, integer variation returns destructable
        public delegate JassDestructable CreateDestructableZPrototype(JassObjectId objectid, JassRealArg x, JassRealArg y, JassRealArg z, JassRealArg face, JassRealArg scale, JassInteger variation);
        private CreateDestructableZPrototype _CreateDestructableZ;
        public JassDestructable CreateDestructableZ(JassObjectId objectid, float x, float y, float z, float face, float scale, JassInteger variation)
        {
            return this._CreateDestructableZ(objectid, x, y, z, face, scale, variation);
        }
        
        //native CreateDeadDestructable takes integer objectid, real x, real y, real face, real scale, integer variation returns destructable
        public delegate JassDestructable CreateDeadDestructablePrototype(JassObjectId objectid, JassRealArg x, JassRealArg y, JassRealArg face, JassRealArg scale, JassInteger variation);
        private CreateDeadDestructablePrototype _CreateDeadDestructable;
        public JassDestructable CreateDeadDestructable(JassObjectId objectid, float x, float y, float face, float scale, JassInteger variation)
        {
            return this._CreateDeadDestructable(objectid, x, y, face, scale, variation);
        }
        
        //native CreateDeadDestructableZ takes integer objectid, real x, real y, real z, real face, real scale, integer variation returns destructable
        public delegate JassDestructable CreateDeadDestructableZPrototype(JassObjectId objectid, JassRealArg x, JassRealArg y, JassRealArg z, JassRealArg face, JassRealArg scale, JassInteger variation);
        private CreateDeadDestructableZPrototype _CreateDeadDestructableZ;
        public JassDestructable CreateDeadDestructableZ(JassObjectId objectid, float x, float y, float z, float face, float scale, JassInteger variation)
        {
            return this._CreateDeadDestructableZ(objectid, x, y, z, face, scale, variation);
        }
        
        //native RemoveDestructable takes destructable d returns nothing
        public delegate void RemoveDestructablePrototype(JassDestructable d);
        private RemoveDestructablePrototype _RemoveDestructable;
        public void RemoveDestructable(JassDestructable d)
        {
            this._RemoveDestructable(d);
        }
        
        //native KillDestructable takes destructable d returns nothing
        public delegate void KillDestructablePrototype(JassDestructable d);
        private KillDestructablePrototype _KillDestructable;
        public void KillDestructable(JassDestructable d)
        {
            this._KillDestructable(d);
        }
        
        //native SetDestructableInvulnerable takes destructable d, boolean flag returns nothing
        public delegate void SetDestructableInvulnerablePrototype(JassDestructable d, JassBoolean flag);
        private SetDestructableInvulnerablePrototype _SetDestructableInvulnerable;
        public void SetDestructableInvulnerable(JassDestructable d, bool flag)
        {
            this._SetDestructableInvulnerable(d, flag);
        }
        
        //native IsDestructableInvulnerable takes destructable d returns boolean
        public delegate JassBoolean IsDestructableInvulnerablePrototype(JassDestructable d);
        private IsDestructableInvulnerablePrototype _IsDestructableInvulnerable;
        public bool IsDestructableInvulnerable(JassDestructable d)
        {
            return this._IsDestructableInvulnerable(d);
        }
        
        //native EnumDestructablesInRect takes rect r, boolexpr filter, code actionFunc returns nothing
        public delegate void EnumDestructablesInRectPrototype(JassRect r, JassBooleanExpression filter, JassCode actionFunc);
        private EnumDestructablesInRectPrototype _EnumDestructablesInRect;
        public void EnumDestructablesInRect(JassRect r, JassBooleanExpression filter, JassCode actionFunc)
        {
            this._EnumDestructablesInRect(r, filter, actionFunc);
        }
        
        //native GetDestructableTypeId takes destructable d returns integer
        public delegate JassObjectId GetDestructableTypeIdPrototype(JassDestructable d);
        private GetDestructableTypeIdPrototype _GetDestructableTypeId;
        public JassObjectId GetDestructableTypeId(JassDestructable d)
        {
            return this._GetDestructableTypeId(d);
        }
        
        //native GetDestructableX takes destructable d returns real
        public delegate JassRealRet GetDestructableXPrototype(JassDestructable d);
        private GetDestructableXPrototype _GetDestructableX;
        public float GetDestructableX(JassDestructable d)
        {
            return this._GetDestructableX(d);
        }
        
        //native GetDestructableY takes destructable d returns real
        public delegate JassRealRet GetDestructableYPrototype(JassDestructable d);
        private GetDestructableYPrototype _GetDestructableY;
        public float GetDestructableY(JassDestructable d)
        {
            return this._GetDestructableY(d);
        }
        
        //native SetDestructableLife takes destructable d, real life returns nothing
        public delegate void SetDestructableLifePrototype(JassDestructable d, JassRealArg life);
        private SetDestructableLifePrototype _SetDestructableLife;
        public void SetDestructableLife(JassDestructable d, float life)
        {
            this._SetDestructableLife(d, life);
        }
        
        //native GetDestructableLife takes destructable d returns real
        public delegate JassRealRet GetDestructableLifePrototype(JassDestructable d);
        private GetDestructableLifePrototype _GetDestructableLife;
        public float GetDestructableLife(JassDestructable d)
        {
            return this._GetDestructableLife(d);
        }
        
        //native SetDestructableMaxLife takes destructable d, real max returns nothing
        public delegate void SetDestructableMaxLifePrototype(JassDestructable d, JassRealArg max);
        private SetDestructableMaxLifePrototype _SetDestructableMaxLife;
        public void SetDestructableMaxLife(JassDestructable d, float max)
        {
            this._SetDestructableMaxLife(d, max);
        }
        
        //native GetDestructableMaxLife takes destructable d returns real
        public delegate JassRealRet GetDestructableMaxLifePrototype(JassDestructable d);
        private GetDestructableMaxLifePrototype _GetDestructableMaxLife;
        public float GetDestructableMaxLife(JassDestructable d)
        {
            return this._GetDestructableMaxLife(d);
        }
        
        //native DestructableRestoreLife takes destructable d, real life, boolean birth returns nothing
        public delegate void DestructableRestoreLifePrototype(JassDestructable d, JassRealArg life, JassBoolean birth);
        private DestructableRestoreLifePrototype _DestructableRestoreLife;
        public void DestructableRestoreLife(JassDestructable d, float life, bool birth)
        {
            this._DestructableRestoreLife(d, life, birth);
        }
        
        //native QueueDestructableAnimation takes destructable d, string whichAnimation returns nothing
        public delegate void QueueDestructableAnimationPrototype(JassDestructable d, JassStringArg whichAnimation);
        private QueueDestructableAnimationPrototype _QueueDestructableAnimation;
        public void QueueDestructableAnimation(JassDestructable d, string whichAnimation)
        {
            this._QueueDestructableAnimation(d, whichAnimation);
        }
        
        //native SetDestructableAnimation takes destructable d, string whichAnimation returns nothing
        public delegate void SetDestructableAnimationPrototype(JassDestructable d, JassStringArg whichAnimation);
        private SetDestructableAnimationPrototype _SetDestructableAnimation;
        public void SetDestructableAnimation(JassDestructable d, string whichAnimation)
        {
            this._SetDestructableAnimation(d, whichAnimation);
        }
        
        //native SetDestructableAnimationSpeed takes destructable d, real speedFactor returns nothing
        public delegate void SetDestructableAnimationSpeedPrototype(JassDestructable d, JassRealArg speedFactor);
        private SetDestructableAnimationSpeedPrototype _SetDestructableAnimationSpeed;
        public void SetDestructableAnimationSpeed(JassDestructable d, float speedFactor)
        {
            this._SetDestructableAnimationSpeed(d, speedFactor);
        }
        
        //native ShowDestructable takes destructable d, boolean flag returns nothing
        public delegate void ShowDestructablePrototype(JassDestructable d, JassBoolean flag);
        private ShowDestructablePrototype _ShowDestructable;
        public void ShowDestructable(JassDestructable d, bool flag)
        {
            this._ShowDestructable(d, flag);
        }
        
        //native GetDestructableOccluderHeight takes destructable d returns real
        public delegate JassRealRet GetDestructableOccluderHeightPrototype(JassDestructable d);
        private GetDestructableOccluderHeightPrototype _GetDestructableOccluderHeight;
        public float GetDestructableOccluderHeight(JassDestructable d)
        {
            return this._GetDestructableOccluderHeight(d);
        }
        
        //native SetDestructableOccluderHeight takes destructable d, real height returns nothing
        public delegate void SetDestructableOccluderHeightPrototype(JassDestructable d, JassRealArg height);
        private SetDestructableOccluderHeightPrototype _SetDestructableOccluderHeight;
        public void SetDestructableOccluderHeight(JassDestructable d, float height)
        {
            this._SetDestructableOccluderHeight(d, height);
        }
        
        //native GetDestructableName takes destructable d returns string
        public delegate JassStringRet GetDestructableNamePrototype(JassDestructable d);
        private GetDestructableNamePrototype _GetDestructableName;
        public string GetDestructableName(JassDestructable d)
        {
            return this._GetDestructableName(d);
        }
        
        //native GetTriggerDestructable takes nothing returns destructable
        public delegate JassDestructable GetTriggerDestructablePrototype();
        private GetTriggerDestructablePrototype _GetTriggerDestructable;
        public JassDestructable GetTriggerDestructable()
        {
            return this._GetTriggerDestructable();
        }
        
        //native CreateItem takes integer itemid, real x, real y returns item
        public delegate JassItem CreateItemPrototype(JassObjectId itemid, JassRealArg x, JassRealArg y);
        private CreateItemPrototype _CreateItem;
        public JassItem CreateItem(JassObjectId itemid, float x, float y)
        {
            return this._CreateItem(itemid, x, y);
        }
        
        //native RemoveItem takes item whichItem returns nothing
        public delegate void RemoveItemPrototype(JassItem whichItem);
        private RemoveItemPrototype _RemoveItem;
        public void RemoveItem(JassItem whichItem)
        {
            this._RemoveItem(whichItem);
        }
        
        //native GetItemPlayer takes item whichItem returns player
        public delegate JassPlayer GetItemPlayerPrototype(JassItem whichItem);
        private GetItemPlayerPrototype _GetItemPlayer;
        public JassPlayer GetItemPlayer(JassItem whichItem)
        {
            return this._GetItemPlayer(whichItem);
        }
        
        //native GetItemTypeId takes item i returns integer
        public delegate JassObjectId GetItemTypeIdPrototype(JassItem i);
        private GetItemTypeIdPrototype _GetItemTypeId;
        public JassObjectId GetItemTypeId(JassItem i)
        {
            return this._GetItemTypeId(i);
        }
        
        //native GetItemX takes item i returns real
        public delegate JassRealRet GetItemXPrototype(JassItem i);
        private GetItemXPrototype _GetItemX;
        public float GetItemX(JassItem i)
        {
            return this._GetItemX(i);
        }
        
        //native GetItemY takes item i returns real
        public delegate JassRealRet GetItemYPrototype(JassItem i);
        private GetItemYPrototype _GetItemY;
        public float GetItemY(JassItem i)
        {
            return this._GetItemY(i);
        }
        
        //native SetItemPosition takes item i, real x, real y returns nothing
        public delegate void SetItemPositionPrototype(JassItem i, JassRealArg x, JassRealArg y);
        private SetItemPositionPrototype _SetItemPosition;
        public void SetItemPosition(JassItem i, float x, float y)
        {
            this._SetItemPosition(i, x, y);
        }
        
        //native SetItemDropOnDeath takes item whichItem, boolean flag returns nothing
        public delegate void SetItemDropOnDeathPrototype(JassItem whichItem, JassBoolean flag);
        private SetItemDropOnDeathPrototype _SetItemDropOnDeath;
        public void SetItemDropOnDeath(JassItem whichItem, bool flag)
        {
            this._SetItemDropOnDeath(whichItem, flag);
        }
        
        //native SetItemDroppable takes item i, boolean flag returns nothing
        public delegate void SetItemDroppablePrototype(JassItem i, JassBoolean flag);
        private SetItemDroppablePrototype _SetItemDroppable;
        public void SetItemDroppable(JassItem i, bool flag)
        {
            this._SetItemDroppable(i, flag);
        }
        
        //native SetItemPawnable takes item i, boolean flag returns nothing
        public delegate void SetItemPawnablePrototype(JassItem i, JassBoolean flag);
        private SetItemPawnablePrototype _SetItemPawnable;
        public void SetItemPawnable(JassItem i, bool flag)
        {
            this._SetItemPawnable(i, flag);
        }
        
        //native SetItemPlayer takes item whichItem, player whichPlayer, boolean changeColor returns nothing
        public delegate void SetItemPlayerPrototype(JassItem whichItem, JassPlayer whichPlayer, JassBoolean changeColor);
        private SetItemPlayerPrototype _SetItemPlayer;
        public void SetItemPlayer(JassItem whichItem, JassPlayer whichPlayer, bool changeColor)
        {
            this._SetItemPlayer(whichItem, whichPlayer, changeColor);
        }
        
        //native SetItemInvulnerable takes item whichItem, boolean flag returns nothing
        public delegate void SetItemInvulnerablePrototype(JassItem whichItem, JassBoolean flag);
        private SetItemInvulnerablePrototype _SetItemInvulnerable;
        public void SetItemInvulnerable(JassItem whichItem, bool flag)
        {
            this._SetItemInvulnerable(whichItem, flag);
        }
        
        //native IsItemInvulnerable takes item whichItem returns boolean
        public delegate JassBoolean IsItemInvulnerablePrototype(JassItem whichItem);
        private IsItemInvulnerablePrototype _IsItemInvulnerable;
        public bool IsItemInvulnerable(JassItem whichItem)
        {
            return this._IsItemInvulnerable(whichItem);
        }
        
        //native SetItemVisible takes item whichItem, boolean show returns nothing
        public delegate void SetItemVisiblePrototype(JassItem whichItem, JassBoolean show);
        private SetItemVisiblePrototype _SetItemVisible;
        public void SetItemVisible(JassItem whichItem, bool show)
        {
            this._SetItemVisible(whichItem, show);
        }
        
        //native IsItemVisible takes item whichItem returns boolean
        public delegate JassBoolean IsItemVisiblePrototype(JassItem whichItem);
        private IsItemVisiblePrototype _IsItemVisible;
        public bool IsItemVisible(JassItem whichItem)
        {
            return this._IsItemVisible(whichItem);
        }
        
        //native IsItemOwned takes item whichItem returns boolean
        public delegate JassBoolean IsItemOwnedPrototype(JassItem whichItem);
        private IsItemOwnedPrototype _IsItemOwned;
        public bool IsItemOwned(JassItem whichItem)
        {
            return this._IsItemOwned(whichItem);
        }
        
        //native IsItemPowerup takes item whichItem returns boolean
        public delegate JassBoolean IsItemPowerupPrototype(JassItem whichItem);
        private IsItemPowerupPrototype _IsItemPowerup;
        public bool IsItemPowerup(JassItem whichItem)
        {
            return this._IsItemPowerup(whichItem);
        }
        
        //native IsItemSellable takes item whichItem returns boolean
        public delegate JassBoolean IsItemSellablePrototype(JassItem whichItem);
        private IsItemSellablePrototype _IsItemSellable;
        public bool IsItemSellable(JassItem whichItem)
        {
            return this._IsItemSellable(whichItem);
        }
        
        //native IsItemPawnable takes item whichItem returns boolean
        public delegate JassBoolean IsItemPawnablePrototype(JassItem whichItem);
        private IsItemPawnablePrototype _IsItemPawnable;
        public bool IsItemPawnable(JassItem whichItem)
        {
            return this._IsItemPawnable(whichItem);
        }
        
        //native IsItemIdPowerup takes integer itemId returns boolean
        public delegate JassBoolean IsItemIdPowerupPrototype(JassObjectId itemId);
        private IsItemIdPowerupPrototype _IsItemIdPowerup;
        public bool IsItemIdPowerup(JassObjectId itemId)
        {
            return this._IsItemIdPowerup(itemId);
        }
        
        //native IsItemIdSellable takes integer itemId returns boolean
        public delegate JassBoolean IsItemIdSellablePrototype(JassObjectId itemId);
        private IsItemIdSellablePrototype _IsItemIdSellable;
        public bool IsItemIdSellable(JassObjectId itemId)
        {
            return this._IsItemIdSellable(itemId);
        }
        
        //native IsItemIdPawnable takes integer itemId returns boolean
        public delegate JassBoolean IsItemIdPawnablePrototype(JassObjectId itemId);
        private IsItemIdPawnablePrototype _IsItemIdPawnable;
        public bool IsItemIdPawnable(JassObjectId itemId)
        {
            return this._IsItemIdPawnable(itemId);
        }
        
        //native EnumItemsInRect takes rect r, boolexpr filter, code actionFunc returns nothing
        public delegate void EnumItemsInRectPrototype(JassRect r, JassBooleanExpression filter, JassCode actionFunc);
        private EnumItemsInRectPrototype _EnumItemsInRect;
        public void EnumItemsInRect(JassRect r, JassBooleanExpression filter, JassCode actionFunc)
        {
            this._EnumItemsInRect(r, filter, actionFunc);
        }
        
        //native GetItemLevel takes item whichItem returns integer
        public delegate JassInteger GetItemLevelPrototype(JassItem whichItem);
        private GetItemLevelPrototype _GetItemLevel;
        public JassInteger GetItemLevel(JassItem whichItem)
        {
            return this._GetItemLevel(whichItem);
        }
        
        //native GetItemType takes item whichItem returns itemtype
        public delegate JassItemType GetItemTypePrototype(JassItem whichItem);
        private GetItemTypePrototype _GetItemType;
        public JassItemType GetItemType(JassItem whichItem)
        {
            return this._GetItemType(whichItem);
        }
        
        //native SetItemDropID takes item whichItem, integer unitId returns nothing
        public delegate void SetItemDropIDPrototype(JassItem whichItem, JassObjectId unitId);
        private SetItemDropIDPrototype _SetItemDropID;
        public void SetItemDropID(JassItem whichItem, JassObjectId unitId)
        {
            this._SetItemDropID(whichItem, unitId);
        }
        
        //native GetItemName takes item whichItem returns string
        public delegate JassStringRet GetItemNamePrototype(JassItem whichItem);
        private GetItemNamePrototype _GetItemName;
        public string GetItemName(JassItem whichItem)
        {
            return this._GetItemName(whichItem);
        }
        
        //native GetItemCharges takes item whichItem returns integer
        public delegate JassInteger GetItemChargesPrototype(JassItem whichItem);
        private GetItemChargesPrototype _GetItemCharges;
        public JassInteger GetItemCharges(JassItem whichItem)
        {
            return this._GetItemCharges(whichItem);
        }
        
        //native SetItemCharges takes item whichItem, integer charges returns nothing
        public delegate void SetItemChargesPrototype(JassItem whichItem, JassInteger charges);
        private SetItemChargesPrototype _SetItemCharges;
        public void SetItemCharges(JassItem whichItem, JassInteger charges)
        {
            this._SetItemCharges(whichItem, charges);
        }
        
        //native GetItemUserData takes item whichItem returns integer
        public delegate JassInteger GetItemUserDataPrototype(JassItem whichItem);
        private GetItemUserDataPrototype _GetItemUserData;
        public JassInteger GetItemUserData(JassItem whichItem)
        {
            return this._GetItemUserData(whichItem);
        }
        
        //native SetItemUserData takes item whichItem, integer data returns nothing
        public delegate void SetItemUserDataPrototype(JassItem whichItem, JassInteger data);
        private SetItemUserDataPrototype _SetItemUserData;
        public void SetItemUserData(JassItem whichItem, JassInteger data)
        {
            this._SetItemUserData(whichItem, data);
        }
        
        //native CreateUnit takes player id, integer unitid, real x, real y, real face returns unit
        public delegate JassUnit CreateUnitPrototype(JassPlayer id, JassObjectId unitid, JassRealArg x, JassRealArg y, JassRealArg face);
        private CreateUnitPrototype _CreateUnit;
        public JassUnit CreateUnit(JassPlayer id, JassObjectId unitid, float x, float y, float face)
        {
            return this._CreateUnit(id, unitid, x, y, face);
        }
        
        //native CreateUnitByName takes player whichPlayer, string unitname, real x, real y, real face returns unit
        public delegate JassUnit CreateUnitByNamePrototype(JassPlayer whichPlayer, JassStringArg unitname, JassRealArg x, JassRealArg y, JassRealArg face);
        private CreateUnitByNamePrototype _CreateUnitByName;
        public JassUnit CreateUnitByName(JassPlayer whichPlayer, string unitname, float x, float y, float face)
        {
            return this._CreateUnitByName(whichPlayer, unitname, x, y, face);
        }
        
        //native CreateUnitAtLoc takes player id, integer unitid, location whichLocation, real face returns unit
        public delegate JassUnit CreateUnitAtLocPrototype(JassPlayer id, JassObjectId unitid, JassLocation whichLocation, JassRealArg face);
        private CreateUnitAtLocPrototype _CreateUnitAtLoc;
        public JassUnit CreateUnitAtLoc(JassPlayer id, JassObjectId unitid, JassLocation whichLocation, float face)
        {
            return this._CreateUnitAtLoc(id, unitid, whichLocation, face);
        }
        
        //native CreateUnitAtLocByName takes player id, string unitname, location whichLocation, real face returns unit
        public delegate JassUnit CreateUnitAtLocByNamePrototype(JassPlayer id, JassStringArg unitname, JassLocation whichLocation, JassRealArg face);
        private CreateUnitAtLocByNamePrototype _CreateUnitAtLocByName;
        public JassUnit CreateUnitAtLocByName(JassPlayer id, string unitname, JassLocation whichLocation, float face)
        {
            return this._CreateUnitAtLocByName(id, unitname, whichLocation, face);
        }
        
        //native CreateCorpse takes player whichPlayer, integer unitid, real x, real y, real face returns unit
        public delegate JassUnit CreateCorpsePrototype(JassPlayer whichPlayer, JassObjectId unitid, JassRealArg x, JassRealArg y, JassRealArg face);
        private CreateCorpsePrototype _CreateCorpse;
        public JassUnit CreateCorpse(JassPlayer whichPlayer, JassObjectId unitid, float x, float y, float face)
        {
            return this._CreateCorpse(whichPlayer, unitid, x, y, face);
        }
        
        //native KillUnit takes unit whichUnit returns nothing
        public delegate void KillUnitPrototype(JassUnit whichUnit);
        private KillUnitPrototype _KillUnit;
        public void KillUnit(JassUnit whichUnit)
        {
            this._KillUnit(whichUnit);
        }
        
        //native RemoveUnit takes unit whichUnit returns nothing
        public delegate void RemoveUnitPrototype(JassUnit whichUnit);
        private RemoveUnitPrototype _RemoveUnit;
        public void RemoveUnit(JassUnit whichUnit)
        {
            this._RemoveUnit(whichUnit);
        }
        
        //native ShowUnit takes unit whichUnit, boolean show returns nothing
        public delegate void ShowUnitPrototype(JassUnit whichUnit, JassBoolean show);
        private ShowUnitPrototype _ShowUnit;
        public void ShowUnit(JassUnit whichUnit, bool show)
        {
            this._ShowUnit(whichUnit, show);
        }
        
        //native SetUnitState takes unit whichUnit, unitstate whichUnitState, real newVal returns nothing
        public delegate void SetUnitStatePrototype(JassUnit whichUnit, JassUnitState whichUnitState, JassRealArg newVal);
        private SetUnitStatePrototype _SetUnitState;
        public void SetUnitState(JassUnit whichUnit, JassUnitState whichUnitState, float newVal)
        {
            this._SetUnitState(whichUnit, whichUnitState, newVal);
        }
        
        //native SetUnitX takes unit whichUnit, real newX returns nothing
        public delegate void SetUnitXPrototype(JassUnit whichUnit, JassRealArg newX);
        private SetUnitXPrototype _SetUnitX;
        public void SetUnitX(JassUnit whichUnit, float newX)
        {
            this._SetUnitX(whichUnit, newX);
        }
        
        //native SetUnitY takes unit whichUnit, real newY returns nothing
        public delegate void SetUnitYPrototype(JassUnit whichUnit, JassRealArg newY);
        private SetUnitYPrototype _SetUnitY;
        public void SetUnitY(JassUnit whichUnit, float newY)
        {
            this._SetUnitY(whichUnit, newY);
        }
        
        //native SetUnitPosition takes unit whichUnit, real newX, real newY returns nothing
        public delegate void SetUnitPositionPrototype(JassUnit whichUnit, JassRealArg newX, JassRealArg newY);
        private SetUnitPositionPrototype _SetUnitPosition;
        public void SetUnitPosition(JassUnit whichUnit, float newX, float newY)
        {
            this._SetUnitPosition(whichUnit, newX, newY);
        }
        
        //native SetUnitPositionLoc takes unit whichUnit, location whichLocation returns nothing
        public delegate void SetUnitPositionLocPrototype(JassUnit whichUnit, JassLocation whichLocation);
        private SetUnitPositionLocPrototype _SetUnitPositionLoc;
        public void SetUnitPositionLoc(JassUnit whichUnit, JassLocation whichLocation)
        {
            this._SetUnitPositionLoc(whichUnit, whichLocation);
        }
        
        //native SetUnitFacing takes unit whichUnit, real facingAngle returns nothing
        public delegate void SetUnitFacingPrototype(JassUnit whichUnit, JassRealArg facingAngle);
        private SetUnitFacingPrototype _SetUnitFacing;
        public void SetUnitFacing(JassUnit whichUnit, float facingAngle)
        {
            this._SetUnitFacing(whichUnit, facingAngle);
        }
        
        //native SetUnitFacingTimed takes unit whichUnit, real facingAngle, real duration returns nothing
        public delegate void SetUnitFacingTimedPrototype(JassUnit whichUnit, JassRealArg facingAngle, JassRealArg duration);
        private SetUnitFacingTimedPrototype _SetUnitFacingTimed;
        public void SetUnitFacingTimed(JassUnit whichUnit, float facingAngle, float duration)
        {
            this._SetUnitFacingTimed(whichUnit, facingAngle, duration);
        }
        
        //native SetUnitMoveSpeed takes unit whichUnit, real newSpeed returns nothing
        public delegate void SetUnitMoveSpeedPrototype(JassUnit whichUnit, JassRealArg newSpeed);
        private SetUnitMoveSpeedPrototype _SetUnitMoveSpeed;
        public void SetUnitMoveSpeed(JassUnit whichUnit, float newSpeed)
        {
            this._SetUnitMoveSpeed(whichUnit, newSpeed);
        }
        
        //native SetUnitFlyHeight takes unit whichUnit, real newHeight, real rate returns nothing
        public delegate void SetUnitFlyHeightPrototype(JassUnit whichUnit, JassRealArg newHeight, JassRealArg rate);
        private SetUnitFlyHeightPrototype _SetUnitFlyHeight;
        public void SetUnitFlyHeight(JassUnit whichUnit, float newHeight, float rate)
        {
            this._SetUnitFlyHeight(whichUnit, newHeight, rate);
        }
        
        //native SetUnitTurnSpeed takes unit whichUnit, real newTurnSpeed returns nothing
        public delegate void SetUnitTurnSpeedPrototype(JassUnit whichUnit, JassRealArg newTurnSpeed);
        private SetUnitTurnSpeedPrototype _SetUnitTurnSpeed;
        public void SetUnitTurnSpeed(JassUnit whichUnit, float newTurnSpeed)
        {
            this._SetUnitTurnSpeed(whichUnit, newTurnSpeed);
        }
        
        //native SetUnitPropWindow takes unit whichUnit, real newPropWindowAngle returns nothing
        public delegate void SetUnitPropWindowPrototype(JassUnit whichUnit, JassRealArg newPropWindowAngle);
        private SetUnitPropWindowPrototype _SetUnitPropWindow;
        public void SetUnitPropWindow(JassUnit whichUnit, float newPropWindowAngle)
        {
            this._SetUnitPropWindow(whichUnit, newPropWindowAngle);
        }
        
        //native SetUnitAcquireRange takes unit whichUnit, real newAcquireRange returns nothing
        public delegate void SetUnitAcquireRangePrototype(JassUnit whichUnit, JassRealArg newAcquireRange);
        private SetUnitAcquireRangePrototype _SetUnitAcquireRange;
        public void SetUnitAcquireRange(JassUnit whichUnit, float newAcquireRange)
        {
            this._SetUnitAcquireRange(whichUnit, newAcquireRange);
        }
        
        //native SetUnitCreepGuard takes unit whichUnit, boolean creepGuard returns nothing
        public delegate void SetUnitCreepGuardPrototype(JassUnit whichUnit, JassBoolean creepGuard);
        private SetUnitCreepGuardPrototype _SetUnitCreepGuard;
        public void SetUnitCreepGuard(JassUnit whichUnit, bool creepGuard)
        {
            this._SetUnitCreepGuard(whichUnit, creepGuard);
        }
        
        //native GetUnitAcquireRange takes unit whichUnit returns real
        public delegate JassRealRet GetUnitAcquireRangePrototype(JassUnit whichUnit);
        private GetUnitAcquireRangePrototype _GetUnitAcquireRange;
        public float GetUnitAcquireRange(JassUnit whichUnit)
        {
            return this._GetUnitAcquireRange(whichUnit);
        }
        
        //native GetUnitTurnSpeed takes unit whichUnit returns real
        public delegate JassRealRet GetUnitTurnSpeedPrototype(JassUnit whichUnit);
        private GetUnitTurnSpeedPrototype _GetUnitTurnSpeed;
        public float GetUnitTurnSpeed(JassUnit whichUnit)
        {
            return this._GetUnitTurnSpeed(whichUnit);
        }
        
        //native GetUnitPropWindow takes unit whichUnit returns real
        public delegate JassRealRet GetUnitPropWindowPrototype(JassUnit whichUnit);
        private GetUnitPropWindowPrototype _GetUnitPropWindow;
        public float GetUnitPropWindow(JassUnit whichUnit)
        {
            return this._GetUnitPropWindow(whichUnit);
        }
        
        //native GetUnitFlyHeight takes unit whichUnit returns real
        public delegate JassRealRet GetUnitFlyHeightPrototype(JassUnit whichUnit);
        private GetUnitFlyHeightPrototype _GetUnitFlyHeight;
        public float GetUnitFlyHeight(JassUnit whichUnit)
        {
            return this._GetUnitFlyHeight(whichUnit);
        }
        
        //native GetUnitDefaultAcquireRange takes unit whichUnit returns real
        public delegate JassRealRet GetUnitDefaultAcquireRangePrototype(JassUnit whichUnit);
        private GetUnitDefaultAcquireRangePrototype _GetUnitDefaultAcquireRange;
        public float GetUnitDefaultAcquireRange(JassUnit whichUnit)
        {
            return this._GetUnitDefaultAcquireRange(whichUnit);
        }
        
        //native GetUnitDefaultTurnSpeed takes unit whichUnit returns real
        public delegate JassRealRet GetUnitDefaultTurnSpeedPrototype(JassUnit whichUnit);
        private GetUnitDefaultTurnSpeedPrototype _GetUnitDefaultTurnSpeed;
        public float GetUnitDefaultTurnSpeed(JassUnit whichUnit)
        {
            return this._GetUnitDefaultTurnSpeed(whichUnit);
        }
        
        //native GetUnitDefaultPropWindow takes unit whichUnit returns real
        public delegate JassRealRet GetUnitDefaultPropWindowPrototype(JassUnit whichUnit);
        private GetUnitDefaultPropWindowPrototype _GetUnitDefaultPropWindow;
        public float GetUnitDefaultPropWindow(JassUnit whichUnit)
        {
            return this._GetUnitDefaultPropWindow(whichUnit);
        }
        
        //native GetUnitDefaultFlyHeight takes unit whichUnit returns real
        public delegate JassRealRet GetUnitDefaultFlyHeightPrototype(JassUnit whichUnit);
        private GetUnitDefaultFlyHeightPrototype _GetUnitDefaultFlyHeight;
        public float GetUnitDefaultFlyHeight(JassUnit whichUnit)
        {
            return this._GetUnitDefaultFlyHeight(whichUnit);
        }
        
        //native SetUnitOwner takes unit whichUnit, player whichPlayer, boolean changeColor returns nothing
        public delegate void SetUnitOwnerPrototype(JassUnit whichUnit, JassPlayer whichPlayer, JassBoolean changeColor);
        private SetUnitOwnerPrototype _SetUnitOwner;
        public void SetUnitOwner(JassUnit whichUnit, JassPlayer whichPlayer, bool changeColor)
        {
            this._SetUnitOwner(whichUnit, whichPlayer, changeColor);
        }
        
        //native SetUnitColor takes unit whichUnit, playercolor whichColor returns nothing
        public delegate void SetUnitColorPrototype(JassUnit whichUnit, JassPlayerColor whichColor);
        private SetUnitColorPrototype _SetUnitColor;
        public void SetUnitColor(JassUnit whichUnit, JassPlayerColor whichColor)
        {
            this._SetUnitColor(whichUnit, whichColor);
        }
        
        //native SetUnitScale takes unit whichUnit, real scaleX, real scaleY, real scaleZ returns nothing
        public delegate void SetUnitScalePrototype(JassUnit whichUnit, JassRealArg scaleX, JassRealArg scaleY, JassRealArg scaleZ);
        private SetUnitScalePrototype _SetUnitScale;
        public void SetUnitScale(JassUnit whichUnit, float scaleX, float scaleY, float scaleZ)
        {
            this._SetUnitScale(whichUnit, scaleX, scaleY, scaleZ);
        }
        
        //native SetUnitTimeScale takes unit whichUnit, real timeScale returns nothing
        public delegate void SetUnitTimeScalePrototype(JassUnit whichUnit, JassRealArg timeScale);
        private SetUnitTimeScalePrototype _SetUnitTimeScale;
        public void SetUnitTimeScale(JassUnit whichUnit, float timeScale)
        {
            this._SetUnitTimeScale(whichUnit, timeScale);
        }
        
        //native SetUnitBlendTime takes unit whichUnit, real blendTime returns nothing
        public delegate void SetUnitBlendTimePrototype(JassUnit whichUnit, JassRealArg blendTime);
        private SetUnitBlendTimePrototype _SetUnitBlendTime;
        public void SetUnitBlendTime(JassUnit whichUnit, float blendTime)
        {
            this._SetUnitBlendTime(whichUnit, blendTime);
        }
        
        //native SetUnitVertexColor takes unit whichUnit, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void SetUnitVertexColorPrototype(JassUnit whichUnit, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private SetUnitVertexColorPrototype _SetUnitVertexColor;
        public void SetUnitVertexColor(JassUnit whichUnit, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._SetUnitVertexColor(whichUnit, red, green, blue, alpha);
        }
        
        //native QueueUnitAnimation takes unit whichUnit, string whichAnimation returns nothing
        public delegate void QueueUnitAnimationPrototype(JassUnit whichUnit, JassStringArg whichAnimation);
        private QueueUnitAnimationPrototype _QueueUnitAnimation;
        public void QueueUnitAnimation(JassUnit whichUnit, string whichAnimation)
        {
            this._QueueUnitAnimation(whichUnit, whichAnimation);
        }
        
        //native SetUnitAnimation takes unit whichUnit, string whichAnimation returns nothing
        public delegate void SetUnitAnimationPrototype(JassUnit whichUnit, JassStringArg whichAnimation);
        private SetUnitAnimationPrototype _SetUnitAnimation;
        public void SetUnitAnimation(JassUnit whichUnit, string whichAnimation)
        {
            this._SetUnitAnimation(whichUnit, whichAnimation);
        }
        
        //native SetUnitAnimationByIndex takes unit whichUnit, integer whichAnimation returns nothing
        public delegate void SetUnitAnimationByIndexPrototype(JassUnit whichUnit, JassInteger whichAnimation);
        private SetUnitAnimationByIndexPrototype _SetUnitAnimationByIndex;
        public void SetUnitAnimationByIndex(JassUnit whichUnit, JassInteger whichAnimation)
        {
            this._SetUnitAnimationByIndex(whichUnit, whichAnimation);
        }
        
        //native SetUnitAnimationWithRarity takes unit whichUnit, string whichAnimation, raritycontrol rarity returns nothing
        public delegate void SetUnitAnimationWithRarityPrototype(JassUnit whichUnit, JassStringArg whichAnimation, JassRarityControl rarity);
        private SetUnitAnimationWithRarityPrototype _SetUnitAnimationWithRarity;
        public void SetUnitAnimationWithRarity(JassUnit whichUnit, string whichAnimation, JassRarityControl rarity)
        {
            this._SetUnitAnimationWithRarity(whichUnit, whichAnimation, rarity);
        }
        
        //native AddUnitAnimationProperties takes unit whichUnit, string animProperties, boolean add returns nothing
        public delegate void AddUnitAnimationPropertiesPrototype(JassUnit whichUnit, JassStringArg animProperties, JassBoolean add);
        private AddUnitAnimationPropertiesPrototype _AddUnitAnimationProperties;
        public void AddUnitAnimationProperties(JassUnit whichUnit, string animProperties, bool add)
        {
            this._AddUnitAnimationProperties(whichUnit, animProperties, add);
        }
        
        //native SetUnitLookAt takes unit whichUnit, string whichBone, unit lookAtTarget, real offsetX, real offsetY, real offsetZ returns nothing
        public delegate void SetUnitLookAtPrototype(JassUnit whichUnit, JassStringArg whichBone, JassUnit lookAtTarget, JassRealArg offsetX, JassRealArg offsetY, JassRealArg offsetZ);
        private SetUnitLookAtPrototype _SetUnitLookAt;
        public void SetUnitLookAt(JassUnit whichUnit, string whichBone, JassUnit lookAtTarget, float offsetX, float offsetY, float offsetZ)
        {
            this._SetUnitLookAt(whichUnit, whichBone, lookAtTarget, offsetX, offsetY, offsetZ);
        }
        
        //native ResetUnitLookAt takes unit whichUnit returns nothing
        public delegate void ResetUnitLookAtPrototype(JassUnit whichUnit);
        private ResetUnitLookAtPrototype _ResetUnitLookAt;
        public void ResetUnitLookAt(JassUnit whichUnit)
        {
            this._ResetUnitLookAt(whichUnit);
        }
        
        //native SetUnitRescuable takes unit whichUnit, player byWhichPlayer, boolean flag returns nothing
        public delegate void SetUnitRescuablePrototype(JassUnit whichUnit, JassPlayer byWhichPlayer, JassBoolean flag);
        private SetUnitRescuablePrototype _SetUnitRescuable;
        public void SetUnitRescuable(JassUnit whichUnit, JassPlayer byWhichPlayer, bool flag)
        {
            this._SetUnitRescuable(whichUnit, byWhichPlayer, flag);
        }
        
        //native SetUnitRescueRange takes unit whichUnit, real range returns nothing
        public delegate void SetUnitRescueRangePrototype(JassUnit whichUnit, JassRealArg range);
        private SetUnitRescueRangePrototype _SetUnitRescueRange;
        public void SetUnitRescueRange(JassUnit whichUnit, float range)
        {
            this._SetUnitRescueRange(whichUnit, range);
        }
        
        //native SetHeroStr takes unit whichHero, integer newStr, boolean permanent returns nothing
        public delegate void SetHeroStrPrototype(JassUnit whichHero, JassInteger newStr, JassBoolean permanent);
        private SetHeroStrPrototype _SetHeroStr;
        public void SetHeroStr(JassUnit whichHero, JassInteger newStr, bool permanent)
        {
            this._SetHeroStr(whichHero, newStr, permanent);
        }
        
        //native SetHeroAgi takes unit whichHero, integer newAgi, boolean permanent returns nothing
        public delegate void SetHeroAgiPrototype(JassUnit whichHero, JassInteger newAgi, JassBoolean permanent);
        private SetHeroAgiPrototype _SetHeroAgi;
        public void SetHeroAgi(JassUnit whichHero, JassInteger newAgi, bool permanent)
        {
            this._SetHeroAgi(whichHero, newAgi, permanent);
        }
        
        //native SetHeroInt takes unit whichHero, integer newInt, boolean permanent returns nothing
        public delegate void SetHeroIntPrototype(JassUnit whichHero, JassInteger newInt, JassBoolean permanent);
        private SetHeroIntPrototype _SetHeroInt;
        public void SetHeroInt(JassUnit whichHero, JassInteger newInt, bool permanent)
        {
            this._SetHeroInt(whichHero, newInt, permanent);
        }
        
        //native GetHeroStr takes unit whichHero, boolean includeBonuses returns integer
        public delegate JassInteger GetHeroStrPrototype(JassUnit whichHero, JassBoolean includeBonuses);
        private GetHeroStrPrototype _GetHeroStr;
        public JassInteger GetHeroStr(JassUnit whichHero, bool includeBonuses)
        {
            return this._GetHeroStr(whichHero, includeBonuses);
        }
        
        //native GetHeroAgi takes unit whichHero, boolean includeBonuses returns integer
        public delegate JassInteger GetHeroAgiPrototype(JassUnit whichHero, JassBoolean includeBonuses);
        private GetHeroAgiPrototype _GetHeroAgi;
        public JassInteger GetHeroAgi(JassUnit whichHero, bool includeBonuses)
        {
            return this._GetHeroAgi(whichHero, includeBonuses);
        }
        
        //native GetHeroInt takes unit whichHero, boolean includeBonuses returns integer
        public delegate JassInteger GetHeroIntPrototype(JassUnit whichHero, JassBoolean includeBonuses);
        private GetHeroIntPrototype _GetHeroInt;
        public JassInteger GetHeroInt(JassUnit whichHero, bool includeBonuses)
        {
            return this._GetHeroInt(whichHero, includeBonuses);
        }
        
        //native UnitStripHeroLevel takes unit whichHero, integer howManyLevels returns boolean
        public delegate JassBoolean UnitStripHeroLevelPrototype(JassUnit whichHero, JassInteger howManyLevels);
        private UnitStripHeroLevelPrototype _UnitStripHeroLevel;
        public bool UnitStripHeroLevel(JassUnit whichHero, JassInteger howManyLevels)
        {
            return this._UnitStripHeroLevel(whichHero, howManyLevels);
        }
        
        //native GetHeroXP takes unit whichHero returns integer
        public delegate JassInteger GetHeroXPPrototype(JassUnit whichHero);
        private GetHeroXPPrototype _GetHeroXP;
        public JassInteger GetHeroXP(JassUnit whichHero)
        {
            return this._GetHeroXP(whichHero);
        }
        
        //native SetHeroXP takes unit whichHero, integer newXpVal, boolean showEyeCandy returns nothing
        public delegate void SetHeroXPPrototype(JassUnit whichHero, JassInteger newXpVal, JassBoolean showEyeCandy);
        private SetHeroXPPrototype _SetHeroXP;
        public void SetHeroXP(JassUnit whichHero, JassInteger newXpVal, bool showEyeCandy)
        {
            this._SetHeroXP(whichHero, newXpVal, showEyeCandy);
        }
        
        //native GetHeroSkillPoints takes unit whichHero returns integer
        public delegate JassInteger GetHeroSkillPointsPrototype(JassUnit whichHero);
        private GetHeroSkillPointsPrototype _GetHeroSkillPoints;
        public JassInteger GetHeroSkillPoints(JassUnit whichHero)
        {
            return this._GetHeroSkillPoints(whichHero);
        }
        
        //native UnitModifySkillPoints takes unit whichHero, integer skillPointDelta returns boolean
        public delegate JassBoolean UnitModifySkillPointsPrototype(JassUnit whichHero, JassInteger skillPointDelta);
        private UnitModifySkillPointsPrototype _UnitModifySkillPoints;
        public bool UnitModifySkillPoints(JassUnit whichHero, JassInteger skillPointDelta)
        {
            return this._UnitModifySkillPoints(whichHero, skillPointDelta);
        }
        
        //native AddHeroXP takes unit whichHero, integer xpToAdd, boolean showEyeCandy returns nothing
        public delegate void AddHeroXPPrototype(JassUnit whichHero, JassInteger xpToAdd, JassBoolean showEyeCandy);
        private AddHeroXPPrototype _AddHeroXP;
        public void AddHeroXP(JassUnit whichHero, JassInteger xpToAdd, bool showEyeCandy)
        {
            this._AddHeroXP(whichHero, xpToAdd, showEyeCandy);
        }
        
        //native SetHeroLevel takes unit whichHero, integer level, boolean showEyeCandy returns nothing
        public delegate void SetHeroLevelPrototype(JassUnit whichHero, JassInteger level, JassBoolean showEyeCandy);
        private SetHeroLevelPrototype _SetHeroLevel;
        public void SetHeroLevel(JassUnit whichHero, JassInteger level, bool showEyeCandy)
        {
            this._SetHeroLevel(whichHero, level, showEyeCandy);
        }
        
        //native GetHeroLevel takes unit whichHero returns integer
        public delegate JassInteger GetHeroLevelPrototype(JassUnit whichHero);
        private GetHeroLevelPrototype _GetHeroLevel;
        public JassInteger GetHeroLevel(JassUnit whichHero)
        {
            return this._GetHeroLevel(whichHero);
        }
        
        //native GetUnitLevel takes unit whichUnit returns integer
        public delegate JassInteger GetUnitLevelPrototype(JassUnit whichUnit);
        private GetUnitLevelPrototype _GetUnitLevel;
        public JassInteger GetUnitLevel(JassUnit whichUnit)
        {
            return this._GetUnitLevel(whichUnit);
        }
        
        //native GetHeroProperName takes unit whichHero returns string
        public delegate JassStringRet GetHeroProperNamePrototype(JassUnit whichHero);
        private GetHeroProperNamePrototype _GetHeroProperName;
        public string GetHeroProperName(JassUnit whichHero)
        {
            return this._GetHeroProperName(whichHero);
        }
        
        //native SuspendHeroXP takes unit whichHero, boolean flag returns nothing
        public delegate void SuspendHeroXPPrototype(JassUnit whichHero, JassBoolean flag);
        private SuspendHeroXPPrototype _SuspendHeroXP;
        public void SuspendHeroXP(JassUnit whichHero, bool flag)
        {
            this._SuspendHeroXP(whichHero, flag);
        }
        
        //native IsSuspendedXP takes unit whichHero returns boolean
        public delegate JassBoolean IsSuspendedXPPrototype(JassUnit whichHero);
        private IsSuspendedXPPrototype _IsSuspendedXP;
        public bool IsSuspendedXP(JassUnit whichHero)
        {
            return this._IsSuspendedXP(whichHero);
        }
        
        //native SelectHeroSkill takes unit whichHero, integer abilcode returns nothing
        public delegate void SelectHeroSkillPrototype(JassUnit whichHero, JassObjectId abilcode);
        private SelectHeroSkillPrototype _SelectHeroSkill;
        public void SelectHeroSkill(JassUnit whichHero, JassObjectId abilcode)
        {
            this._SelectHeroSkill(whichHero, abilcode);
        }
        
        //native GetUnitAbilityLevel takes unit whichUnit, integer abilcode returns integer
        public delegate JassInteger GetUnitAbilityLevelPrototype(JassUnit whichUnit, JassObjectId abilcode);
        private GetUnitAbilityLevelPrototype _GetUnitAbilityLevel;
        public JassInteger GetUnitAbilityLevel(JassUnit whichUnit, JassObjectId abilcode)
        {
            return this._GetUnitAbilityLevel(whichUnit, abilcode);
        }
        
        //native DecUnitAbilityLevel takes unit whichUnit, integer abilcode returns integer
        public delegate JassInteger DecUnitAbilityLevelPrototype(JassUnit whichUnit, JassObjectId abilcode);
        private DecUnitAbilityLevelPrototype _DecUnitAbilityLevel;
        public JassInteger DecUnitAbilityLevel(JassUnit whichUnit, JassObjectId abilcode)
        {
            return this._DecUnitAbilityLevel(whichUnit, abilcode);
        }
        
        //native IncUnitAbilityLevel takes unit whichUnit, integer abilcode returns integer
        public delegate JassInteger IncUnitAbilityLevelPrototype(JassUnit whichUnit, JassObjectId abilcode);
        private IncUnitAbilityLevelPrototype _IncUnitAbilityLevel;
        public JassInteger IncUnitAbilityLevel(JassUnit whichUnit, JassObjectId abilcode)
        {
            return this._IncUnitAbilityLevel(whichUnit, abilcode);
        }
        
        //native SetUnitAbilityLevel takes unit whichUnit, integer abilcode, integer level returns integer
        public delegate JassInteger SetUnitAbilityLevelPrototype(JassUnit whichUnit, JassObjectId abilcode, JassInteger level);
        private SetUnitAbilityLevelPrototype _SetUnitAbilityLevel;
        public JassInteger SetUnitAbilityLevel(JassUnit whichUnit, JassObjectId abilcode, JassInteger level)
        {
            return this._SetUnitAbilityLevel(whichUnit, abilcode, level);
        }
        
        //native ReviveHero takes unit whichHero, real x, real y, boolean doEyecandy returns boolean
        public delegate JassBoolean ReviveHeroPrototype(JassUnit whichHero, JassRealArg x, JassRealArg y, JassBoolean doEyecandy);
        private ReviveHeroPrototype _ReviveHero;
        public bool ReviveHero(JassUnit whichHero, float x, float y, bool doEyecandy)
        {
            return this._ReviveHero(whichHero, x, y, doEyecandy);
        }
        
        //native ReviveHeroLoc takes unit whichHero, location loc, boolean doEyecandy returns boolean
        public delegate JassBoolean ReviveHeroLocPrototype(JassUnit whichHero, JassLocation loc, JassBoolean doEyecandy);
        private ReviveHeroLocPrototype _ReviveHeroLoc;
        public bool ReviveHeroLoc(JassUnit whichHero, JassLocation loc, bool doEyecandy)
        {
            return this._ReviveHeroLoc(whichHero, loc, doEyecandy);
        }
        
        //native SetUnitExploded takes unit whichUnit, boolean exploded returns nothing
        public delegate void SetUnitExplodedPrototype(JassUnit whichUnit, JassBoolean exploded);
        private SetUnitExplodedPrototype _SetUnitExploded;
        public void SetUnitExploded(JassUnit whichUnit, bool exploded)
        {
            this._SetUnitExploded(whichUnit, exploded);
        }
        
        //native SetUnitInvulnerable takes unit whichUnit, boolean flag returns nothing
        public delegate void SetUnitInvulnerablePrototype(JassUnit whichUnit, JassBoolean flag);
        private SetUnitInvulnerablePrototype _SetUnitInvulnerable;
        public void SetUnitInvulnerable(JassUnit whichUnit, bool flag)
        {
            this._SetUnitInvulnerable(whichUnit, flag);
        }
        
        //native PauseUnit takes unit whichUnit, boolean flag returns nothing
        public delegate void PauseUnitPrototype(JassUnit whichUnit, JassBoolean flag);
        private PauseUnitPrototype _PauseUnit;
        public void PauseUnit(JassUnit whichUnit, bool flag)
        {
            this._PauseUnit(whichUnit, flag);
        }
        
        //native IsUnitPaused takes unit whichHero returns boolean
        public delegate JassBoolean IsUnitPausedPrototype(JassUnit whichHero);
        private IsUnitPausedPrototype _IsUnitPaused;
        public bool IsUnitPaused(JassUnit whichHero)
        {
            return this._IsUnitPaused(whichHero);
        }
        
        //native SetUnitPathing takes unit whichUnit, boolean flag returns nothing
        public delegate void SetUnitPathingPrototype(JassUnit whichUnit, JassBoolean flag);
        private SetUnitPathingPrototype _SetUnitPathing;
        public void SetUnitPathing(JassUnit whichUnit, bool flag)
        {
            this._SetUnitPathing(whichUnit, flag);
        }
        
        //native ClearSelection takes nothing returns nothing
        public delegate void ClearSelectionPrototype();
        private ClearSelectionPrototype _ClearSelection;
        public void ClearSelection()
        {
            this._ClearSelection();
        }
        
        //native SelectUnit takes unit whichUnit, boolean flag returns nothing
        public delegate void SelectUnitPrototype(JassUnit whichUnit, JassBoolean flag);
        private SelectUnitPrototype _SelectUnit;
        public void SelectUnit(JassUnit whichUnit, bool flag)
        {
            this._SelectUnit(whichUnit, flag);
        }
        
        //native GetUnitPointValue takes unit whichUnit returns integer
        public delegate JassInteger GetUnitPointValuePrototype(JassUnit whichUnit);
        private GetUnitPointValuePrototype _GetUnitPointValue;
        public JassInteger GetUnitPointValue(JassUnit whichUnit)
        {
            return this._GetUnitPointValue(whichUnit);
        }
        
        //native GetUnitPointValueByType takes integer unitType returns integer
        public delegate JassInteger GetUnitPointValueByTypePrototype(JassInteger unitType);
        private GetUnitPointValueByTypePrototype _GetUnitPointValueByType;
        public JassInteger GetUnitPointValueByType(JassInteger unitType)
        {
            return this._GetUnitPointValueByType(unitType);
        }
        
        //native UnitAddItem takes unit whichUnit, item whichItem returns boolean
        public delegate JassBoolean UnitAddItemPrototype(JassUnit whichUnit, JassItem whichItem);
        private UnitAddItemPrototype _UnitAddItem;
        public bool UnitAddItem(JassUnit whichUnit, JassItem whichItem)
        {
            return this._UnitAddItem(whichUnit, whichItem);
        }
        
        //native UnitAddItemById takes unit whichUnit, integer itemId returns item
        public delegate JassItem UnitAddItemByIdPrototype(JassUnit whichUnit, JassObjectId itemId);
        private UnitAddItemByIdPrototype _UnitAddItemById;
        public JassItem UnitAddItemById(JassUnit whichUnit, JassObjectId itemId)
        {
            return this._UnitAddItemById(whichUnit, itemId);
        }
        
        //native UnitAddItemToSlotById takes unit whichUnit, integer itemId, integer itemSlot returns boolean
        public delegate JassBoolean UnitAddItemToSlotByIdPrototype(JassUnit whichUnit, JassObjectId itemId, JassInteger itemSlot);
        private UnitAddItemToSlotByIdPrototype _UnitAddItemToSlotById;
        public bool UnitAddItemToSlotById(JassUnit whichUnit, JassObjectId itemId, JassInteger itemSlot)
        {
            return this._UnitAddItemToSlotById(whichUnit, itemId, itemSlot);
        }
        
        //native UnitRemoveItem takes unit whichUnit, item whichItem returns nothing
        public delegate void UnitRemoveItemPrototype(JassUnit whichUnit, JassItem whichItem);
        private UnitRemoveItemPrototype _UnitRemoveItem;
        public void UnitRemoveItem(JassUnit whichUnit, JassItem whichItem)
        {
            this._UnitRemoveItem(whichUnit, whichItem);
        }
        
        //native UnitRemoveItemFromSlot takes unit whichUnit, integer itemSlot returns item
        public delegate JassItem UnitRemoveItemFromSlotPrototype(JassUnit whichUnit, JassInteger itemSlot);
        private UnitRemoveItemFromSlotPrototype _UnitRemoveItemFromSlot;
        public JassItem UnitRemoveItemFromSlot(JassUnit whichUnit, JassInteger itemSlot)
        {
            return this._UnitRemoveItemFromSlot(whichUnit, itemSlot);
        }
        
        //native UnitHasItem takes unit whichUnit, item whichItem returns boolean
        public delegate JassBoolean UnitHasItemPrototype(JassUnit whichUnit, JassItem whichItem);
        private UnitHasItemPrototype _UnitHasItem;
        public bool UnitHasItem(JassUnit whichUnit, JassItem whichItem)
        {
            return this._UnitHasItem(whichUnit, whichItem);
        }
        
        //native UnitItemInSlot takes unit whichUnit, integer itemSlot returns item
        public delegate JassItem UnitItemInSlotPrototype(JassUnit whichUnit, JassInteger itemSlot);
        private UnitItemInSlotPrototype _UnitItemInSlot;
        public JassItem UnitItemInSlot(JassUnit whichUnit, JassInteger itemSlot)
        {
            return this._UnitItemInSlot(whichUnit, itemSlot);
        }
        
        //native UnitInventorySize takes unit whichUnit returns integer
        public delegate JassInteger UnitInventorySizePrototype(JassUnit whichUnit);
        private UnitInventorySizePrototype _UnitInventorySize;
        public JassInteger UnitInventorySize(JassUnit whichUnit)
        {
            return this._UnitInventorySize(whichUnit);
        }
        
        //native UnitDropItemPoint takes unit whichUnit, item whichItem, real x, real y returns boolean
        public delegate JassBoolean UnitDropItemPointPrototype(JassUnit whichUnit, JassItem whichItem, JassRealArg x, JassRealArg y);
        private UnitDropItemPointPrototype _UnitDropItemPoint;
        public bool UnitDropItemPoint(JassUnit whichUnit, JassItem whichItem, float x, float y)
        {
            return this._UnitDropItemPoint(whichUnit, whichItem, x, y);
        }
        
        //native UnitDropItemSlot takes unit whichUnit, item whichItem, integer slot returns boolean
        public delegate JassBoolean UnitDropItemSlotPrototype(JassUnit whichUnit, JassItem whichItem, JassInteger slot);
        private UnitDropItemSlotPrototype _UnitDropItemSlot;
        public bool UnitDropItemSlot(JassUnit whichUnit, JassItem whichItem, JassInteger slot)
        {
            return this._UnitDropItemSlot(whichUnit, whichItem, slot);
        }
        
        //native UnitDropItemTarget takes unit whichUnit, item whichItem, widget target returns boolean
        public delegate JassBoolean UnitDropItemTargetPrototype(JassUnit whichUnit, JassItem whichItem, JassWidget target);
        private UnitDropItemTargetPrototype _UnitDropItemTarget;
        public bool UnitDropItemTarget(JassUnit whichUnit, JassItem whichItem, JassWidget target)
        {
            return this._UnitDropItemTarget(whichUnit, whichItem, target);
        }
        
        //native UnitUseItem takes unit whichUnit, item whichItem returns boolean
        public delegate JassBoolean UnitUseItemPrototype(JassUnit whichUnit, JassItem whichItem);
        private UnitUseItemPrototype _UnitUseItem;
        public bool UnitUseItem(JassUnit whichUnit, JassItem whichItem)
        {
            return this._UnitUseItem(whichUnit, whichItem);
        }
        
        //native UnitUseItemPoint takes unit whichUnit, item whichItem, real x, real y returns boolean
        public delegate JassBoolean UnitUseItemPointPrototype(JassUnit whichUnit, JassItem whichItem, JassRealArg x, JassRealArg y);
        private UnitUseItemPointPrototype _UnitUseItemPoint;
        public bool UnitUseItemPoint(JassUnit whichUnit, JassItem whichItem, float x, float y)
        {
            return this._UnitUseItemPoint(whichUnit, whichItem, x, y);
        }
        
        //native UnitUseItemTarget takes unit whichUnit, item whichItem, widget target returns boolean
        public delegate JassBoolean UnitUseItemTargetPrototype(JassUnit whichUnit, JassItem whichItem, JassWidget target);
        private UnitUseItemTargetPrototype _UnitUseItemTarget;
        public bool UnitUseItemTarget(JassUnit whichUnit, JassItem whichItem, JassWidget target)
        {
            return this._UnitUseItemTarget(whichUnit, whichItem, target);
        }
        
        //native GetUnitX takes unit whichUnit returns real
        public delegate JassRealRet GetUnitXPrototype(JassUnit whichUnit);
        private GetUnitXPrototype _GetUnitX;
        public float GetUnitX(JassUnit whichUnit)
        {
            return this._GetUnitX(whichUnit);
        }
        
        //native GetUnitY takes unit whichUnit returns real
        public delegate JassRealRet GetUnitYPrototype(JassUnit whichUnit);
        private GetUnitYPrototype _GetUnitY;
        public float GetUnitY(JassUnit whichUnit)
        {
            return this._GetUnitY(whichUnit);
        }
        
        //native GetUnitLoc takes unit whichUnit returns location
        public delegate JassLocation GetUnitLocPrototype(JassUnit whichUnit);
        private GetUnitLocPrototype _GetUnitLoc;
        public JassLocation GetUnitLoc(JassUnit whichUnit)
        {
            return this._GetUnitLoc(whichUnit);
        }
        
        //native GetUnitFacing takes unit whichUnit returns real
        public delegate JassRealRet GetUnitFacingPrototype(JassUnit whichUnit);
        private GetUnitFacingPrototype _GetUnitFacing;
        public float GetUnitFacing(JassUnit whichUnit)
        {
            return this._GetUnitFacing(whichUnit);
        }
        
        //native GetUnitMoveSpeed takes unit whichUnit returns real
        public delegate JassRealRet GetUnitMoveSpeedPrototype(JassUnit whichUnit);
        private GetUnitMoveSpeedPrototype _GetUnitMoveSpeed;
        public float GetUnitMoveSpeed(JassUnit whichUnit)
        {
            return this._GetUnitMoveSpeed(whichUnit);
        }
        
        //native GetUnitDefaultMoveSpeed takes unit whichUnit returns real
        public delegate JassRealRet GetUnitDefaultMoveSpeedPrototype(JassUnit whichUnit);
        private GetUnitDefaultMoveSpeedPrototype _GetUnitDefaultMoveSpeed;
        public float GetUnitDefaultMoveSpeed(JassUnit whichUnit)
        {
            return this._GetUnitDefaultMoveSpeed(whichUnit);
        }
        
        //native GetUnitState takes unit whichUnit, unitstate whichUnitState returns real
        public delegate JassRealRet GetUnitStatePrototype(JassUnit whichUnit, JassUnitState whichUnitState);
        private GetUnitStatePrototype _GetUnitState;
        public float GetUnitState(JassUnit whichUnit, JassUnitState whichUnitState)
        {
            return this._GetUnitState(whichUnit, whichUnitState);
        }
        
        //native GetOwningPlayer takes unit whichUnit returns player
        public delegate JassPlayer GetOwningPlayerPrototype(JassUnit whichUnit);
        private GetOwningPlayerPrototype _GetOwningPlayer;
        public JassPlayer GetOwningPlayer(JassUnit whichUnit)
        {
            return this._GetOwningPlayer(whichUnit);
        }
        
        //native GetUnitTypeId takes unit whichUnit returns integer
        public delegate JassObjectId GetUnitTypeIdPrototype(JassUnit whichUnit);
        private GetUnitTypeIdPrototype _GetUnitTypeId;
        public JassObjectId GetUnitTypeId(JassUnit whichUnit)
        {
            return this._GetUnitTypeId(whichUnit);
        }
        
        //native GetUnitRace takes unit whichUnit returns race
        public delegate JassRace GetUnitRacePrototype(JassUnit whichUnit);
        private GetUnitRacePrototype _GetUnitRace;
        public JassRace GetUnitRace(JassUnit whichUnit)
        {
            return this._GetUnitRace(whichUnit);
        }
        
        //native GetUnitName takes unit whichUnit returns string
        public delegate JassStringRet GetUnitNamePrototype(JassUnit whichUnit);
        private GetUnitNamePrototype _GetUnitName;
        public string GetUnitName(JassUnit whichUnit)
        {
            return this._GetUnitName(whichUnit);
        }
        
        //native GetUnitFoodUsed takes unit whichUnit returns integer
        public delegate JassInteger GetUnitFoodUsedPrototype(JassUnit whichUnit);
        private GetUnitFoodUsedPrototype _GetUnitFoodUsed;
        public JassInteger GetUnitFoodUsed(JassUnit whichUnit)
        {
            return this._GetUnitFoodUsed(whichUnit);
        }
        
        //native GetUnitFoodMade takes unit whichUnit returns integer
        public delegate JassInteger GetUnitFoodMadePrototype(JassUnit whichUnit);
        private GetUnitFoodMadePrototype _GetUnitFoodMade;
        public JassInteger GetUnitFoodMade(JassUnit whichUnit)
        {
            return this._GetUnitFoodMade(whichUnit);
        }
        
        //native GetFoodMade takes integer unitId returns integer
        public delegate JassInteger GetFoodMadePrototype(JassObjectId unitId);
        private GetFoodMadePrototype _GetFoodMade;
        public JassInteger GetFoodMade(JassObjectId unitId)
        {
            return this._GetFoodMade(unitId);
        }
        
        //native GetFoodUsed takes integer unitId returns integer
        public delegate JassInteger GetFoodUsedPrototype(JassObjectId unitId);
        private GetFoodUsedPrototype _GetFoodUsed;
        public JassInteger GetFoodUsed(JassObjectId unitId)
        {
            return this._GetFoodUsed(unitId);
        }
        
        //native SetUnitUseFood takes unit whichUnit, boolean useFood returns nothing
        public delegate void SetUnitUseFoodPrototype(JassUnit whichUnit, JassBoolean useFood);
        private SetUnitUseFoodPrototype _SetUnitUseFood;
        public void SetUnitUseFood(JassUnit whichUnit, bool useFood)
        {
            this._SetUnitUseFood(whichUnit, useFood);
        }
        
        //native GetUnitRallyPoint takes unit whichUnit returns location
        public delegate JassLocation GetUnitRallyPointPrototype(JassUnit whichUnit);
        private GetUnitRallyPointPrototype _GetUnitRallyPoint;
        public JassLocation GetUnitRallyPoint(JassUnit whichUnit)
        {
            return this._GetUnitRallyPoint(whichUnit);
        }
        
        //native GetUnitRallyUnit takes unit whichUnit returns unit
        public delegate JassUnit GetUnitRallyUnitPrototype(JassUnit whichUnit);
        private GetUnitRallyUnitPrototype _GetUnitRallyUnit;
        public JassUnit GetUnitRallyUnit(JassUnit whichUnit)
        {
            return this._GetUnitRallyUnit(whichUnit);
        }
        
        //native GetUnitRallyDestructable takes unit whichUnit returns destructable
        public delegate JassDestructable GetUnitRallyDestructablePrototype(JassUnit whichUnit);
        private GetUnitRallyDestructablePrototype _GetUnitRallyDestructable;
        public JassDestructable GetUnitRallyDestructable(JassUnit whichUnit)
        {
            return this._GetUnitRallyDestructable(whichUnit);
        }
        
        //native IsUnitInGroup takes unit whichUnit, group whichGroup returns boolean
        public delegate JassBoolean IsUnitInGroupPrototype(JassUnit whichUnit, JassGroup whichGroup);
        private IsUnitInGroupPrototype _IsUnitInGroup;
        public bool IsUnitInGroup(JassUnit whichUnit, JassGroup whichGroup)
        {
            return this._IsUnitInGroup(whichUnit, whichGroup);
        }
        
        //native IsUnitInForce takes unit whichUnit, force whichForce returns boolean
        public delegate JassBoolean IsUnitInForcePrototype(JassUnit whichUnit, JassForce whichForce);
        private IsUnitInForcePrototype _IsUnitInForce;
        public bool IsUnitInForce(JassUnit whichUnit, JassForce whichForce)
        {
            return this._IsUnitInForce(whichUnit, whichForce);
        }
        
        //native IsUnitOwnedByPlayer takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitOwnedByPlayerPrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitOwnedByPlayerPrototype _IsUnitOwnedByPlayer;
        public bool IsUnitOwnedByPlayer(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitOwnedByPlayer(whichUnit, whichPlayer);
        }
        
        //native IsUnitAlly takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitAllyPrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitAllyPrototype _IsUnitAlly;
        public bool IsUnitAlly(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitAlly(whichUnit, whichPlayer);
        }
        
        //native IsUnitEnemy takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitEnemyPrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitEnemyPrototype _IsUnitEnemy;
        public bool IsUnitEnemy(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitEnemy(whichUnit, whichPlayer);
        }
        
        //native IsUnitVisible takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitVisiblePrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitVisiblePrototype _IsUnitVisible;
        public bool IsUnitVisible(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitVisible(whichUnit, whichPlayer);
        }
        
        //native IsUnitDetected takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitDetectedPrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitDetectedPrototype _IsUnitDetected;
        public bool IsUnitDetected(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitDetected(whichUnit, whichPlayer);
        }
        
        //native IsUnitInvisible takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitInvisiblePrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitInvisiblePrototype _IsUnitInvisible;
        public bool IsUnitInvisible(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitInvisible(whichUnit, whichPlayer);
        }
        
        //native IsUnitFogged takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitFoggedPrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitFoggedPrototype _IsUnitFogged;
        public bool IsUnitFogged(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitFogged(whichUnit, whichPlayer);
        }
        
        //native IsUnitMasked takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitMaskedPrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitMaskedPrototype _IsUnitMasked;
        public bool IsUnitMasked(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitMasked(whichUnit, whichPlayer);
        }
        
        //native IsUnitSelected takes unit whichUnit, player whichPlayer returns boolean
        public delegate JassBoolean IsUnitSelectedPrototype(JassUnit whichUnit, JassPlayer whichPlayer);
        private IsUnitSelectedPrototype _IsUnitSelected;
        public bool IsUnitSelected(JassUnit whichUnit, JassPlayer whichPlayer)
        {
            return this._IsUnitSelected(whichUnit, whichPlayer);
        }
        
        //native IsUnitRace takes unit whichUnit, race whichRace returns boolean
        public delegate JassBoolean IsUnitRacePrototype(JassUnit whichUnit, JassRace whichRace);
        private IsUnitRacePrototype _IsUnitRace;
        public bool IsUnitRace(JassUnit whichUnit, JassRace whichRace)
        {
            return this._IsUnitRace(whichUnit, whichRace);
        }
        
        //native IsUnitType takes unit whichUnit, unittype whichUnitType returns boolean
        public delegate JassBoolean IsUnitTypePrototype(JassUnit whichUnit, JassUnitType whichUnitType);
        private IsUnitTypePrototype _IsUnitType;
        public bool IsUnitType(JassUnit whichUnit, JassUnitType whichUnitType)
        {
            return this._IsUnitType(whichUnit, whichUnitType);
        }
        
        //native IsUnit takes unit whichUnit, unit whichSpecifiedUnit returns boolean
        public delegate JassBoolean IsUnitPrototype(JassUnit whichUnit, JassUnit whichSpecifiedUnit);
        private IsUnitPrototype _IsUnit;
        public bool IsUnit(JassUnit whichUnit, JassUnit whichSpecifiedUnit)
        {
            return this._IsUnit(whichUnit, whichSpecifiedUnit);
        }
        
        //native IsUnitInRange takes unit whichUnit, unit otherUnit, real distance returns boolean
        public delegate JassBoolean IsUnitInRangePrototype(JassUnit whichUnit, JassUnit otherUnit, JassRealArg distance);
        private IsUnitInRangePrototype _IsUnitInRange;
        public bool IsUnitInRange(JassUnit whichUnit, JassUnit otherUnit, float distance)
        {
            return this._IsUnitInRange(whichUnit, otherUnit, distance);
        }
        
        //native IsUnitInRangeXY takes unit whichUnit, real x, real y, real distance returns boolean
        public delegate JassBoolean IsUnitInRangeXYPrototype(JassUnit whichUnit, JassRealArg x, JassRealArg y, JassRealArg distance);
        private IsUnitInRangeXYPrototype _IsUnitInRangeXY;
        public bool IsUnitInRangeXY(JassUnit whichUnit, float x, float y, float distance)
        {
            return this._IsUnitInRangeXY(whichUnit, x, y, distance);
        }
        
        //native IsUnitInRangeLoc takes unit whichUnit, location whichLocation, real distance returns boolean
        public delegate JassBoolean IsUnitInRangeLocPrototype(JassUnit whichUnit, JassLocation whichLocation, JassRealArg distance);
        private IsUnitInRangeLocPrototype _IsUnitInRangeLoc;
        public bool IsUnitInRangeLoc(JassUnit whichUnit, JassLocation whichLocation, float distance)
        {
            return this._IsUnitInRangeLoc(whichUnit, whichLocation, distance);
        }
        
        //native IsUnitHidden takes unit whichUnit returns boolean
        public delegate JassBoolean IsUnitHiddenPrototype(JassUnit whichUnit);
        private IsUnitHiddenPrototype _IsUnitHidden;
        public bool IsUnitHidden(JassUnit whichUnit)
        {
            return this._IsUnitHidden(whichUnit);
        }
        
        //native IsUnitIllusion takes unit whichUnit returns boolean
        public delegate JassBoolean IsUnitIllusionPrototype(JassUnit whichUnit);
        private IsUnitIllusionPrototype _IsUnitIllusion;
        public bool IsUnitIllusion(JassUnit whichUnit)
        {
            return this._IsUnitIllusion(whichUnit);
        }
        
        //native IsUnitInTransport takes unit whichUnit, unit whichTransport returns boolean
        public delegate JassBoolean IsUnitInTransportPrototype(JassUnit whichUnit, JassUnit whichTransport);
        private IsUnitInTransportPrototype _IsUnitInTransport;
        public bool IsUnitInTransport(JassUnit whichUnit, JassUnit whichTransport)
        {
            return this._IsUnitInTransport(whichUnit, whichTransport);
        }
        
        //native IsUnitLoaded takes unit whichUnit returns boolean
        public delegate JassBoolean IsUnitLoadedPrototype(JassUnit whichUnit);
        private IsUnitLoadedPrototype _IsUnitLoaded;
        public bool IsUnitLoaded(JassUnit whichUnit)
        {
            return this._IsUnitLoaded(whichUnit);
        }
        
        //native IsHeroUnitId takes integer unitId returns boolean
        public delegate JassBoolean IsHeroUnitIdPrototype(JassObjectId unitId);
        private IsHeroUnitIdPrototype _IsHeroUnitId;
        public bool IsHeroUnitId(JassObjectId unitId)
        {
            return this._IsHeroUnitId(unitId);
        }
        
        //native IsUnitIdType takes integer unitId, unittype whichUnitType returns boolean
        public delegate JassBoolean IsUnitIdTypePrototype(JassObjectId unitId, JassUnitType whichUnitType);
        private IsUnitIdTypePrototype _IsUnitIdType;
        public bool IsUnitIdType(JassObjectId unitId, JassUnitType whichUnitType)
        {
            return this._IsUnitIdType(unitId, whichUnitType);
        }
        
        //native UnitShareVision takes unit whichUnit, player whichPlayer, boolean share returns nothing
        public delegate void UnitShareVisionPrototype(JassUnit whichUnit, JassPlayer whichPlayer, JassBoolean share);
        private UnitShareVisionPrototype _UnitShareVision;
        public void UnitShareVision(JassUnit whichUnit, JassPlayer whichPlayer, bool share)
        {
            this._UnitShareVision(whichUnit, whichPlayer, share);
        }
        
        //native UnitSuspendDecay takes unit whichUnit, boolean suspend returns nothing
        public delegate void UnitSuspendDecayPrototype(JassUnit whichUnit, JassBoolean suspend);
        private UnitSuspendDecayPrototype _UnitSuspendDecay;
        public void UnitSuspendDecay(JassUnit whichUnit, bool suspend)
        {
            this._UnitSuspendDecay(whichUnit, suspend);
        }
        
        //native UnitAddType takes unit whichUnit, unittype whichUnitType returns boolean
        public delegate JassBoolean UnitAddTypePrototype(JassUnit whichUnit, JassUnitType whichUnitType);
        private UnitAddTypePrototype _UnitAddType;
        public bool UnitAddType(JassUnit whichUnit, JassUnitType whichUnitType)
        {
            return this._UnitAddType(whichUnit, whichUnitType);
        }
        
        //native UnitRemoveType takes unit whichUnit, unittype whichUnitType returns boolean
        public delegate JassBoolean UnitRemoveTypePrototype(JassUnit whichUnit, JassUnitType whichUnitType);
        private UnitRemoveTypePrototype _UnitRemoveType;
        public bool UnitRemoveType(JassUnit whichUnit, JassUnitType whichUnitType)
        {
            return this._UnitRemoveType(whichUnit, whichUnitType);
        }
        
        //native UnitAddAbility takes unit whichUnit, integer abilityId returns boolean
        public delegate JassBoolean UnitAddAbilityPrototype(JassUnit whichUnit, JassObjectId abilityId);
        private UnitAddAbilityPrototype _UnitAddAbility;
        public bool UnitAddAbility(JassUnit whichUnit, JassObjectId abilityId)
        {
            return this._UnitAddAbility(whichUnit, abilityId);
        }
        
        //native UnitRemoveAbility takes unit whichUnit, integer abilityId returns boolean
        public delegate JassBoolean UnitRemoveAbilityPrototype(JassUnit whichUnit, JassObjectId abilityId);
        private UnitRemoveAbilityPrototype _UnitRemoveAbility;
        public bool UnitRemoveAbility(JassUnit whichUnit, JassObjectId abilityId)
        {
            return this._UnitRemoveAbility(whichUnit, abilityId);
        }
        
        //native UnitMakeAbilityPermanent takes unit whichUnit, boolean permanent, integer abilityId returns boolean
        public delegate JassBoolean UnitMakeAbilityPermanentPrototype(JassUnit whichUnit, JassBoolean permanent, JassObjectId abilityId);
        private UnitMakeAbilityPermanentPrototype _UnitMakeAbilityPermanent;
        public bool UnitMakeAbilityPermanent(JassUnit whichUnit, bool permanent, JassObjectId abilityId)
        {
            return this._UnitMakeAbilityPermanent(whichUnit, permanent, abilityId);
        }
        
        //native UnitRemoveBuffs takes unit whichUnit, boolean removePositive, boolean removeNegative returns nothing
        public delegate void UnitRemoveBuffsPrototype(JassUnit whichUnit, JassBoolean removePositive, JassBoolean removeNegative);
        private UnitRemoveBuffsPrototype _UnitRemoveBuffs;
        public void UnitRemoveBuffs(JassUnit whichUnit, bool removePositive, bool removeNegative)
        {
            this._UnitRemoveBuffs(whichUnit, removePositive, removeNegative);
        }
        
        //native UnitRemoveBuffsEx takes unit whichUnit, boolean removePositive, boolean removeNegative, boolean magic, boolean physical, boolean timedLife, boolean aura, boolean autoDispel returns nothing
        public delegate void UnitRemoveBuffsExPrototype(JassUnit whichUnit, JassBoolean removePositive, JassBoolean removeNegative, JassBoolean magic, JassBoolean physical, JassBoolean timedLife, JassBoolean aura, JassBoolean autoDispel);
        private UnitRemoveBuffsExPrototype _UnitRemoveBuffsEx;
        public void UnitRemoveBuffsEx(JassUnit whichUnit, bool removePositive, bool removeNegative, bool magic, bool physical, bool timedLife, bool aura, bool autoDispel)
        {
            this._UnitRemoveBuffsEx(whichUnit, removePositive, removeNegative, magic, physical, timedLife, aura, autoDispel);
        }
        
        //native UnitHasBuffsEx takes unit whichUnit, boolean removePositive, boolean removeNegative, boolean magic, boolean physical, boolean timedLife, boolean aura, boolean autoDispel returns boolean
        public delegate JassBoolean UnitHasBuffsExPrototype(JassUnit whichUnit, JassBoolean removePositive, JassBoolean removeNegative, JassBoolean magic, JassBoolean physical, JassBoolean timedLife, JassBoolean aura, JassBoolean autoDispel);
        private UnitHasBuffsExPrototype _UnitHasBuffsEx;
        public bool UnitHasBuffsEx(JassUnit whichUnit, bool removePositive, bool removeNegative, bool magic, bool physical, bool timedLife, bool aura, bool autoDispel)
        {
            return this._UnitHasBuffsEx(whichUnit, removePositive, removeNegative, magic, physical, timedLife, aura, autoDispel);
        }
        
        //native UnitCountBuffsEx takes unit whichUnit, boolean removePositive, boolean removeNegative, boolean magic, boolean physical, boolean timedLife, boolean aura, boolean autoDispel returns integer
        public delegate JassInteger UnitCountBuffsExPrototype(JassUnit whichUnit, JassBoolean removePositive, JassBoolean removeNegative, JassBoolean magic, JassBoolean physical, JassBoolean timedLife, JassBoolean aura, JassBoolean autoDispel);
        private UnitCountBuffsExPrototype _UnitCountBuffsEx;
        public JassInteger UnitCountBuffsEx(JassUnit whichUnit, bool removePositive, bool removeNegative, bool magic, bool physical, bool timedLife, bool aura, bool autoDispel)
        {
            return this._UnitCountBuffsEx(whichUnit, removePositive, removeNegative, magic, physical, timedLife, aura, autoDispel);
        }
        
        //native UnitAddSleep takes unit whichUnit, boolean add returns nothing
        public delegate void UnitAddSleepPrototype(JassUnit whichUnit, JassBoolean add);
        private UnitAddSleepPrototype _UnitAddSleep;
        public void UnitAddSleep(JassUnit whichUnit, bool add)
        {
            this._UnitAddSleep(whichUnit, add);
        }
        
        //native UnitCanSleep takes unit whichUnit returns boolean
        public delegate JassBoolean UnitCanSleepPrototype(JassUnit whichUnit);
        private UnitCanSleepPrototype _UnitCanSleep;
        public bool UnitCanSleep(JassUnit whichUnit)
        {
            return this._UnitCanSleep(whichUnit);
        }
        
        //native UnitAddSleepPerm takes unit whichUnit, boolean add returns nothing
        public delegate void UnitAddSleepPermPrototype(JassUnit whichUnit, JassBoolean add);
        private UnitAddSleepPermPrototype _UnitAddSleepPerm;
        public void UnitAddSleepPerm(JassUnit whichUnit, bool add)
        {
            this._UnitAddSleepPerm(whichUnit, add);
        }
        
        //native UnitCanSleepPerm takes unit whichUnit returns boolean
        public delegate JassBoolean UnitCanSleepPermPrototype(JassUnit whichUnit);
        private UnitCanSleepPermPrototype _UnitCanSleepPerm;
        public bool UnitCanSleepPerm(JassUnit whichUnit)
        {
            return this._UnitCanSleepPerm(whichUnit);
        }
        
        //native UnitIsSleeping takes unit whichUnit returns boolean
        public delegate JassBoolean UnitIsSleepingPrototype(JassUnit whichUnit);
        private UnitIsSleepingPrototype _UnitIsSleeping;
        public bool UnitIsSleeping(JassUnit whichUnit)
        {
            return this._UnitIsSleeping(whichUnit);
        }
        
        //native UnitWakeUp takes unit whichUnit returns nothing
        public delegate void UnitWakeUpPrototype(JassUnit whichUnit);
        private UnitWakeUpPrototype _UnitWakeUp;
        public void UnitWakeUp(JassUnit whichUnit)
        {
            this._UnitWakeUp(whichUnit);
        }
        
        //native UnitApplyTimedLife takes unit whichUnit, integer buffId, real duration returns nothing
        public delegate void UnitApplyTimedLifePrototype(JassUnit whichUnit, JassInteger buffId, JassRealArg duration);
        private UnitApplyTimedLifePrototype _UnitApplyTimedLife;
        public void UnitApplyTimedLife(JassUnit whichUnit, JassInteger buffId, float duration)
        {
            this._UnitApplyTimedLife(whichUnit, buffId, duration);
        }
        
        //native UnitIgnoreAlarm takes unit whichUnit, boolean flag returns boolean
        public delegate JassBoolean UnitIgnoreAlarmPrototype(JassUnit whichUnit, JassBoolean flag);
        private UnitIgnoreAlarmPrototype _UnitIgnoreAlarm;
        public bool UnitIgnoreAlarm(JassUnit whichUnit, bool flag)
        {
            return this._UnitIgnoreAlarm(whichUnit, flag);
        }
        
        //native UnitIgnoreAlarmToggled takes unit whichUnit returns boolean
        public delegate JassBoolean UnitIgnoreAlarmToggledPrototype(JassUnit whichUnit);
        private UnitIgnoreAlarmToggledPrototype _UnitIgnoreAlarmToggled;
        public bool UnitIgnoreAlarmToggled(JassUnit whichUnit)
        {
            return this._UnitIgnoreAlarmToggled(whichUnit);
        }
        
        //native UnitResetCooldown takes unit whichUnit returns nothing
        public delegate void UnitResetCooldownPrototype(JassUnit whichUnit);
        private UnitResetCooldownPrototype _UnitResetCooldown;
        public void UnitResetCooldown(JassUnit whichUnit)
        {
            this._UnitResetCooldown(whichUnit);
        }
        
        //native UnitSetConstructionProgress takes unit whichUnit, integer constructionPercentage returns nothing
        public delegate void UnitSetConstructionProgressPrototype(JassUnit whichUnit, JassInteger constructionPercentage);
        private UnitSetConstructionProgressPrototype _UnitSetConstructionProgress;
        public void UnitSetConstructionProgress(JassUnit whichUnit, JassInteger constructionPercentage)
        {
            this._UnitSetConstructionProgress(whichUnit, constructionPercentage);
        }
        
        //native UnitSetUpgradeProgress takes unit whichUnit, integer upgradePercentage returns nothing
        public delegate void UnitSetUpgradeProgressPrototype(JassUnit whichUnit, JassInteger upgradePercentage);
        private UnitSetUpgradeProgressPrototype _UnitSetUpgradeProgress;
        public void UnitSetUpgradeProgress(JassUnit whichUnit, JassInteger upgradePercentage)
        {
            this._UnitSetUpgradeProgress(whichUnit, upgradePercentage);
        }
        
        //native UnitPauseTimedLife takes unit whichUnit, boolean flag returns nothing
        public delegate void UnitPauseTimedLifePrototype(JassUnit whichUnit, JassBoolean flag);
        private UnitPauseTimedLifePrototype _UnitPauseTimedLife;
        public void UnitPauseTimedLife(JassUnit whichUnit, bool flag)
        {
            this._UnitPauseTimedLife(whichUnit, flag);
        }
        
        //native UnitSetUsesAltIcon takes unit whichUnit, boolean flag returns nothing
        public delegate void UnitSetUsesAltIconPrototype(JassUnit whichUnit, JassBoolean flag);
        private UnitSetUsesAltIconPrototype _UnitSetUsesAltIcon;
        public void UnitSetUsesAltIcon(JassUnit whichUnit, bool flag)
        {
            this._UnitSetUsesAltIcon(whichUnit, flag);
        }
        
        //native UnitDamagePoint takes unit whichUnit, real delay, real radius, real x, real y, real amount, boolean attack, boolean ranged, attacktype attackType, damagetype damageType, weapontype weaponType returns boolean
        public delegate JassBoolean UnitDamagePointPrototype(JassUnit whichUnit, JassRealArg delay, JassRealArg radius, JassRealArg x, JassRealArg y, JassRealArg amount, JassBoolean attack, JassBoolean ranged, JassAttackType attackType, JassDamageType damageType, JassWeaponType weaponType);
        private UnitDamagePointPrototype _UnitDamagePoint;
        public bool UnitDamagePoint(JassUnit whichUnit, float delay, float radius, float x, float y, float amount, bool attack, bool ranged, JassAttackType attackType, JassDamageType damageType, JassWeaponType weaponType)
        {
            return this._UnitDamagePoint(whichUnit, delay, radius, x, y, amount, attack, ranged, attackType, damageType, weaponType);
        }
        
        //native UnitDamageTarget takes unit whichUnit, widget target, real amount, boolean attack, boolean ranged, attacktype attackType, damagetype damageType, weapontype weaponType returns boolean
        public delegate JassBoolean UnitDamageTargetPrototype(JassUnit whichUnit, JassWidget target, JassRealArg amount, JassBoolean attack, JassBoolean ranged, JassAttackType attackType, JassDamageType damageType, JassWeaponType weaponType);
        private UnitDamageTargetPrototype _UnitDamageTarget;
        public bool UnitDamageTarget(JassUnit whichUnit, JassWidget target, float amount, bool attack, bool ranged, JassAttackType attackType, JassDamageType damageType, JassWeaponType weaponType)
        {
            return this._UnitDamageTarget(whichUnit, target, amount, attack, ranged, attackType, damageType, weaponType);
        }
        
        //native IssueImmediateOrder takes unit whichUnit, string order returns boolean
        public delegate JassBoolean IssueImmediateOrderPrototype(JassUnit whichUnit, JassStringArg order);
        private IssueImmediateOrderPrototype _IssueImmediateOrder;
        public bool IssueImmediateOrder(JassUnit whichUnit, string order)
        {
            return this._IssueImmediateOrder(whichUnit, order);
        }
        
        //native IssueImmediateOrderById takes unit whichUnit, integer order returns boolean
        public delegate JassBoolean IssueImmediateOrderByIdPrototype(JassUnit whichUnit, JassOrder order);
        private IssueImmediateOrderByIdPrototype _IssueImmediateOrderById;
        public bool IssueImmediateOrderById(JassUnit whichUnit, JassOrder order)
        {
            return this._IssueImmediateOrderById(whichUnit, order);
        }
        
        //native IssuePointOrder takes unit whichUnit, string order, real x, real y returns boolean
        public delegate JassBoolean IssuePointOrderPrototype(JassUnit whichUnit, JassStringArg order, JassRealArg x, JassRealArg y);
        private IssuePointOrderPrototype _IssuePointOrder;
        public bool IssuePointOrder(JassUnit whichUnit, string order, float x, float y)
        {
            return this._IssuePointOrder(whichUnit, order, x, y);
        }
        
        //native IssuePointOrderLoc takes unit whichUnit, string order, location whichLocation returns boolean
        public delegate JassBoolean IssuePointOrderLocPrototype(JassUnit whichUnit, JassStringArg order, JassLocation whichLocation);
        private IssuePointOrderLocPrototype _IssuePointOrderLoc;
        public bool IssuePointOrderLoc(JassUnit whichUnit, string order, JassLocation whichLocation)
        {
            return this._IssuePointOrderLoc(whichUnit, order, whichLocation);
        }
        
        //native IssuePointOrderById takes unit whichUnit, integer order, real x, real y returns boolean
        public delegate JassBoolean IssuePointOrderByIdPrototype(JassUnit whichUnit, JassOrder order, JassRealArg x, JassRealArg y);
        private IssuePointOrderByIdPrototype _IssuePointOrderById;
        public bool IssuePointOrderById(JassUnit whichUnit, JassOrder order, float x, float y)
        {
            return this._IssuePointOrderById(whichUnit, order, x, y);
        }
        
        //native IssuePointOrderByIdLoc takes unit whichUnit, integer order, location whichLocation returns boolean
        public delegate JassBoolean IssuePointOrderByIdLocPrototype(JassUnit whichUnit, JassOrder order, JassLocation whichLocation);
        private IssuePointOrderByIdLocPrototype _IssuePointOrderByIdLoc;
        public bool IssuePointOrderByIdLoc(JassUnit whichUnit, JassOrder order, JassLocation whichLocation)
        {
            return this._IssuePointOrderByIdLoc(whichUnit, order, whichLocation);
        }
        
        //native IssueTargetOrder takes unit whichUnit, string order, widget targetWidget returns boolean
        public delegate JassBoolean IssueTargetOrderPrototype(JassUnit whichUnit, JassStringArg order, JassWidget targetWidget);
        private IssueTargetOrderPrototype _IssueTargetOrder;
        public bool IssueTargetOrder(JassUnit whichUnit, string order, JassWidget targetWidget)
        {
            return this._IssueTargetOrder(whichUnit, order, targetWidget);
        }
        
        //native IssueTargetOrderById takes unit whichUnit, integer order, widget targetWidget returns boolean
        public delegate JassBoolean IssueTargetOrderByIdPrototype(JassUnit whichUnit, JassOrder order, JassWidget targetWidget);
        private IssueTargetOrderByIdPrototype _IssueTargetOrderById;
        public bool IssueTargetOrderById(JassUnit whichUnit, JassOrder order, JassWidget targetWidget)
        {
            return this._IssueTargetOrderById(whichUnit, order, targetWidget);
        }
        
        //native IssueInstantPointOrder takes unit whichUnit, string order, real x, real y, widget instantTargetWidget returns boolean
        public delegate JassBoolean IssueInstantPointOrderPrototype(JassUnit whichUnit, JassStringArg order, JassRealArg x, JassRealArg y, JassWidget instantTargetWidget);
        private IssueInstantPointOrderPrototype _IssueInstantPointOrder;
        public bool IssueInstantPointOrder(JassUnit whichUnit, string order, float x, float y, JassWidget instantTargetWidget)
        {
            return this._IssueInstantPointOrder(whichUnit, order, x, y, instantTargetWidget);
        }
        
        //native IssueInstantPointOrderById takes unit whichUnit, integer order, real x, real y, widget instantTargetWidget returns boolean
        public delegate JassBoolean IssueInstantPointOrderByIdPrototype(JassUnit whichUnit, JassOrder order, JassRealArg x, JassRealArg y, JassWidget instantTargetWidget);
        private IssueInstantPointOrderByIdPrototype _IssueInstantPointOrderById;
        public bool IssueInstantPointOrderById(JassUnit whichUnit, JassOrder order, float x, float y, JassWidget instantTargetWidget)
        {
            return this._IssueInstantPointOrderById(whichUnit, order, x, y, instantTargetWidget);
        }
        
        //native IssueInstantTargetOrder takes unit whichUnit, string order, widget targetWidget, widget instantTargetWidget returns boolean
        public delegate JassBoolean IssueInstantTargetOrderPrototype(JassUnit whichUnit, JassStringArg order, JassWidget targetWidget, JassWidget instantTargetWidget);
        private IssueInstantTargetOrderPrototype _IssueInstantTargetOrder;
        public bool IssueInstantTargetOrder(JassUnit whichUnit, string order, JassWidget targetWidget, JassWidget instantTargetWidget)
        {
            return this._IssueInstantTargetOrder(whichUnit, order, targetWidget, instantTargetWidget);
        }
        
        //native IssueInstantTargetOrderById takes unit whichUnit, integer order, widget targetWidget, widget instantTargetWidget returns boolean
        public delegate JassBoolean IssueInstantTargetOrderByIdPrototype(JassUnit whichUnit, JassOrder order, JassWidget targetWidget, JassWidget instantTargetWidget);
        private IssueInstantTargetOrderByIdPrototype _IssueInstantTargetOrderById;
        public bool IssueInstantTargetOrderById(JassUnit whichUnit, JassOrder order, JassWidget targetWidget, JassWidget instantTargetWidget)
        {
            return this._IssueInstantTargetOrderById(whichUnit, order, targetWidget, instantTargetWidget);
        }
        
        //native IssueBuildOrder takes unit whichPeon, string unitToBuild, real x, real y returns boolean
        public delegate JassBoolean IssueBuildOrderPrototype(JassUnit whichPeon, JassStringArg unitToBuild, JassRealArg x, JassRealArg y);
        private IssueBuildOrderPrototype _IssueBuildOrder;
        public bool IssueBuildOrder(JassUnit whichPeon, string unitToBuild, float x, float y)
        {
            return this._IssueBuildOrder(whichPeon, unitToBuild, x, y);
        }
        
        //native IssueBuildOrderById takes unit whichPeon, integer unitId, real x, real y returns boolean
        public delegate JassBoolean IssueBuildOrderByIdPrototype(JassUnit whichPeon, JassObjectId unitId, JassRealArg x, JassRealArg y);
        private IssueBuildOrderByIdPrototype _IssueBuildOrderById;
        public bool IssueBuildOrderById(JassUnit whichPeon, JassObjectId unitId, float x, float y)
        {
            return this._IssueBuildOrderById(whichPeon, unitId, x, y);
        }
        
        //native IssueNeutralImmediateOrder takes player forWhichPlayer, unit neutralStructure, string unitToBuild returns boolean
        public delegate JassBoolean IssueNeutralImmediateOrderPrototype(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassStringArg unitToBuild);
        private IssueNeutralImmediateOrderPrototype _IssueNeutralImmediateOrder;
        public bool IssueNeutralImmediateOrder(JassPlayer forWhichPlayer, JassUnit neutralStructure, string unitToBuild)
        {
            return this._IssueNeutralImmediateOrder(forWhichPlayer, neutralStructure, unitToBuild);
        }
        
        //native IssueNeutralImmediateOrderById takes player forWhichPlayer, unit neutralStructure, integer unitId returns boolean
        public delegate JassBoolean IssueNeutralImmediateOrderByIdPrototype(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassObjectId unitId);
        private IssueNeutralImmediateOrderByIdPrototype _IssueNeutralImmediateOrderById;
        public bool IssueNeutralImmediateOrderById(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassObjectId unitId)
        {
            return this._IssueNeutralImmediateOrderById(forWhichPlayer, neutralStructure, unitId);
        }
        
        //native IssueNeutralPointOrder takes player forWhichPlayer, unit neutralStructure, string unitToBuild, real x, real y returns boolean
        public delegate JassBoolean IssueNeutralPointOrderPrototype(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassStringArg unitToBuild, JassRealArg x, JassRealArg y);
        private IssueNeutralPointOrderPrototype _IssueNeutralPointOrder;
        public bool IssueNeutralPointOrder(JassPlayer forWhichPlayer, JassUnit neutralStructure, string unitToBuild, float x, float y)
        {
            return this._IssueNeutralPointOrder(forWhichPlayer, neutralStructure, unitToBuild, x, y);
        }
        
        //native IssueNeutralPointOrderById takes player forWhichPlayer, unit neutralStructure, integer unitId, real x, real y returns boolean
        public delegate JassBoolean IssueNeutralPointOrderByIdPrototype(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassObjectId unitId, JassRealArg x, JassRealArg y);
        private IssueNeutralPointOrderByIdPrototype _IssueNeutralPointOrderById;
        public bool IssueNeutralPointOrderById(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassObjectId unitId, float x, float y)
        {
            return this._IssueNeutralPointOrderById(forWhichPlayer, neutralStructure, unitId, x, y);
        }
        
        //native IssueNeutralTargetOrder takes player forWhichPlayer, unit neutralStructure, string unitToBuild, widget target returns boolean
        public delegate JassBoolean IssueNeutralTargetOrderPrototype(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassStringArg unitToBuild, JassWidget target);
        private IssueNeutralTargetOrderPrototype _IssueNeutralTargetOrder;
        public bool IssueNeutralTargetOrder(JassPlayer forWhichPlayer, JassUnit neutralStructure, string unitToBuild, JassWidget target)
        {
            return this._IssueNeutralTargetOrder(forWhichPlayer, neutralStructure, unitToBuild, target);
        }
        
        //native IssueNeutralTargetOrderById takes player forWhichPlayer, unit neutralStructure, integer unitId, widget target returns boolean
        public delegate JassBoolean IssueNeutralTargetOrderByIdPrototype(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassObjectId unitId, JassWidget target);
        private IssueNeutralTargetOrderByIdPrototype _IssueNeutralTargetOrderById;
        public bool IssueNeutralTargetOrderById(JassPlayer forWhichPlayer, JassUnit neutralStructure, JassObjectId unitId, JassWidget target)
        {
            return this._IssueNeutralTargetOrderById(forWhichPlayer, neutralStructure, unitId, target);
        }
        
        //native GetUnitCurrentOrder takes unit whichUnit returns integer
        public delegate JassInteger GetUnitCurrentOrderPrototype(JassUnit whichUnit);
        private GetUnitCurrentOrderPrototype _GetUnitCurrentOrder;
        public JassInteger GetUnitCurrentOrder(JassUnit whichUnit)
        {
            return this._GetUnitCurrentOrder(whichUnit);
        }
        
        //native SetResourceAmount takes unit whichUnit, integer amount returns nothing
        public delegate void SetResourceAmountPrototype(JassUnit whichUnit, JassInteger amount);
        private SetResourceAmountPrototype _SetResourceAmount;
        public void SetResourceAmount(JassUnit whichUnit, JassInteger amount)
        {
            this._SetResourceAmount(whichUnit, amount);
        }
        
        //native AddResourceAmount takes unit whichUnit, integer amount returns nothing
        public delegate void AddResourceAmountPrototype(JassUnit whichUnit, JassInteger amount);
        private AddResourceAmountPrototype _AddResourceAmount;
        public void AddResourceAmount(JassUnit whichUnit, JassInteger amount)
        {
            this._AddResourceAmount(whichUnit, amount);
        }
        
        //native GetResourceAmount takes unit whichUnit returns integer
        public delegate JassInteger GetResourceAmountPrototype(JassUnit whichUnit);
        private GetResourceAmountPrototype _GetResourceAmount;
        public JassInteger GetResourceAmount(JassUnit whichUnit)
        {
            return this._GetResourceAmount(whichUnit);
        }
        
        //native WaygateGetDestinationX takes unit waygate returns real
        public delegate JassRealRet WaygateGetDestinationXPrototype(JassUnit waygate);
        private WaygateGetDestinationXPrototype _WaygateGetDestinationX;
        public float WaygateGetDestinationX(JassUnit waygate)
        {
            return this._WaygateGetDestinationX(waygate);
        }
        
        //native WaygateGetDestinationY takes unit waygate returns real
        public delegate JassRealRet WaygateGetDestinationYPrototype(JassUnit waygate);
        private WaygateGetDestinationYPrototype _WaygateGetDestinationY;
        public float WaygateGetDestinationY(JassUnit waygate)
        {
            return this._WaygateGetDestinationY(waygate);
        }
        
        //native WaygateSetDestination takes unit waygate, real x, real y returns nothing
        public delegate void WaygateSetDestinationPrototype(JassUnit waygate, JassRealArg x, JassRealArg y);
        private WaygateSetDestinationPrototype _WaygateSetDestination;
        public void WaygateSetDestination(JassUnit waygate, float x, float y)
        {
            this._WaygateSetDestination(waygate, x, y);
        }
        
        //native WaygateActivate takes unit waygate, boolean activate returns nothing
        public delegate void WaygateActivatePrototype(JassUnit waygate, JassBoolean activate);
        private WaygateActivatePrototype _WaygateActivate;
        public void WaygateActivate(JassUnit waygate, bool activate)
        {
            this._WaygateActivate(waygate, activate);
        }
        
        //native WaygateIsActive takes unit waygate returns boolean
        public delegate JassBoolean WaygateIsActivePrototype(JassUnit waygate);
        private WaygateIsActivePrototype _WaygateIsActive;
        public bool WaygateIsActive(JassUnit waygate)
        {
            return this._WaygateIsActive(waygate);
        }
        
        //native AddItemToAllStock takes integer itemId, integer currentStock, integer stockMax returns nothing
        public delegate void AddItemToAllStockPrototype(JassObjectId itemId, JassInteger currentStock, JassInteger stockMax);
        private AddItemToAllStockPrototype _AddItemToAllStock;
        public void AddItemToAllStock(JassObjectId itemId, JassInteger currentStock, JassInteger stockMax)
        {
            this._AddItemToAllStock(itemId, currentStock, stockMax);
        }
        
        //native AddItemToStock takes unit whichUnit, integer itemId, integer currentStock, integer stockMax returns nothing
        public delegate void AddItemToStockPrototype(JassUnit whichUnit, JassObjectId itemId, JassInteger currentStock, JassInteger stockMax);
        private AddItemToStockPrototype _AddItemToStock;
        public void AddItemToStock(JassUnit whichUnit, JassObjectId itemId, JassInteger currentStock, JassInteger stockMax)
        {
            this._AddItemToStock(whichUnit, itemId, currentStock, stockMax);
        }
        
        //native AddUnitToAllStock takes integer unitId, integer currentStock, integer stockMax returns nothing
        public delegate void AddUnitToAllStockPrototype(JassObjectId unitId, JassInteger currentStock, JassInteger stockMax);
        private AddUnitToAllStockPrototype _AddUnitToAllStock;
        public void AddUnitToAllStock(JassObjectId unitId, JassInteger currentStock, JassInteger stockMax)
        {
            this._AddUnitToAllStock(unitId, currentStock, stockMax);
        }
        
        //native AddUnitToStock takes unit whichUnit, integer unitId, integer currentStock, integer stockMax returns nothing
        public delegate void AddUnitToStockPrototype(JassUnit whichUnit, JassObjectId unitId, JassInteger currentStock, JassInteger stockMax);
        private AddUnitToStockPrototype _AddUnitToStock;
        public void AddUnitToStock(JassUnit whichUnit, JassObjectId unitId, JassInteger currentStock, JassInteger stockMax)
        {
            this._AddUnitToStock(whichUnit, unitId, currentStock, stockMax);
        }
        
        //native RemoveItemFromAllStock takes integer itemId returns nothing
        public delegate void RemoveItemFromAllStockPrototype(JassObjectId itemId);
        private RemoveItemFromAllStockPrototype _RemoveItemFromAllStock;
        public void RemoveItemFromAllStock(JassObjectId itemId)
        {
            this._RemoveItemFromAllStock(itemId);
        }
        
        //native RemoveItemFromStock takes unit whichUnit, integer itemId returns nothing
        public delegate void RemoveItemFromStockPrototype(JassUnit whichUnit, JassObjectId itemId);
        private RemoveItemFromStockPrototype _RemoveItemFromStock;
        public void RemoveItemFromStock(JassUnit whichUnit, JassObjectId itemId)
        {
            this._RemoveItemFromStock(whichUnit, itemId);
        }
        
        //native RemoveUnitFromAllStock takes integer unitId returns nothing
        public delegate void RemoveUnitFromAllStockPrototype(JassObjectId unitId);
        private RemoveUnitFromAllStockPrototype _RemoveUnitFromAllStock;
        public void RemoveUnitFromAllStock(JassObjectId unitId)
        {
            this._RemoveUnitFromAllStock(unitId);
        }
        
        //native RemoveUnitFromStock takes unit whichUnit, integer unitId returns nothing
        public delegate void RemoveUnitFromStockPrototype(JassUnit whichUnit, JassObjectId unitId);
        private RemoveUnitFromStockPrototype _RemoveUnitFromStock;
        public void RemoveUnitFromStock(JassUnit whichUnit, JassObjectId unitId)
        {
            this._RemoveUnitFromStock(whichUnit, unitId);
        }
        
        //native SetAllItemTypeSlots takes integer slots returns nothing
        public delegate void SetAllItemTypeSlotsPrototype(JassInteger slots);
        private SetAllItemTypeSlotsPrototype _SetAllItemTypeSlots;
        public void SetAllItemTypeSlots(JassInteger slots)
        {
            this._SetAllItemTypeSlots(slots);
        }
        
        //native SetAllUnitTypeSlots takes integer slots returns nothing
        public delegate void SetAllUnitTypeSlotsPrototype(JassInteger slots);
        private SetAllUnitTypeSlotsPrototype _SetAllUnitTypeSlots;
        public void SetAllUnitTypeSlots(JassInteger slots)
        {
            this._SetAllUnitTypeSlots(slots);
        }
        
        //native SetItemTypeSlots takes unit whichUnit, integer slots returns nothing
        public delegate void SetItemTypeSlotsPrototype(JassUnit whichUnit, JassInteger slots);
        private SetItemTypeSlotsPrototype _SetItemTypeSlots;
        public void SetItemTypeSlots(JassUnit whichUnit, JassInteger slots)
        {
            this._SetItemTypeSlots(whichUnit, slots);
        }
        
        //native SetUnitTypeSlots takes unit whichUnit, integer slots returns nothing
        public delegate void SetUnitTypeSlotsPrototype(JassUnit whichUnit, JassInteger slots);
        private SetUnitTypeSlotsPrototype _SetUnitTypeSlots;
        public void SetUnitTypeSlots(JassUnit whichUnit, JassInteger slots)
        {
            this._SetUnitTypeSlots(whichUnit, slots);
        }
        
        //native GetUnitUserData takes unit whichUnit returns integer
        public delegate JassInteger GetUnitUserDataPrototype(JassUnit whichUnit);
        private GetUnitUserDataPrototype _GetUnitUserData;
        public JassInteger GetUnitUserData(JassUnit whichUnit)
        {
            return this._GetUnitUserData(whichUnit);
        }
        
        //native SetUnitUserData takes unit whichUnit, integer data returns nothing
        public delegate void SetUnitUserDataPrototype(JassUnit whichUnit, JassInteger data);
        private SetUnitUserDataPrototype _SetUnitUserData;
        public void SetUnitUserData(JassUnit whichUnit, JassInteger data)
        {
            this._SetUnitUserData(whichUnit, data);
        }
        
        //native Player takes integer number returns player
        public delegate JassPlayer PlayerPrototype(JassInteger number);
        private PlayerPrototype _Player;
        public JassPlayer Player(JassInteger number)
        {
            return this._Player(number);
        }
        
        //native GetLocalPlayer takes nothing returns player
        public delegate JassPlayer GetLocalPlayerPrototype();
        private GetLocalPlayerPrototype _GetLocalPlayer;
        public JassPlayer GetLocalPlayer()
        {
            return this._GetLocalPlayer();
        }
        
        //native IsPlayerAlly takes player whichPlayer, player otherPlayer returns boolean
        public delegate JassBoolean IsPlayerAllyPrototype(JassPlayer whichPlayer, JassPlayer otherPlayer);
        private IsPlayerAllyPrototype _IsPlayerAlly;
        public bool IsPlayerAlly(JassPlayer whichPlayer, JassPlayer otherPlayer)
        {
            return this._IsPlayerAlly(whichPlayer, otherPlayer);
        }
        
        //native IsPlayerEnemy takes player whichPlayer, player otherPlayer returns boolean
        public delegate JassBoolean IsPlayerEnemyPrototype(JassPlayer whichPlayer, JassPlayer otherPlayer);
        private IsPlayerEnemyPrototype _IsPlayerEnemy;
        public bool IsPlayerEnemy(JassPlayer whichPlayer, JassPlayer otherPlayer)
        {
            return this._IsPlayerEnemy(whichPlayer, otherPlayer);
        }
        
        //native IsPlayerInForce takes player whichPlayer, force whichForce returns boolean
        public delegate JassBoolean IsPlayerInForcePrototype(JassPlayer whichPlayer, JassForce whichForce);
        private IsPlayerInForcePrototype _IsPlayerInForce;
        public bool IsPlayerInForce(JassPlayer whichPlayer, JassForce whichForce)
        {
            return this._IsPlayerInForce(whichPlayer, whichForce);
        }
        
        //native IsPlayerObserver takes player whichPlayer returns boolean
        public delegate JassBoolean IsPlayerObserverPrototype(JassPlayer whichPlayer);
        private IsPlayerObserverPrototype _IsPlayerObserver;
        public bool IsPlayerObserver(JassPlayer whichPlayer)
        {
            return this._IsPlayerObserver(whichPlayer);
        }
        
        //native IsVisibleToPlayer takes real x, real y, player whichPlayer returns boolean
        public delegate JassBoolean IsVisibleToPlayerPrototype(JassRealArg x, JassRealArg y, JassPlayer whichPlayer);
        private IsVisibleToPlayerPrototype _IsVisibleToPlayer;
        public bool IsVisibleToPlayer(float x, float y, JassPlayer whichPlayer)
        {
            return this._IsVisibleToPlayer(x, y, whichPlayer);
        }
        
        //native IsLocationVisibleToPlayer takes location whichLocation, player whichPlayer returns boolean
        public delegate JassBoolean IsLocationVisibleToPlayerPrototype(JassLocation whichLocation, JassPlayer whichPlayer);
        private IsLocationVisibleToPlayerPrototype _IsLocationVisibleToPlayer;
        public bool IsLocationVisibleToPlayer(JassLocation whichLocation, JassPlayer whichPlayer)
        {
            return this._IsLocationVisibleToPlayer(whichLocation, whichPlayer);
        }
        
        //native IsFoggedToPlayer takes real x, real y, player whichPlayer returns boolean
        public delegate JassBoolean IsFoggedToPlayerPrototype(JassRealArg x, JassRealArg y, JassPlayer whichPlayer);
        private IsFoggedToPlayerPrototype _IsFoggedToPlayer;
        public bool IsFoggedToPlayer(float x, float y, JassPlayer whichPlayer)
        {
            return this._IsFoggedToPlayer(x, y, whichPlayer);
        }
        
        //native IsLocationFoggedToPlayer takes location whichLocation, player whichPlayer returns boolean
        public delegate JassBoolean IsLocationFoggedToPlayerPrototype(JassLocation whichLocation, JassPlayer whichPlayer);
        private IsLocationFoggedToPlayerPrototype _IsLocationFoggedToPlayer;
        public bool IsLocationFoggedToPlayer(JassLocation whichLocation, JassPlayer whichPlayer)
        {
            return this._IsLocationFoggedToPlayer(whichLocation, whichPlayer);
        }
        
        //native IsMaskedToPlayer takes real x, real y, player whichPlayer returns boolean
        public delegate JassBoolean IsMaskedToPlayerPrototype(JassRealArg x, JassRealArg y, JassPlayer whichPlayer);
        private IsMaskedToPlayerPrototype _IsMaskedToPlayer;
        public bool IsMaskedToPlayer(float x, float y, JassPlayer whichPlayer)
        {
            return this._IsMaskedToPlayer(x, y, whichPlayer);
        }
        
        //native IsLocationMaskedToPlayer takes location whichLocation, player whichPlayer returns boolean
        public delegate JassBoolean IsLocationMaskedToPlayerPrototype(JassLocation whichLocation, JassPlayer whichPlayer);
        private IsLocationMaskedToPlayerPrototype _IsLocationMaskedToPlayer;
        public bool IsLocationMaskedToPlayer(JassLocation whichLocation, JassPlayer whichPlayer)
        {
            return this._IsLocationMaskedToPlayer(whichLocation, whichPlayer);
        }
        
        //native GetPlayerRace takes player whichPlayer returns race
        public delegate JassRace GetPlayerRacePrototype(JassPlayer whichPlayer);
        private GetPlayerRacePrototype _GetPlayerRace;
        public JassRace GetPlayerRace(JassPlayer whichPlayer)
        {
            return this._GetPlayerRace(whichPlayer);
        }
        
        //native GetPlayerId takes player whichPlayer returns integer
        public delegate JassInteger GetPlayerIdPrototype(JassPlayer whichPlayer);
        private GetPlayerIdPrototype _GetPlayerId;
        public JassInteger GetPlayerId(JassPlayer whichPlayer)
        {
            return this._GetPlayerId(whichPlayer);
        }
        
        //native GetPlayerUnitCount takes player whichPlayer, boolean includeIncomplete returns integer
        public delegate JassInteger GetPlayerUnitCountPrototype(JassPlayer whichPlayer, JassBoolean includeIncomplete);
        private GetPlayerUnitCountPrototype _GetPlayerUnitCount;
        public JassInteger GetPlayerUnitCount(JassPlayer whichPlayer, bool includeIncomplete)
        {
            return this._GetPlayerUnitCount(whichPlayer, includeIncomplete);
        }
        
        //native GetPlayerTypedUnitCount takes player whichPlayer, string unitName, boolean includeIncomplete, boolean includeUpgrades returns integer
        public delegate JassInteger GetPlayerTypedUnitCountPrototype(JassPlayer whichPlayer, JassStringArg unitName, JassBoolean includeIncomplete, JassBoolean includeUpgrades);
        private GetPlayerTypedUnitCountPrototype _GetPlayerTypedUnitCount;
        public JassInteger GetPlayerTypedUnitCount(JassPlayer whichPlayer, string unitName, bool includeIncomplete, bool includeUpgrades)
        {
            return this._GetPlayerTypedUnitCount(whichPlayer, unitName, includeIncomplete, includeUpgrades);
        }
        
        //native GetPlayerStructureCount takes player whichPlayer, boolean includeIncomplete returns integer
        public delegate JassInteger GetPlayerStructureCountPrototype(JassPlayer whichPlayer, JassBoolean includeIncomplete);
        private GetPlayerStructureCountPrototype _GetPlayerStructureCount;
        public JassInteger GetPlayerStructureCount(JassPlayer whichPlayer, bool includeIncomplete)
        {
            return this._GetPlayerStructureCount(whichPlayer, includeIncomplete);
        }
        
        //native GetPlayerState takes player whichPlayer, playerstate whichPlayerState returns integer
        public delegate JassInteger GetPlayerStatePrototype(JassPlayer whichPlayer, JassPlayerState whichPlayerState);
        private GetPlayerStatePrototype _GetPlayerState;
        public JassInteger GetPlayerState(JassPlayer whichPlayer, JassPlayerState whichPlayerState)
        {
            return this._GetPlayerState(whichPlayer, whichPlayerState);
        }
        
        //native GetPlayerScore takes player whichPlayer, playerscore whichPlayerScore returns integer
        public delegate JassInteger GetPlayerScorePrototype(JassPlayer whichPlayer, JassPlayerScore whichPlayerScore);
        private GetPlayerScorePrototype _GetPlayerScore;
        public JassInteger GetPlayerScore(JassPlayer whichPlayer, JassPlayerScore whichPlayerScore)
        {
            return this._GetPlayerScore(whichPlayer, whichPlayerScore);
        }
        
        //native GetPlayerAlliance takes player sourcePlayer, player otherPlayer, alliancetype whichAllianceSetting returns boolean
        public delegate JassBoolean GetPlayerAlliancePrototype(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassAllianceType whichAllianceSetting);
        private GetPlayerAlliancePrototype _GetPlayerAlliance;
        public bool GetPlayerAlliance(JassPlayer sourcePlayer, JassPlayer otherPlayer, JassAllianceType whichAllianceSetting)
        {
            return this._GetPlayerAlliance(sourcePlayer, otherPlayer, whichAllianceSetting);
        }
        
        //native GetPlayerHandicap takes player whichPlayer returns real
        public delegate JassRealRet GetPlayerHandicapPrototype(JassPlayer whichPlayer);
        private GetPlayerHandicapPrototype _GetPlayerHandicap;
        public float GetPlayerHandicap(JassPlayer whichPlayer)
        {
            return this._GetPlayerHandicap(whichPlayer);
        }
        
        //native GetPlayerHandicapXP takes player whichPlayer returns real
        public delegate JassRealRet GetPlayerHandicapXPPrototype(JassPlayer whichPlayer);
        private GetPlayerHandicapXPPrototype _GetPlayerHandicapXP;
        public float GetPlayerHandicapXP(JassPlayer whichPlayer)
        {
            return this._GetPlayerHandicapXP(whichPlayer);
        }
        
        //native SetPlayerHandicap takes player whichPlayer, real handicap returns nothing
        public delegate void SetPlayerHandicapPrototype(JassPlayer whichPlayer, JassRealArg handicap);
        private SetPlayerHandicapPrototype _SetPlayerHandicap;
        public void SetPlayerHandicap(JassPlayer whichPlayer, float handicap)
        {
            this._SetPlayerHandicap(whichPlayer, handicap);
        }
        
        //native SetPlayerHandicapXP takes player whichPlayer, real handicap returns nothing
        public delegate void SetPlayerHandicapXPPrototype(JassPlayer whichPlayer, JassRealArg handicap);
        private SetPlayerHandicapXPPrototype _SetPlayerHandicapXP;
        public void SetPlayerHandicapXP(JassPlayer whichPlayer, float handicap)
        {
            this._SetPlayerHandicapXP(whichPlayer, handicap);
        }
        
        //native SetPlayerTechMaxAllowed takes player whichPlayer, integer techid, integer maximum returns nothing
        public delegate void SetPlayerTechMaxAllowedPrototype(JassPlayer whichPlayer, JassInteger techid, JassInteger maximum);
        private SetPlayerTechMaxAllowedPrototype _SetPlayerTechMaxAllowed;
        public void SetPlayerTechMaxAllowed(JassPlayer whichPlayer, JassInteger techid, JassInteger maximum)
        {
            this._SetPlayerTechMaxAllowed(whichPlayer, techid, maximum);
        }
        
        //native GetPlayerTechMaxAllowed takes player whichPlayer, integer techid returns integer
        public delegate JassInteger GetPlayerTechMaxAllowedPrototype(JassPlayer whichPlayer, JassInteger techid);
        private GetPlayerTechMaxAllowedPrototype _GetPlayerTechMaxAllowed;
        public JassInteger GetPlayerTechMaxAllowed(JassPlayer whichPlayer, JassInteger techid)
        {
            return this._GetPlayerTechMaxAllowed(whichPlayer, techid);
        }
        
        //native AddPlayerTechResearched takes player whichPlayer, integer techid, integer levels returns nothing
        public delegate void AddPlayerTechResearchedPrototype(JassPlayer whichPlayer, JassInteger techid, JassInteger levels);
        private AddPlayerTechResearchedPrototype _AddPlayerTechResearched;
        public void AddPlayerTechResearched(JassPlayer whichPlayer, JassInteger techid, JassInteger levels)
        {
            this._AddPlayerTechResearched(whichPlayer, techid, levels);
        }
        
        //native SetPlayerTechResearched takes player whichPlayer, integer techid, integer setToLevel returns nothing
        public delegate void SetPlayerTechResearchedPrototype(JassPlayer whichPlayer, JassInteger techid, JassInteger setToLevel);
        private SetPlayerTechResearchedPrototype _SetPlayerTechResearched;
        public void SetPlayerTechResearched(JassPlayer whichPlayer, JassInteger techid, JassInteger setToLevel)
        {
            this._SetPlayerTechResearched(whichPlayer, techid, setToLevel);
        }
        
        //native GetPlayerTechResearched takes player whichPlayer, integer techid, boolean specificonly returns boolean
        public delegate JassBoolean GetPlayerTechResearchedPrototype(JassPlayer whichPlayer, JassInteger techid, JassBoolean specificonly);
        private GetPlayerTechResearchedPrototype _GetPlayerTechResearched;
        public bool GetPlayerTechResearched(JassPlayer whichPlayer, JassInteger techid, bool specificonly)
        {
            return this._GetPlayerTechResearched(whichPlayer, techid, specificonly);
        }
        
        //native GetPlayerTechCount takes player whichPlayer, integer techid, boolean specificonly returns integer
        public delegate JassInteger GetPlayerTechCountPrototype(JassPlayer whichPlayer, JassInteger techid, JassBoolean specificonly);
        private GetPlayerTechCountPrototype _GetPlayerTechCount;
        public JassInteger GetPlayerTechCount(JassPlayer whichPlayer, JassInteger techid, bool specificonly)
        {
            return this._GetPlayerTechCount(whichPlayer, techid, specificonly);
        }
        
        //native SetPlayerUnitsOwner takes player whichPlayer, integer newOwner returns nothing
        public delegate void SetPlayerUnitsOwnerPrototype(JassPlayer whichPlayer, JassInteger newOwner);
        private SetPlayerUnitsOwnerPrototype _SetPlayerUnitsOwner;
        public void SetPlayerUnitsOwner(JassPlayer whichPlayer, JassInteger newOwner)
        {
            this._SetPlayerUnitsOwner(whichPlayer, newOwner);
        }
        
        //native CripplePlayer takes player whichPlayer, force toWhichPlayers, boolean flag returns nothing
        public delegate void CripplePlayerPrototype(JassPlayer whichPlayer, JassForce toWhichPlayers, JassBoolean flag);
        private CripplePlayerPrototype _CripplePlayer;
        public void CripplePlayer(JassPlayer whichPlayer, JassForce toWhichPlayers, bool flag)
        {
            this._CripplePlayer(whichPlayer, toWhichPlayers, flag);
        }
        
        //native SetPlayerAbilityAvailable takes player whichPlayer, integer abilid, boolean avail returns nothing
        public delegate void SetPlayerAbilityAvailablePrototype(JassPlayer whichPlayer, JassObjectId abilid, JassBoolean avail);
        private SetPlayerAbilityAvailablePrototype _SetPlayerAbilityAvailable;
        public void SetPlayerAbilityAvailable(JassPlayer whichPlayer, JassObjectId abilid, bool avail)
        {
            this._SetPlayerAbilityAvailable(whichPlayer, abilid, avail);
        }
        
        //native SetPlayerState takes player whichPlayer, playerstate whichPlayerState, integer value returns nothing
        public delegate void SetPlayerStatePrototype(JassPlayer whichPlayer, JassPlayerState whichPlayerState, JassInteger value);
        private SetPlayerStatePrototype _SetPlayerState;
        public void SetPlayerState(JassPlayer whichPlayer, JassPlayerState whichPlayerState, JassInteger value)
        {
            this._SetPlayerState(whichPlayer, whichPlayerState, value);
        }
        
        //native RemovePlayer takes player whichPlayer, playergameresult gameResult returns nothing
        public delegate void RemovePlayerPrototype(JassPlayer whichPlayer, JassPlayerGameResult gameResult);
        private RemovePlayerPrototype _RemovePlayer;
        public void RemovePlayer(JassPlayer whichPlayer, JassPlayerGameResult gameResult)
        {
            this._RemovePlayer(whichPlayer, gameResult);
        }
        
        //native CachePlayerHeroData takes player whichPlayer returns nothing
        public delegate void CachePlayerHeroDataPrototype(JassPlayer whichPlayer);
        private CachePlayerHeroDataPrototype _CachePlayerHeroData;
        public void CachePlayerHeroData(JassPlayer whichPlayer)
        {
            this._CachePlayerHeroData(whichPlayer);
        }
        
        //native SetFogStateRect takes player forWhichPlayer, fogstate whichState, rect where, boolean useSharedVision returns nothing
        public delegate void SetFogStateRectPrototype(JassPlayer forWhichPlayer, JassFogState whichState, JassRect where, JassBoolean useSharedVision);
        private SetFogStateRectPrototype _SetFogStateRect;
        public void SetFogStateRect(JassPlayer forWhichPlayer, JassFogState whichState, JassRect where, bool useSharedVision)
        {
            this._SetFogStateRect(forWhichPlayer, whichState, where, useSharedVision);
        }
        
        //native SetFogStateRadius takes player forWhichPlayer, fogstate whichState, real centerx, real centerY, real radius, boolean useSharedVision returns nothing
        public delegate void SetFogStateRadiusPrototype(JassPlayer forWhichPlayer, JassFogState whichState, JassRealArg centerx, JassRealArg centerY, JassRealArg radius, JassBoolean useSharedVision);
        private SetFogStateRadiusPrototype _SetFogStateRadius;
        public void SetFogStateRadius(JassPlayer forWhichPlayer, JassFogState whichState, float centerx, float centerY, float radius, bool useSharedVision)
        {
            this._SetFogStateRadius(forWhichPlayer, whichState, centerx, centerY, radius, useSharedVision);
        }
        
        //native SetFogStateRadiusLoc takes player forWhichPlayer, fogstate whichState, location center, real radius, boolean useSharedVision returns nothing
        public delegate void SetFogStateRadiusLocPrototype(JassPlayer forWhichPlayer, JassFogState whichState, JassLocation center, JassRealArg radius, JassBoolean useSharedVision);
        private SetFogStateRadiusLocPrototype _SetFogStateRadiusLoc;
        public void SetFogStateRadiusLoc(JassPlayer forWhichPlayer, JassFogState whichState, JassLocation center, float radius, bool useSharedVision)
        {
            this._SetFogStateRadiusLoc(forWhichPlayer, whichState, center, radius, useSharedVision);
        }
        
        //native FogMaskEnable takes boolean enable returns nothing
        public delegate void FogMaskEnablePrototype(JassBoolean enable);
        private FogMaskEnablePrototype _FogMaskEnable;
        public void FogMaskEnable(bool enable)
        {
            this._FogMaskEnable(enable);
        }
        
        //native IsFogMaskEnabled takes nothing returns boolean
        public delegate JassBoolean IsFogMaskEnabledPrototype();
        private IsFogMaskEnabledPrototype _IsFogMaskEnabled;
        public bool IsFogMaskEnabled()
        {
            return this._IsFogMaskEnabled();
        }
        
        //native FogEnable takes boolean enable returns nothing
        public delegate void FogEnablePrototype(JassBoolean enable);
        private FogEnablePrototype _FogEnable;
        public void FogEnable(bool enable)
        {
            this._FogEnable(enable);
        }
        
        //native IsFogEnabled takes nothing returns boolean
        public delegate JassBoolean IsFogEnabledPrototype();
        private IsFogEnabledPrototype _IsFogEnabled;
        public bool IsFogEnabled()
        {
            return this._IsFogEnabled();
        }
        
        //native CreateFogModifierRect takes player forWhichPlayer, fogstate whichState, rect where, boolean useSharedVision, boolean afterUnits returns fogmodifier
        public delegate JassFogModifier CreateFogModifierRectPrototype(JassPlayer forWhichPlayer, JassFogState whichState, JassRect where, JassBoolean useSharedVision, JassBoolean afterUnits);
        private CreateFogModifierRectPrototype _CreateFogModifierRect;
        public JassFogModifier CreateFogModifierRect(JassPlayer forWhichPlayer, JassFogState whichState, JassRect where, bool useSharedVision, bool afterUnits)
        {
            return this._CreateFogModifierRect(forWhichPlayer, whichState, where, useSharedVision, afterUnits);
        }
        
        //native CreateFogModifierRadius takes player forWhichPlayer, fogstate whichState, real centerx, real centerY, real radius, boolean useSharedVision, boolean afterUnits returns fogmodifier
        public delegate JassFogModifier CreateFogModifierRadiusPrototype(JassPlayer forWhichPlayer, JassFogState whichState, JassRealArg centerx, JassRealArg centerY, JassRealArg radius, JassBoolean useSharedVision, JassBoolean afterUnits);
        private CreateFogModifierRadiusPrototype _CreateFogModifierRadius;
        public JassFogModifier CreateFogModifierRadius(JassPlayer forWhichPlayer, JassFogState whichState, float centerx, float centerY, float radius, bool useSharedVision, bool afterUnits)
        {
            return this._CreateFogModifierRadius(forWhichPlayer, whichState, centerx, centerY, radius, useSharedVision, afterUnits);
        }
        
        //native CreateFogModifierRadiusLoc takes player forWhichPlayer, fogstate whichState, location center, real radius, boolean useSharedVision, boolean afterUnits returns fogmodifier
        public delegate JassFogModifier CreateFogModifierRadiusLocPrototype(JassPlayer forWhichPlayer, JassFogState whichState, JassLocation center, JassRealArg radius, JassBoolean useSharedVision, JassBoolean afterUnits);
        private CreateFogModifierRadiusLocPrototype _CreateFogModifierRadiusLoc;
        public JassFogModifier CreateFogModifierRadiusLoc(JassPlayer forWhichPlayer, JassFogState whichState, JassLocation center, float radius, bool useSharedVision, bool afterUnits)
        {
            return this._CreateFogModifierRadiusLoc(forWhichPlayer, whichState, center, radius, useSharedVision, afterUnits);
        }
        
        //native DestroyFogModifier takes fogmodifier whichFogModifier returns nothing
        public delegate void DestroyFogModifierPrototype(JassFogModifier whichFogModifier);
        private DestroyFogModifierPrototype _DestroyFogModifier;
        public void DestroyFogModifier(JassFogModifier whichFogModifier)
        {
            this._DestroyFogModifier(whichFogModifier);
        }
        
        //native FogModifierStart takes fogmodifier whichFogModifier returns nothing
        public delegate void FogModifierStartPrototype(JassFogModifier whichFogModifier);
        private FogModifierStartPrototype _FogModifierStart;
        public void FogModifierStart(JassFogModifier whichFogModifier)
        {
            this._FogModifierStart(whichFogModifier);
        }
        
        //native FogModifierStop takes fogmodifier whichFogModifier returns nothing
        public delegate void FogModifierStopPrototype(JassFogModifier whichFogModifier);
        private FogModifierStopPrototype _FogModifierStop;
        public void FogModifierStop(JassFogModifier whichFogModifier)
        {
            this._FogModifierStop(whichFogModifier);
        }
        
        //native VersionGet takes nothing returns version
        public delegate JassVersion VersionGetPrototype();
        private VersionGetPrototype _VersionGet;
        public JassVersion VersionGet()
        {
            return this._VersionGet();
        }
        
        //native VersionCompatible takes version whichVersion returns boolean
        public delegate JassBoolean VersionCompatiblePrototype(JassVersion whichVersion);
        private VersionCompatiblePrototype _VersionCompatible;
        public bool VersionCompatible(JassVersion whichVersion)
        {
            return this._VersionCompatible(whichVersion);
        }
        
        //native VersionSupported takes version whichVersion returns boolean
        public delegate JassBoolean VersionSupportedPrototype(JassVersion whichVersion);
        private VersionSupportedPrototype _VersionSupported;
        public bool VersionSupported(JassVersion whichVersion)
        {
            return this._VersionSupported(whichVersion);
        }
        
        //native EndGame takes boolean doScoreScreen returns nothing
        public delegate void EndGamePrototype(JassBoolean doScoreScreen);
        private EndGamePrototype _EndGame;
        public void EndGame(bool doScoreScreen)
        {
            this._EndGame(doScoreScreen);
        }
        
        //native ChangeLevel takes string newLevel, boolean doScoreScreen returns nothing
        public delegate void ChangeLevelPrototype(JassStringArg newLevel, JassBoolean doScoreScreen);
        private ChangeLevelPrototype _ChangeLevel;
        public void ChangeLevel(string newLevel, bool doScoreScreen)
        {
            this._ChangeLevel(newLevel, doScoreScreen);
        }
        
        //native RestartGame takes boolean doScoreScreen returns nothing
        public delegate void RestartGamePrototype(JassBoolean doScoreScreen);
        private RestartGamePrototype _RestartGame;
        public void RestartGame(bool doScoreScreen)
        {
            this._RestartGame(doScoreScreen);
        }
        
        //native ReloadGame takes nothing returns nothing
        public delegate void ReloadGamePrototype();
        private ReloadGamePrototype _ReloadGame;
        public void ReloadGame()
        {
            this._ReloadGame();
        }
        
        //native SetCampaignMenuRace takes race r returns nothing
        public delegate void SetCampaignMenuRacePrototype(JassRace r);
        private SetCampaignMenuRacePrototype _SetCampaignMenuRace;
        public void SetCampaignMenuRace(JassRace r)
        {
            this._SetCampaignMenuRace(r);
        }
        
        //native SetCampaignMenuRaceEx takes integer campaignIndex returns nothing
        public delegate void SetCampaignMenuRaceExPrototype(JassInteger campaignIndex);
        private SetCampaignMenuRaceExPrototype _SetCampaignMenuRaceEx;
        public void SetCampaignMenuRaceEx(JassInteger campaignIndex)
        {
            this._SetCampaignMenuRaceEx(campaignIndex);
        }
        
        //native ForceCampaignSelectScreen takes nothing returns nothing
        public delegate void ForceCampaignSelectScreenPrototype();
        private ForceCampaignSelectScreenPrototype _ForceCampaignSelectScreen;
        public void ForceCampaignSelectScreen()
        {
            this._ForceCampaignSelectScreen();
        }
        
        //native LoadGame takes string saveFileName, boolean doScoreScreen returns nothing
        public delegate void LoadGamePrototype(JassStringArg saveFileName, JassBoolean doScoreScreen);
        private LoadGamePrototype _LoadGame;
        public void LoadGame(string saveFileName, bool doScoreScreen)
        {
            this._LoadGame(saveFileName, doScoreScreen);
        }
        
        //native SaveGame takes string saveFileName returns nothing
        public delegate void SaveGamePrototype(JassStringArg saveFileName);
        private SaveGamePrototype _SaveGame;
        public void SaveGame(string saveFileName)
        {
            this._SaveGame(saveFileName);
        }
        
        //native RenameSaveDirectory takes string sourceDirName, string destDirName returns boolean
        public delegate JassBoolean RenameSaveDirectoryPrototype(JassStringArg sourceDirName, JassStringArg destDirName);
        private RenameSaveDirectoryPrototype _RenameSaveDirectory;
        public bool RenameSaveDirectory(string sourceDirName, string destDirName)
        {
            return this._RenameSaveDirectory(sourceDirName, destDirName);
        }
        
        //native RemoveSaveDirectory takes string sourceDirName returns boolean
        public delegate JassBoolean RemoveSaveDirectoryPrototype(JassStringArg sourceDirName);
        private RemoveSaveDirectoryPrototype _RemoveSaveDirectory;
        public bool RemoveSaveDirectory(string sourceDirName)
        {
            return this._RemoveSaveDirectory(sourceDirName);
        }
        
        //native CopySaveGame takes string sourceSaveName, string destSaveName returns boolean
        public delegate JassBoolean CopySaveGamePrototype(JassStringArg sourceSaveName, JassStringArg destSaveName);
        private CopySaveGamePrototype _CopySaveGame;
        public bool CopySaveGame(string sourceSaveName, string destSaveName)
        {
            return this._CopySaveGame(sourceSaveName, destSaveName);
        }
        
        //native SaveGameExists takes string saveName returns boolean
        public delegate JassBoolean SaveGameExistsPrototype(JassStringArg saveName);
        private SaveGameExistsPrototype _SaveGameExists;
        public bool SaveGameExists(string saveName)
        {
            return this._SaveGameExists(saveName);
        }
        
        //native SyncSelections takes nothing returns nothing
        public delegate void SyncSelectionsPrototype();
        private SyncSelectionsPrototype _SyncSelections;
        public void SyncSelections()
        {
            this._SyncSelections();
        }
        
        //native SetFloatGameState takes fgamestate whichFloatGameState, real value returns nothing
        public delegate void SetFloatGameStatePrototype(JassFGameState whichFloatGameState, JassRealArg value);
        private SetFloatGameStatePrototype _SetFloatGameState;
        public void SetFloatGameState(JassFGameState whichFloatGameState, float value)
        {
            this._SetFloatGameState(whichFloatGameState, value);
        }
        
        //native GetFloatGameState takes fgamestate whichFloatGameState returns real
        public delegate JassRealRet GetFloatGameStatePrototype(JassFGameState whichFloatGameState);
        private GetFloatGameStatePrototype _GetFloatGameState;
        public float GetFloatGameState(JassFGameState whichFloatGameState)
        {
            return this._GetFloatGameState(whichFloatGameState);
        }
        
        //native SetIntegerGameState takes igamestate whichIntegerGameState, integer value returns nothing
        public delegate void SetIntegerGameStatePrototype(JassIGameState whichIntegerGameState, JassInteger value);
        private SetIntegerGameStatePrototype _SetIntegerGameState;
        public void SetIntegerGameState(JassIGameState whichIntegerGameState, JassInteger value)
        {
            this._SetIntegerGameState(whichIntegerGameState, value);
        }
        
        //native GetIntegerGameState takes igamestate whichIntegerGameState returns integer
        public delegate JassInteger GetIntegerGameStatePrototype(JassIGameState whichIntegerGameState);
        private GetIntegerGameStatePrototype _GetIntegerGameState;
        public JassInteger GetIntegerGameState(JassIGameState whichIntegerGameState)
        {
            return this._GetIntegerGameState(whichIntegerGameState);
        }
        
        //native SetTutorialCleared takes boolean cleared returns nothing
        public delegate void SetTutorialClearedPrototype(JassBoolean cleared);
        private SetTutorialClearedPrototype _SetTutorialCleared;
        public void SetTutorialCleared(bool cleared)
        {
            this._SetTutorialCleared(cleared);
        }
        
        //native SetMissionAvailable takes integer campaignNumber, integer missionNumber, boolean available returns nothing
        public delegate void SetMissionAvailablePrototype(JassInteger campaignNumber, JassInteger missionNumber, JassBoolean available);
        private SetMissionAvailablePrototype _SetMissionAvailable;
        public void SetMissionAvailable(JassInteger campaignNumber, JassInteger missionNumber, bool available)
        {
            this._SetMissionAvailable(campaignNumber, missionNumber, available);
        }
        
        //native SetCampaignAvailable takes integer campaignNumber, boolean available returns nothing
        public delegate void SetCampaignAvailablePrototype(JassInteger campaignNumber, JassBoolean available);
        private SetCampaignAvailablePrototype _SetCampaignAvailable;
        public void SetCampaignAvailable(JassInteger campaignNumber, bool available)
        {
            this._SetCampaignAvailable(campaignNumber, available);
        }
        
        //native SetOpCinematicAvailable takes integer campaignNumber, boolean available returns nothing
        public delegate void SetOpCinematicAvailablePrototype(JassInteger campaignNumber, JassBoolean available);
        private SetOpCinematicAvailablePrototype _SetOpCinematicAvailable;
        public void SetOpCinematicAvailable(JassInteger campaignNumber, bool available)
        {
            this._SetOpCinematicAvailable(campaignNumber, available);
        }
        
        //native SetEdCinematicAvailable takes integer campaignNumber, boolean available returns nothing
        public delegate void SetEdCinematicAvailablePrototype(JassInteger campaignNumber, JassBoolean available);
        private SetEdCinematicAvailablePrototype _SetEdCinematicAvailable;
        public void SetEdCinematicAvailable(JassInteger campaignNumber, bool available)
        {
            this._SetEdCinematicAvailable(campaignNumber, available);
        }
        
        //native GetDefaultDifficulty takes nothing returns gamedifficulty
        public delegate JassGameDifficulty GetDefaultDifficultyPrototype();
        private GetDefaultDifficultyPrototype _GetDefaultDifficulty;
        public JassGameDifficulty GetDefaultDifficulty()
        {
            return this._GetDefaultDifficulty();
        }
        
        //native SetDefaultDifficulty takes gamedifficulty g returns nothing
        public delegate void SetDefaultDifficultyPrototype(JassGameDifficulty g);
        private SetDefaultDifficultyPrototype _SetDefaultDifficulty;
        public void SetDefaultDifficulty(JassGameDifficulty g)
        {
            this._SetDefaultDifficulty(g);
        }
        
        //native SetCustomCampaignButtonVisible takes integer whichButton, boolean visible returns nothing
        public delegate void SetCustomCampaignButtonVisiblePrototype(JassInteger whichButton, JassBoolean visible);
        private SetCustomCampaignButtonVisiblePrototype _SetCustomCampaignButtonVisible;
        public void SetCustomCampaignButtonVisible(JassInteger whichButton, bool visible)
        {
            this._SetCustomCampaignButtonVisible(whichButton, visible);
        }
        
        //native GetCustomCampaignButtonVisible takes integer whichButton returns boolean
        public delegate JassBoolean GetCustomCampaignButtonVisiblePrototype(JassInteger whichButton);
        private GetCustomCampaignButtonVisiblePrototype _GetCustomCampaignButtonVisible;
        public bool GetCustomCampaignButtonVisible(JassInteger whichButton)
        {
            return this._GetCustomCampaignButtonVisible(whichButton);
        }
        
        //native DoNotSaveReplay takes nothing returns nothing
        public delegate void DoNotSaveReplayPrototype();
        private DoNotSaveReplayPrototype _DoNotSaveReplay;
        public void DoNotSaveReplay()
        {
            this._DoNotSaveReplay();
        }
        
        //native DialogCreate takes nothing returns dialog
        public delegate JassDialog DialogCreatePrototype();
        private DialogCreatePrototype _DialogCreate;
        public JassDialog DialogCreate()
        {
            return this._DialogCreate();
        }
        
        //native DialogDestroy takes dialog whichDialog returns nothing
        public delegate void DialogDestroyPrototype(JassDialog whichDialog);
        private DialogDestroyPrototype _DialogDestroy;
        public void DialogDestroy(JassDialog whichDialog)
        {
            this._DialogDestroy(whichDialog);
        }
        
        //native DialogClear takes dialog whichDialog returns nothing
        public delegate void DialogClearPrototype(JassDialog whichDialog);
        private DialogClearPrototype _DialogClear;
        public void DialogClear(JassDialog whichDialog)
        {
            this._DialogClear(whichDialog);
        }
        
        //native DialogSetMessage takes dialog whichDialog, string messageText returns nothing
        public delegate void DialogSetMessagePrototype(JassDialog whichDialog, JassStringArg messageText);
        private DialogSetMessagePrototype _DialogSetMessage;
        public void DialogSetMessage(JassDialog whichDialog, string messageText)
        {
            this._DialogSetMessage(whichDialog, messageText);
        }
        
        //native DialogAddButton takes dialog whichDialog, string buttonText, integer hotkey returns button
        public delegate JassButton DialogAddButtonPrototype(JassDialog whichDialog, JassStringArg buttonText, JassInteger hotkey);
        private DialogAddButtonPrototype _DialogAddButton;
        public JassButton DialogAddButton(JassDialog whichDialog, string buttonText, JassInteger hotkey)
        {
            return this._DialogAddButton(whichDialog, buttonText, hotkey);
        }
        
        //native DialogAddQuitButton takes dialog whichDialog, boolean doScoreScreen, string buttonText, integer hotkey returns button
        public delegate JassButton DialogAddQuitButtonPrototype(JassDialog whichDialog, JassBoolean doScoreScreen, JassStringArg buttonText, JassInteger hotkey);
        private DialogAddQuitButtonPrototype _DialogAddQuitButton;
        public JassButton DialogAddQuitButton(JassDialog whichDialog, bool doScoreScreen, string buttonText, JassInteger hotkey)
        {
            return this._DialogAddQuitButton(whichDialog, doScoreScreen, buttonText, hotkey);
        }
        
        //native DialogDisplay takes player whichPlayer, dialog whichDialog, boolean flag returns nothing
        public delegate void DialogDisplayPrototype(JassPlayer whichPlayer, JassDialog whichDialog, JassBoolean flag);
        private DialogDisplayPrototype _DialogDisplay;
        public void DialogDisplay(JassPlayer whichPlayer, JassDialog whichDialog, bool flag)
        {
            this._DialogDisplay(whichPlayer, whichDialog, flag);
        }
        
        //native ReloadGameCachesFromDisk takes nothing returns boolean
        public delegate JassBoolean ReloadGameCachesFromDiskPrototype();
        private ReloadGameCachesFromDiskPrototype _ReloadGameCachesFromDisk;
        public bool ReloadGameCachesFromDisk()
        {
            return this._ReloadGameCachesFromDisk();
        }
        
        //native InitGameCache takes string campaignFile returns gamecache
        public delegate JassGameCache InitGameCachePrototype(JassStringArg campaignFile);
        private InitGameCachePrototype _InitGameCache;
        public JassGameCache InitGameCache(string campaignFile)
        {
            return this._InitGameCache(campaignFile);
        }
        
        //native SaveGameCache takes gamecache whichCache returns boolean
        public delegate JassBoolean SaveGameCachePrototype(JassGameCache whichCache);
        private SaveGameCachePrototype _SaveGameCache;
        public bool SaveGameCache(JassGameCache whichCache)
        {
            return this._SaveGameCache(whichCache);
        }
        
        //native StoreInteger takes gamecache cache, string missionKey, string key, integer value returns nothing
        public delegate void StoreIntegerPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key, JassInteger value);
        private StoreIntegerPrototype _StoreInteger;
        public void StoreInteger(JassGameCache cache, string missionKey, string key, JassInteger value)
        {
            this._StoreInteger(cache, missionKey, key, value);
        }
        
        //native StoreReal takes gamecache cache, string missionKey, string key, real value returns nothing
        public delegate void StoreRealPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key, JassRealArg value);
        private StoreRealPrototype _StoreReal;
        public void StoreReal(JassGameCache cache, string missionKey, string key, float value)
        {
            this._StoreReal(cache, missionKey, key, value);
        }
        
        //native StoreBoolean takes gamecache cache, string missionKey, string key, boolean value returns nothing
        public delegate void StoreBooleanPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key, JassBoolean value);
        private StoreBooleanPrototype _StoreBoolean;
        public void StoreBoolean(JassGameCache cache, string missionKey, string key, bool value)
        {
            this._StoreBoolean(cache, missionKey, key, value);
        }
        
        //native StoreUnit takes gamecache cache, string missionKey, string key, unit whichUnit returns boolean
        public delegate JassBoolean StoreUnitPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key, JassUnit whichUnit);
        private StoreUnitPrototype _StoreUnit;
        public bool StoreUnit(JassGameCache cache, string missionKey, string key, JassUnit whichUnit)
        {
            return this._StoreUnit(cache, missionKey, key, whichUnit);
        }
        
        //native StoreString takes gamecache cache, string missionKey, string key, string value returns boolean
        public delegate JassBoolean StoreStringPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key, JassStringArg value);
        private StoreStringPrototype _StoreString;
        public bool StoreString(JassGameCache cache, string missionKey, string key, string value)
        {
            return this._StoreString(cache, missionKey, key, value);
        }
        
        //native SyncStoredInteger takes gamecache cache, string missionKey, string key returns nothing
        public delegate void SyncStoredIntegerPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private SyncStoredIntegerPrototype _SyncStoredInteger;
        public void SyncStoredInteger(JassGameCache cache, string missionKey, string key)
        {
            this._SyncStoredInteger(cache, missionKey, key);
        }
        
        //native SyncStoredReal takes gamecache cache, string missionKey, string key returns nothing
        public delegate void SyncStoredRealPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private SyncStoredRealPrototype _SyncStoredReal;
        public void SyncStoredReal(JassGameCache cache, string missionKey, string key)
        {
            this._SyncStoredReal(cache, missionKey, key);
        }
        
        //native SyncStoredBoolean takes gamecache cache, string missionKey, string key returns nothing
        public delegate void SyncStoredBooleanPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private SyncStoredBooleanPrototype _SyncStoredBoolean;
        public void SyncStoredBoolean(JassGameCache cache, string missionKey, string key)
        {
            this._SyncStoredBoolean(cache, missionKey, key);
        }
        
        //native SyncStoredUnit takes gamecache cache, string missionKey, string key returns nothing
        public delegate void SyncStoredUnitPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private SyncStoredUnitPrototype _SyncStoredUnit;
        public void SyncStoredUnit(JassGameCache cache, string missionKey, string key)
        {
            this._SyncStoredUnit(cache, missionKey, key);
        }
        
        //native SyncStoredString takes gamecache cache, string missionKey, string key returns nothing
        public delegate void SyncStoredStringPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private SyncStoredStringPrototype _SyncStoredString;
        public void SyncStoredString(JassGameCache cache, string missionKey, string key)
        {
            this._SyncStoredString(cache, missionKey, key);
        }
        
        //native HaveStoredInteger takes gamecache cache, string missionKey, string key returns boolean
        public delegate JassBoolean HaveStoredIntegerPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private HaveStoredIntegerPrototype _HaveStoredInteger;
        public bool HaveStoredInteger(JassGameCache cache, string missionKey, string key)
        {
            return this._HaveStoredInteger(cache, missionKey, key);
        }
        
        //native HaveStoredReal takes gamecache cache, string missionKey, string key returns boolean
        public delegate JassBoolean HaveStoredRealPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private HaveStoredRealPrototype _HaveStoredReal;
        public bool HaveStoredReal(JassGameCache cache, string missionKey, string key)
        {
            return this._HaveStoredReal(cache, missionKey, key);
        }
        
        //native HaveStoredBoolean takes gamecache cache, string missionKey, string key returns boolean
        public delegate JassBoolean HaveStoredBooleanPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private HaveStoredBooleanPrototype _HaveStoredBoolean;
        public bool HaveStoredBoolean(JassGameCache cache, string missionKey, string key)
        {
            return this._HaveStoredBoolean(cache, missionKey, key);
        }
        
        //native HaveStoredUnit takes gamecache cache, string missionKey, string key returns boolean
        public delegate JassBoolean HaveStoredUnitPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private HaveStoredUnitPrototype _HaveStoredUnit;
        public bool HaveStoredUnit(JassGameCache cache, string missionKey, string key)
        {
            return this._HaveStoredUnit(cache, missionKey, key);
        }
        
        //native HaveStoredString takes gamecache cache, string missionKey, string key returns boolean
        public delegate JassBoolean HaveStoredStringPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private HaveStoredStringPrototype _HaveStoredString;
        public bool HaveStoredString(JassGameCache cache, string missionKey, string key)
        {
            return this._HaveStoredString(cache, missionKey, key);
        }
        
        //native FlushGameCache takes gamecache cache returns nothing
        public delegate void FlushGameCachePrototype(JassGameCache cache);
        private FlushGameCachePrototype _FlushGameCache;
        public void FlushGameCache(JassGameCache cache)
        {
            this._FlushGameCache(cache);
        }
        
        //native FlushStoredMission takes gamecache cache, string missionKey returns nothing
        public delegate void FlushStoredMissionPrototype(JassGameCache cache, JassStringArg missionKey);
        private FlushStoredMissionPrototype _FlushStoredMission;
        public void FlushStoredMission(JassGameCache cache, string missionKey)
        {
            this._FlushStoredMission(cache, missionKey);
        }
        
        //native FlushStoredInteger takes gamecache cache, string missionKey, string key returns nothing
        public delegate void FlushStoredIntegerPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private FlushStoredIntegerPrototype _FlushStoredInteger;
        public void FlushStoredInteger(JassGameCache cache, string missionKey, string key)
        {
            this._FlushStoredInteger(cache, missionKey, key);
        }
        
        //native FlushStoredReal takes gamecache cache, string missionKey, string key returns nothing
        public delegate void FlushStoredRealPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private FlushStoredRealPrototype _FlushStoredReal;
        public void FlushStoredReal(JassGameCache cache, string missionKey, string key)
        {
            this._FlushStoredReal(cache, missionKey, key);
        }
        
        //native FlushStoredBoolean takes gamecache cache, string missionKey, string key returns nothing
        public delegate void FlushStoredBooleanPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private FlushStoredBooleanPrototype _FlushStoredBoolean;
        public void FlushStoredBoolean(JassGameCache cache, string missionKey, string key)
        {
            this._FlushStoredBoolean(cache, missionKey, key);
        }
        
        //native FlushStoredUnit takes gamecache cache, string missionKey, string key returns nothing
        public delegate void FlushStoredUnitPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private FlushStoredUnitPrototype _FlushStoredUnit;
        public void FlushStoredUnit(JassGameCache cache, string missionKey, string key)
        {
            this._FlushStoredUnit(cache, missionKey, key);
        }
        
        //native FlushStoredString takes gamecache cache, string missionKey, string key returns nothing
        public delegate void FlushStoredStringPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private FlushStoredStringPrototype _FlushStoredString;
        public void FlushStoredString(JassGameCache cache, string missionKey, string key)
        {
            this._FlushStoredString(cache, missionKey, key);
        }
        
        //native GetStoredInteger takes gamecache cache, string missionKey, string key returns integer
        public delegate JassInteger GetStoredIntegerPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private GetStoredIntegerPrototype _GetStoredInteger;
        public JassInteger GetStoredInteger(JassGameCache cache, string missionKey, string key)
        {
            return this._GetStoredInteger(cache, missionKey, key);
        }
        
        //native GetStoredReal takes gamecache cache, string missionKey, string key returns real
        public delegate JassRealRet GetStoredRealPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private GetStoredRealPrototype _GetStoredReal;
        public float GetStoredReal(JassGameCache cache, string missionKey, string key)
        {
            return this._GetStoredReal(cache, missionKey, key);
        }
        
        //native GetStoredBoolean takes gamecache cache, string missionKey, string key returns boolean
        public delegate JassBoolean GetStoredBooleanPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private GetStoredBooleanPrototype _GetStoredBoolean;
        public bool GetStoredBoolean(JassGameCache cache, string missionKey, string key)
        {
            return this._GetStoredBoolean(cache, missionKey, key);
        }
        
        //native GetStoredString takes gamecache cache, string missionKey, string key returns string
        public delegate JassStringRet GetStoredStringPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key);
        private GetStoredStringPrototype _GetStoredString;
        public string GetStoredString(JassGameCache cache, string missionKey, string key)
        {
            return this._GetStoredString(cache, missionKey, key);
        }
        
        //native RestoreUnit takes gamecache cache, string missionKey, string key, player forWhichPlayer, real x, real y, real facing returns unit
        public delegate JassUnit RestoreUnitPrototype(JassGameCache cache, JassStringArg missionKey, JassStringArg key, JassPlayer forWhichPlayer, JassRealArg x, JassRealArg y, JassRealArg facing);
        private RestoreUnitPrototype _RestoreUnit;
        public JassUnit RestoreUnit(JassGameCache cache, string missionKey, string key, JassPlayer forWhichPlayer, float x, float y, float facing)
        {
            return this._RestoreUnit(cache, missionKey, key, forWhichPlayer, x, y, facing);
        }
        
        //native InitHashtable takes nothing returns hashtable
        public delegate JassHashTable InitHashtablePrototype();
        private InitHashtablePrototype _InitHashtable;
        public JassHashTable InitHashtable()
        {
            return this._InitHashtable();
        }
        
        //native SaveInteger takes hashtable table, integer parentKey, integer childKey, integer value returns nothing
        public delegate void SaveIntegerPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassInteger value);
        private SaveIntegerPrototype _SaveInteger;
        public void SaveInteger(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassInteger value)
        {
            this._SaveInteger(table, parentKey, childKey, value);
        }
        
        //native SaveReal takes hashtable table, integer parentKey, integer childKey, real value returns nothing
        public delegate void SaveRealPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassRealArg value);
        private SaveRealPrototype _SaveReal;
        public void SaveReal(JassHashTable table, JassInteger parentKey, JassInteger childKey, float value)
        {
            this._SaveReal(table, parentKey, childKey, value);
        }
        
        //native SaveBoolean takes hashtable table, integer parentKey, integer childKey, boolean value returns nothing
        public delegate void SaveBooleanPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassBoolean value);
        private SaveBooleanPrototype _SaveBoolean;
        public void SaveBoolean(JassHashTable table, JassInteger parentKey, JassInteger childKey, bool value)
        {
            this._SaveBoolean(table, parentKey, childKey, value);
        }
        
        //native SaveStr takes hashtable table, integer parentKey, integer childKey, string value returns boolean
        public delegate JassBoolean SaveStrPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassStringArg value);
        private SaveStrPrototype _SaveStr;
        public bool SaveStr(JassHashTable table, JassInteger parentKey, JassInteger childKey, string value)
        {
            return this._SaveStr(table, parentKey, childKey, value);
        }
        
        //native SavePlayerHandle takes hashtable table, integer parentKey, integer childKey, player whichPlayer returns boolean
        public delegate JassBoolean SavePlayerHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassPlayer whichPlayer);
        private SavePlayerHandlePrototype _SavePlayerHandle;
        public bool SavePlayerHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassPlayer whichPlayer)
        {
            return this._SavePlayerHandle(table, parentKey, childKey, whichPlayer);
        }
        
        //native SaveWidgetHandle takes hashtable table, integer parentKey, integer childKey, widget whichWidget returns boolean
        public delegate JassBoolean SaveWidgetHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassWidget whichWidget);
        private SaveWidgetHandlePrototype _SaveWidgetHandle;
        public bool SaveWidgetHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassWidget whichWidget)
        {
            return this._SaveWidgetHandle(table, parentKey, childKey, whichWidget);
        }
        
        //native SaveDestructableHandle takes hashtable table, integer parentKey, integer childKey, destructable whichDestructable returns boolean
        public delegate JassBoolean SaveDestructableHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassDestructable whichDestructable);
        private SaveDestructableHandlePrototype _SaveDestructableHandle;
        public bool SaveDestructableHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassDestructable whichDestructable)
        {
            return this._SaveDestructableHandle(table, parentKey, childKey, whichDestructable);
        }
        
        //native SaveItemHandle takes hashtable table, integer parentKey, integer childKey, item whichItem returns boolean
        public delegate JassBoolean SaveItemHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassItem whichItem);
        private SaveItemHandlePrototype _SaveItemHandle;
        public bool SaveItemHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassItem whichItem)
        {
            return this._SaveItemHandle(table, parentKey, childKey, whichItem);
        }
        
        //native SaveUnitHandle takes hashtable table, integer parentKey, integer childKey, unit whichUnit returns boolean
        public delegate JassBoolean SaveUnitHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassUnit whichUnit);
        private SaveUnitHandlePrototype _SaveUnitHandle;
        public bool SaveUnitHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassUnit whichUnit)
        {
            return this._SaveUnitHandle(table, parentKey, childKey, whichUnit);
        }
        
        //native SaveAbilityHandle takes hashtable table, integer parentKey, integer childKey, ability whichAbility returns boolean
        public delegate JassBoolean SaveAbilityHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassAbility whichAbility);
        private SaveAbilityHandlePrototype _SaveAbilityHandle;
        public bool SaveAbilityHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassAbility whichAbility)
        {
            return this._SaveAbilityHandle(table, parentKey, childKey, whichAbility);
        }
        
        //native SaveTimerHandle takes hashtable table, integer parentKey, integer childKey, timer whichTimer returns boolean
        public delegate JassBoolean SaveTimerHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTimer whichTimer);
        private SaveTimerHandlePrototype _SaveTimerHandle;
        public bool SaveTimerHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTimer whichTimer)
        {
            return this._SaveTimerHandle(table, parentKey, childKey, whichTimer);
        }
        
        //native SaveTriggerHandle takes hashtable table, integer parentKey, integer childKey, trigger whichTrigger returns boolean
        public delegate JassBoolean SaveTriggerHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTrigger whichTrigger);
        private SaveTriggerHandlePrototype _SaveTriggerHandle;
        public bool SaveTriggerHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTrigger whichTrigger)
        {
            return this._SaveTriggerHandle(table, parentKey, childKey, whichTrigger);
        }
        
        //native SaveTriggerConditionHandle takes hashtable table, integer parentKey, integer childKey, triggercondition whichTriggercondition returns boolean
        public delegate JassBoolean SaveTriggerConditionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTriggerCondition whichTriggercondition);
        private SaveTriggerConditionHandlePrototype _SaveTriggerConditionHandle;
        public bool SaveTriggerConditionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTriggerCondition whichTriggercondition)
        {
            return this._SaveTriggerConditionHandle(table, parentKey, childKey, whichTriggercondition);
        }
        
        //native SaveTriggerActionHandle takes hashtable table, integer parentKey, integer childKey, triggeraction whichTriggeraction returns boolean
        public delegate JassBoolean SaveTriggerActionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTriggerAction whichTriggeraction);
        private SaveTriggerActionHandlePrototype _SaveTriggerActionHandle;
        public bool SaveTriggerActionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTriggerAction whichTriggeraction)
        {
            return this._SaveTriggerActionHandle(table, parentKey, childKey, whichTriggeraction);
        }
        
        //native SaveTriggerEventHandle takes hashtable table, integer parentKey, integer childKey, event whichEvent returns boolean
        public delegate JassBoolean SaveTriggerEventHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassEvent whichEvent);
        private SaveTriggerEventHandlePrototype _SaveTriggerEventHandle;
        public bool SaveTriggerEventHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassEvent whichEvent)
        {
            return this._SaveTriggerEventHandle(table, parentKey, childKey, whichEvent);
        }
        
        //native SaveForceHandle takes hashtable table, integer parentKey, integer childKey, force whichForce returns boolean
        public delegate JassBoolean SaveForceHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassForce whichForce);
        private SaveForceHandlePrototype _SaveForceHandle;
        public bool SaveForceHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassForce whichForce)
        {
            return this._SaveForceHandle(table, parentKey, childKey, whichForce);
        }
        
        //native SaveGroupHandle takes hashtable table, integer parentKey, integer childKey, group whichGroup returns boolean
        public delegate JassBoolean SaveGroupHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassGroup whichGroup);
        private SaveGroupHandlePrototype _SaveGroupHandle;
        public bool SaveGroupHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassGroup whichGroup)
        {
            return this._SaveGroupHandle(table, parentKey, childKey, whichGroup);
        }
        
        //native SaveLocationHandle takes hashtable table, integer parentKey, integer childKey, location whichLocation returns boolean
        public delegate JassBoolean SaveLocationHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassLocation whichLocation);
        private SaveLocationHandlePrototype _SaveLocationHandle;
        public bool SaveLocationHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassLocation whichLocation)
        {
            return this._SaveLocationHandle(table, parentKey, childKey, whichLocation);
        }
        
        //native SaveRectHandle takes hashtable table, integer parentKey, integer childKey, rect whichRect returns boolean
        public delegate JassBoolean SaveRectHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassRect whichRect);
        private SaveRectHandlePrototype _SaveRectHandle;
        public bool SaveRectHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassRect whichRect)
        {
            return this._SaveRectHandle(table, parentKey, childKey, whichRect);
        }
        
        //native SaveBooleanExprHandle takes hashtable table, integer parentKey, integer childKey, boolexpr whichBoolexpr returns boolean
        public delegate JassBoolean SaveBooleanExprHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassBooleanExpression whichBoolexpr);
        private SaveBooleanExprHandlePrototype _SaveBooleanExprHandle;
        public bool SaveBooleanExprHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassBooleanExpression whichBoolexpr)
        {
            return this._SaveBooleanExprHandle(table, parentKey, childKey, whichBoolexpr);
        }
        
        //native SaveSoundHandle takes hashtable table, integer parentKey, integer childKey, sound whichSound returns boolean
        public delegate JassBoolean SaveSoundHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassSound whichSound);
        private SaveSoundHandlePrototype _SaveSoundHandle;
        public bool SaveSoundHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassSound whichSound)
        {
            return this._SaveSoundHandle(table, parentKey, childKey, whichSound);
        }
        
        //native SaveEffectHandle takes hashtable table, integer parentKey, integer childKey, effect whichEffect returns boolean
        public delegate JassBoolean SaveEffectHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassEffect whichEffect);
        private SaveEffectHandlePrototype _SaveEffectHandle;
        public bool SaveEffectHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassEffect whichEffect)
        {
            return this._SaveEffectHandle(table, parentKey, childKey, whichEffect);
        }
        
        //native SaveUnitPoolHandle takes hashtable table, integer parentKey, integer childKey, unitpool whichUnitpool returns boolean
        public delegate JassBoolean SaveUnitPoolHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassUnitPool whichUnitpool);
        private SaveUnitPoolHandlePrototype _SaveUnitPoolHandle;
        public bool SaveUnitPoolHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassUnitPool whichUnitpool)
        {
            return this._SaveUnitPoolHandle(table, parentKey, childKey, whichUnitpool);
        }
        
        //native SaveItemPoolHandle takes hashtable table, integer parentKey, integer childKey, itempool whichItempool returns boolean
        public delegate JassBoolean SaveItemPoolHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassItemPool whichItempool);
        private SaveItemPoolHandlePrototype _SaveItemPoolHandle;
        public bool SaveItemPoolHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassItemPool whichItempool)
        {
            return this._SaveItemPoolHandle(table, parentKey, childKey, whichItempool);
        }
        
        //native SaveQuestHandle takes hashtable table, integer parentKey, integer childKey, quest whichQuest returns boolean
        public delegate JassBoolean SaveQuestHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassQuest whichQuest);
        private SaveQuestHandlePrototype _SaveQuestHandle;
        public bool SaveQuestHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassQuest whichQuest)
        {
            return this._SaveQuestHandle(table, parentKey, childKey, whichQuest);
        }
        
        //native SaveQuestItemHandle takes hashtable table, integer parentKey, integer childKey, questitem whichQuestitem returns boolean
        public delegate JassBoolean SaveQuestItemHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassQuestItem whichQuestitem);
        private SaveQuestItemHandlePrototype _SaveQuestItemHandle;
        public bool SaveQuestItemHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassQuestItem whichQuestitem)
        {
            return this._SaveQuestItemHandle(table, parentKey, childKey, whichQuestitem);
        }
        
        //native SaveDefeatConditionHandle takes hashtable table, integer parentKey, integer childKey, defeatcondition whichDefeatcondition returns boolean
        public delegate JassBoolean SaveDefeatConditionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassDefeatCondition whichDefeatcondition);
        private SaveDefeatConditionHandlePrototype _SaveDefeatConditionHandle;
        public bool SaveDefeatConditionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassDefeatCondition whichDefeatcondition)
        {
            return this._SaveDefeatConditionHandle(table, parentKey, childKey, whichDefeatcondition);
        }
        
        //native SaveTimerDialogHandle takes hashtable table, integer parentKey, integer childKey, timerdialog whichTimerdialog returns boolean
        public delegate JassBoolean SaveTimerDialogHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTimerDialog whichTimerdialog);
        private SaveTimerDialogHandlePrototype _SaveTimerDialogHandle;
        public bool SaveTimerDialogHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTimerDialog whichTimerdialog)
        {
            return this._SaveTimerDialogHandle(table, parentKey, childKey, whichTimerdialog);
        }
        
        //native SaveLeaderboardHandle takes hashtable table, integer parentKey, integer childKey, leaderboard whichLeaderboard returns boolean
        public delegate JassBoolean SaveLeaderboardHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassLeaderboard whichLeaderboard);
        private SaveLeaderboardHandlePrototype _SaveLeaderboardHandle;
        public bool SaveLeaderboardHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassLeaderboard whichLeaderboard)
        {
            return this._SaveLeaderboardHandle(table, parentKey, childKey, whichLeaderboard);
        }
        
        //native SaveMultiboardHandle takes hashtable table, integer parentKey, integer childKey, multiboard whichMultiboard returns boolean
        public delegate JassBoolean SaveMultiboardHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassMultiboard whichMultiboard);
        private SaveMultiboardHandlePrototype _SaveMultiboardHandle;
        public bool SaveMultiboardHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassMultiboard whichMultiboard)
        {
            return this._SaveMultiboardHandle(table, parentKey, childKey, whichMultiboard);
        }
        
        //native SaveMultiboardItemHandle takes hashtable table, integer parentKey, integer childKey, multiboarditem whichMultiboarditem returns boolean
        public delegate JassBoolean SaveMultiboardItemHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassMultiboardItem whichMultiboarditem);
        private SaveMultiboardItemHandlePrototype _SaveMultiboardItemHandle;
        public bool SaveMultiboardItemHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassMultiboardItem whichMultiboarditem)
        {
            return this._SaveMultiboardItemHandle(table, parentKey, childKey, whichMultiboarditem);
        }
        
        //native SaveTrackableHandle takes hashtable table, integer parentKey, integer childKey, trackable whichTrackable returns boolean
        public delegate JassBoolean SaveTrackableHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTrackable whichTrackable);
        private SaveTrackableHandlePrototype _SaveTrackableHandle;
        public bool SaveTrackableHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTrackable whichTrackable)
        {
            return this._SaveTrackableHandle(table, parentKey, childKey, whichTrackable);
        }
        
        //native SaveDialogHandle takes hashtable table, integer parentKey, integer childKey, dialog whichDialog returns boolean
        public delegate JassBoolean SaveDialogHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassDialog whichDialog);
        private SaveDialogHandlePrototype _SaveDialogHandle;
        public bool SaveDialogHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassDialog whichDialog)
        {
            return this._SaveDialogHandle(table, parentKey, childKey, whichDialog);
        }
        
        //native SaveButtonHandle takes hashtable table, integer parentKey, integer childKey, button whichButton returns boolean
        public delegate JassBoolean SaveButtonHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassButton whichButton);
        private SaveButtonHandlePrototype _SaveButtonHandle;
        public bool SaveButtonHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassButton whichButton)
        {
            return this._SaveButtonHandle(table, parentKey, childKey, whichButton);
        }
        
        //native SaveTextTagHandle takes hashtable table, integer parentKey, integer childKey, texttag whichTexttag returns boolean
        public delegate JassBoolean SaveTextTagHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTextTag whichTexttag);
        private SaveTextTagHandlePrototype _SaveTextTagHandle;
        public bool SaveTextTagHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassTextTag whichTexttag)
        {
            return this._SaveTextTagHandle(table, parentKey, childKey, whichTexttag);
        }
        
        //native SaveLightningHandle takes hashtable table, integer parentKey, integer childKey, lightning whichLightning returns boolean
        public delegate JassBoolean SaveLightningHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassLightning whichLightning);
        private SaveLightningHandlePrototype _SaveLightningHandle;
        public bool SaveLightningHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassLightning whichLightning)
        {
            return this._SaveLightningHandle(table, parentKey, childKey, whichLightning);
        }
        
        //native SaveImageHandle takes hashtable table, integer parentKey, integer childKey, image whichImage returns boolean
        public delegate JassBoolean SaveImageHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassImage whichImage);
        private SaveImageHandlePrototype _SaveImageHandle;
        public bool SaveImageHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassImage whichImage)
        {
            return this._SaveImageHandle(table, parentKey, childKey, whichImage);
        }
        
        //native SaveUbersplatHandle takes hashtable table, integer parentKey, integer childKey, ubersplat whichUbersplat returns boolean
        public delegate JassBoolean SaveUbersplatHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassUberSplat whichUbersplat);
        private SaveUbersplatHandlePrototype _SaveUbersplatHandle;
        public bool SaveUbersplatHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassUberSplat whichUbersplat)
        {
            return this._SaveUbersplatHandle(table, parentKey, childKey, whichUbersplat);
        }
        
        //native SaveRegionHandle takes hashtable table, integer parentKey, integer childKey, region whichRegion returns boolean
        public delegate JassBoolean SaveRegionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassRegion whichRegion);
        private SaveRegionHandlePrototype _SaveRegionHandle;
        public bool SaveRegionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassRegion whichRegion)
        {
            return this._SaveRegionHandle(table, parentKey, childKey, whichRegion);
        }
        
        //native SaveFogStateHandle takes hashtable table, integer parentKey, integer childKey, fogstate whichFogState returns boolean
        public delegate JassBoolean SaveFogStateHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassFogState whichFogState);
        private SaveFogStateHandlePrototype _SaveFogStateHandle;
        public bool SaveFogStateHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassFogState whichFogState)
        {
            return this._SaveFogStateHandle(table, parentKey, childKey, whichFogState);
        }
        
        //native SaveFogModifierHandle takes hashtable table, integer parentKey, integer childKey, fogmodifier whichFogModifier returns boolean
        public delegate JassBoolean SaveFogModifierHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassFogModifier whichFogModifier);
        private SaveFogModifierHandlePrototype _SaveFogModifierHandle;
        public bool SaveFogModifierHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassFogModifier whichFogModifier)
        {
            return this._SaveFogModifierHandle(table, parentKey, childKey, whichFogModifier);
        }
        
        //native SaveAgentHandle takes hashtable table, integer parentKey, integer childKey, agent whichAgent returns boolean
        public delegate JassBoolean SaveAgentHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassAgent whichAgent);
        private SaveAgentHandlePrototype _SaveAgentHandle;
        public bool SaveAgentHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassAgent whichAgent)
        {
            return this._SaveAgentHandle(table, parentKey, childKey, whichAgent);
        }
        
        //native SaveHashtableHandle takes hashtable table, integer parentKey, integer childKey, hashtable whichHashtable returns boolean
        public delegate JassBoolean SaveHashtableHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassHashTable whichHashtable);
        private SaveHashtableHandlePrototype _SaveHashtableHandle;
        public bool SaveHashtableHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey, JassHashTable whichHashtable)
        {
            return this._SaveHashtableHandle(table, parentKey, childKey, whichHashtable);
        }
        
        //native LoadInteger takes hashtable table, integer parentKey, integer childKey returns integer
        public delegate JassInteger LoadIntegerPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadIntegerPrototype _LoadInteger;
        public JassInteger LoadInteger(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadInteger(table, parentKey, childKey);
        }
        
        //native LoadReal takes hashtable table, integer parentKey, integer childKey returns real
        public delegate JassRealRet LoadRealPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadRealPrototype _LoadReal;
        public float LoadReal(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadReal(table, parentKey, childKey);
        }
        
        //native LoadBoolean takes hashtable table, integer parentKey, integer childKey returns boolean
        public delegate JassBoolean LoadBooleanPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadBooleanPrototype _LoadBoolean;
        public bool LoadBoolean(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadBoolean(table, parentKey, childKey);
        }
        
        //native LoadStr takes hashtable table, integer parentKey, integer childKey returns string
        public delegate JassStringRet LoadStrPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadStrPrototype _LoadStr;
        public string LoadStr(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadStr(table, parentKey, childKey);
        }
        
        //native LoadPlayerHandle takes hashtable table, integer parentKey, integer childKey returns player
        public delegate JassPlayer LoadPlayerHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadPlayerHandlePrototype _LoadPlayerHandle;
        public JassPlayer LoadPlayerHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadPlayerHandle(table, parentKey, childKey);
        }
        
        //native LoadWidgetHandle takes hashtable table, integer parentKey, integer childKey returns widget
        public delegate JassWidget LoadWidgetHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadWidgetHandlePrototype _LoadWidgetHandle;
        public JassWidget LoadWidgetHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadWidgetHandle(table, parentKey, childKey);
        }
        
        //native LoadDestructableHandle takes hashtable table, integer parentKey, integer childKey returns destructable
        public delegate JassDestructable LoadDestructableHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadDestructableHandlePrototype _LoadDestructableHandle;
        public JassDestructable LoadDestructableHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadDestructableHandle(table, parentKey, childKey);
        }
        
        //native LoadItemHandle takes hashtable table, integer parentKey, integer childKey returns item
        public delegate JassItem LoadItemHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadItemHandlePrototype _LoadItemHandle;
        public JassItem LoadItemHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadItemHandle(table, parentKey, childKey);
        }
        
        //native LoadUnitHandle takes hashtable table, integer parentKey, integer childKey returns unit
        public delegate JassUnit LoadUnitHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadUnitHandlePrototype _LoadUnitHandle;
        public JassUnit LoadUnitHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadUnitHandle(table, parentKey, childKey);
        }
        
        //native LoadAbilityHandle takes hashtable table, integer parentKey, integer childKey returns ability
        public delegate JassAbility LoadAbilityHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadAbilityHandlePrototype _LoadAbilityHandle;
        public JassAbility LoadAbilityHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadAbilityHandle(table, parentKey, childKey);
        }
        
        //native LoadTimerHandle takes hashtable table, integer parentKey, integer childKey returns timer
        public delegate JassTimer LoadTimerHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTimerHandlePrototype _LoadTimerHandle;
        public JassTimer LoadTimerHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTimerHandle(table, parentKey, childKey);
        }
        
        //native LoadTriggerHandle takes hashtable table, integer parentKey, integer childKey returns trigger
        public delegate JassTrigger LoadTriggerHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTriggerHandlePrototype _LoadTriggerHandle;
        public JassTrigger LoadTriggerHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTriggerHandle(table, parentKey, childKey);
        }
        
        //native LoadTriggerConditionHandle takes hashtable table, integer parentKey, integer childKey returns triggercondition
        public delegate JassTriggerCondition LoadTriggerConditionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTriggerConditionHandlePrototype _LoadTriggerConditionHandle;
        public JassTriggerCondition LoadTriggerConditionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTriggerConditionHandle(table, parentKey, childKey);
        }
        
        //native LoadTriggerActionHandle takes hashtable table, integer parentKey, integer childKey returns triggeraction
        public delegate JassTriggerAction LoadTriggerActionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTriggerActionHandlePrototype _LoadTriggerActionHandle;
        public JassTriggerAction LoadTriggerActionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTriggerActionHandle(table, parentKey, childKey);
        }
        
        //native LoadTriggerEventHandle takes hashtable table, integer parentKey, integer childKey returns event
        public delegate JassEvent LoadTriggerEventHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTriggerEventHandlePrototype _LoadTriggerEventHandle;
        public JassEvent LoadTriggerEventHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTriggerEventHandle(table, parentKey, childKey);
        }
        
        //native LoadForceHandle takes hashtable table, integer parentKey, integer childKey returns force
        public delegate JassForce LoadForceHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadForceHandlePrototype _LoadForceHandle;
        public JassForce LoadForceHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadForceHandle(table, parentKey, childKey);
        }
        
        //native LoadGroupHandle takes hashtable table, integer parentKey, integer childKey returns group
        public delegate JassGroup LoadGroupHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadGroupHandlePrototype _LoadGroupHandle;
        public JassGroup LoadGroupHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadGroupHandle(table, parentKey, childKey);
        }
        
        //native LoadLocationHandle takes hashtable table, integer parentKey, integer childKey returns location
        public delegate JassLocation LoadLocationHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadLocationHandlePrototype _LoadLocationHandle;
        public JassLocation LoadLocationHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadLocationHandle(table, parentKey, childKey);
        }
        
        //native LoadRectHandle takes hashtable table, integer parentKey, integer childKey returns rect
        public delegate JassRect LoadRectHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadRectHandlePrototype _LoadRectHandle;
        public JassRect LoadRectHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadRectHandle(table, parentKey, childKey);
        }
        
        //native LoadBooleanExprHandle takes hashtable table, integer parentKey, integer childKey returns boolexpr
        public delegate JassBooleanExpression LoadBooleanExprHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadBooleanExprHandlePrototype _LoadBooleanExprHandle;
        public JassBooleanExpression LoadBooleanExprHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadBooleanExprHandle(table, parentKey, childKey);
        }
        
        //native LoadSoundHandle takes hashtable table, integer parentKey, integer childKey returns sound
        public delegate JassSound LoadSoundHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadSoundHandlePrototype _LoadSoundHandle;
        public JassSound LoadSoundHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadSoundHandle(table, parentKey, childKey);
        }
        
        //native LoadEffectHandle takes hashtable table, integer parentKey, integer childKey returns effect
        public delegate JassEffect LoadEffectHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadEffectHandlePrototype _LoadEffectHandle;
        public JassEffect LoadEffectHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadEffectHandle(table, parentKey, childKey);
        }
        
        //native LoadUnitPoolHandle takes hashtable table, integer parentKey, integer childKey returns unitpool
        public delegate JassUnitPool LoadUnitPoolHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadUnitPoolHandlePrototype _LoadUnitPoolHandle;
        public JassUnitPool LoadUnitPoolHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadUnitPoolHandle(table, parentKey, childKey);
        }
        
        //native LoadItemPoolHandle takes hashtable table, integer parentKey, integer childKey returns itempool
        public delegate JassItemPool LoadItemPoolHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadItemPoolHandlePrototype _LoadItemPoolHandle;
        public JassItemPool LoadItemPoolHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadItemPoolHandle(table, parentKey, childKey);
        }
        
        //native LoadQuestHandle takes hashtable table, integer parentKey, integer childKey returns quest
        public delegate JassQuest LoadQuestHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadQuestHandlePrototype _LoadQuestHandle;
        public JassQuest LoadQuestHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadQuestHandle(table, parentKey, childKey);
        }
        
        //native LoadQuestItemHandle takes hashtable table, integer parentKey, integer childKey returns questitem
        public delegate JassQuestItem LoadQuestItemHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadQuestItemHandlePrototype _LoadQuestItemHandle;
        public JassQuestItem LoadQuestItemHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadQuestItemHandle(table, parentKey, childKey);
        }
        
        //native LoadDefeatConditionHandle takes hashtable table, integer parentKey, integer childKey returns defeatcondition
        public delegate JassDefeatCondition LoadDefeatConditionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadDefeatConditionHandlePrototype _LoadDefeatConditionHandle;
        public JassDefeatCondition LoadDefeatConditionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadDefeatConditionHandle(table, parentKey, childKey);
        }
        
        //native LoadTimerDialogHandle takes hashtable table, integer parentKey, integer childKey returns timerdialog
        public delegate JassTimerDialog LoadTimerDialogHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTimerDialogHandlePrototype _LoadTimerDialogHandle;
        public JassTimerDialog LoadTimerDialogHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTimerDialogHandle(table, parentKey, childKey);
        }
        
        //native LoadLeaderboardHandle takes hashtable table, integer parentKey, integer childKey returns leaderboard
        public delegate JassLeaderboard LoadLeaderboardHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadLeaderboardHandlePrototype _LoadLeaderboardHandle;
        public JassLeaderboard LoadLeaderboardHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadLeaderboardHandle(table, parentKey, childKey);
        }
        
        //native LoadMultiboardHandle takes hashtable table, integer parentKey, integer childKey returns multiboard
        public delegate JassMultiboard LoadMultiboardHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadMultiboardHandlePrototype _LoadMultiboardHandle;
        public JassMultiboard LoadMultiboardHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadMultiboardHandle(table, parentKey, childKey);
        }
        
        //native LoadMultiboardItemHandle takes hashtable table, integer parentKey, integer childKey returns multiboarditem
        public delegate JassMultiboardItem LoadMultiboardItemHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadMultiboardItemHandlePrototype _LoadMultiboardItemHandle;
        public JassMultiboardItem LoadMultiboardItemHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadMultiboardItemHandle(table, parentKey, childKey);
        }
        
        //native LoadTrackableHandle takes hashtable table, integer parentKey, integer childKey returns trackable
        public delegate JassTrackable LoadTrackableHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTrackableHandlePrototype _LoadTrackableHandle;
        public JassTrackable LoadTrackableHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTrackableHandle(table, parentKey, childKey);
        }
        
        //native LoadDialogHandle takes hashtable table, integer parentKey, integer childKey returns dialog
        public delegate JassDialog LoadDialogHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadDialogHandlePrototype _LoadDialogHandle;
        public JassDialog LoadDialogHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadDialogHandle(table, parentKey, childKey);
        }
        
        //native LoadButtonHandle takes hashtable table, integer parentKey, integer childKey returns button
        public delegate JassButton LoadButtonHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadButtonHandlePrototype _LoadButtonHandle;
        public JassButton LoadButtonHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadButtonHandle(table, parentKey, childKey);
        }
        
        //native LoadTextTagHandle takes hashtable table, integer parentKey, integer childKey returns texttag
        public delegate JassTextTag LoadTextTagHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadTextTagHandlePrototype _LoadTextTagHandle;
        public JassTextTag LoadTextTagHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadTextTagHandle(table, parentKey, childKey);
        }
        
        //native LoadLightningHandle takes hashtable table, integer parentKey, integer childKey returns lightning
        public delegate JassLightning LoadLightningHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadLightningHandlePrototype _LoadLightningHandle;
        public JassLightning LoadLightningHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadLightningHandle(table, parentKey, childKey);
        }
        
        //native LoadImageHandle takes hashtable table, integer parentKey, integer childKey returns image
        public delegate JassImage LoadImageHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadImageHandlePrototype _LoadImageHandle;
        public JassImage LoadImageHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadImageHandle(table, parentKey, childKey);
        }
        
        //native LoadUbersplatHandle takes hashtable table, integer parentKey, integer childKey returns ubersplat
        public delegate JassUberSplat LoadUbersplatHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadUbersplatHandlePrototype _LoadUbersplatHandle;
        public JassUberSplat LoadUbersplatHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadUbersplatHandle(table, parentKey, childKey);
        }
        
        //native LoadRegionHandle takes hashtable table, integer parentKey, integer childKey returns region
        public delegate JassRegion LoadRegionHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadRegionHandlePrototype _LoadRegionHandle;
        public JassRegion LoadRegionHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadRegionHandle(table, parentKey, childKey);
        }
        
        //native LoadFogStateHandle takes hashtable table, integer parentKey, integer childKey returns fogstate
        public delegate JassFogState LoadFogStateHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadFogStateHandlePrototype _LoadFogStateHandle;
        public JassFogState LoadFogStateHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadFogStateHandle(table, parentKey, childKey);
        }
        
        //native LoadFogModifierHandle takes hashtable table, integer parentKey, integer childKey returns fogmodifier
        public delegate JassFogModifier LoadFogModifierHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadFogModifierHandlePrototype _LoadFogModifierHandle;
        public JassFogModifier LoadFogModifierHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadFogModifierHandle(table, parentKey, childKey);
        }
        
        //native LoadHashtableHandle takes hashtable table, integer parentKey, integer childKey returns hashtable
        public delegate JassHashTable LoadHashtableHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private LoadHashtableHandlePrototype _LoadHashtableHandle;
        public JassHashTable LoadHashtableHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._LoadHashtableHandle(table, parentKey, childKey);
        }
        
        //native HaveSavedInteger takes hashtable table, integer parentKey, integer childKey returns boolean
        public delegate JassBoolean HaveSavedIntegerPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private HaveSavedIntegerPrototype _HaveSavedInteger;
        public bool HaveSavedInteger(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._HaveSavedInteger(table, parentKey, childKey);
        }
        
        //native HaveSavedReal takes hashtable table, integer parentKey, integer childKey returns boolean
        public delegate JassBoolean HaveSavedRealPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private HaveSavedRealPrototype _HaveSavedReal;
        public bool HaveSavedReal(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._HaveSavedReal(table, parentKey, childKey);
        }
        
        //native HaveSavedBoolean takes hashtable table, integer parentKey, integer childKey returns boolean
        public delegate JassBoolean HaveSavedBooleanPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private HaveSavedBooleanPrototype _HaveSavedBoolean;
        public bool HaveSavedBoolean(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._HaveSavedBoolean(table, parentKey, childKey);
        }
        
        //native HaveSavedString takes hashtable table, integer parentKey, integer childKey returns boolean
        public delegate JassBoolean HaveSavedStringPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private HaveSavedStringPrototype _HaveSavedString;
        public bool HaveSavedString(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._HaveSavedString(table, parentKey, childKey);
        }
        
        //native HaveSavedHandle takes hashtable table, integer parentKey, integer childKey returns boolean
        public delegate JassBoolean HaveSavedHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private HaveSavedHandlePrototype _HaveSavedHandle;
        public bool HaveSavedHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            return this._HaveSavedHandle(table, parentKey, childKey);
        }
        
        //native RemoveSavedInteger takes hashtable table, integer parentKey, integer childKey returns nothing
        public delegate void RemoveSavedIntegerPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private RemoveSavedIntegerPrototype _RemoveSavedInteger;
        public void RemoveSavedInteger(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            this._RemoveSavedInteger(table, parentKey, childKey);
        }
        
        //native RemoveSavedReal takes hashtable table, integer parentKey, integer childKey returns nothing
        public delegate void RemoveSavedRealPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private RemoveSavedRealPrototype _RemoveSavedReal;
        public void RemoveSavedReal(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            this._RemoveSavedReal(table, parentKey, childKey);
        }
        
        //native RemoveSavedBoolean takes hashtable table, integer parentKey, integer childKey returns nothing
        public delegate void RemoveSavedBooleanPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private RemoveSavedBooleanPrototype _RemoveSavedBoolean;
        public void RemoveSavedBoolean(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            this._RemoveSavedBoolean(table, parentKey, childKey);
        }
        
        //native RemoveSavedString takes hashtable table, integer parentKey, integer childKey returns nothing
        public delegate void RemoveSavedStringPrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private RemoveSavedStringPrototype _RemoveSavedString;
        public void RemoveSavedString(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            this._RemoveSavedString(table, parentKey, childKey);
        }
        
        //native RemoveSavedHandle takes hashtable table, integer parentKey, integer childKey returns nothing
        public delegate void RemoveSavedHandlePrototype(JassHashTable table, JassInteger parentKey, JassInteger childKey);
        private RemoveSavedHandlePrototype _RemoveSavedHandle;
        public void RemoveSavedHandle(JassHashTable table, JassInteger parentKey, JassInteger childKey)
        {
            this._RemoveSavedHandle(table, parentKey, childKey);
        }
        
        //native FlushParentHashtable takes hashtable table returns nothing
        public delegate void FlushParentHashtablePrototype(JassHashTable table);
        private FlushParentHashtablePrototype _FlushParentHashtable;
        public void FlushParentHashtable(JassHashTable table)
        {
            this._FlushParentHashtable(table);
        }
        
        //native FlushChildHashtable takes hashtable table, integer parentKey returns nothing
        public delegate void FlushChildHashtablePrototype(JassHashTable table, JassInteger parentKey);
        private FlushChildHashtablePrototype _FlushChildHashtable;
        public void FlushChildHashtable(JassHashTable table, JassInteger parentKey)
        {
            this._FlushChildHashtable(table, parentKey);
        }
        
        //native GetRandomInt takes integer lowBound, integer highBound returns integer
        public delegate JassInteger GetRandomIntPrototype(JassInteger lowBound, JassInteger highBound);
        private GetRandomIntPrototype _GetRandomInt;
        public JassInteger GetRandomInt(JassInteger lowBound, JassInteger highBound)
        {
            return this._GetRandomInt(lowBound, highBound);
        }
        
        //native GetRandomReal takes real lowBound, real highBound returns real
        public delegate JassRealRet GetRandomRealPrototype(JassRealArg lowBound, JassRealArg highBound);
        private GetRandomRealPrototype _GetRandomReal;
        public float GetRandomReal(float lowBound, float highBound)
        {
            return this._GetRandomReal(lowBound, highBound);
        }
        
        //native CreateUnitPool takes nothing returns unitpool
        public delegate JassUnitPool CreateUnitPoolPrototype();
        private CreateUnitPoolPrototype _CreateUnitPool;
        public JassUnitPool CreateUnitPool()
        {
            return this._CreateUnitPool();
        }
        
        //native DestroyUnitPool takes unitpool whichPool returns nothing
        public delegate void DestroyUnitPoolPrototype(JassUnitPool whichPool);
        private DestroyUnitPoolPrototype _DestroyUnitPool;
        public void DestroyUnitPool(JassUnitPool whichPool)
        {
            this._DestroyUnitPool(whichPool);
        }
        
        //native UnitPoolAddUnitType takes unitpool whichPool, integer unitId, real weight returns nothing
        public delegate void UnitPoolAddUnitTypePrototype(JassUnitPool whichPool, JassObjectId unitId, JassRealArg weight);
        private UnitPoolAddUnitTypePrototype _UnitPoolAddUnitType;
        public void UnitPoolAddUnitType(JassUnitPool whichPool, JassObjectId unitId, float weight)
        {
            this._UnitPoolAddUnitType(whichPool, unitId, weight);
        }
        
        //native UnitPoolRemoveUnitType takes unitpool whichPool, integer unitId returns nothing
        public delegate void UnitPoolRemoveUnitTypePrototype(JassUnitPool whichPool, JassObjectId unitId);
        private UnitPoolRemoveUnitTypePrototype _UnitPoolRemoveUnitType;
        public void UnitPoolRemoveUnitType(JassUnitPool whichPool, JassObjectId unitId)
        {
            this._UnitPoolRemoveUnitType(whichPool, unitId);
        }
        
        //native PlaceRandomUnit takes unitpool whichPool, player forWhichPlayer, real x, real y, real facing returns unit
        public delegate JassUnit PlaceRandomUnitPrototype(JassUnitPool whichPool, JassPlayer forWhichPlayer, JassRealArg x, JassRealArg y, JassRealArg facing);
        private PlaceRandomUnitPrototype _PlaceRandomUnit;
        public JassUnit PlaceRandomUnit(JassUnitPool whichPool, JassPlayer forWhichPlayer, float x, float y, float facing)
        {
            return this._PlaceRandomUnit(whichPool, forWhichPlayer, x, y, facing);
        }
        
        //native CreateItemPool takes nothing returns itempool
        public delegate JassItemPool CreateItemPoolPrototype();
        private CreateItemPoolPrototype _CreateItemPool;
        public JassItemPool CreateItemPool()
        {
            return this._CreateItemPool();
        }
        
        //native DestroyItemPool takes itempool whichItemPool returns nothing
        public delegate void DestroyItemPoolPrototype(JassItemPool whichItemPool);
        private DestroyItemPoolPrototype _DestroyItemPool;
        public void DestroyItemPool(JassItemPool whichItemPool)
        {
            this._DestroyItemPool(whichItemPool);
        }
        
        //native ItemPoolAddItemType takes itempool whichItemPool, integer itemId, real weight returns nothing
        public delegate void ItemPoolAddItemTypePrototype(JassItemPool whichItemPool, JassObjectId itemId, JassRealArg weight);
        private ItemPoolAddItemTypePrototype _ItemPoolAddItemType;
        public void ItemPoolAddItemType(JassItemPool whichItemPool, JassObjectId itemId, float weight)
        {
            this._ItemPoolAddItemType(whichItemPool, itemId, weight);
        }
        
        //native ItemPoolRemoveItemType takes itempool whichItemPool, integer itemId returns nothing
        public delegate void ItemPoolRemoveItemTypePrototype(JassItemPool whichItemPool, JassObjectId itemId);
        private ItemPoolRemoveItemTypePrototype _ItemPoolRemoveItemType;
        public void ItemPoolRemoveItemType(JassItemPool whichItemPool, JassObjectId itemId)
        {
            this._ItemPoolRemoveItemType(whichItemPool, itemId);
        }
        
        //native PlaceRandomItem takes itempool whichItemPool, real x, real y returns item
        public delegate JassItem PlaceRandomItemPrototype(JassItemPool whichItemPool, JassRealArg x, JassRealArg y);
        private PlaceRandomItemPrototype _PlaceRandomItem;
        public JassItem PlaceRandomItem(JassItemPool whichItemPool, float x, float y)
        {
            return this._PlaceRandomItem(whichItemPool, x, y);
        }
        
        //native ChooseRandomCreep takes integer level returns integer
        public delegate JassInteger ChooseRandomCreepPrototype(JassInteger level);
        private ChooseRandomCreepPrototype _ChooseRandomCreep;
        public JassInteger ChooseRandomCreep(JassInteger level)
        {
            return this._ChooseRandomCreep(level);
        }
        
        //native ChooseRandomNPBuilding takes nothing returns integer
        public delegate JassInteger ChooseRandomNPBuildingPrototype();
        private ChooseRandomNPBuildingPrototype _ChooseRandomNPBuilding;
        public JassInteger ChooseRandomNPBuilding()
        {
            return this._ChooseRandomNPBuilding();
        }
        
        //native ChooseRandomItem takes integer level returns integer
        public delegate JassInteger ChooseRandomItemPrototype(JassInteger level);
        private ChooseRandomItemPrototype _ChooseRandomItem;
        public JassInteger ChooseRandomItem(JassInteger level)
        {
            return this._ChooseRandomItem(level);
        }
        
        //native ChooseRandomItemEx takes itemtype whichType, integer level returns integer
        public delegate JassInteger ChooseRandomItemExPrototype(JassItemType whichType, JassInteger level);
        private ChooseRandomItemExPrototype _ChooseRandomItemEx;
        public JassInteger ChooseRandomItemEx(JassItemType whichType, JassInteger level)
        {
            return this._ChooseRandomItemEx(whichType, level);
        }
        
        //native SetRandomSeed takes integer seed returns nothing
        public delegate void SetRandomSeedPrototype(JassInteger seed);
        private SetRandomSeedPrototype _SetRandomSeed;
        public void SetRandomSeed(JassInteger seed)
        {
            this._SetRandomSeed(seed);
        }
        
        //native SetTerrainFog takes real a, real b, real c, real d, real e returns nothing
        public delegate void SetTerrainFogPrototype(JassRealArg a, JassRealArg b, JassRealArg c, JassRealArg d, JassRealArg e);
        private SetTerrainFogPrototype _SetTerrainFog;
        public void SetTerrainFog(float a, float b, float c, float d, float e)
        {
            this._SetTerrainFog(a, b, c, d, e);
        }
        
        //native ResetTerrainFog takes nothing returns nothing
        public delegate void ResetTerrainFogPrototype();
        private ResetTerrainFogPrototype _ResetTerrainFog;
        public void ResetTerrainFog()
        {
            this._ResetTerrainFog();
        }
        
        //native SetUnitFog takes real a, real b, real c, real d, real e returns nothing
        public delegate void SetUnitFogPrototype(JassRealArg a, JassRealArg b, JassRealArg c, JassRealArg d, JassRealArg e);
        private SetUnitFogPrototype _SetUnitFog;
        public void SetUnitFog(float a, float b, float c, float d, float e)
        {
            this._SetUnitFog(a, b, c, d, e);
        }
        
        //native SetTerrainFogEx takes integer style, real zstart, real zend, real density, real red, real green, real blue returns nothing
        public delegate void SetTerrainFogExPrototype(JassInteger style, JassRealArg zstart, JassRealArg zend, JassRealArg density, JassRealArg red, JassRealArg green, JassRealArg blue);
        private SetTerrainFogExPrototype _SetTerrainFogEx;
        public void SetTerrainFogEx(JassInteger style, float zstart, float zend, float density, float red, float green, float blue)
        {
            this._SetTerrainFogEx(style, zstart, zend, density, red, green, blue);
        }
        
        //native DisplayTextToPlayer takes player toPlayer, real x, real y, string message returns nothing
        public delegate void DisplayTextToPlayerPrototype(JassPlayer toPlayer, JassRealArg x, JassRealArg y, JassStringArg message);
        private DisplayTextToPlayerPrototype _DisplayTextToPlayer;
        public void DisplayTextToPlayer(JassPlayer toPlayer, float x, float y, string message)
        {
            this._DisplayTextToPlayer(toPlayer, x, y, message);
        }
        
        //native DisplayTimedTextToPlayer takes player toPlayer, real x, real y, real duration, string message returns nothing
        public delegate void DisplayTimedTextToPlayerPrototype(JassPlayer toPlayer, JassRealArg x, JassRealArg y, JassRealArg duration, JassStringArg message);
        private DisplayTimedTextToPlayerPrototype _DisplayTimedTextToPlayer;
        public void DisplayTimedTextToPlayer(JassPlayer toPlayer, float x, float y, float duration, string message)
        {
            this._DisplayTimedTextToPlayer(toPlayer, x, y, duration, message);
        }
        
        //native DisplayTimedTextFromPlayer takes player toPlayer, real x, real y, real duration, string message returns nothing
        public delegate void DisplayTimedTextFromPlayerPrototype(JassPlayer toPlayer, JassRealArg x, JassRealArg y, JassRealArg duration, JassStringArg message);
        private DisplayTimedTextFromPlayerPrototype _DisplayTimedTextFromPlayer;
        public void DisplayTimedTextFromPlayer(JassPlayer toPlayer, float x, float y, float duration, string message)
        {
            this._DisplayTimedTextFromPlayer(toPlayer, x, y, duration, message);
        }
        
        //native ClearTextMessages takes nothing returns nothing
        public delegate void ClearTextMessagesPrototype();
        private ClearTextMessagesPrototype _ClearTextMessages;
        public void ClearTextMessages()
        {
            this._ClearTextMessages();
        }
        
        //native SetDayNightModels takes string terrainDNCFile, string unitDNCFile returns nothing
        public delegate void SetDayNightModelsPrototype(JassStringArg terrainDNCFile, JassStringArg unitDNCFile);
        private SetDayNightModelsPrototype _SetDayNightModels;
        public void SetDayNightModels(string terrainDNCFile, string unitDNCFile)
        {
            this._SetDayNightModels(terrainDNCFile, unitDNCFile);
        }
        
        //native SetSkyModel takes string skyModelFile returns nothing
        public delegate void SetSkyModelPrototype(JassStringArg skyModelFile);
        private SetSkyModelPrototype _SetSkyModel;
        public void SetSkyModel(string skyModelFile)
        {
            this._SetSkyModel(skyModelFile);
        }
        
        //native EnableUserControl takes boolean b returns nothing
        public delegate void EnableUserControlPrototype(JassBoolean b);
        private EnableUserControlPrototype _EnableUserControl;
        public void EnableUserControl(bool b)
        {
            this._EnableUserControl(b);
        }
        
        //native EnableUserUI takes boolean b returns nothing
        public delegate void EnableUserUIPrototype(JassBoolean b);
        private EnableUserUIPrototype _EnableUserUI;
        public void EnableUserUI(bool b)
        {
            this._EnableUserUI(b);
        }
        
        //native SuspendTimeOfDay takes boolean b returns nothing
        public delegate void SuspendTimeOfDayPrototype(JassBoolean b);
        private SuspendTimeOfDayPrototype _SuspendTimeOfDay;
        public void SuspendTimeOfDay(bool b)
        {
            this._SuspendTimeOfDay(b);
        }
        
        //native SetTimeOfDayScale takes real r returns nothing
        public delegate void SetTimeOfDayScalePrototype(JassRealArg r);
        private SetTimeOfDayScalePrototype _SetTimeOfDayScale;
        public void SetTimeOfDayScale(float r)
        {
            this._SetTimeOfDayScale(r);
        }
        
        //native GetTimeOfDayScale takes nothing returns real
        public delegate JassRealRet GetTimeOfDayScalePrototype();
        private GetTimeOfDayScalePrototype _GetTimeOfDayScale;
        public float GetTimeOfDayScale()
        {
            return this._GetTimeOfDayScale();
        }
        
        //native ShowInterface takes boolean flag, real fadeDuration returns nothing
        public delegate void ShowInterfacePrototype(JassBoolean flag, JassRealArg fadeDuration);
        private ShowInterfacePrototype _ShowInterface;
        public void ShowInterface(bool flag, float fadeDuration)
        {
            this._ShowInterface(flag, fadeDuration);
        }
        
        //native PauseGame takes boolean flag returns nothing
        public delegate void PauseGamePrototype(JassBoolean flag);
        private PauseGamePrototype _PauseGame;
        public void PauseGame(bool flag)
        {
            this._PauseGame(flag);
        }
        
        //native UnitAddIndicator takes unit whichUnit, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void UnitAddIndicatorPrototype(JassUnit whichUnit, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private UnitAddIndicatorPrototype _UnitAddIndicator;
        public void UnitAddIndicator(JassUnit whichUnit, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._UnitAddIndicator(whichUnit, red, green, blue, alpha);
        }
        
        //native AddIndicator takes widget whichWidget, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void AddIndicatorPrototype(JassWidget whichWidget, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private AddIndicatorPrototype _AddIndicator;
        public void AddIndicator(JassWidget whichWidget, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._AddIndicator(whichWidget, red, green, blue, alpha);
        }
        
        //native PingMinimap takes real x, real y, real duration returns nothing
        public delegate void PingMinimapPrototype(JassRealArg x, JassRealArg y, JassRealArg duration);
        private PingMinimapPrototype _PingMinimap;
        public void PingMinimap(float x, float y, float duration)
        {
            this._PingMinimap(x, y, duration);
        }
        
        //native PingMinimapEx takes real x, real y, real duration, integer red, integer green, integer blue, boolean extraEffects returns nothing
        public delegate void PingMinimapExPrototype(JassRealArg x, JassRealArg y, JassRealArg duration, JassInteger red, JassInteger green, JassInteger blue, JassBoolean extraEffects);
        private PingMinimapExPrototype _PingMinimapEx;
        public void PingMinimapEx(float x, float y, float duration, JassInteger red, JassInteger green, JassInteger blue, bool extraEffects)
        {
            this._PingMinimapEx(x, y, duration, red, green, blue, extraEffects);
        }
        
        //native EnableOcclusion takes boolean flag returns nothing
        public delegate void EnableOcclusionPrototype(JassBoolean flag);
        private EnableOcclusionPrototype _EnableOcclusion;
        public void EnableOcclusion(bool flag)
        {
            this._EnableOcclusion(flag);
        }
        
        //native SetIntroShotText takes string introText returns nothing
        public delegate void SetIntroShotTextPrototype(JassStringArg introText);
        private SetIntroShotTextPrototype _SetIntroShotText;
        public void SetIntroShotText(string introText)
        {
            this._SetIntroShotText(introText);
        }
        
        //native SetIntroShotModel takes string introModelPath returns nothing
        public delegate void SetIntroShotModelPrototype(JassStringArg introModelPath);
        private SetIntroShotModelPrototype _SetIntroShotModel;
        public void SetIntroShotModel(string introModelPath)
        {
            this._SetIntroShotModel(introModelPath);
        }
        
        //native EnableWorldFogBoundary takes boolean b returns nothing
        public delegate void EnableWorldFogBoundaryPrototype(JassBoolean b);
        private EnableWorldFogBoundaryPrototype _EnableWorldFogBoundary;
        public void EnableWorldFogBoundary(bool b)
        {
            this._EnableWorldFogBoundary(b);
        }
        
        //native PlayModelCinematic takes string modelName returns nothing
        public delegate void PlayModelCinematicPrototype(JassStringArg modelName);
        private PlayModelCinematicPrototype _PlayModelCinematic;
        public void PlayModelCinematic(string modelName)
        {
            this._PlayModelCinematic(modelName);
        }
        
        //native PlayCinematic takes string movieName returns nothing
        public delegate void PlayCinematicPrototype(JassStringArg movieName);
        private PlayCinematicPrototype _PlayCinematic;
        public void PlayCinematic(string movieName)
        {
            this._PlayCinematic(movieName);
        }
        
        //native ForceUIKey takes string key returns nothing
        public delegate void ForceUIKeyPrototype(JassStringArg key);
        private ForceUIKeyPrototype _ForceUIKey;
        public void ForceUIKey(string key)
        {
            this._ForceUIKey(key);
        }
        
        //native ForceUICancel takes nothing returns nothing
        public delegate void ForceUICancelPrototype();
        private ForceUICancelPrototype _ForceUICancel;
        public void ForceUICancel()
        {
            this._ForceUICancel();
        }
        
        //native DisplayLoadDialog takes nothing returns nothing
        public delegate void DisplayLoadDialogPrototype();
        private DisplayLoadDialogPrototype _DisplayLoadDialog;
        public void DisplayLoadDialog()
        {
            this._DisplayLoadDialog();
        }
        
        //native SetAltMinimapIcon takes string iconPath returns nothing
        public delegate void SetAltMinimapIconPrototype(JassStringArg iconPath);
        private SetAltMinimapIconPrototype _SetAltMinimapIcon;
        public void SetAltMinimapIcon(string iconPath)
        {
            this._SetAltMinimapIcon(iconPath);
        }
        
        //native DisableRestartMission takes boolean flag returns nothing
        public delegate void DisableRestartMissionPrototype(JassBoolean flag);
        private DisableRestartMissionPrototype _DisableRestartMission;
        public void DisableRestartMission(bool flag)
        {
            this._DisableRestartMission(flag);
        }
        
        //native CreateTextTag takes nothing returns texttag
        public delegate JassTextTag CreateTextTagPrototype();
        private CreateTextTagPrototype _CreateTextTag;
        public JassTextTag CreateTextTag()
        {
            return this._CreateTextTag();
        }
        
        //native DestroyTextTag takes texttag t returns nothing
        public delegate void DestroyTextTagPrototype(JassTextTag t);
        private DestroyTextTagPrototype _DestroyTextTag;
        public void DestroyTextTag(JassTextTag t)
        {
            this._DestroyTextTag(t);
        }
        
        //native SetTextTagText takes texttag t, string s, real height returns nothing
        public delegate void SetTextTagTextPrototype(JassTextTag t, JassStringArg s, JassRealArg height);
        private SetTextTagTextPrototype _SetTextTagText;
        public void SetTextTagText(JassTextTag t, string s, float height)
        {
            this._SetTextTagText(t, s, height);
        }
        
        //native SetTextTagPos takes texttag t, real x, real y, real heightOffset returns nothing
        public delegate void SetTextTagPosPrototype(JassTextTag t, JassRealArg x, JassRealArg y, JassRealArg heightOffset);
        private SetTextTagPosPrototype _SetTextTagPos;
        public void SetTextTagPos(JassTextTag t, float x, float y, float heightOffset)
        {
            this._SetTextTagPos(t, x, y, heightOffset);
        }
        
        //native SetTextTagPosUnit takes texttag t, unit whichUnit, real heightOffset returns nothing
        public delegate void SetTextTagPosUnitPrototype(JassTextTag t, JassUnit whichUnit, JassRealArg heightOffset);
        private SetTextTagPosUnitPrototype _SetTextTagPosUnit;
        public void SetTextTagPosUnit(JassTextTag t, JassUnit whichUnit, float heightOffset)
        {
            this._SetTextTagPosUnit(t, whichUnit, heightOffset);
        }
        
        //native SetTextTagColor takes texttag t, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void SetTextTagColorPrototype(JassTextTag t, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private SetTextTagColorPrototype _SetTextTagColor;
        public void SetTextTagColor(JassTextTag t, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._SetTextTagColor(t, red, green, blue, alpha);
        }
        
        //native SetTextTagVelocity takes texttag t, real xvel, real yvel returns nothing
        public delegate void SetTextTagVelocityPrototype(JassTextTag t, JassRealArg xvel, JassRealArg yvel);
        private SetTextTagVelocityPrototype _SetTextTagVelocity;
        public void SetTextTagVelocity(JassTextTag t, float xvel, float yvel)
        {
            this._SetTextTagVelocity(t, xvel, yvel);
        }
        
        //native SetTextTagVisibility takes texttag t, boolean flag returns nothing
        public delegate void SetTextTagVisibilityPrototype(JassTextTag t, JassBoolean flag);
        private SetTextTagVisibilityPrototype _SetTextTagVisibility;
        public void SetTextTagVisibility(JassTextTag t, bool flag)
        {
            this._SetTextTagVisibility(t, flag);
        }
        
        //native SetTextTagSuspended takes texttag t, boolean flag returns nothing
        public delegate void SetTextTagSuspendedPrototype(JassTextTag t, JassBoolean flag);
        private SetTextTagSuspendedPrototype _SetTextTagSuspended;
        public void SetTextTagSuspended(JassTextTag t, bool flag)
        {
            this._SetTextTagSuspended(t, flag);
        }
        
        //native SetTextTagPermanent takes texttag t, boolean flag returns nothing
        public delegate void SetTextTagPermanentPrototype(JassTextTag t, JassBoolean flag);
        private SetTextTagPermanentPrototype _SetTextTagPermanent;
        public void SetTextTagPermanent(JassTextTag t, bool flag)
        {
            this._SetTextTagPermanent(t, flag);
        }
        
        //native SetTextTagAge takes texttag t, real age returns nothing
        public delegate void SetTextTagAgePrototype(JassTextTag t, JassRealArg age);
        private SetTextTagAgePrototype _SetTextTagAge;
        public void SetTextTagAge(JassTextTag t, float age)
        {
            this._SetTextTagAge(t, age);
        }
        
        //native SetTextTagLifespan takes texttag t, real lifespan returns nothing
        public delegate void SetTextTagLifespanPrototype(JassTextTag t, JassRealArg lifespan);
        private SetTextTagLifespanPrototype _SetTextTagLifespan;
        public void SetTextTagLifespan(JassTextTag t, float lifespan)
        {
            this._SetTextTagLifespan(t, lifespan);
        }
        
        //native SetTextTagFadepoint takes texttag t, real fadepoint returns nothing
        public delegate void SetTextTagFadepointPrototype(JassTextTag t, JassRealArg fadepoint);
        private SetTextTagFadepointPrototype _SetTextTagFadepoint;
        public void SetTextTagFadepoint(JassTextTag t, float fadepoint)
        {
            this._SetTextTagFadepoint(t, fadepoint);
        }
        
        //native SetReservedLocalHeroButtons takes integer reserved returns nothing
        public delegate void SetReservedLocalHeroButtonsPrototype(JassInteger reserved);
        private SetReservedLocalHeroButtonsPrototype _SetReservedLocalHeroButtons;
        public void SetReservedLocalHeroButtons(JassInteger reserved)
        {
            this._SetReservedLocalHeroButtons(reserved);
        }
        
        //native GetAllyColorFilterState takes nothing returns integer
        public delegate JassInteger GetAllyColorFilterStatePrototype();
        private GetAllyColorFilterStatePrototype _GetAllyColorFilterState;
        public JassInteger GetAllyColorFilterState()
        {
            return this._GetAllyColorFilterState();
        }
        
        //native SetAllyColorFilterState takes integer state returns nothing
        public delegate void SetAllyColorFilterStatePrototype(JassInteger state);
        private SetAllyColorFilterStatePrototype _SetAllyColorFilterState;
        public void SetAllyColorFilterState(JassInteger state)
        {
            this._SetAllyColorFilterState(state);
        }
        
        //native GetCreepCampFilterState takes nothing returns boolean
        public delegate JassBoolean GetCreepCampFilterStatePrototype();
        private GetCreepCampFilterStatePrototype _GetCreepCampFilterState;
        public bool GetCreepCampFilterState()
        {
            return this._GetCreepCampFilterState();
        }
        
        //native SetCreepCampFilterState takes boolean state returns nothing
        public delegate void SetCreepCampFilterStatePrototype(JassBoolean state);
        private SetCreepCampFilterStatePrototype _SetCreepCampFilterState;
        public void SetCreepCampFilterState(bool state)
        {
            this._SetCreepCampFilterState(state);
        }
        
        //native EnableMinimapFilterButtons takes boolean enableAlly, boolean enableCreep returns nothing
        public delegate void EnableMinimapFilterButtonsPrototype(JassBoolean enableAlly, JassBoolean enableCreep);
        private EnableMinimapFilterButtonsPrototype _EnableMinimapFilterButtons;
        public void EnableMinimapFilterButtons(bool enableAlly, bool enableCreep)
        {
            this._EnableMinimapFilterButtons(enableAlly, enableCreep);
        }
        
        //native EnableDragSelect takes boolean state, boolean ui returns nothing
        public delegate void EnableDragSelectPrototype(JassBoolean state, JassBoolean ui);
        private EnableDragSelectPrototype _EnableDragSelect;
        public void EnableDragSelect(bool state, bool ui)
        {
            this._EnableDragSelect(state, ui);
        }
        
        //native EnablePreSelect takes boolean state, boolean ui returns nothing
        public delegate void EnablePreSelectPrototype(JassBoolean state, JassBoolean ui);
        private EnablePreSelectPrototype _EnablePreSelect;
        public void EnablePreSelect(bool state, bool ui)
        {
            this._EnablePreSelect(state, ui);
        }
        
        //native EnableSelect takes boolean state, boolean ui returns nothing
        public delegate void EnableSelectPrototype(JassBoolean state, JassBoolean ui);
        private EnableSelectPrototype _EnableSelect;
        public void EnableSelect(bool state, bool ui)
        {
            this._EnableSelect(state, ui);
        }
        
        //native CreateTrackable takes string trackableModelPath, real x, real y, real facing returns trackable
        public delegate JassTrackable CreateTrackablePrototype(JassStringArg trackableModelPath, JassRealArg x, JassRealArg y, JassRealArg facing);
        private CreateTrackablePrototype _CreateTrackable;
        public JassTrackable CreateTrackable(string trackableModelPath, float x, float y, float facing)
        {
            return this._CreateTrackable(trackableModelPath, x, y, facing);
        }
        
        //native CreateQuest takes nothing returns quest
        public delegate JassQuest CreateQuestPrototype();
        private CreateQuestPrototype _CreateQuest;
        public JassQuest CreateQuest()
        {
            return this._CreateQuest();
        }
        
        //native DestroyQuest takes quest whichQuest returns nothing
        public delegate void DestroyQuestPrototype(JassQuest whichQuest);
        private DestroyQuestPrototype _DestroyQuest;
        public void DestroyQuest(JassQuest whichQuest)
        {
            this._DestroyQuest(whichQuest);
        }
        
        //native QuestSetTitle takes quest whichQuest, string title returns nothing
        public delegate void QuestSetTitlePrototype(JassQuest whichQuest, JassStringArg title);
        private QuestSetTitlePrototype _QuestSetTitle;
        public void QuestSetTitle(JassQuest whichQuest, string title)
        {
            this._QuestSetTitle(whichQuest, title);
        }
        
        //native QuestSetDescription takes quest whichQuest, string description returns nothing
        public delegate void QuestSetDescriptionPrototype(JassQuest whichQuest, JassStringArg description);
        private QuestSetDescriptionPrototype _QuestSetDescription;
        public void QuestSetDescription(JassQuest whichQuest, string description)
        {
            this._QuestSetDescription(whichQuest, description);
        }
        
        //native QuestSetIconPath takes quest whichQuest, string iconPath returns nothing
        public delegate void QuestSetIconPathPrototype(JassQuest whichQuest, JassStringArg iconPath);
        private QuestSetIconPathPrototype _QuestSetIconPath;
        public void QuestSetIconPath(JassQuest whichQuest, string iconPath)
        {
            this._QuestSetIconPath(whichQuest, iconPath);
        }
        
        //native QuestSetRequired takes quest whichQuest, boolean required returns nothing
        public delegate void QuestSetRequiredPrototype(JassQuest whichQuest, JassBoolean required);
        private QuestSetRequiredPrototype _QuestSetRequired;
        public void QuestSetRequired(JassQuest whichQuest, bool required)
        {
            this._QuestSetRequired(whichQuest, required);
        }
        
        //native QuestSetCompleted takes quest whichQuest, boolean completed returns nothing
        public delegate void QuestSetCompletedPrototype(JassQuest whichQuest, JassBoolean completed);
        private QuestSetCompletedPrototype _QuestSetCompleted;
        public void QuestSetCompleted(JassQuest whichQuest, bool completed)
        {
            this._QuestSetCompleted(whichQuest, completed);
        }
        
        //native QuestSetDiscovered takes quest whichQuest, boolean discovered returns nothing
        public delegate void QuestSetDiscoveredPrototype(JassQuest whichQuest, JassBoolean discovered);
        private QuestSetDiscoveredPrototype _QuestSetDiscovered;
        public void QuestSetDiscovered(JassQuest whichQuest, bool discovered)
        {
            this._QuestSetDiscovered(whichQuest, discovered);
        }
        
        //native QuestSetFailed takes quest whichQuest, boolean failed returns nothing
        public delegate void QuestSetFailedPrototype(JassQuest whichQuest, JassBoolean failed);
        private QuestSetFailedPrototype _QuestSetFailed;
        public void QuestSetFailed(JassQuest whichQuest, bool failed)
        {
            this._QuestSetFailed(whichQuest, failed);
        }
        
        //native QuestSetEnabled takes quest whichQuest, boolean enabled returns nothing
        public delegate void QuestSetEnabledPrototype(JassQuest whichQuest, JassBoolean enabled);
        private QuestSetEnabledPrototype _QuestSetEnabled;
        public void QuestSetEnabled(JassQuest whichQuest, bool enabled)
        {
            this._QuestSetEnabled(whichQuest, enabled);
        }
        
        //native IsQuestRequired takes quest whichQuest returns boolean
        public delegate JassBoolean IsQuestRequiredPrototype(JassQuest whichQuest);
        private IsQuestRequiredPrototype _IsQuestRequired;
        public bool IsQuestRequired(JassQuest whichQuest)
        {
            return this._IsQuestRequired(whichQuest);
        }
        
        //native IsQuestCompleted takes quest whichQuest returns boolean
        public delegate JassBoolean IsQuestCompletedPrototype(JassQuest whichQuest);
        private IsQuestCompletedPrototype _IsQuestCompleted;
        public bool IsQuestCompleted(JassQuest whichQuest)
        {
            return this._IsQuestCompleted(whichQuest);
        }
        
        //native IsQuestDiscovered takes quest whichQuest returns boolean
        public delegate JassBoolean IsQuestDiscoveredPrototype(JassQuest whichQuest);
        private IsQuestDiscoveredPrototype _IsQuestDiscovered;
        public bool IsQuestDiscovered(JassQuest whichQuest)
        {
            return this._IsQuestDiscovered(whichQuest);
        }
        
        //native IsQuestFailed takes quest whichQuest returns boolean
        public delegate JassBoolean IsQuestFailedPrototype(JassQuest whichQuest);
        private IsQuestFailedPrototype _IsQuestFailed;
        public bool IsQuestFailed(JassQuest whichQuest)
        {
            return this._IsQuestFailed(whichQuest);
        }
        
        //native IsQuestEnabled takes quest whichQuest returns boolean
        public delegate JassBoolean IsQuestEnabledPrototype(JassQuest whichQuest);
        private IsQuestEnabledPrototype _IsQuestEnabled;
        public bool IsQuestEnabled(JassQuest whichQuest)
        {
            return this._IsQuestEnabled(whichQuest);
        }
        
        //native QuestCreateItem takes quest whichQuest returns questitem
        public delegate JassQuestItem QuestCreateItemPrototype(JassQuest whichQuest);
        private QuestCreateItemPrototype _QuestCreateItem;
        public JassQuestItem QuestCreateItem(JassQuest whichQuest)
        {
            return this._QuestCreateItem(whichQuest);
        }
        
        //native QuestItemSetDescription takes questitem whichQuestItem, string description returns nothing
        public delegate void QuestItemSetDescriptionPrototype(JassQuestItem whichQuestItem, JassStringArg description);
        private QuestItemSetDescriptionPrototype _QuestItemSetDescription;
        public void QuestItemSetDescription(JassQuestItem whichQuestItem, string description)
        {
            this._QuestItemSetDescription(whichQuestItem, description);
        }
        
        //native QuestItemSetCompleted takes questitem whichQuestItem, boolean completed returns nothing
        public delegate void QuestItemSetCompletedPrototype(JassQuestItem whichQuestItem, JassBoolean completed);
        private QuestItemSetCompletedPrototype _QuestItemSetCompleted;
        public void QuestItemSetCompleted(JassQuestItem whichQuestItem, bool completed)
        {
            this._QuestItemSetCompleted(whichQuestItem, completed);
        }
        
        //native IsQuestItemCompleted takes questitem whichQuestItem returns boolean
        public delegate JassBoolean IsQuestItemCompletedPrototype(JassQuestItem whichQuestItem);
        private IsQuestItemCompletedPrototype _IsQuestItemCompleted;
        public bool IsQuestItemCompleted(JassQuestItem whichQuestItem)
        {
            return this._IsQuestItemCompleted(whichQuestItem);
        }
        
        //native CreateDefeatCondition takes nothing returns defeatcondition
        public delegate JassDefeatCondition CreateDefeatConditionPrototype();
        private CreateDefeatConditionPrototype _CreateDefeatCondition;
        public JassDefeatCondition CreateDefeatCondition()
        {
            return this._CreateDefeatCondition();
        }
        
        //native DestroyDefeatCondition takes defeatcondition whichCondition returns nothing
        public delegate void DestroyDefeatConditionPrototype(JassDefeatCondition whichCondition);
        private DestroyDefeatConditionPrototype _DestroyDefeatCondition;
        public void DestroyDefeatCondition(JassDefeatCondition whichCondition)
        {
            this._DestroyDefeatCondition(whichCondition);
        }
        
        //native DefeatConditionSetDescription takes defeatcondition whichCondition, string description returns nothing
        public delegate void DefeatConditionSetDescriptionPrototype(JassDefeatCondition whichCondition, JassStringArg description);
        private DefeatConditionSetDescriptionPrototype _DefeatConditionSetDescription;
        public void DefeatConditionSetDescription(JassDefeatCondition whichCondition, string description)
        {
            this._DefeatConditionSetDescription(whichCondition, description);
        }
        
        //native FlashQuestDialogButton takes nothing returns nothing
        public delegate void FlashQuestDialogButtonPrototype();
        private FlashQuestDialogButtonPrototype _FlashQuestDialogButton;
        public void FlashQuestDialogButton()
        {
            this._FlashQuestDialogButton();
        }
        
        //native ForceQuestDialogUpdate takes nothing returns nothing
        public delegate void ForceQuestDialogUpdatePrototype();
        private ForceQuestDialogUpdatePrototype _ForceQuestDialogUpdate;
        public void ForceQuestDialogUpdate()
        {
            this._ForceQuestDialogUpdate();
        }
        
        //native CreateTimerDialog takes timer t returns timerdialog
        public delegate JassTimerDialog CreateTimerDialogPrototype(JassTimer t);
        private CreateTimerDialogPrototype _CreateTimerDialog;
        public JassTimerDialog CreateTimerDialog(JassTimer t)
        {
            return this._CreateTimerDialog(t);
        }
        
        //native DestroyTimerDialog takes timerdialog whichDialog returns nothing
        public delegate void DestroyTimerDialogPrototype(JassTimerDialog whichDialog);
        private DestroyTimerDialogPrototype _DestroyTimerDialog;
        public void DestroyTimerDialog(JassTimerDialog whichDialog)
        {
            this._DestroyTimerDialog(whichDialog);
        }
        
        //native TimerDialogSetTitle takes timerdialog whichDialog, string title returns nothing
        public delegate void TimerDialogSetTitlePrototype(JassTimerDialog whichDialog, JassStringArg title);
        private TimerDialogSetTitlePrototype _TimerDialogSetTitle;
        public void TimerDialogSetTitle(JassTimerDialog whichDialog, string title)
        {
            this._TimerDialogSetTitle(whichDialog, title);
        }
        
        //native TimerDialogSetTitleColor takes timerdialog whichDialog, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void TimerDialogSetTitleColorPrototype(JassTimerDialog whichDialog, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private TimerDialogSetTitleColorPrototype _TimerDialogSetTitleColor;
        public void TimerDialogSetTitleColor(JassTimerDialog whichDialog, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._TimerDialogSetTitleColor(whichDialog, red, green, blue, alpha);
        }
        
        //native TimerDialogSetTimeColor takes timerdialog whichDialog, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void TimerDialogSetTimeColorPrototype(JassTimerDialog whichDialog, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private TimerDialogSetTimeColorPrototype _TimerDialogSetTimeColor;
        public void TimerDialogSetTimeColor(JassTimerDialog whichDialog, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._TimerDialogSetTimeColor(whichDialog, red, green, blue, alpha);
        }
        
        //native TimerDialogSetSpeed takes timerdialog whichDialog, real speedMultFactor returns nothing
        public delegate void TimerDialogSetSpeedPrototype(JassTimerDialog whichDialog, JassRealArg speedMultFactor);
        private TimerDialogSetSpeedPrototype _TimerDialogSetSpeed;
        public void TimerDialogSetSpeed(JassTimerDialog whichDialog, float speedMultFactor)
        {
            this._TimerDialogSetSpeed(whichDialog, speedMultFactor);
        }
        
        //native TimerDialogDisplay takes timerdialog whichDialog, boolean display returns nothing
        public delegate void TimerDialogDisplayPrototype(JassTimerDialog whichDialog, JassBoolean display);
        private TimerDialogDisplayPrototype _TimerDialogDisplay;
        public void TimerDialogDisplay(JassTimerDialog whichDialog, bool display)
        {
            this._TimerDialogDisplay(whichDialog, display);
        }
        
        //native IsTimerDialogDisplayed takes timerdialog whichDialog returns boolean
        public delegate JassBoolean IsTimerDialogDisplayedPrototype(JassTimerDialog whichDialog);
        private IsTimerDialogDisplayedPrototype _IsTimerDialogDisplayed;
        public bool IsTimerDialogDisplayed(JassTimerDialog whichDialog)
        {
            return this._IsTimerDialogDisplayed(whichDialog);
        }
        
        //native TimerDialogSetRealTimeRemaining takes timerdialog whichDialog, real timeRemaining returns nothing
        public delegate void TimerDialogSetRealTimeRemainingPrototype(JassTimerDialog whichDialog, JassRealArg timeRemaining);
        private TimerDialogSetRealTimeRemainingPrototype _TimerDialogSetRealTimeRemaining;
        public void TimerDialogSetRealTimeRemaining(JassTimerDialog whichDialog, float timeRemaining)
        {
            this._TimerDialogSetRealTimeRemaining(whichDialog, timeRemaining);
        }
        
        //native CreateLeaderboard takes nothing returns leaderboard
        public delegate JassLeaderboard CreateLeaderboardPrototype();
        private CreateLeaderboardPrototype _CreateLeaderboard;
        public JassLeaderboard CreateLeaderboard()
        {
            return this._CreateLeaderboard();
        }
        
        //native DestroyLeaderboard takes leaderboard lb returns nothing
        public delegate void DestroyLeaderboardPrototype(JassLeaderboard lb);
        private DestroyLeaderboardPrototype _DestroyLeaderboard;
        public void DestroyLeaderboard(JassLeaderboard lb)
        {
            this._DestroyLeaderboard(lb);
        }
        
        //native LeaderboardDisplay takes leaderboard lb, boolean show returns nothing
        public delegate void LeaderboardDisplayPrototype(JassLeaderboard lb, JassBoolean show);
        private LeaderboardDisplayPrototype _LeaderboardDisplay;
        public void LeaderboardDisplay(JassLeaderboard lb, bool show)
        {
            this._LeaderboardDisplay(lb, show);
        }
        
        //native IsLeaderboardDisplayed takes leaderboard lb returns boolean
        public delegate JassBoolean IsLeaderboardDisplayedPrototype(JassLeaderboard lb);
        private IsLeaderboardDisplayedPrototype _IsLeaderboardDisplayed;
        public bool IsLeaderboardDisplayed(JassLeaderboard lb)
        {
            return this._IsLeaderboardDisplayed(lb);
        }
        
        //native LeaderboardGetItemCount takes leaderboard lb returns integer
        public delegate JassInteger LeaderboardGetItemCountPrototype(JassLeaderboard lb);
        private LeaderboardGetItemCountPrototype _LeaderboardGetItemCount;
        public JassInteger LeaderboardGetItemCount(JassLeaderboard lb)
        {
            return this._LeaderboardGetItemCount(lb);
        }
        
        //native LeaderboardSetSizeByItemCount takes leaderboard lb, integer count returns nothing
        public delegate void LeaderboardSetSizeByItemCountPrototype(JassLeaderboard lb, JassInteger count);
        private LeaderboardSetSizeByItemCountPrototype _LeaderboardSetSizeByItemCount;
        public void LeaderboardSetSizeByItemCount(JassLeaderboard lb, JassInteger count)
        {
            this._LeaderboardSetSizeByItemCount(lb, count);
        }
        
        //native LeaderboardAddItem takes leaderboard lb, string label, integer value, player p returns nothing
        public delegate void LeaderboardAddItemPrototype(JassLeaderboard lb, JassStringArg label, JassInteger value, JassPlayer p);
        private LeaderboardAddItemPrototype _LeaderboardAddItem;
        public void LeaderboardAddItem(JassLeaderboard lb, string label, JassInteger value, JassPlayer p)
        {
            this._LeaderboardAddItem(lb, label, value, p);
        }
        
        //native LeaderboardRemoveItem takes leaderboard lb, integer index returns nothing
        public delegate void LeaderboardRemoveItemPrototype(JassLeaderboard lb, JassInteger index);
        private LeaderboardRemoveItemPrototype _LeaderboardRemoveItem;
        public void LeaderboardRemoveItem(JassLeaderboard lb, JassInteger index)
        {
            this._LeaderboardRemoveItem(lb, index);
        }
        
        //native LeaderboardRemovePlayerItem takes leaderboard lb, player p returns nothing
        public delegate void LeaderboardRemovePlayerItemPrototype(JassLeaderboard lb, JassPlayer p);
        private LeaderboardRemovePlayerItemPrototype _LeaderboardRemovePlayerItem;
        public void LeaderboardRemovePlayerItem(JassLeaderboard lb, JassPlayer p)
        {
            this._LeaderboardRemovePlayerItem(lb, p);
        }
        
        //native LeaderboardClear takes leaderboard lb returns nothing
        public delegate void LeaderboardClearPrototype(JassLeaderboard lb);
        private LeaderboardClearPrototype _LeaderboardClear;
        public void LeaderboardClear(JassLeaderboard lb)
        {
            this._LeaderboardClear(lb);
        }
        
        //native LeaderboardSortItemsByValue takes leaderboard lb, boolean ascending returns nothing
        public delegate void LeaderboardSortItemsByValuePrototype(JassLeaderboard lb, JassBoolean ascending);
        private LeaderboardSortItemsByValuePrototype _LeaderboardSortItemsByValue;
        public void LeaderboardSortItemsByValue(JassLeaderboard lb, bool ascending)
        {
            this._LeaderboardSortItemsByValue(lb, ascending);
        }
        
        //native LeaderboardSortItemsByPlayer takes leaderboard lb, boolean ascending returns nothing
        public delegate void LeaderboardSortItemsByPlayerPrototype(JassLeaderboard lb, JassBoolean ascending);
        private LeaderboardSortItemsByPlayerPrototype _LeaderboardSortItemsByPlayer;
        public void LeaderboardSortItemsByPlayer(JassLeaderboard lb, bool ascending)
        {
            this._LeaderboardSortItemsByPlayer(lb, ascending);
        }
        
        //native LeaderboardSortItemsByLabel takes leaderboard lb, boolean ascending returns nothing
        public delegate void LeaderboardSortItemsByLabelPrototype(JassLeaderboard lb, JassBoolean ascending);
        private LeaderboardSortItemsByLabelPrototype _LeaderboardSortItemsByLabel;
        public void LeaderboardSortItemsByLabel(JassLeaderboard lb, bool ascending)
        {
            this._LeaderboardSortItemsByLabel(lb, ascending);
        }
        
        //native LeaderboardHasPlayerItem takes leaderboard lb, player p returns boolean
        public delegate JassBoolean LeaderboardHasPlayerItemPrototype(JassLeaderboard lb, JassPlayer p);
        private LeaderboardHasPlayerItemPrototype _LeaderboardHasPlayerItem;
        public bool LeaderboardHasPlayerItem(JassLeaderboard lb, JassPlayer p)
        {
            return this._LeaderboardHasPlayerItem(lb, p);
        }
        
        //native LeaderboardGetPlayerIndex takes leaderboard lb, player p returns integer
        public delegate JassInteger LeaderboardGetPlayerIndexPrototype(JassLeaderboard lb, JassPlayer p);
        private LeaderboardGetPlayerIndexPrototype _LeaderboardGetPlayerIndex;
        public JassInteger LeaderboardGetPlayerIndex(JassLeaderboard lb, JassPlayer p)
        {
            return this._LeaderboardGetPlayerIndex(lb, p);
        }
        
        //native LeaderboardSetLabel takes leaderboard lb, string label returns nothing
        public delegate void LeaderboardSetLabelPrototype(JassLeaderboard lb, JassStringArg label);
        private LeaderboardSetLabelPrototype _LeaderboardSetLabel;
        public void LeaderboardSetLabel(JassLeaderboard lb, string label)
        {
            this._LeaderboardSetLabel(lb, label);
        }
        
        //native LeaderboardGetLabelText takes leaderboard lb returns string
        public delegate JassStringRet LeaderboardGetLabelTextPrototype(JassLeaderboard lb);
        private LeaderboardGetLabelTextPrototype _LeaderboardGetLabelText;
        public string LeaderboardGetLabelText(JassLeaderboard lb)
        {
            return this._LeaderboardGetLabelText(lb);
        }
        
        //native PlayerSetLeaderboard takes player toPlayer, leaderboard lb returns nothing
        public delegate void PlayerSetLeaderboardPrototype(JassPlayer toPlayer, JassLeaderboard lb);
        private PlayerSetLeaderboardPrototype _PlayerSetLeaderboard;
        public void PlayerSetLeaderboard(JassPlayer toPlayer, JassLeaderboard lb)
        {
            this._PlayerSetLeaderboard(toPlayer, lb);
        }
        
        //native PlayerGetLeaderboard takes player toPlayer returns leaderboard
        public delegate JassLeaderboard PlayerGetLeaderboardPrototype(JassPlayer toPlayer);
        private PlayerGetLeaderboardPrototype _PlayerGetLeaderboard;
        public JassLeaderboard PlayerGetLeaderboard(JassPlayer toPlayer)
        {
            return this._PlayerGetLeaderboard(toPlayer);
        }
        
        //native LeaderboardSetLabelColor takes leaderboard lb, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void LeaderboardSetLabelColorPrototype(JassLeaderboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private LeaderboardSetLabelColorPrototype _LeaderboardSetLabelColor;
        public void LeaderboardSetLabelColor(JassLeaderboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._LeaderboardSetLabelColor(lb, red, green, blue, alpha);
        }
        
        //native LeaderboardSetValueColor takes leaderboard lb, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void LeaderboardSetValueColorPrototype(JassLeaderboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private LeaderboardSetValueColorPrototype _LeaderboardSetValueColor;
        public void LeaderboardSetValueColor(JassLeaderboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._LeaderboardSetValueColor(lb, red, green, blue, alpha);
        }
        
        //native LeaderboardSetStyle takes leaderboard lb, boolean showLabel, boolean showNames, boolean showValues, boolean showIcons returns nothing
        public delegate void LeaderboardSetStylePrototype(JassLeaderboard lb, JassBoolean showLabel, JassBoolean showNames, JassBoolean showValues, JassBoolean showIcons);
        private LeaderboardSetStylePrototype _LeaderboardSetStyle;
        public void LeaderboardSetStyle(JassLeaderboard lb, bool showLabel, bool showNames, bool showValues, bool showIcons)
        {
            this._LeaderboardSetStyle(lb, showLabel, showNames, showValues, showIcons);
        }
        
        //native LeaderboardSetItemValue takes leaderboard lb, integer whichItem, integer val returns nothing
        public delegate void LeaderboardSetItemValuePrototype(JassLeaderboard lb, JassInteger whichItem, JassInteger val);
        private LeaderboardSetItemValuePrototype _LeaderboardSetItemValue;
        public void LeaderboardSetItemValue(JassLeaderboard lb, JassInteger whichItem, JassInteger val)
        {
            this._LeaderboardSetItemValue(lb, whichItem, val);
        }
        
        //native LeaderboardSetItemLabel takes leaderboard lb, integer whichItem, string val returns nothing
        public delegate void LeaderboardSetItemLabelPrototype(JassLeaderboard lb, JassInteger whichItem, JassStringArg val);
        private LeaderboardSetItemLabelPrototype _LeaderboardSetItemLabel;
        public void LeaderboardSetItemLabel(JassLeaderboard lb, JassInteger whichItem, string val)
        {
            this._LeaderboardSetItemLabel(lb, whichItem, val);
        }
        
        //native LeaderboardSetItemStyle takes leaderboard lb, integer whichItem, boolean showLabel, boolean showValue, boolean showIcon returns nothing
        public delegate void LeaderboardSetItemStylePrototype(JassLeaderboard lb, JassInteger whichItem, JassBoolean showLabel, JassBoolean showValue, JassBoolean showIcon);
        private LeaderboardSetItemStylePrototype _LeaderboardSetItemStyle;
        public void LeaderboardSetItemStyle(JassLeaderboard lb, JassInteger whichItem, bool showLabel, bool showValue, bool showIcon)
        {
            this._LeaderboardSetItemStyle(lb, whichItem, showLabel, showValue, showIcon);
        }
        
        //native LeaderboardSetItemLabelColor takes leaderboard lb, integer whichItem, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void LeaderboardSetItemLabelColorPrototype(JassLeaderboard lb, JassInteger whichItem, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private LeaderboardSetItemLabelColorPrototype _LeaderboardSetItemLabelColor;
        public void LeaderboardSetItemLabelColor(JassLeaderboard lb, JassInteger whichItem, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._LeaderboardSetItemLabelColor(lb, whichItem, red, green, blue, alpha);
        }
        
        //native LeaderboardSetItemValueColor takes leaderboard lb, integer whichItem, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void LeaderboardSetItemValueColorPrototype(JassLeaderboard lb, JassInteger whichItem, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private LeaderboardSetItemValueColorPrototype _LeaderboardSetItemValueColor;
        public void LeaderboardSetItemValueColor(JassLeaderboard lb, JassInteger whichItem, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._LeaderboardSetItemValueColor(lb, whichItem, red, green, blue, alpha);
        }
        
        //native CreateMultiboard takes nothing returns multiboard
        public delegate JassMultiboard CreateMultiboardPrototype();
        private CreateMultiboardPrototype _CreateMultiboard;
        public JassMultiboard CreateMultiboard()
        {
            return this._CreateMultiboard();
        }
        
        //native DestroyMultiboard takes multiboard lb returns nothing
        public delegate void DestroyMultiboardPrototype(JassMultiboard lb);
        private DestroyMultiboardPrototype _DestroyMultiboard;
        public void DestroyMultiboard(JassMultiboard lb)
        {
            this._DestroyMultiboard(lb);
        }
        
        //native MultiboardDisplay takes multiboard lb, boolean show returns nothing
        public delegate void MultiboardDisplayPrototype(JassMultiboard lb, JassBoolean show);
        private MultiboardDisplayPrototype _MultiboardDisplay;
        public void MultiboardDisplay(JassMultiboard lb, bool show)
        {
            this._MultiboardDisplay(lb, show);
        }
        
        //native IsMultiboardDisplayed takes multiboard lb returns boolean
        public delegate JassBoolean IsMultiboardDisplayedPrototype(JassMultiboard lb);
        private IsMultiboardDisplayedPrototype _IsMultiboardDisplayed;
        public bool IsMultiboardDisplayed(JassMultiboard lb)
        {
            return this._IsMultiboardDisplayed(lb);
        }
        
        //native MultiboardMinimize takes multiboard lb, boolean minimize returns nothing
        public delegate void MultiboardMinimizePrototype(JassMultiboard lb, JassBoolean minimize);
        private MultiboardMinimizePrototype _MultiboardMinimize;
        public void MultiboardMinimize(JassMultiboard lb, bool minimize)
        {
            this._MultiboardMinimize(lb, minimize);
        }
        
        //native IsMultiboardMinimized takes multiboard lb returns boolean
        public delegate JassBoolean IsMultiboardMinimizedPrototype(JassMultiboard lb);
        private IsMultiboardMinimizedPrototype _IsMultiboardMinimized;
        public bool IsMultiboardMinimized(JassMultiboard lb)
        {
            return this._IsMultiboardMinimized(lb);
        }
        
        //native MultiboardClear takes multiboard lb returns nothing
        public delegate void MultiboardClearPrototype(JassMultiboard lb);
        private MultiboardClearPrototype _MultiboardClear;
        public void MultiboardClear(JassMultiboard lb)
        {
            this._MultiboardClear(lb);
        }
        
        //native MultiboardSetTitleText takes multiboard lb, string label returns nothing
        public delegate void MultiboardSetTitleTextPrototype(JassMultiboard lb, JassStringArg label);
        private MultiboardSetTitleTextPrototype _MultiboardSetTitleText;
        public void MultiboardSetTitleText(JassMultiboard lb, string label)
        {
            this._MultiboardSetTitleText(lb, label);
        }
        
        //native MultiboardGetTitleText takes multiboard lb returns string
        public delegate JassStringRet MultiboardGetTitleTextPrototype(JassMultiboard lb);
        private MultiboardGetTitleTextPrototype _MultiboardGetTitleText;
        public string MultiboardGetTitleText(JassMultiboard lb)
        {
            return this._MultiboardGetTitleText(lb);
        }
        
        //native MultiboardSetTitleTextColor takes multiboard lb, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void MultiboardSetTitleTextColorPrototype(JassMultiboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private MultiboardSetTitleTextColorPrototype _MultiboardSetTitleTextColor;
        public void MultiboardSetTitleTextColor(JassMultiboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._MultiboardSetTitleTextColor(lb, red, green, blue, alpha);
        }
        
        //native MultiboardGetRowCount takes multiboard lb returns integer
        public delegate JassInteger MultiboardGetRowCountPrototype(JassMultiboard lb);
        private MultiboardGetRowCountPrototype _MultiboardGetRowCount;
        public JassInteger MultiboardGetRowCount(JassMultiboard lb)
        {
            return this._MultiboardGetRowCount(lb);
        }
        
        //native MultiboardGetColumnCount takes multiboard lb returns integer
        public delegate JassInteger MultiboardGetColumnCountPrototype(JassMultiboard lb);
        private MultiboardGetColumnCountPrototype _MultiboardGetColumnCount;
        public JassInteger MultiboardGetColumnCount(JassMultiboard lb)
        {
            return this._MultiboardGetColumnCount(lb);
        }
        
        //native MultiboardSetColumnCount takes multiboard lb, integer count returns nothing
        public delegate void MultiboardSetColumnCountPrototype(JassMultiboard lb, JassInteger count);
        private MultiboardSetColumnCountPrototype _MultiboardSetColumnCount;
        public void MultiboardSetColumnCount(JassMultiboard lb, JassInteger count)
        {
            this._MultiboardSetColumnCount(lb, count);
        }
        
        //native MultiboardSetRowCount takes multiboard lb, integer count returns nothing
        public delegate void MultiboardSetRowCountPrototype(JassMultiboard lb, JassInteger count);
        private MultiboardSetRowCountPrototype _MultiboardSetRowCount;
        public void MultiboardSetRowCount(JassMultiboard lb, JassInteger count)
        {
            this._MultiboardSetRowCount(lb, count);
        }
        
        //native MultiboardSetItemsStyle takes multiboard lb, boolean showValues, boolean showIcons returns nothing
        public delegate void MultiboardSetItemsStylePrototype(JassMultiboard lb, JassBoolean showValues, JassBoolean showIcons);
        private MultiboardSetItemsStylePrototype _MultiboardSetItemsStyle;
        public void MultiboardSetItemsStyle(JassMultiboard lb, bool showValues, bool showIcons)
        {
            this._MultiboardSetItemsStyle(lb, showValues, showIcons);
        }
        
        //native MultiboardSetItemsValue takes multiboard lb, string value returns nothing
        public delegate void MultiboardSetItemsValuePrototype(JassMultiboard lb, JassStringArg value);
        private MultiboardSetItemsValuePrototype _MultiboardSetItemsValue;
        public void MultiboardSetItemsValue(JassMultiboard lb, string value)
        {
            this._MultiboardSetItemsValue(lb, value);
        }
        
        //native MultiboardSetItemsValueColor takes multiboard lb, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void MultiboardSetItemsValueColorPrototype(JassMultiboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private MultiboardSetItemsValueColorPrototype _MultiboardSetItemsValueColor;
        public void MultiboardSetItemsValueColor(JassMultiboard lb, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._MultiboardSetItemsValueColor(lb, red, green, blue, alpha);
        }
        
        //native MultiboardSetItemsWidth takes multiboard lb, real width returns nothing
        public delegate void MultiboardSetItemsWidthPrototype(JassMultiboard lb, JassRealArg width);
        private MultiboardSetItemsWidthPrototype _MultiboardSetItemsWidth;
        public void MultiboardSetItemsWidth(JassMultiboard lb, float width)
        {
            this._MultiboardSetItemsWidth(lb, width);
        }
        
        //native MultiboardSetItemsIcon takes multiboard lb, string iconPath returns nothing
        public delegate void MultiboardSetItemsIconPrototype(JassMultiboard lb, JassStringArg iconPath);
        private MultiboardSetItemsIconPrototype _MultiboardSetItemsIcon;
        public void MultiboardSetItemsIcon(JassMultiboard lb, string iconPath)
        {
            this._MultiboardSetItemsIcon(lb, iconPath);
        }
        
        //native MultiboardGetItem takes multiboard lb, integer row, integer column returns multiboarditem
        public delegate JassMultiboardItem MultiboardGetItemPrototype(JassMultiboard lb, JassInteger row, JassInteger column);
        private MultiboardGetItemPrototype _MultiboardGetItem;
        public JassMultiboardItem MultiboardGetItem(JassMultiboard lb, JassInteger row, JassInteger column)
        {
            return this._MultiboardGetItem(lb, row, column);
        }
        
        //native MultiboardReleaseItem takes multiboarditem mbi returns nothing
        public delegate void MultiboardReleaseItemPrototype(JassMultiboardItem mbi);
        private MultiboardReleaseItemPrototype _MultiboardReleaseItem;
        public void MultiboardReleaseItem(JassMultiboardItem mbi)
        {
            this._MultiboardReleaseItem(mbi);
        }
        
        //native MultiboardSetItemStyle takes multiboarditem mbi, boolean showValue, boolean showIcon returns nothing
        public delegate void MultiboardSetItemStylePrototype(JassMultiboardItem mbi, JassBoolean showValue, JassBoolean showIcon);
        private MultiboardSetItemStylePrototype _MultiboardSetItemStyle;
        public void MultiboardSetItemStyle(JassMultiboardItem mbi, bool showValue, bool showIcon)
        {
            this._MultiboardSetItemStyle(mbi, showValue, showIcon);
        }
        
        //native MultiboardSetItemValue takes multiboarditem mbi, string val returns nothing
        public delegate void MultiboardSetItemValuePrototype(JassMultiboardItem mbi, JassStringArg val);
        private MultiboardSetItemValuePrototype _MultiboardSetItemValue;
        public void MultiboardSetItemValue(JassMultiboardItem mbi, string val)
        {
            this._MultiboardSetItemValue(mbi, val);
        }
        
        //native MultiboardSetItemValueColor takes multiboarditem mbi, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void MultiboardSetItemValueColorPrototype(JassMultiboardItem mbi, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private MultiboardSetItemValueColorPrototype _MultiboardSetItemValueColor;
        public void MultiboardSetItemValueColor(JassMultiboardItem mbi, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._MultiboardSetItemValueColor(mbi, red, green, blue, alpha);
        }
        
        //native MultiboardSetItemWidth takes multiboarditem mbi, real width returns nothing
        public delegate void MultiboardSetItemWidthPrototype(JassMultiboardItem mbi, JassRealArg width);
        private MultiboardSetItemWidthPrototype _MultiboardSetItemWidth;
        public void MultiboardSetItemWidth(JassMultiboardItem mbi, float width)
        {
            this._MultiboardSetItemWidth(mbi, width);
        }
        
        //native MultiboardSetItemIcon takes multiboarditem mbi, string iconFileName returns nothing
        public delegate void MultiboardSetItemIconPrototype(JassMultiboardItem mbi, JassStringArg iconFileName);
        private MultiboardSetItemIconPrototype _MultiboardSetItemIcon;
        public void MultiboardSetItemIcon(JassMultiboardItem mbi, string iconFileName)
        {
            this._MultiboardSetItemIcon(mbi, iconFileName);
        }
        
        //native MultiboardSuppressDisplay takes boolean flag returns nothing
        public delegate void MultiboardSuppressDisplayPrototype(JassBoolean flag);
        private MultiboardSuppressDisplayPrototype _MultiboardSuppressDisplay;
        public void MultiboardSuppressDisplay(bool flag)
        {
            this._MultiboardSuppressDisplay(flag);
        }
        
        //native SetCameraPosition takes real x, real y returns nothing
        public delegate void SetCameraPositionPrototype(JassRealArg x, JassRealArg y);
        private SetCameraPositionPrototype _SetCameraPosition;
        public void SetCameraPosition(float x, float y)
        {
            this._SetCameraPosition(x, y);
        }
        
        //native SetCameraQuickPosition takes real x, real y returns nothing
        public delegate void SetCameraQuickPositionPrototype(JassRealArg x, JassRealArg y);
        private SetCameraQuickPositionPrototype _SetCameraQuickPosition;
        public void SetCameraQuickPosition(float x, float y)
        {
            this._SetCameraQuickPosition(x, y);
        }
        
        //native SetCameraBounds takes real x1, real y1, real x2, real y2, real x3, real y3, real x4, real y4 returns nothing
        public delegate void SetCameraBoundsPrototype(JassRealArg x1, JassRealArg y1, JassRealArg x2, JassRealArg y2, JassRealArg x3, JassRealArg y3, JassRealArg x4, JassRealArg y4);
        private SetCameraBoundsPrototype _SetCameraBounds;
        public void SetCameraBounds(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
        {
            this._SetCameraBounds(x1, y1, x2, y2, x3, y3, x4, y4);
        }
        
        //native StopCamera takes nothing returns nothing
        public delegate void StopCameraPrototype();
        private StopCameraPrototype _StopCamera;
        public void StopCamera()
        {
            this._StopCamera();
        }
        
        //native ResetToGameCamera takes real duration returns nothing
        public delegate void ResetToGameCameraPrototype(JassRealArg duration);
        private ResetToGameCameraPrototype _ResetToGameCamera;
        public void ResetToGameCamera(float duration)
        {
            this._ResetToGameCamera(duration);
        }
        
        //native PanCameraTo takes real x, real y returns nothing
        public delegate void PanCameraToPrototype(JassRealArg x, JassRealArg y);
        private PanCameraToPrototype _PanCameraTo;
        public void PanCameraTo(float x, float y)
        {
            this._PanCameraTo(x, y);
        }
        
        //native PanCameraToTimed takes real x, real y, real duration returns nothing
        public delegate void PanCameraToTimedPrototype(JassRealArg x, JassRealArg y, JassRealArg duration);
        private PanCameraToTimedPrototype _PanCameraToTimed;
        public void PanCameraToTimed(float x, float y, float duration)
        {
            this._PanCameraToTimed(x, y, duration);
        }
        
        //native PanCameraToWithZ takes real x, real y, real zOffsetDest returns nothing
        public delegate void PanCameraToWithZPrototype(JassRealArg x, JassRealArg y, JassRealArg zOffsetDest);
        private PanCameraToWithZPrototype _PanCameraToWithZ;
        public void PanCameraToWithZ(float x, float y, float zOffsetDest)
        {
            this._PanCameraToWithZ(x, y, zOffsetDest);
        }
        
        //native PanCameraToTimedWithZ takes real x, real y, real zOffsetDest, real duration returns nothing
        public delegate void PanCameraToTimedWithZPrototype(JassRealArg x, JassRealArg y, JassRealArg zOffsetDest, JassRealArg duration);
        private PanCameraToTimedWithZPrototype _PanCameraToTimedWithZ;
        public void PanCameraToTimedWithZ(float x, float y, float zOffsetDest, float duration)
        {
            this._PanCameraToTimedWithZ(x, y, zOffsetDest, duration);
        }
        
        //native SetCinematicCamera takes string cameraModelFile returns nothing
        public delegate void SetCinematicCameraPrototype(JassStringArg cameraModelFile);
        private SetCinematicCameraPrototype _SetCinematicCamera;
        public void SetCinematicCamera(string cameraModelFile)
        {
            this._SetCinematicCamera(cameraModelFile);
        }
        
        //native SetCameraRotateMode takes real x, real y, real radiansToSweep, real duration returns nothing
        public delegate void SetCameraRotateModePrototype(JassRealArg x, JassRealArg y, JassRealArg radiansToSweep, JassRealArg duration);
        private SetCameraRotateModePrototype _SetCameraRotateMode;
        public void SetCameraRotateMode(float x, float y, float radiansToSweep, float duration)
        {
            this._SetCameraRotateMode(x, y, radiansToSweep, duration);
        }
        
        //native SetCameraField takes camerafield whichField, real value, real duration returns nothing
        public delegate void SetCameraFieldPrototype(JassCameraField whichField, JassRealArg value, JassRealArg duration);
        private SetCameraFieldPrototype _SetCameraField;
        public void SetCameraField(JassCameraField whichField, float value, float duration)
        {
            this._SetCameraField(whichField, value, duration);
        }
        
        //native AdjustCameraField takes camerafield whichField, real offset, real duration returns nothing
        public delegate void AdjustCameraFieldPrototype(JassCameraField whichField, JassRealArg offset, JassRealArg duration);
        private AdjustCameraFieldPrototype _AdjustCameraField;
        public void AdjustCameraField(JassCameraField whichField, float offset, float duration)
        {
            this._AdjustCameraField(whichField, offset, duration);
        }
        
        //native SetCameraTargetController takes unit whichUnit, real xoffset, real yoffset, boolean inheritOrientation returns nothing
        public delegate void SetCameraTargetControllerPrototype(JassUnit whichUnit, JassRealArg xoffset, JassRealArg yoffset, JassBoolean inheritOrientation);
        private SetCameraTargetControllerPrototype _SetCameraTargetController;
        public void SetCameraTargetController(JassUnit whichUnit, float xoffset, float yoffset, bool inheritOrientation)
        {
            this._SetCameraTargetController(whichUnit, xoffset, yoffset, inheritOrientation);
        }
        
        //native SetCameraOrientController takes unit whichUnit, real xoffset, real yoffset returns nothing
        public delegate void SetCameraOrientControllerPrototype(JassUnit whichUnit, JassRealArg xoffset, JassRealArg yoffset);
        private SetCameraOrientControllerPrototype _SetCameraOrientController;
        public void SetCameraOrientController(JassUnit whichUnit, float xoffset, float yoffset)
        {
            this._SetCameraOrientController(whichUnit, xoffset, yoffset);
        }
        
        //native CreateCameraSetup takes nothing returns camerasetup
        public delegate JassCameraSetup CreateCameraSetupPrototype();
        private CreateCameraSetupPrototype _CreateCameraSetup;
        public JassCameraSetup CreateCameraSetup()
        {
            return this._CreateCameraSetup();
        }
        
        //native CameraSetupSetField takes camerasetup whichSetup, camerafield whichField, real value, real duration returns nothing
        public delegate void CameraSetupSetFieldPrototype(JassCameraSetup whichSetup, JassCameraField whichField, JassRealArg value, JassRealArg duration);
        private CameraSetupSetFieldPrototype _CameraSetupSetField;
        public void CameraSetupSetField(JassCameraSetup whichSetup, JassCameraField whichField, float value, float duration)
        {
            this._CameraSetupSetField(whichSetup, whichField, value, duration);
        }
        
        //native CameraSetupGetField takes camerasetup whichSetup, camerafield whichField returns real
        public delegate JassRealRet CameraSetupGetFieldPrototype(JassCameraSetup whichSetup, JassCameraField whichField);
        private CameraSetupGetFieldPrototype _CameraSetupGetField;
        public float CameraSetupGetField(JassCameraSetup whichSetup, JassCameraField whichField)
        {
            return this._CameraSetupGetField(whichSetup, whichField);
        }
        
        //native CameraSetupSetDestPosition takes camerasetup whichSetup, real x, real y, real duration returns nothing
        public delegate void CameraSetupSetDestPositionPrototype(JassCameraSetup whichSetup, JassRealArg x, JassRealArg y, JassRealArg duration);
        private CameraSetupSetDestPositionPrototype _CameraSetupSetDestPosition;
        public void CameraSetupSetDestPosition(JassCameraSetup whichSetup, float x, float y, float duration)
        {
            this._CameraSetupSetDestPosition(whichSetup, x, y, duration);
        }
        
        //native CameraSetupGetDestPositionLoc takes camerasetup whichSetup returns location
        public delegate JassLocation CameraSetupGetDestPositionLocPrototype(JassCameraSetup whichSetup);
        private CameraSetupGetDestPositionLocPrototype _CameraSetupGetDestPositionLoc;
        public JassLocation CameraSetupGetDestPositionLoc(JassCameraSetup whichSetup)
        {
            return this._CameraSetupGetDestPositionLoc(whichSetup);
        }
        
        //native CameraSetupGetDestPositionX takes camerasetup whichSetup returns real
        public delegate JassRealRet CameraSetupGetDestPositionXPrototype(JassCameraSetup whichSetup);
        private CameraSetupGetDestPositionXPrototype _CameraSetupGetDestPositionX;
        public float CameraSetupGetDestPositionX(JassCameraSetup whichSetup)
        {
            return this._CameraSetupGetDestPositionX(whichSetup);
        }
        
        //native CameraSetupGetDestPositionY takes camerasetup whichSetup returns real
        public delegate JassRealRet CameraSetupGetDestPositionYPrototype(JassCameraSetup whichSetup);
        private CameraSetupGetDestPositionYPrototype _CameraSetupGetDestPositionY;
        public float CameraSetupGetDestPositionY(JassCameraSetup whichSetup)
        {
            return this._CameraSetupGetDestPositionY(whichSetup);
        }
        
        //native CameraSetupApply takes camerasetup whichSetup, boolean doPan, boolean panTimed returns nothing
        public delegate void CameraSetupApplyPrototype(JassCameraSetup whichSetup, JassBoolean doPan, JassBoolean panTimed);
        private CameraSetupApplyPrototype _CameraSetupApply;
        public void CameraSetupApply(JassCameraSetup whichSetup, bool doPan, bool panTimed)
        {
            this._CameraSetupApply(whichSetup, doPan, panTimed);
        }
        
        //native CameraSetupApplyWithZ takes camerasetup whichSetup, real zDestOffset returns nothing
        public delegate void CameraSetupApplyWithZPrototype(JassCameraSetup whichSetup, JassRealArg zDestOffset);
        private CameraSetupApplyWithZPrototype _CameraSetupApplyWithZ;
        public void CameraSetupApplyWithZ(JassCameraSetup whichSetup, float zDestOffset)
        {
            this._CameraSetupApplyWithZ(whichSetup, zDestOffset);
        }
        
        //native CameraSetupApplyForceDuration takes camerasetup whichSetup, boolean doPan, real forceDuration returns nothing
        public delegate void CameraSetupApplyForceDurationPrototype(JassCameraSetup whichSetup, JassBoolean doPan, JassRealArg forceDuration);
        private CameraSetupApplyForceDurationPrototype _CameraSetupApplyForceDuration;
        public void CameraSetupApplyForceDuration(JassCameraSetup whichSetup, bool doPan, float forceDuration)
        {
            this._CameraSetupApplyForceDuration(whichSetup, doPan, forceDuration);
        }
        
        //native CameraSetupApplyForceDurationWithZ takes camerasetup whichSetup, real zDestOffset, real forceDuration returns nothing
        public delegate void CameraSetupApplyForceDurationWithZPrototype(JassCameraSetup whichSetup, JassRealArg zDestOffset, JassRealArg forceDuration);
        private CameraSetupApplyForceDurationWithZPrototype _CameraSetupApplyForceDurationWithZ;
        public void CameraSetupApplyForceDurationWithZ(JassCameraSetup whichSetup, float zDestOffset, float forceDuration)
        {
            this._CameraSetupApplyForceDurationWithZ(whichSetup, zDestOffset, forceDuration);
        }
        
        //native CameraSetTargetNoise takes real mag, real velocity returns nothing
        public delegate void CameraSetTargetNoisePrototype(JassRealArg mag, JassRealArg velocity);
        private CameraSetTargetNoisePrototype _CameraSetTargetNoise;
        public void CameraSetTargetNoise(float mag, float velocity)
        {
            this._CameraSetTargetNoise(mag, velocity);
        }
        
        //native CameraSetSourceNoise takes real mag, real velocity returns nothing
        public delegate void CameraSetSourceNoisePrototype(JassRealArg mag, JassRealArg velocity);
        private CameraSetSourceNoisePrototype _CameraSetSourceNoise;
        public void CameraSetSourceNoise(float mag, float velocity)
        {
            this._CameraSetSourceNoise(mag, velocity);
        }
        
        //native CameraSetTargetNoiseEx takes real mag, real velocity, boolean vertOnly returns nothing
        public delegate void CameraSetTargetNoiseExPrototype(JassRealArg mag, JassRealArg velocity, JassBoolean vertOnly);
        private CameraSetTargetNoiseExPrototype _CameraSetTargetNoiseEx;
        public void CameraSetTargetNoiseEx(float mag, float velocity, bool vertOnly)
        {
            this._CameraSetTargetNoiseEx(mag, velocity, vertOnly);
        }
        
        //native CameraSetSourceNoiseEx takes real mag, real velocity, boolean vertOnly returns nothing
        public delegate void CameraSetSourceNoiseExPrototype(JassRealArg mag, JassRealArg velocity, JassBoolean vertOnly);
        private CameraSetSourceNoiseExPrototype _CameraSetSourceNoiseEx;
        public void CameraSetSourceNoiseEx(float mag, float velocity, bool vertOnly)
        {
            this._CameraSetSourceNoiseEx(mag, velocity, vertOnly);
        }
        
        //native CameraSetSmoothingFactor takes real factor returns nothing
        public delegate void CameraSetSmoothingFactorPrototype(JassRealArg factor);
        private CameraSetSmoothingFactorPrototype _CameraSetSmoothingFactor;
        public void CameraSetSmoothingFactor(float factor)
        {
            this._CameraSetSmoothingFactor(factor);
        }
        
        //native SetCineFilterTexture takes string filename returns nothing
        public delegate void SetCineFilterTexturePrototype(JassStringArg filename);
        private SetCineFilterTexturePrototype _SetCineFilterTexture;
        public void SetCineFilterTexture(string filename)
        {
            this._SetCineFilterTexture(filename);
        }
        
        //native SetCineFilterBlendMode takes blendmode whichMode returns nothing
        public delegate void SetCineFilterBlendModePrototype(JassBlendMode whichMode);
        private SetCineFilterBlendModePrototype _SetCineFilterBlendMode;
        public void SetCineFilterBlendMode(JassBlendMode whichMode)
        {
            this._SetCineFilterBlendMode(whichMode);
        }
        
        //native SetCineFilterTexMapFlags takes texmapflags whichFlags returns nothing
        public delegate void SetCineFilterTexMapFlagsPrototype(JassTextureMapFlags whichFlags);
        private SetCineFilterTexMapFlagsPrototype _SetCineFilterTexMapFlags;
        public void SetCineFilterTexMapFlags(JassTextureMapFlags whichFlags)
        {
            this._SetCineFilterTexMapFlags(whichFlags);
        }
        
        //native SetCineFilterStartUV takes real minu, real minv, real maxu, real maxv returns nothing
        public delegate void SetCineFilterStartUVPrototype(JassRealArg minu, JassRealArg minv, JassRealArg maxu, JassRealArg maxv);
        private SetCineFilterStartUVPrototype _SetCineFilterStartUV;
        public void SetCineFilterStartUV(float minu, float minv, float maxu, float maxv)
        {
            this._SetCineFilterStartUV(minu, minv, maxu, maxv);
        }
        
        //native SetCineFilterEndUV takes real minu, real minv, real maxu, real maxv returns nothing
        public delegate void SetCineFilterEndUVPrototype(JassRealArg minu, JassRealArg minv, JassRealArg maxu, JassRealArg maxv);
        private SetCineFilterEndUVPrototype _SetCineFilterEndUV;
        public void SetCineFilterEndUV(float minu, float minv, float maxu, float maxv)
        {
            this._SetCineFilterEndUV(minu, minv, maxu, maxv);
        }
        
        //native SetCineFilterStartColor takes integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void SetCineFilterStartColorPrototype(JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private SetCineFilterStartColorPrototype _SetCineFilterStartColor;
        public void SetCineFilterStartColor(JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._SetCineFilterStartColor(red, green, blue, alpha);
        }
        
        //native SetCineFilterEndColor takes integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void SetCineFilterEndColorPrototype(JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private SetCineFilterEndColorPrototype _SetCineFilterEndColor;
        public void SetCineFilterEndColor(JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._SetCineFilterEndColor(red, green, blue, alpha);
        }
        
        //native SetCineFilterDuration takes real duration returns nothing
        public delegate void SetCineFilterDurationPrototype(JassRealArg duration);
        private SetCineFilterDurationPrototype _SetCineFilterDuration;
        public void SetCineFilterDuration(float duration)
        {
            this._SetCineFilterDuration(duration);
        }
        
        //native DisplayCineFilter takes boolean flag returns nothing
        public delegate void DisplayCineFilterPrototype(JassBoolean flag);
        private DisplayCineFilterPrototype _DisplayCineFilter;
        public void DisplayCineFilter(bool flag)
        {
            this._DisplayCineFilter(flag);
        }
        
        //native IsCineFilterDisplayed takes nothing returns boolean
        public delegate JassBoolean IsCineFilterDisplayedPrototype();
        private IsCineFilterDisplayedPrototype _IsCineFilterDisplayed;
        public bool IsCineFilterDisplayed()
        {
            return this._IsCineFilterDisplayed();
        }
        
        //native SetCinematicScene takes integer portraitUnitId, playercolor color, string speakerTitle, string text, real sceneDuration, real voiceoverDuration returns nothing
        public delegate void SetCinematicScenePrototype(JassInteger portraitUnitId, JassPlayerColor color, JassStringArg speakerTitle, JassStringArg text, JassRealArg sceneDuration, JassRealArg voiceoverDuration);
        private SetCinematicScenePrototype _SetCinematicScene;
        public void SetCinematicScene(JassInteger portraitUnitId, JassPlayerColor color, string speakerTitle, string text, float sceneDuration, float voiceoverDuration)
        {
            this._SetCinematicScene(portraitUnitId, color, speakerTitle, text, sceneDuration, voiceoverDuration);
        }
        
        //native EndCinematicScene takes nothing returns nothing
        public delegate void EndCinematicScenePrototype();
        private EndCinematicScenePrototype _EndCinematicScene;
        public void EndCinematicScene()
        {
            this._EndCinematicScene();
        }
        
        //native ForceCinematicSubtitles takes boolean flag returns nothing
        public delegate void ForceCinematicSubtitlesPrototype(JassBoolean flag);
        private ForceCinematicSubtitlesPrototype _ForceCinematicSubtitles;
        public void ForceCinematicSubtitles(bool flag)
        {
            this._ForceCinematicSubtitles(flag);
        }
        
        //native GetCameraMargin takes integer whichMargin returns real
        public delegate JassRealRet GetCameraMarginPrototype(JassInteger whichMargin);
        private GetCameraMarginPrototype _GetCameraMargin;
        public float GetCameraMargin(JassInteger whichMargin)
        {
            return this._GetCameraMargin(whichMargin);
        }
        
        //native GetCameraBoundMinX takes nothing returns real
        public delegate JassRealRet GetCameraBoundMinXPrototype();
        private GetCameraBoundMinXPrototype _GetCameraBoundMinX;
        public float GetCameraBoundMinX()
        {
            return this._GetCameraBoundMinX();
        }
        
        //native GetCameraBoundMinY takes nothing returns real
        public delegate JassRealRet GetCameraBoundMinYPrototype();
        private GetCameraBoundMinYPrototype _GetCameraBoundMinY;
        public float GetCameraBoundMinY()
        {
            return this._GetCameraBoundMinY();
        }
        
        //native GetCameraBoundMaxX takes nothing returns real
        public delegate JassRealRet GetCameraBoundMaxXPrototype();
        private GetCameraBoundMaxXPrototype _GetCameraBoundMaxX;
        public float GetCameraBoundMaxX()
        {
            return this._GetCameraBoundMaxX();
        }
        
        //native GetCameraBoundMaxY takes nothing returns real
        public delegate JassRealRet GetCameraBoundMaxYPrototype();
        private GetCameraBoundMaxYPrototype _GetCameraBoundMaxY;
        public float GetCameraBoundMaxY()
        {
            return this._GetCameraBoundMaxY();
        }
        
        //native GetCameraField takes camerafield whichField returns real
        public delegate JassRealRet GetCameraFieldPrototype(JassCameraField whichField);
        private GetCameraFieldPrototype _GetCameraField;
        public float GetCameraField(JassCameraField whichField)
        {
            return this._GetCameraField(whichField);
        }
        
        //native GetCameraTargetPositionX takes nothing returns real
        public delegate JassRealRet GetCameraTargetPositionXPrototype();
        private GetCameraTargetPositionXPrototype _GetCameraTargetPositionX;
        public float GetCameraTargetPositionX()
        {
            return this._GetCameraTargetPositionX();
        }
        
        //native GetCameraTargetPositionY takes nothing returns real
        public delegate JassRealRet GetCameraTargetPositionYPrototype();
        private GetCameraTargetPositionYPrototype _GetCameraTargetPositionY;
        public float GetCameraTargetPositionY()
        {
            return this._GetCameraTargetPositionY();
        }
        
        //native GetCameraTargetPositionZ takes nothing returns real
        public delegate JassRealRet GetCameraTargetPositionZPrototype();
        private GetCameraTargetPositionZPrototype _GetCameraTargetPositionZ;
        public float GetCameraTargetPositionZ()
        {
            return this._GetCameraTargetPositionZ();
        }
        
        //native GetCameraTargetPositionLoc takes nothing returns location
        public delegate JassLocation GetCameraTargetPositionLocPrototype();
        private GetCameraTargetPositionLocPrototype _GetCameraTargetPositionLoc;
        public JassLocation GetCameraTargetPositionLoc()
        {
            return this._GetCameraTargetPositionLoc();
        }
        
        //native GetCameraEyePositionX takes nothing returns real
        public delegate JassRealRet GetCameraEyePositionXPrototype();
        private GetCameraEyePositionXPrototype _GetCameraEyePositionX;
        public float GetCameraEyePositionX()
        {
            return this._GetCameraEyePositionX();
        }
        
        //native GetCameraEyePositionY takes nothing returns real
        public delegate JassRealRet GetCameraEyePositionYPrototype();
        private GetCameraEyePositionYPrototype _GetCameraEyePositionY;
        public float GetCameraEyePositionY()
        {
            return this._GetCameraEyePositionY();
        }
        
        //native GetCameraEyePositionZ takes nothing returns real
        public delegate JassRealRet GetCameraEyePositionZPrototype();
        private GetCameraEyePositionZPrototype _GetCameraEyePositionZ;
        public float GetCameraEyePositionZ()
        {
            return this._GetCameraEyePositionZ();
        }
        
        //native GetCameraEyePositionLoc takes nothing returns location
        public delegate JassLocation GetCameraEyePositionLocPrototype();
        private GetCameraEyePositionLocPrototype _GetCameraEyePositionLoc;
        public JassLocation GetCameraEyePositionLoc()
        {
            return this._GetCameraEyePositionLoc();
        }
        
        //native NewSoundEnvironment takes string environmentName returns nothing
        public delegate void NewSoundEnvironmentPrototype(JassStringArg environmentName);
        private NewSoundEnvironmentPrototype _NewSoundEnvironment;
        public void NewSoundEnvironment(string environmentName)
        {
            this._NewSoundEnvironment(environmentName);
        }
        
        //native CreateSound takes string fileName, boolean looping, boolean is3D, boolean stopwhenoutofrange, integer fadeInRate, integer fadeOutRate, string eaxSetting returns sound
        public delegate JassSound CreateSoundPrototype(JassStringArg fileName, JassBoolean looping, JassBoolean is3D, JassBoolean stopwhenoutofrange, JassInteger fadeInRate, JassInteger fadeOutRate, JassStringArg eaxSetting);
        private CreateSoundPrototype _CreateSound;
        public JassSound CreateSound(string fileName, bool looping, bool is3D, bool stopwhenoutofrange, JassInteger fadeInRate, JassInteger fadeOutRate, string eaxSetting)
        {
            return this._CreateSound(fileName, looping, is3D, stopwhenoutofrange, fadeInRate, fadeOutRate, eaxSetting);
        }
        
        //native CreateSoundFilenameWithLabel takes string fileName, boolean looping, boolean is3D, boolean stopwhenoutofrange, integer fadeInRate, integer fadeOutRate, string SLKEntryName returns sound
        public delegate JassSound CreateSoundFilenameWithLabelPrototype(JassStringArg fileName, JassBoolean looping, JassBoolean is3D, JassBoolean stopwhenoutofrange, JassInteger fadeInRate, JassInteger fadeOutRate, JassStringArg SLKEntryName);
        private CreateSoundFilenameWithLabelPrototype _CreateSoundFilenameWithLabel;
        public JassSound CreateSoundFilenameWithLabel(string fileName, bool looping, bool is3D, bool stopwhenoutofrange, JassInteger fadeInRate, JassInteger fadeOutRate, string SLKEntryName)
        {
            return this._CreateSoundFilenameWithLabel(fileName, looping, is3D, stopwhenoutofrange, fadeInRate, fadeOutRate, SLKEntryName);
        }
        
        //native CreateSoundFromLabel takes string soundLabel, boolean looping, boolean is3D, boolean stopwhenoutofrange, integer fadeInRate, integer fadeOutRate returns sound
        public delegate JassSound CreateSoundFromLabelPrototype(JassStringArg soundLabel, JassBoolean looping, JassBoolean is3D, JassBoolean stopwhenoutofrange, JassInteger fadeInRate, JassInteger fadeOutRate);
        private CreateSoundFromLabelPrototype _CreateSoundFromLabel;
        public JassSound CreateSoundFromLabel(string soundLabel, bool looping, bool is3D, bool stopwhenoutofrange, JassInteger fadeInRate, JassInteger fadeOutRate)
        {
            return this._CreateSoundFromLabel(soundLabel, looping, is3D, stopwhenoutofrange, fadeInRate, fadeOutRate);
        }
        
        //native CreateMIDISound takes string soundLabel, integer fadeInRate, integer fadeOutRate returns sound
        public delegate JassSound CreateMIDISoundPrototype(JassStringArg soundLabel, JassInteger fadeInRate, JassInteger fadeOutRate);
        private CreateMIDISoundPrototype _CreateMIDISound;
        public JassSound CreateMIDISound(string soundLabel, JassInteger fadeInRate, JassInteger fadeOutRate)
        {
            return this._CreateMIDISound(soundLabel, fadeInRate, fadeOutRate);
        }
        
        //native SetSoundParamsFromLabel takes sound soundHandle, string soundLabel returns nothing
        public delegate void SetSoundParamsFromLabelPrototype(JassSound soundHandle, JassStringArg soundLabel);
        private SetSoundParamsFromLabelPrototype _SetSoundParamsFromLabel;
        public void SetSoundParamsFromLabel(JassSound soundHandle, string soundLabel)
        {
            this._SetSoundParamsFromLabel(soundHandle, soundLabel);
        }
        
        //native SetSoundDistanceCutoff takes sound soundHandle, real cutoff returns nothing
        public delegate void SetSoundDistanceCutoffPrototype(JassSound soundHandle, JassRealArg cutoff);
        private SetSoundDistanceCutoffPrototype _SetSoundDistanceCutoff;
        public void SetSoundDistanceCutoff(JassSound soundHandle, float cutoff)
        {
            this._SetSoundDistanceCutoff(soundHandle, cutoff);
        }
        
        //native SetSoundChannel takes sound soundHandle, integer channel returns nothing
        public delegate void SetSoundChannelPrototype(JassSound soundHandle, JassInteger channel);
        private SetSoundChannelPrototype _SetSoundChannel;
        public void SetSoundChannel(JassSound soundHandle, JassInteger channel)
        {
            this._SetSoundChannel(soundHandle, channel);
        }
        
        //native SetSoundVolume takes sound soundHandle, integer volume returns nothing
        public delegate void SetSoundVolumePrototype(JassSound soundHandle, JassInteger volume);
        private SetSoundVolumePrototype _SetSoundVolume;
        public void SetSoundVolume(JassSound soundHandle, JassInteger volume)
        {
            this._SetSoundVolume(soundHandle, volume);
        }
        
        //native SetSoundPitch takes sound soundHandle, real pitch returns nothing
        public delegate void SetSoundPitchPrototype(JassSound soundHandle, JassRealArg pitch);
        private SetSoundPitchPrototype _SetSoundPitch;
        public void SetSoundPitch(JassSound soundHandle, float pitch)
        {
            this._SetSoundPitch(soundHandle, pitch);
        }
        
        //native SetSoundPlayPosition takes sound soundHandle, integer millisecs returns nothing
        public delegate void SetSoundPlayPositionPrototype(JassSound soundHandle, JassInteger millisecs);
        private SetSoundPlayPositionPrototype _SetSoundPlayPosition;
        public void SetSoundPlayPosition(JassSound soundHandle, JassInteger millisecs)
        {
            this._SetSoundPlayPosition(soundHandle, millisecs);
        }
        
        //native SetSoundDistances takes sound soundHandle, real minDist, real maxDist returns nothing
        public delegate void SetSoundDistancesPrototype(JassSound soundHandle, JassRealArg minDist, JassRealArg maxDist);
        private SetSoundDistancesPrototype _SetSoundDistances;
        public void SetSoundDistances(JassSound soundHandle, float minDist, float maxDist)
        {
            this._SetSoundDistances(soundHandle, minDist, maxDist);
        }
        
        //native SetSoundConeAngles takes sound soundHandle, real inside, real outside, integer outsideVolume returns nothing
        public delegate void SetSoundConeAnglesPrototype(JassSound soundHandle, JassRealArg inside, JassRealArg outside, JassInteger outsideVolume);
        private SetSoundConeAnglesPrototype _SetSoundConeAngles;
        public void SetSoundConeAngles(JassSound soundHandle, float inside, float outside, JassInteger outsideVolume)
        {
            this._SetSoundConeAngles(soundHandle, inside, outside, outsideVolume);
        }
        
        //native SetSoundConeOrientation takes sound soundHandle, real x, real y, real z returns nothing
        public delegate void SetSoundConeOrientationPrototype(JassSound soundHandle, JassRealArg x, JassRealArg y, JassRealArg z);
        private SetSoundConeOrientationPrototype _SetSoundConeOrientation;
        public void SetSoundConeOrientation(JassSound soundHandle, float x, float y, float z)
        {
            this._SetSoundConeOrientation(soundHandle, x, y, z);
        }
        
        //native SetSoundPosition takes sound soundHandle, real x, real y, real z returns nothing
        public delegate void SetSoundPositionPrototype(JassSound soundHandle, JassRealArg x, JassRealArg y, JassRealArg z);
        private SetSoundPositionPrototype _SetSoundPosition;
        public void SetSoundPosition(JassSound soundHandle, float x, float y, float z)
        {
            this._SetSoundPosition(soundHandle, x, y, z);
        }
        
        //native SetSoundVelocity takes sound soundHandle, real x, real y, real z returns nothing
        public delegate void SetSoundVelocityPrototype(JassSound soundHandle, JassRealArg x, JassRealArg y, JassRealArg z);
        private SetSoundVelocityPrototype _SetSoundVelocity;
        public void SetSoundVelocity(JassSound soundHandle, float x, float y, float z)
        {
            this._SetSoundVelocity(soundHandle, x, y, z);
        }
        
        //native AttachSoundToUnit takes sound soundHandle, unit whichUnit returns nothing
        public delegate void AttachSoundToUnitPrototype(JassSound soundHandle, JassUnit whichUnit);
        private AttachSoundToUnitPrototype _AttachSoundToUnit;
        public void AttachSoundToUnit(JassSound soundHandle, JassUnit whichUnit)
        {
            this._AttachSoundToUnit(soundHandle, whichUnit);
        }
        
        //native StartSound takes sound soundHandle returns nothing
        public delegate void StartSoundPrototype(JassSound soundHandle);
        private StartSoundPrototype _StartSound;
        public void StartSound(JassSound soundHandle)
        {
            this._StartSound(soundHandle);
        }
        
        //native StopSound takes sound soundHandle, boolean killWhenDone, boolean fadeOut returns nothing
        public delegate void StopSoundPrototype(JassSound soundHandle, JassBoolean killWhenDone, JassBoolean fadeOut);
        private StopSoundPrototype _StopSound;
        public void StopSound(JassSound soundHandle, bool killWhenDone, bool fadeOut)
        {
            this._StopSound(soundHandle, killWhenDone, fadeOut);
        }
        
        //native KillSoundWhenDone takes sound soundHandle returns nothing
        public delegate void KillSoundWhenDonePrototype(JassSound soundHandle);
        private KillSoundWhenDonePrototype _KillSoundWhenDone;
        public void KillSoundWhenDone(JassSound soundHandle)
        {
            this._KillSoundWhenDone(soundHandle);
        }
        
        //native SetMapMusic takes string musicName, boolean random, integer index returns nothing
        public delegate void SetMapMusicPrototype(JassStringArg musicName, JassBoolean random, JassInteger index);
        private SetMapMusicPrototype _SetMapMusic;
        public void SetMapMusic(string musicName, bool random, JassInteger index)
        {
            this._SetMapMusic(musicName, random, index);
        }
        
        //native ClearMapMusic takes nothing returns nothing
        public delegate void ClearMapMusicPrototype();
        private ClearMapMusicPrototype _ClearMapMusic;
        public void ClearMapMusic()
        {
            this._ClearMapMusic();
        }
        
        //native PlayMusic takes string musicName returns nothing
        public delegate void PlayMusicPrototype(JassStringArg musicName);
        private PlayMusicPrototype _PlayMusic;
        public void PlayMusic(string musicName)
        {
            this._PlayMusic(musicName);
        }
        
        //native PlayMusicEx takes string musicName, integer frommsecs, integer fadeinmsecs returns nothing
        public delegate void PlayMusicExPrototype(JassStringArg musicName, JassInteger frommsecs, JassInteger fadeinmsecs);
        private PlayMusicExPrototype _PlayMusicEx;
        public void PlayMusicEx(string musicName, JassInteger frommsecs, JassInteger fadeinmsecs)
        {
            this._PlayMusicEx(musicName, frommsecs, fadeinmsecs);
        }
        
        //native StopMusic takes boolean fadeOut returns nothing
        public delegate void StopMusicPrototype(JassBoolean fadeOut);
        private StopMusicPrototype _StopMusic;
        public void StopMusic(bool fadeOut)
        {
            this._StopMusic(fadeOut);
        }
        
        //native ResumeMusic takes nothing returns nothing
        public delegate void ResumeMusicPrototype();
        private ResumeMusicPrototype _ResumeMusic;
        public void ResumeMusic()
        {
            this._ResumeMusic();
        }
        
        //native PlayThematicMusic takes string musicFileName returns nothing
        public delegate void PlayThematicMusicPrototype(JassStringArg musicFileName);
        private PlayThematicMusicPrototype _PlayThematicMusic;
        public void PlayThematicMusic(string musicFileName)
        {
            this._PlayThematicMusic(musicFileName);
        }
        
        //native PlayThematicMusicEx takes string musicFileName, integer frommsecs returns nothing
        public delegate void PlayThematicMusicExPrototype(JassStringArg musicFileName, JassInteger frommsecs);
        private PlayThematicMusicExPrototype _PlayThematicMusicEx;
        public void PlayThematicMusicEx(string musicFileName, JassInteger frommsecs)
        {
            this._PlayThematicMusicEx(musicFileName, frommsecs);
        }
        
        //native EndThematicMusic takes nothing returns nothing
        public delegate void EndThematicMusicPrototype();
        private EndThematicMusicPrototype _EndThematicMusic;
        public void EndThematicMusic()
        {
            this._EndThematicMusic();
        }
        
        //native SetMusicVolume takes integer volume returns nothing
        public delegate void SetMusicVolumePrototype(JassInteger volume);
        private SetMusicVolumePrototype _SetMusicVolume;
        public void SetMusicVolume(JassInteger volume)
        {
            this._SetMusicVolume(volume);
        }
        
        //native SetMusicPlayPosition takes integer millisecs returns nothing
        public delegate void SetMusicPlayPositionPrototype(JassInteger millisecs);
        private SetMusicPlayPositionPrototype _SetMusicPlayPosition;
        public void SetMusicPlayPosition(JassInteger millisecs)
        {
            this._SetMusicPlayPosition(millisecs);
        }
        
        //native SetThematicMusicPlayPosition takes integer millisecs returns nothing
        public delegate void SetThematicMusicPlayPositionPrototype(JassInteger millisecs);
        private SetThematicMusicPlayPositionPrototype _SetThematicMusicPlayPosition;
        public void SetThematicMusicPlayPosition(JassInteger millisecs)
        {
            this._SetThematicMusicPlayPosition(millisecs);
        }
        
        //native SetSoundDuration takes sound soundHandle, integer duration returns nothing
        public delegate void SetSoundDurationPrototype(JassSound soundHandle, JassInteger duration);
        private SetSoundDurationPrototype _SetSoundDuration;
        public void SetSoundDuration(JassSound soundHandle, JassInteger duration)
        {
            this._SetSoundDuration(soundHandle, duration);
        }
        
        //native GetSoundDuration takes sound soundHandle returns integer
        public delegate JassInteger GetSoundDurationPrototype(JassSound soundHandle);
        private GetSoundDurationPrototype _GetSoundDuration;
        public JassInteger GetSoundDuration(JassSound soundHandle)
        {
            return this._GetSoundDuration(soundHandle);
        }
        
        //native GetSoundFileDuration takes string musicFileName returns integer
        public delegate JassInteger GetSoundFileDurationPrototype(JassStringArg musicFileName);
        private GetSoundFileDurationPrototype _GetSoundFileDuration;
        public JassInteger GetSoundFileDuration(string musicFileName)
        {
            return this._GetSoundFileDuration(musicFileName);
        }
        
        //native VolumeGroupSetVolume takes volumegroup vgroup, real scale returns nothing
        public delegate void VolumeGroupSetVolumePrototype(JassVolumeGroup vgroup, JassRealArg scale);
        private VolumeGroupSetVolumePrototype _VolumeGroupSetVolume;
        public void VolumeGroupSetVolume(JassVolumeGroup vgroup, float scale)
        {
            this._VolumeGroupSetVolume(vgroup, scale);
        }
        
        //native VolumeGroupReset takes nothing returns nothing
        public delegate void VolumeGroupResetPrototype();
        private VolumeGroupResetPrototype _VolumeGroupReset;
        public void VolumeGroupReset()
        {
            this._VolumeGroupReset();
        }
        
        //native GetSoundIsPlaying takes sound soundHandle returns boolean
        public delegate JassBoolean GetSoundIsPlayingPrototype(JassSound soundHandle);
        private GetSoundIsPlayingPrototype _GetSoundIsPlaying;
        public bool GetSoundIsPlaying(JassSound soundHandle)
        {
            return this._GetSoundIsPlaying(soundHandle);
        }
        
        //native GetSoundIsLoading takes sound soundHandle returns boolean
        public delegate JassBoolean GetSoundIsLoadingPrototype(JassSound soundHandle);
        private GetSoundIsLoadingPrototype _GetSoundIsLoading;
        public bool GetSoundIsLoading(JassSound soundHandle)
        {
            return this._GetSoundIsLoading(soundHandle);
        }
        
        //native RegisterStackedSound takes sound soundHandle, boolean byPosition, real rectwidth, real rectheight returns nothing
        public delegate void RegisterStackedSoundPrototype(JassSound soundHandle, JassBoolean byPosition, JassRealArg rectwidth, JassRealArg rectheight);
        private RegisterStackedSoundPrototype _RegisterStackedSound;
        public void RegisterStackedSound(JassSound soundHandle, bool byPosition, float rectwidth, float rectheight)
        {
            this._RegisterStackedSound(soundHandle, byPosition, rectwidth, rectheight);
        }
        
        //native UnregisterStackedSound takes sound soundHandle, boolean byPosition, real rectwidth, real rectheight returns nothing
        public delegate void UnregisterStackedSoundPrototype(JassSound soundHandle, JassBoolean byPosition, JassRealArg rectwidth, JassRealArg rectheight);
        private UnregisterStackedSoundPrototype _UnregisterStackedSound;
        public void UnregisterStackedSound(JassSound soundHandle, bool byPosition, float rectwidth, float rectheight)
        {
            this._UnregisterStackedSound(soundHandle, byPosition, rectwidth, rectheight);
        }
        
        //native AddWeatherEffect takes rect where, integer effectID returns weathereffect
        public delegate JassWeatherEffect AddWeatherEffectPrototype(JassRect where, JassInteger effectID);
        private AddWeatherEffectPrototype _AddWeatherEffect;
        public JassWeatherEffect AddWeatherEffect(JassRect where, JassInteger effectID)
        {
            return this._AddWeatherEffect(where, effectID);
        }
        
        //native RemoveWeatherEffect takes weathereffect whichEffect returns nothing
        public delegate void RemoveWeatherEffectPrototype(JassWeatherEffect whichEffect);
        private RemoveWeatherEffectPrototype _RemoveWeatherEffect;
        public void RemoveWeatherEffect(JassWeatherEffect whichEffect)
        {
            this._RemoveWeatherEffect(whichEffect);
        }
        
        //native EnableWeatherEffect takes weathereffect whichEffect, boolean enable returns nothing
        public delegate void EnableWeatherEffectPrototype(JassWeatherEffect whichEffect, JassBoolean enable);
        private EnableWeatherEffectPrototype _EnableWeatherEffect;
        public void EnableWeatherEffect(JassWeatherEffect whichEffect, bool enable)
        {
            this._EnableWeatherEffect(whichEffect, enable);
        }
        
        //native TerrainDeformCrater takes real x, real y, real radius, real depth, integer duration, boolean permanent returns terraindeformation
        public delegate JassTerrainDeformation TerrainDeformCraterPrototype(JassRealArg x, JassRealArg y, JassRealArg radius, JassRealArg depth, JassInteger duration, JassBoolean permanent);
        private TerrainDeformCraterPrototype _TerrainDeformCrater;
        public JassTerrainDeformation TerrainDeformCrater(float x, float y, float radius, float depth, JassInteger duration, bool permanent)
        {
            return this._TerrainDeformCrater(x, y, radius, depth, duration, permanent);
        }
        
        //native TerrainDeformRipple takes real x, real y, real radius, real depth, integer duration, integer count, real spaceWaves, real timeWaves, real radiusStartPct, boolean limitNeg returns terraindeformation
        public delegate JassTerrainDeformation TerrainDeformRipplePrototype(JassRealArg x, JassRealArg y, JassRealArg radius, JassRealArg depth, JassInteger duration, JassInteger count, JassRealArg spaceWaves, JassRealArg timeWaves, JassRealArg radiusStartPct, JassBoolean limitNeg);
        private TerrainDeformRipplePrototype _TerrainDeformRipple;
        public JassTerrainDeformation TerrainDeformRipple(float x, float y, float radius, float depth, JassInteger duration, JassInteger count, float spaceWaves, float timeWaves, float radiusStartPct, bool limitNeg)
        {
            return this._TerrainDeformRipple(x, y, radius, depth, duration, count, spaceWaves, timeWaves, radiusStartPct, limitNeg);
        }
        
        //native TerrainDeformWave takes real x, real y, real dirX, real dirY, real distance, real speed, real radius, real depth, integer trailTime, integer count returns terraindeformation
        public delegate JassTerrainDeformation TerrainDeformWavePrototype(JassRealArg x, JassRealArg y, JassRealArg dirX, JassRealArg dirY, JassRealArg distance, JassRealArg speed, JassRealArg radius, JassRealArg depth, JassInteger trailTime, JassInteger count);
        private TerrainDeformWavePrototype _TerrainDeformWave;
        public JassTerrainDeformation TerrainDeformWave(float x, float y, float dirX, float dirY, float distance, float speed, float radius, float depth, JassInteger trailTime, JassInteger count)
        {
            return this._TerrainDeformWave(x, y, dirX, dirY, distance, speed, radius, depth, trailTime, count);
        }
        
        //native TerrainDeformRandom takes real x, real y, real radius, real minDelta, real maxDelta, integer duration, integer updateInterval returns terraindeformation
        public delegate JassTerrainDeformation TerrainDeformRandomPrototype(JassRealArg x, JassRealArg y, JassRealArg radius, JassRealArg minDelta, JassRealArg maxDelta, JassInteger duration, JassInteger updateInterval);
        private TerrainDeformRandomPrototype _TerrainDeformRandom;
        public JassTerrainDeformation TerrainDeformRandom(float x, float y, float radius, float minDelta, float maxDelta, JassInteger duration, JassInteger updateInterval)
        {
            return this._TerrainDeformRandom(x, y, radius, minDelta, maxDelta, duration, updateInterval);
        }
        
        //native TerrainDeformStop takes terraindeformation deformation, integer duration returns nothing
        public delegate void TerrainDeformStopPrototype(JassTerrainDeformation deformation, JassInteger duration);
        private TerrainDeformStopPrototype _TerrainDeformStop;
        public void TerrainDeformStop(JassTerrainDeformation deformation, JassInteger duration)
        {
            this._TerrainDeformStop(deformation, duration);
        }
        
        //native TerrainDeformStopAll takes nothing returns nothing
        public delegate void TerrainDeformStopAllPrototype();
        private TerrainDeformStopAllPrototype _TerrainDeformStopAll;
        public void TerrainDeformStopAll()
        {
            this._TerrainDeformStopAll();
        }
        
        //native AddSpecialEffect takes string modelName, real x, real y returns effect
        public delegate JassEffect AddSpecialEffectPrototype(JassStringArg modelName, JassRealArg x, JassRealArg y);
        private AddSpecialEffectPrototype _AddSpecialEffect;
        public JassEffect AddSpecialEffect(string modelName, float x, float y)
        {
            return this._AddSpecialEffect(modelName, x, y);
        }
        
        //native AddSpecialEffectLoc takes string modelName, location where returns effect
        public delegate JassEffect AddSpecialEffectLocPrototype(JassStringArg modelName, JassLocation where);
        private AddSpecialEffectLocPrototype _AddSpecialEffectLoc;
        public JassEffect AddSpecialEffectLoc(string modelName, JassLocation where)
        {
            return this._AddSpecialEffectLoc(modelName, where);
        }
        
        //native AddSpecialEffectTarget takes string modelName, widget targetWidget, string attachPointName returns effect
        public delegate JassEffect AddSpecialEffectTargetPrototype(JassStringArg modelName, JassWidget targetWidget, JassStringArg attachPointName);
        private AddSpecialEffectTargetPrototype _AddSpecialEffectTarget;
        public JassEffect AddSpecialEffectTarget(string modelName, JassWidget targetWidget, string attachPointName)
        {
            return this._AddSpecialEffectTarget(modelName, targetWidget, attachPointName);
        }
        
        //native DestroyEffect takes effect whichEffect returns nothing
        public delegate void DestroyEffectPrototype(JassEffect whichEffect);
        private DestroyEffectPrototype _DestroyEffect;
        public void DestroyEffect(JassEffect whichEffect)
        {
            this._DestroyEffect(whichEffect);
        }
        
        //native AddSpellEffect takes string abilityString, effecttype t, real x, real y returns effect
        public delegate JassEffect AddSpellEffectPrototype(JassStringArg abilityString, JassEffectType t, JassRealArg x, JassRealArg y);
        private AddSpellEffectPrototype _AddSpellEffect;
        public JassEffect AddSpellEffect(string abilityString, JassEffectType t, float x, float y)
        {
            return this._AddSpellEffect(abilityString, t, x, y);
        }
        
        //native AddSpellEffectLoc takes string abilityString, effecttype t, location where returns effect
        public delegate JassEffect AddSpellEffectLocPrototype(JassStringArg abilityString, JassEffectType t, JassLocation where);
        private AddSpellEffectLocPrototype _AddSpellEffectLoc;
        public JassEffect AddSpellEffectLoc(string abilityString, JassEffectType t, JassLocation where)
        {
            return this._AddSpellEffectLoc(abilityString, t, where);
        }
        
        //native AddSpellEffectById takes integer abilityId, effecttype t, real x, real y returns effect
        public delegate JassEffect AddSpellEffectByIdPrototype(JassObjectId abilityId, JassEffectType t, JassRealArg x, JassRealArg y);
        private AddSpellEffectByIdPrototype _AddSpellEffectById;
        public JassEffect AddSpellEffectById(JassObjectId abilityId, JassEffectType t, float x, float y)
        {
            return this._AddSpellEffectById(abilityId, t, x, y);
        }
        
        //native AddSpellEffectByIdLoc takes integer abilityId, effecttype t, location where returns effect
        public delegate JassEffect AddSpellEffectByIdLocPrototype(JassObjectId abilityId, JassEffectType t, JassLocation where);
        private AddSpellEffectByIdLocPrototype _AddSpellEffectByIdLoc;
        public JassEffect AddSpellEffectByIdLoc(JassObjectId abilityId, JassEffectType t, JassLocation where)
        {
            return this._AddSpellEffectByIdLoc(abilityId, t, where);
        }
        
        //native AddSpellEffectTarget takes string modelName, effecttype t, widget targetWidget, string attachPoint returns effect
        public delegate JassEffect AddSpellEffectTargetPrototype(JassStringArg modelName, JassEffectType t, JassWidget targetWidget, JassStringArg attachPoint);
        private AddSpellEffectTargetPrototype _AddSpellEffectTarget;
        public JassEffect AddSpellEffectTarget(string modelName, JassEffectType t, JassWidget targetWidget, string attachPoint)
        {
            return this._AddSpellEffectTarget(modelName, t, targetWidget, attachPoint);
        }
        
        //native AddSpellEffectTargetById takes integer abilityId, effecttype t, widget targetWidget, string attachPoint returns effect
        public delegate JassEffect AddSpellEffectTargetByIdPrototype(JassObjectId abilityId, JassEffectType t, JassWidget targetWidget, JassStringArg attachPoint);
        private AddSpellEffectTargetByIdPrototype _AddSpellEffectTargetById;
        public JassEffect AddSpellEffectTargetById(JassObjectId abilityId, JassEffectType t, JassWidget targetWidget, string attachPoint)
        {
            return this._AddSpellEffectTargetById(abilityId, t, targetWidget, attachPoint);
        }
        
        //native AddLightning takes string codeName, boolean checkVisibility, real x1, real y1, real x2, real y2 returns lightning
        public delegate JassLightning AddLightningPrototype(JassStringArg codeName, JassBoolean checkVisibility, JassRealArg x1, JassRealArg y1, JassRealArg x2, JassRealArg y2);
        private AddLightningPrototype _AddLightning;
        public JassLightning AddLightning(string codeName, bool checkVisibility, float x1, float y1, float x2, float y2)
        {
            return this._AddLightning(codeName, checkVisibility, x1, y1, x2, y2);
        }
        
        //native AddLightningEx takes string codeName, boolean checkVisibility, real x1, real y1, real z1, real x2, real y2, real z2 returns lightning
        public delegate JassLightning AddLightningExPrototype(JassStringArg codeName, JassBoolean checkVisibility, JassRealArg x1, JassRealArg y1, JassRealArg z1, JassRealArg x2, JassRealArg y2, JassRealArg z2);
        private AddLightningExPrototype _AddLightningEx;
        public JassLightning AddLightningEx(string codeName, bool checkVisibility, float x1, float y1, float z1, float x2, float y2, float z2)
        {
            return this._AddLightningEx(codeName, checkVisibility, x1, y1, z1, x2, y2, z2);
        }
        
        //native DestroyLightning takes lightning whichBolt returns boolean
        public delegate JassBoolean DestroyLightningPrototype(JassLightning whichBolt);
        private DestroyLightningPrototype _DestroyLightning;
        public bool DestroyLightning(JassLightning whichBolt)
        {
            return this._DestroyLightning(whichBolt);
        }
        
        //native MoveLightning takes lightning whichBolt, boolean checkVisibility, real x1, real y1, real x2, real y2 returns boolean
        public delegate JassBoolean MoveLightningPrototype(JassLightning whichBolt, JassBoolean checkVisibility, JassRealArg x1, JassRealArg y1, JassRealArg x2, JassRealArg y2);
        private MoveLightningPrototype _MoveLightning;
        public bool MoveLightning(JassLightning whichBolt, bool checkVisibility, float x1, float y1, float x2, float y2)
        {
            return this._MoveLightning(whichBolt, checkVisibility, x1, y1, x2, y2);
        }
        
        //native MoveLightningEx takes lightning whichBolt, boolean checkVisibility, real x1, real y1, real z1, real x2, real y2, real z2 returns boolean
        public delegate JassBoolean MoveLightningExPrototype(JassLightning whichBolt, JassBoolean checkVisibility, JassRealArg x1, JassRealArg y1, JassRealArg z1, JassRealArg x2, JassRealArg y2, JassRealArg z2);
        private MoveLightningExPrototype _MoveLightningEx;
        public bool MoveLightningEx(JassLightning whichBolt, bool checkVisibility, float x1, float y1, float z1, float x2, float y2, float z2)
        {
            return this._MoveLightningEx(whichBolt, checkVisibility, x1, y1, z1, x2, y2, z2);
        }
        
        //native GetLightningColorA takes lightning whichBolt returns real
        public delegate JassRealRet GetLightningColorAPrototype(JassLightning whichBolt);
        private GetLightningColorAPrototype _GetLightningColorA;
        public float GetLightningColorA(JassLightning whichBolt)
        {
            return this._GetLightningColorA(whichBolt);
        }
        
        //native GetLightningColorR takes lightning whichBolt returns real
        public delegate JassRealRet GetLightningColorRPrototype(JassLightning whichBolt);
        private GetLightningColorRPrototype _GetLightningColorR;
        public float GetLightningColorR(JassLightning whichBolt)
        {
            return this._GetLightningColorR(whichBolt);
        }
        
        //native GetLightningColorG takes lightning whichBolt returns real
        public delegate JassRealRet GetLightningColorGPrototype(JassLightning whichBolt);
        private GetLightningColorGPrototype _GetLightningColorG;
        public float GetLightningColorG(JassLightning whichBolt)
        {
            return this._GetLightningColorG(whichBolt);
        }
        
        //native GetLightningColorB takes lightning whichBolt returns real
        public delegate JassRealRet GetLightningColorBPrototype(JassLightning whichBolt);
        private GetLightningColorBPrototype _GetLightningColorB;
        public float GetLightningColorB(JassLightning whichBolt)
        {
            return this._GetLightningColorB(whichBolt);
        }
        
        //native SetLightningColor takes lightning whichBolt, real r, real g, real b, real a returns boolean
        public delegate JassBoolean SetLightningColorPrototype(JassLightning whichBolt, JassRealArg r, JassRealArg g, JassRealArg b, JassRealArg a);
        private SetLightningColorPrototype _SetLightningColor;
        public bool SetLightningColor(JassLightning whichBolt, float r, float g, float b, float a)
        {
            return this._SetLightningColor(whichBolt, r, g, b, a);
        }
        
        //native GetAbilityEffect takes string abilityString, effecttype t, integer index returns string
        public delegate JassStringRet GetAbilityEffectPrototype(JassStringArg abilityString, JassEffectType t, JassInteger index);
        private GetAbilityEffectPrototype _GetAbilityEffect;
        public string GetAbilityEffect(string abilityString, JassEffectType t, JassInteger index)
        {
            return this._GetAbilityEffect(abilityString, t, index);
        }
        
        //native GetAbilityEffectById takes integer abilityId, effecttype t, integer index returns string
        public delegate JassStringRet GetAbilityEffectByIdPrototype(JassObjectId abilityId, JassEffectType t, JassInteger index);
        private GetAbilityEffectByIdPrototype _GetAbilityEffectById;
        public string GetAbilityEffectById(JassObjectId abilityId, JassEffectType t, JassInteger index)
        {
            return this._GetAbilityEffectById(abilityId, t, index);
        }
        
        //native GetAbilitySound takes string abilityString, soundtype t returns string
        public delegate JassStringRet GetAbilitySoundPrototype(JassStringArg abilityString, JassSoundType t);
        private GetAbilitySoundPrototype _GetAbilitySound;
        public string GetAbilitySound(string abilityString, JassSoundType t)
        {
            return this._GetAbilitySound(abilityString, t);
        }
        
        //native GetAbilitySoundById takes integer abilityId, soundtype t returns string
        public delegate JassStringRet GetAbilitySoundByIdPrototype(JassObjectId abilityId, JassSoundType t);
        private GetAbilitySoundByIdPrototype _GetAbilitySoundById;
        public string GetAbilitySoundById(JassObjectId abilityId, JassSoundType t)
        {
            return this._GetAbilitySoundById(abilityId, t);
        }
        
        //native GetTerrainCliffLevel takes real x, real y returns integer
        public delegate JassInteger GetTerrainCliffLevelPrototype(JassRealArg x, JassRealArg y);
        private GetTerrainCliffLevelPrototype _GetTerrainCliffLevel;
        public JassInteger GetTerrainCliffLevel(float x, float y)
        {
            return this._GetTerrainCliffLevel(x, y);
        }
        
        //native SetWaterBaseColor takes integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void SetWaterBaseColorPrototype(JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private SetWaterBaseColorPrototype _SetWaterBaseColor;
        public void SetWaterBaseColor(JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._SetWaterBaseColor(red, green, blue, alpha);
        }
        
        //native SetWaterDeforms takes boolean val returns nothing
        public delegate void SetWaterDeformsPrototype(JassBoolean val);
        private SetWaterDeformsPrototype _SetWaterDeforms;
        public void SetWaterDeforms(bool val)
        {
            this._SetWaterDeforms(val);
        }
        
        //native GetTerrainType takes real x, real y returns integer
        public delegate JassInteger GetTerrainTypePrototype(JassRealArg x, JassRealArg y);
        private GetTerrainTypePrototype _GetTerrainType;
        public JassInteger GetTerrainType(float x, float y)
        {
            return this._GetTerrainType(x, y);
        }
        
        //native GetTerrainVariance takes real x, real y returns integer
        public delegate JassInteger GetTerrainVariancePrototype(JassRealArg x, JassRealArg y);
        private GetTerrainVariancePrototype _GetTerrainVariance;
        public JassInteger GetTerrainVariance(float x, float y)
        {
            return this._GetTerrainVariance(x, y);
        }
        
        //native SetTerrainType takes real x, real y, integer terrainType, integer variation, integer area, integer shape returns nothing
        public delegate void SetTerrainTypePrototype(JassRealArg x, JassRealArg y, JassInteger terrainType, JassInteger variation, JassInteger area, JassInteger shape);
        private SetTerrainTypePrototype _SetTerrainType;
        public void SetTerrainType(float x, float y, JassInteger terrainType, JassInteger variation, JassInteger area, JassInteger shape)
        {
            this._SetTerrainType(x, y, terrainType, variation, area, shape);
        }
        
        //native IsTerrainPathable takes real x, real y, pathingtype t returns boolean
        public delegate JassBoolean IsTerrainPathablePrototype(JassRealArg x, JassRealArg y, JassPathingType t);
        private IsTerrainPathablePrototype _IsTerrainPathable;
        public bool IsTerrainPathable(float x, float y, JassPathingType t)
        {
            return this._IsTerrainPathable(x, y, t);
        }
        
        //native SetTerrainPathable takes real x, real y, pathingtype t, boolean flag returns nothing
        public delegate void SetTerrainPathablePrototype(JassRealArg x, JassRealArg y, JassPathingType t, JassBoolean flag);
        private SetTerrainPathablePrototype _SetTerrainPathable;
        public void SetTerrainPathable(float x, float y, JassPathingType t, bool flag)
        {
            this._SetTerrainPathable(x, y, t, flag);
        }
        
        //native CreateImage takes string file, real sizeX, real sizeY, real sizeZ, real posX, real posY, real posZ, real originX, real originY, real originZ, integer imageType returns image
        public delegate JassImage CreateImagePrototype(JassStringArg file, JassRealArg sizeX, JassRealArg sizeY, JassRealArg sizeZ, JassRealArg posX, JassRealArg posY, JassRealArg posZ, JassRealArg originX, JassRealArg originY, JassRealArg originZ, JassInteger imageType);
        private CreateImagePrototype _CreateImage;
        public JassImage CreateImage(string file, float sizeX, float sizeY, float sizeZ, float posX, float posY, float posZ, float originX, float originY, float originZ, JassInteger imageType)
        {
            return this._CreateImage(file, sizeX, sizeY, sizeZ, posX, posY, posZ, originX, originY, originZ, imageType);
        }
        
        //native DestroyImage takes image whichImage returns nothing
        public delegate void DestroyImagePrototype(JassImage whichImage);
        private DestroyImagePrototype _DestroyImage;
        public void DestroyImage(JassImage whichImage)
        {
            this._DestroyImage(whichImage);
        }
        
        //native ShowImage takes image whichImage, boolean flag returns nothing
        public delegate void ShowImagePrototype(JassImage whichImage, JassBoolean flag);
        private ShowImagePrototype _ShowImage;
        public void ShowImage(JassImage whichImage, bool flag)
        {
            this._ShowImage(whichImage, flag);
        }
        
        //native SetImageConstantHeight takes image whichImage, boolean flag, real height returns nothing
        public delegate void SetImageConstantHeightPrototype(JassImage whichImage, JassBoolean flag, JassRealArg height);
        private SetImageConstantHeightPrototype _SetImageConstantHeight;
        public void SetImageConstantHeight(JassImage whichImage, bool flag, float height)
        {
            this._SetImageConstantHeight(whichImage, flag, height);
        }
        
        //native SetImagePosition takes image whichImage, real x, real y, real z returns nothing
        public delegate void SetImagePositionPrototype(JassImage whichImage, JassRealArg x, JassRealArg y, JassRealArg z);
        private SetImagePositionPrototype _SetImagePosition;
        public void SetImagePosition(JassImage whichImage, float x, float y, float z)
        {
            this._SetImagePosition(whichImage, x, y, z);
        }
        
        //native SetImageColor takes image whichImage, integer red, integer green, integer blue, integer alpha returns nothing
        public delegate void SetImageColorPrototype(JassImage whichImage, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha);
        private SetImageColorPrototype _SetImageColor;
        public void SetImageColor(JassImage whichImage, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha)
        {
            this._SetImageColor(whichImage, red, green, blue, alpha);
        }
        
        //native SetImageRender takes image whichImage, boolean flag returns nothing
        public delegate void SetImageRenderPrototype(JassImage whichImage, JassBoolean flag);
        private SetImageRenderPrototype _SetImageRender;
        public void SetImageRender(JassImage whichImage, bool flag)
        {
            this._SetImageRender(whichImage, flag);
        }
        
        //native SetImageRenderAlways takes image whichImage, boolean flag returns nothing
        public delegate void SetImageRenderAlwaysPrototype(JassImage whichImage, JassBoolean flag);
        private SetImageRenderAlwaysPrototype _SetImageRenderAlways;
        public void SetImageRenderAlways(JassImage whichImage, bool flag)
        {
            this._SetImageRenderAlways(whichImage, flag);
        }
        
        //native SetImageAboveWater takes image whichImage, boolean flag, boolean useWaterAlpha returns nothing
        public delegate void SetImageAboveWaterPrototype(JassImage whichImage, JassBoolean flag, JassBoolean useWaterAlpha);
        private SetImageAboveWaterPrototype _SetImageAboveWater;
        public void SetImageAboveWater(JassImage whichImage, bool flag, bool useWaterAlpha)
        {
            this._SetImageAboveWater(whichImage, flag, useWaterAlpha);
        }
        
        //native SetImageType takes image whichImage, integer imageType returns nothing
        public delegate void SetImageTypePrototype(JassImage whichImage, JassInteger imageType);
        private SetImageTypePrototype _SetImageType;
        public void SetImageType(JassImage whichImage, JassInteger imageType)
        {
            this._SetImageType(whichImage, imageType);
        }
        
        //native CreateUbersplat takes real x, real y, string name, integer red, integer green, integer blue, integer alpha, boolean forcePaused, boolean noBirthTime returns ubersplat
        public delegate JassUberSplat CreateUbersplatPrototype(JassRealArg x, JassRealArg y, JassStringArg name, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha, JassBoolean forcePaused, JassBoolean noBirthTime);
        private CreateUbersplatPrototype _CreateUbersplat;
        public JassUberSplat CreateUbersplat(float x, float y, string name, JassInteger red, JassInteger green, JassInteger blue, JassInteger alpha, bool forcePaused, bool noBirthTime)
        {
            return this._CreateUbersplat(x, y, name, red, green, blue, alpha, forcePaused, noBirthTime);
        }
        
        //native DestroyUbersplat takes ubersplat whichSplat returns nothing
        public delegate void DestroyUbersplatPrototype(JassUberSplat whichSplat);
        private DestroyUbersplatPrototype _DestroyUbersplat;
        public void DestroyUbersplat(JassUberSplat whichSplat)
        {
            this._DestroyUbersplat(whichSplat);
        }
        
        //native ResetUbersplat takes ubersplat whichSplat returns nothing
        public delegate void ResetUbersplatPrototype(JassUberSplat whichSplat);
        private ResetUbersplatPrototype _ResetUbersplat;
        public void ResetUbersplat(JassUberSplat whichSplat)
        {
            this._ResetUbersplat(whichSplat);
        }
        
        //native FinishUbersplat takes ubersplat whichSplat returns nothing
        public delegate void FinishUbersplatPrototype(JassUberSplat whichSplat);
        private FinishUbersplatPrototype _FinishUbersplat;
        public void FinishUbersplat(JassUberSplat whichSplat)
        {
            this._FinishUbersplat(whichSplat);
        }
        
        //native ShowUbersplat takes ubersplat whichSplat, boolean flag returns nothing
        public delegate void ShowUbersplatPrototype(JassUberSplat whichSplat, JassBoolean flag);
        private ShowUbersplatPrototype _ShowUbersplat;
        public void ShowUbersplat(JassUberSplat whichSplat, bool flag)
        {
            this._ShowUbersplat(whichSplat, flag);
        }
        
        //native SetUbersplatRender takes ubersplat whichSplat, boolean flag returns nothing
        public delegate void SetUbersplatRenderPrototype(JassUberSplat whichSplat, JassBoolean flag);
        private SetUbersplatRenderPrototype _SetUbersplatRender;
        public void SetUbersplatRender(JassUberSplat whichSplat, bool flag)
        {
            this._SetUbersplatRender(whichSplat, flag);
        }
        
        //native SetUbersplatRenderAlways takes ubersplat whichSplat, boolean flag returns nothing
        public delegate void SetUbersplatRenderAlwaysPrototype(JassUberSplat whichSplat, JassBoolean flag);
        private SetUbersplatRenderAlwaysPrototype _SetUbersplatRenderAlways;
        public void SetUbersplatRenderAlways(JassUberSplat whichSplat, bool flag)
        {
            this._SetUbersplatRenderAlways(whichSplat, flag);
        }
        
        //native SetBlight takes player whichPlayer, real x, real y, real radius, boolean addBlight returns nothing
        public delegate void SetBlightPrototype(JassPlayer whichPlayer, JassRealArg x, JassRealArg y, JassRealArg radius, JassBoolean addBlight);
        private SetBlightPrototype _SetBlight;
        public void SetBlight(JassPlayer whichPlayer, float x, float y, float radius, bool addBlight)
        {
            this._SetBlight(whichPlayer, x, y, radius, addBlight);
        }
        
        //native SetBlightRect takes player whichPlayer, rect r, boolean addBlight returns nothing
        public delegate void SetBlightRectPrototype(JassPlayer whichPlayer, JassRect r, JassBoolean addBlight);
        private SetBlightRectPrototype _SetBlightRect;
        public void SetBlightRect(JassPlayer whichPlayer, JassRect r, bool addBlight)
        {
            this._SetBlightRect(whichPlayer, r, addBlight);
        }
        
        //native SetBlightPoint takes player whichPlayer, real x, real y, boolean addBlight returns nothing
        public delegate void SetBlightPointPrototype(JassPlayer whichPlayer, JassRealArg x, JassRealArg y, JassBoolean addBlight);
        private SetBlightPointPrototype _SetBlightPoint;
        public void SetBlightPoint(JassPlayer whichPlayer, float x, float y, bool addBlight)
        {
            this._SetBlightPoint(whichPlayer, x, y, addBlight);
        }
        
        //native SetBlightLoc takes player whichPlayer, location whichLocation, real radius, boolean addBlight returns nothing
        public delegate void SetBlightLocPrototype(JassPlayer whichPlayer, JassLocation whichLocation, JassRealArg radius, JassBoolean addBlight);
        private SetBlightLocPrototype _SetBlightLoc;
        public void SetBlightLoc(JassPlayer whichPlayer, JassLocation whichLocation, float radius, bool addBlight)
        {
            this._SetBlightLoc(whichPlayer, whichLocation, radius, addBlight);
        }
        
        //native CreateBlightedGoldmine takes player id, real x, real y, real face returns unit
        public delegate JassUnit CreateBlightedGoldminePrototype(JassPlayer id, JassRealArg x, JassRealArg y, JassRealArg face);
        private CreateBlightedGoldminePrototype _CreateBlightedGoldmine;
        public JassUnit CreateBlightedGoldmine(JassPlayer id, float x, float y, float face)
        {
            return this._CreateBlightedGoldmine(id, x, y, face);
        }
        
        //native IsPointBlighted takes real x, real y returns boolean
        public delegate JassBoolean IsPointBlightedPrototype(JassRealArg x, JassRealArg y);
        private IsPointBlightedPrototype _IsPointBlighted;
        public bool IsPointBlighted(float x, float y)
        {
            return this._IsPointBlighted(x, y);
        }
        
        //native SetDoodadAnimation takes real x, real y, real radius, integer doodadID, boolean nearestOnly, string animName, boolean animRandom returns nothing
        public delegate void SetDoodadAnimationPrototype(JassRealArg x, JassRealArg y, JassRealArg radius, JassObjectId doodadID, JassBoolean nearestOnly, JassStringArg animName, JassBoolean animRandom);
        private SetDoodadAnimationPrototype _SetDoodadAnimation;
        public void SetDoodadAnimation(float x, float y, float radius, JassObjectId doodadID, bool nearestOnly, string animName, bool animRandom)
        {
            this._SetDoodadAnimation(x, y, radius, doodadID, nearestOnly, animName, animRandom);
        }
        
        //native SetDoodadAnimationRect takes rect r, integer doodadID, string animName, boolean animRandom returns nothing
        public delegate void SetDoodadAnimationRectPrototype(JassRect r, JassObjectId doodadID, JassStringArg animName, JassBoolean animRandom);
        private SetDoodadAnimationRectPrototype _SetDoodadAnimationRect;
        public void SetDoodadAnimationRect(JassRect r, JassObjectId doodadID, string animName, bool animRandom)
        {
            this._SetDoodadAnimationRect(r, doodadID, animName, animRandom);
        }
        
        //native StartMeleeAI takes player num, string script returns nothing
        public delegate void StartMeleeAIPrototype(JassPlayer num, JassStringArg script);
        private StartMeleeAIPrototype _StartMeleeAI;
        public void StartMeleeAI(JassPlayer num, string script)
        {
            this._StartMeleeAI(num, script);
        }
        
        //native StartCampaignAI takes player num, string script returns nothing
        public delegate void StartCampaignAIPrototype(JassPlayer num, JassStringArg script);
        private StartCampaignAIPrototype _StartCampaignAI;
        public void StartCampaignAI(JassPlayer num, string script)
        {
            this._StartCampaignAI(num, script);
        }
        
        //native CommandAI takes player num, integer command, integer data returns nothing
        public delegate void CommandAIPrototype(JassPlayer num, JassInteger command, JassInteger data);
        private CommandAIPrototype _CommandAI;
        public void CommandAI(JassPlayer num, JassInteger command, JassInteger data)
        {
            this._CommandAI(num, command, data);
        }
        
        //native PauseCompAI takes player p, boolean pause returns nothing
        public delegate void PauseCompAIPrototype(JassPlayer p, JassBoolean pause);
        private PauseCompAIPrototype _PauseCompAI;
        public void PauseCompAI(JassPlayer p, bool pause)
        {
            this._PauseCompAI(p, pause);
        }
        
        //native GetAIDifficulty takes player num returns aidifficulty
        public delegate JassAIDifficulty GetAIDifficultyPrototype(JassPlayer num);
        private GetAIDifficultyPrototype _GetAIDifficulty;
        public JassAIDifficulty GetAIDifficulty(JassPlayer num)
        {
            return this._GetAIDifficulty(num);
        }
        
        //native RemoveGuardPosition takes unit hUnit returns nothing
        public delegate void RemoveGuardPositionPrototype(JassUnit hUnit);
        private RemoveGuardPositionPrototype _RemoveGuardPosition;
        public void RemoveGuardPosition(JassUnit hUnit)
        {
            this._RemoveGuardPosition(hUnit);
        }
        
        //native RecycleGuardPosition takes unit hUnit returns nothing
        public delegate void RecycleGuardPositionPrototype(JassUnit hUnit);
        private RecycleGuardPositionPrototype _RecycleGuardPosition;
        public void RecycleGuardPosition(JassUnit hUnit)
        {
            this._RecycleGuardPosition(hUnit);
        }
        
        //native RemoveAllGuardPositions takes player num returns nothing
        public delegate void RemoveAllGuardPositionsPrototype(JassPlayer num);
        private RemoveAllGuardPositionsPrototype _RemoveAllGuardPositions;
        public void RemoveAllGuardPositions(JassPlayer num)
        {
            this._RemoveAllGuardPositions(num);
        }
        
        //native Cheat takes string cheatStr returns nothing
        public delegate void CheatPrototype(JassStringArg cheatStr);
        private CheatPrototype _Cheat;
        public void Cheat(string cheatStr)
        {
            this._Cheat(cheatStr);
        }
        
        //native IsNoVictoryCheat takes nothing returns boolean
        public delegate JassBoolean IsNoVictoryCheatPrototype();
        private IsNoVictoryCheatPrototype _IsNoVictoryCheat;
        public bool IsNoVictoryCheat()
        {
            return this._IsNoVictoryCheat();
        }
        
        //native IsNoDefeatCheat takes nothing returns boolean
        public delegate JassBoolean IsNoDefeatCheatPrototype();
        private IsNoDefeatCheatPrototype _IsNoDefeatCheat;
        public bool IsNoDefeatCheat()
        {
            return this._IsNoDefeatCheat();
        }
        
        //native Preload takes string filename returns nothing
        public delegate void PreloadPrototype(JassStringArg filename);
        private PreloadPrototype _Preload;
        public void Preload(string filename)
        {
            this._Preload(filename);
        }
        
        //native PreloadEnd takes real timeout returns nothing
        public delegate void PreloadEndPrototype(JassRealArg timeout);
        private PreloadEndPrototype _PreloadEnd;
        public void PreloadEnd(float timeout)
        {
            this._PreloadEnd(timeout);
        }
        
        //native PreloadStart takes nothing returns nothing
        public delegate void PreloadStartPrototype();
        private PreloadStartPrototype _PreloadStart;
        public void PreloadStart()
        {
            this._PreloadStart();
        }
        
        //native PreloadRefresh takes nothing returns nothing
        public delegate void PreloadRefreshPrototype();
        private PreloadRefreshPrototype _PreloadRefresh;
        public void PreloadRefresh()
        {
            this._PreloadRefresh();
        }
        
        //native PreloadEndEx takes nothing returns nothing
        public delegate void PreloadEndExPrototype();
        private PreloadEndExPrototype _PreloadEndEx;
        public void PreloadEndEx()
        {
            this._PreloadEndEx();
        }
        
        //native PreloadGenClear takes nothing returns nothing
        public delegate void PreloadGenClearPrototype();
        private PreloadGenClearPrototype _PreloadGenClear;
        public void PreloadGenClear()
        {
            this._PreloadGenClear();
        }
        
        //native PreloadGenStart takes nothing returns nothing
        public delegate void PreloadGenStartPrototype();
        private PreloadGenStartPrototype _PreloadGenStart;
        public void PreloadGenStart()
        {
            this._PreloadGenStart();
        }
        
        //native PreloadGenEnd takes string filename returns nothing
        public delegate void PreloadGenEndPrototype(JassStringArg filename);
        private PreloadGenEndPrototype _PreloadGenEnd;
        public void PreloadGenEnd(string filename)
        {
            this._PreloadGenEnd(filename);
        }
        
        //native Preloader takes string filename returns nothing
        public delegate void PreloaderPrototype(JassStringArg filename);
        private PreloaderPrototype _Preloader;
        public void Preloader(string filename)
        {
            this._Preloader(filename);
        }
        
        private void InitializeVanillaNatives()
        {
            this._ConvertRace = this.Get("ConvertRace").ToDelegate<ConvertRacePrototype>();
            this._ConvertAllianceType = this.Get("ConvertAllianceType").ToDelegate<ConvertAllianceTypePrototype>();
            this._ConvertRacePref = this.Get("ConvertRacePref").ToDelegate<ConvertRacePrefPrototype>();
            this._ConvertIGameState = this.Get("ConvertIGameState").ToDelegate<ConvertIGameStatePrototype>();
            this._ConvertFGameState = this.Get("ConvertFGameState").ToDelegate<ConvertFGameStatePrototype>();
            this._ConvertPlayerState = this.Get("ConvertPlayerState").ToDelegate<ConvertPlayerStatePrototype>();
            this._ConvertPlayerScore = this.Get("ConvertPlayerScore").ToDelegate<ConvertPlayerScorePrototype>();
            this._ConvertPlayerGameResult = this.Get("ConvertPlayerGameResult").ToDelegate<ConvertPlayerGameResultPrototype>();
            this._ConvertUnitState = this.Get("ConvertUnitState").ToDelegate<ConvertUnitStatePrototype>();
            this._ConvertAIDifficulty = this.Get("ConvertAIDifficulty").ToDelegate<ConvertAIDifficultyPrototype>();
            this._ConvertGameEvent = this.Get("ConvertGameEvent").ToDelegate<ConvertGameEventPrototype>();
            this._ConvertPlayerEvent = this.Get("ConvertPlayerEvent").ToDelegate<ConvertPlayerEventPrototype>();
            this._ConvertPlayerUnitEvent = this.Get("ConvertPlayerUnitEvent").ToDelegate<ConvertPlayerUnitEventPrototype>();
            this._ConvertWidgetEvent = this.Get("ConvertWidgetEvent").ToDelegate<ConvertWidgetEventPrototype>();
            this._ConvertDialogEvent = this.Get("ConvertDialogEvent").ToDelegate<ConvertDialogEventPrototype>();
            this._ConvertUnitEvent = this.Get("ConvertUnitEvent").ToDelegate<ConvertUnitEventPrototype>();
            this._ConvertLimitOp = this.Get("ConvertLimitOp").ToDelegate<ConvertLimitOpPrototype>();
            this._ConvertUnitType = this.Get("ConvertUnitType").ToDelegate<ConvertUnitTypePrototype>();
            this._ConvertGameSpeed = this.Get("ConvertGameSpeed").ToDelegate<ConvertGameSpeedPrototype>();
            this._ConvertPlacement = this.Get("ConvertPlacement").ToDelegate<ConvertPlacementPrototype>();
            this._ConvertStartLocPrio = this.Get("ConvertStartLocPrio").ToDelegate<ConvertStartLocPrioPrototype>();
            this._ConvertGameDifficulty = this.Get("ConvertGameDifficulty").ToDelegate<ConvertGameDifficultyPrototype>();
            this._ConvertGameType = this.Get("ConvertGameType").ToDelegate<ConvertGameTypePrototype>();
            this._ConvertMapFlag = this.Get("ConvertMapFlag").ToDelegate<ConvertMapFlagPrototype>();
            this._ConvertMapVisibility = this.Get("ConvertMapVisibility").ToDelegate<ConvertMapVisibilityPrototype>();
            this._ConvertMapSetting = this.Get("ConvertMapSetting").ToDelegate<ConvertMapSettingPrototype>();
            this._ConvertMapDensity = this.Get("ConvertMapDensity").ToDelegate<ConvertMapDensityPrototype>();
            this._ConvertMapControl = this.Get("ConvertMapControl").ToDelegate<ConvertMapControlPrototype>();
            this._ConvertPlayerColor = this.Get("ConvertPlayerColor").ToDelegate<ConvertPlayerColorPrototype>();
            this._ConvertPlayerSlotState = this.Get("ConvertPlayerSlotState").ToDelegate<ConvertPlayerSlotStatePrototype>();
            this._ConvertVolumeGroup = this.Get("ConvertVolumeGroup").ToDelegate<ConvertVolumeGroupPrototype>();
            this._ConvertCameraField = this.Get("ConvertCameraField").ToDelegate<ConvertCameraFieldPrototype>();
            this._ConvertBlendMode = this.Get("ConvertBlendMode").ToDelegate<ConvertBlendModePrototype>();
            this._ConvertRarityControl = this.Get("ConvertRarityControl").ToDelegate<ConvertRarityControlPrototype>();
            this._ConvertTexMapFlags = this.Get("ConvertTexMapFlags").ToDelegate<ConvertTexMapFlagsPrototype>();
            this._ConvertFogState = this.Get("ConvertFogState").ToDelegate<ConvertFogStatePrototype>();
            this._ConvertEffectType = this.Get("ConvertEffectType").ToDelegate<ConvertEffectTypePrototype>();
            this._ConvertVersion = this.Get("ConvertVersion").ToDelegate<ConvertVersionPrototype>();
            this._ConvertItemType = this.Get("ConvertItemType").ToDelegate<ConvertItemTypePrototype>();
            this._ConvertAttackType = this.Get("ConvertAttackType").ToDelegate<ConvertAttackTypePrototype>();
            this._ConvertDamageType = this.Get("ConvertDamageType").ToDelegate<ConvertDamageTypePrototype>();
            this._ConvertWeaponType = this.Get("ConvertWeaponType").ToDelegate<ConvertWeaponTypePrototype>();
            this._ConvertSoundType = this.Get("ConvertSoundType").ToDelegate<ConvertSoundTypePrototype>();
            this._ConvertPathingType = this.Get("ConvertPathingType").ToDelegate<ConvertPathingTypePrototype>();
            this._OrderId = this.Get("OrderId").ToDelegate<OrderIdPrototype>();
            this._OrderId2String = this.Get("OrderId2String").ToDelegate<OrderId2StringPrototype>();
            this._UnitId = this.Get("UnitId").ToDelegate<UnitIdPrototype>();
            this._UnitId2String = this.Get("UnitId2String").ToDelegate<UnitId2StringPrototype>();
            this._AbilityId = this.Get("AbilityId").ToDelegate<AbilityIdPrototype>();
            this._AbilityId2String = this.Get("AbilityId2String").ToDelegate<AbilityId2StringPrototype>();
            this._GetObjectName = this.Get("GetObjectName").ToDelegate<GetObjectNamePrototype>();
            this._Deg2Rad = this.Get("Deg2Rad").ToDelegate<Deg2RadPrototype>();
            this._Rad2Deg = this.Get("Rad2Deg").ToDelegate<Rad2DegPrototype>();
            this._Sin = this.Get("Sin").ToDelegate<SinPrototype>();
            this._Cos = this.Get("Cos").ToDelegate<CosPrototype>();
            this._Tan = this.Get("Tan").ToDelegate<TanPrototype>();
            this._Asin = this.Get("Asin").ToDelegate<AsinPrototype>();
            this._Acos = this.Get("Acos").ToDelegate<AcosPrototype>();
            this._Atan = this.Get("Atan").ToDelegate<AtanPrototype>();
            this._Atan2 = this.Get("Atan2").ToDelegate<Atan2Prototype>();
            this._SquareRoot = this.Get("SquareRoot").ToDelegate<SquareRootPrototype>();
            this._Pow = this.Get("Pow").ToDelegate<PowPrototype>();
            this._I2R = this.Get("I2R").ToDelegate<I2RPrototype>();
            this._R2I = this.Get("R2I").ToDelegate<R2IPrototype>();
            this._I2S = this.Get("I2S").ToDelegate<I2SPrototype>();
            this._R2S = this.Get("R2S").ToDelegate<R2SPrototype>();
            this._R2SW = this.Get("R2SW").ToDelegate<R2SWPrototype>();
            this._S2I = this.Get("S2I").ToDelegate<S2IPrototype>();
            this._S2R = this.Get("S2R").ToDelegate<S2RPrototype>();
            this._GetHandleId = this.Get("GetHandleId").ToDelegate<GetHandleIdPrototype>();
            this._SubString = this.Get("SubString").ToDelegate<SubStringPrototype>();
            this._StringLength = this.Get("StringLength").ToDelegate<StringLengthPrototype>();
            this._StringCase = this.Get("StringCase").ToDelegate<StringCasePrototype>();
            this._StringHash = this.Get("StringHash").ToDelegate<StringHashPrototype>();
            this._GetLocalizedString = this.Get("GetLocalizedString").ToDelegate<GetLocalizedStringPrototype>();
            this._GetLocalizedHotkey = this.Get("GetLocalizedHotkey").ToDelegate<GetLocalizedHotkeyPrototype>();
            this._SetMapName = this.Get("SetMapName").ToDelegate<SetMapNamePrototype>();
            this._SetMapDescription = this.Get("SetMapDescription").ToDelegate<SetMapDescriptionPrototype>();
            this._SetTeams = this.Get("SetTeams").ToDelegate<SetTeamsPrototype>();
            this._SetPlayers = this.Get("SetPlayers").ToDelegate<SetPlayersPrototype>();
            this._DefineStartLocation = this.Get("DefineStartLocation").ToDelegate<DefineStartLocationPrototype>();
            this._DefineStartLocationLoc = this.Get("DefineStartLocationLoc").ToDelegate<DefineStartLocationLocPrototype>();
            this._SetStartLocPrioCount = this.Get("SetStartLocPrioCount").ToDelegate<SetStartLocPrioCountPrototype>();
            this._SetStartLocPrio = this.Get("SetStartLocPrio").ToDelegate<SetStartLocPrioPrototype>();
            this._GetStartLocPrioSlot = this.Get("GetStartLocPrioSlot").ToDelegate<GetStartLocPrioSlotPrototype>();
            this._GetStartLocPrio = this.Get("GetStartLocPrio").ToDelegate<GetStartLocPrioPrototype>();
            this._SetGameTypeSupported = this.Get("SetGameTypeSupported").ToDelegate<SetGameTypeSupportedPrototype>();
            this._SetMapFlag = this.Get("SetMapFlag").ToDelegate<SetMapFlagPrototype>();
            this._SetGamePlacement = this.Get("SetGamePlacement").ToDelegate<SetGamePlacementPrototype>();
            this._SetGameSpeed = this.Get("SetGameSpeed").ToDelegate<SetGameSpeedPrototype>();
            this._SetGameDifficulty = this.Get("SetGameDifficulty").ToDelegate<SetGameDifficultyPrototype>();
            this._SetResourceDensity = this.Get("SetResourceDensity").ToDelegate<SetResourceDensityPrototype>();
            this._SetCreatureDensity = this.Get("SetCreatureDensity").ToDelegate<SetCreatureDensityPrototype>();
            this._GetTeams = this.Get("GetTeams").ToDelegate<GetTeamsPrototype>();
            this._GetPlayers = this.Get("GetPlayers").ToDelegate<GetPlayersPrototype>();
            this._IsGameTypeSupported = this.Get("IsGameTypeSupported").ToDelegate<IsGameTypeSupportedPrototype>();
            this._GetGameTypeSelected = this.Get("GetGameTypeSelected").ToDelegate<GetGameTypeSelectedPrototype>();
            this._IsMapFlagSet = this.Get("IsMapFlagSet").ToDelegate<IsMapFlagSetPrototype>();
            this._GetGamePlacement = this.Get("GetGamePlacement").ToDelegate<GetGamePlacementPrototype>();
            this._GetGameSpeed = this.Get("GetGameSpeed").ToDelegate<GetGameSpeedPrototype>();
            this._GetGameDifficulty = this.Get("GetGameDifficulty").ToDelegate<GetGameDifficultyPrototype>();
            this._GetResourceDensity = this.Get("GetResourceDensity").ToDelegate<GetResourceDensityPrototype>();
            this._GetCreatureDensity = this.Get("GetCreatureDensity").ToDelegate<GetCreatureDensityPrototype>();
            this._GetStartLocationX = this.Get("GetStartLocationX").ToDelegate<GetStartLocationXPrototype>();
            this._GetStartLocationY = this.Get("GetStartLocationY").ToDelegate<GetStartLocationYPrototype>();
            this._GetStartLocationLoc = this.Get("GetStartLocationLoc").ToDelegate<GetStartLocationLocPrototype>();
            this._SetPlayerTeam = this.Get("SetPlayerTeam").ToDelegate<SetPlayerTeamPrototype>();
            this._SetPlayerStartLocation = this.Get("SetPlayerStartLocation").ToDelegate<SetPlayerStartLocationPrototype>();
            this._ForcePlayerStartLocation = this.Get("ForcePlayerStartLocation").ToDelegate<ForcePlayerStartLocationPrototype>();
            this._SetPlayerColor = this.Get("SetPlayerColor").ToDelegate<SetPlayerColorPrototype>();
            this._SetPlayerAlliance = this.Get("SetPlayerAlliance").ToDelegate<SetPlayerAlliancePrototype>();
            this._SetPlayerTaxRate = this.Get("SetPlayerTaxRate").ToDelegate<SetPlayerTaxRatePrototype>();
            this._SetPlayerRacePreference = this.Get("SetPlayerRacePreference").ToDelegate<SetPlayerRacePreferencePrototype>();
            this._SetPlayerRaceSelectable = this.Get("SetPlayerRaceSelectable").ToDelegate<SetPlayerRaceSelectablePrototype>();
            this._SetPlayerController = this.Get("SetPlayerController").ToDelegate<SetPlayerControllerPrototype>();
            this._SetPlayerName = this.Get("SetPlayerName").ToDelegate<SetPlayerNamePrototype>();
            this._SetPlayerOnScoreScreen = this.Get("SetPlayerOnScoreScreen").ToDelegate<SetPlayerOnScoreScreenPrototype>();
            this._GetPlayerTeam = this.Get("GetPlayerTeam").ToDelegate<GetPlayerTeamPrototype>();
            this._GetPlayerStartLocation = this.Get("GetPlayerStartLocation").ToDelegate<GetPlayerStartLocationPrototype>();
            this._GetPlayerColor = this.Get("GetPlayerColor").ToDelegate<GetPlayerColorPrototype>();
            this._GetPlayerSelectable = this.Get("GetPlayerSelectable").ToDelegate<GetPlayerSelectablePrototype>();
            this._GetPlayerController = this.Get("GetPlayerController").ToDelegate<GetPlayerControllerPrototype>();
            this._GetPlayerSlotState = this.Get("GetPlayerSlotState").ToDelegate<GetPlayerSlotStatePrototype>();
            this._GetPlayerTaxRate = this.Get("GetPlayerTaxRate").ToDelegate<GetPlayerTaxRatePrototype>();
            this._IsPlayerRacePrefSet = this.Get("IsPlayerRacePrefSet").ToDelegate<IsPlayerRacePrefSetPrototype>();
            this._GetPlayerName = this.Get("GetPlayerName").ToDelegate<GetPlayerNamePrototype>();
            this._CreateTimer = this.Get("CreateTimer").ToDelegate<CreateTimerPrototype>();
            this._DestroyTimer = this.Get("DestroyTimer").ToDelegate<DestroyTimerPrototype>();
            this._TimerStart = this.Get("TimerStart").ToDelegate<TimerStartPrototype>();
            this._TimerGetElapsed = this.Get("TimerGetElapsed").ToDelegate<TimerGetElapsedPrototype>();
            this._TimerGetRemaining = this.Get("TimerGetRemaining").ToDelegate<TimerGetRemainingPrototype>();
            this._TimerGetTimeout = this.Get("TimerGetTimeout").ToDelegate<TimerGetTimeoutPrototype>();
            this._PauseTimer = this.Get("PauseTimer").ToDelegate<PauseTimerPrototype>();
            this._ResumeTimer = this.Get("ResumeTimer").ToDelegate<ResumeTimerPrototype>();
            this._GetExpiredTimer = this.Get("GetExpiredTimer").ToDelegate<GetExpiredTimerPrototype>();
            this._CreateGroup = this.Get("CreateGroup").ToDelegate<CreateGroupPrototype>();
            this._DestroyGroup = this.Get("DestroyGroup").ToDelegate<DestroyGroupPrototype>();
            this._GroupAddUnit = this.Get("GroupAddUnit").ToDelegate<GroupAddUnitPrototype>();
            this._GroupRemoveUnit = this.Get("GroupRemoveUnit").ToDelegate<GroupRemoveUnitPrototype>();
            this._GroupClear = this.Get("GroupClear").ToDelegate<GroupClearPrototype>();
            this._GroupEnumUnitsOfType = this.Get("GroupEnumUnitsOfType").ToDelegate<GroupEnumUnitsOfTypePrototype>();
            this._GroupEnumUnitsOfPlayer = this.Get("GroupEnumUnitsOfPlayer").ToDelegate<GroupEnumUnitsOfPlayerPrototype>();
            this._GroupEnumUnitsOfTypeCounted = this.Get("GroupEnumUnitsOfTypeCounted").ToDelegate<GroupEnumUnitsOfTypeCountedPrototype>();
            this._GroupEnumUnitsInRect = this.Get("GroupEnumUnitsInRect").ToDelegate<GroupEnumUnitsInRectPrototype>();
            this._GroupEnumUnitsInRectCounted = this.Get("GroupEnumUnitsInRectCounted").ToDelegate<GroupEnumUnitsInRectCountedPrototype>();
            this._GroupEnumUnitsInRange = this.Get("GroupEnumUnitsInRange").ToDelegate<GroupEnumUnitsInRangePrototype>();
            this._GroupEnumUnitsInRangeOfLoc = this.Get("GroupEnumUnitsInRangeOfLoc").ToDelegate<GroupEnumUnitsInRangeOfLocPrototype>();
            this._GroupEnumUnitsInRangeCounted = this.Get("GroupEnumUnitsInRangeCounted").ToDelegate<GroupEnumUnitsInRangeCountedPrototype>();
            this._GroupEnumUnitsInRangeOfLocCounted = this.Get("GroupEnumUnitsInRangeOfLocCounted").ToDelegate<GroupEnumUnitsInRangeOfLocCountedPrototype>();
            this._GroupEnumUnitsSelected = this.Get("GroupEnumUnitsSelected").ToDelegate<GroupEnumUnitsSelectedPrototype>();
            this._GroupImmediateOrder = this.Get("GroupImmediateOrder").ToDelegate<GroupImmediateOrderPrototype>();
            this._GroupImmediateOrderById = this.Get("GroupImmediateOrderById").ToDelegate<GroupImmediateOrderByIdPrototype>();
            this._GroupPointOrder = this.Get("GroupPointOrder").ToDelegate<GroupPointOrderPrototype>();
            this._GroupPointOrderLoc = this.Get("GroupPointOrderLoc").ToDelegate<GroupPointOrderLocPrototype>();
            this._GroupPointOrderById = this.Get("GroupPointOrderById").ToDelegate<GroupPointOrderByIdPrototype>();
            this._GroupPointOrderByIdLoc = this.Get("GroupPointOrderByIdLoc").ToDelegate<GroupPointOrderByIdLocPrototype>();
            this._GroupTargetOrder = this.Get("GroupTargetOrder").ToDelegate<GroupTargetOrderPrototype>();
            this._GroupTargetOrderById = this.Get("GroupTargetOrderById").ToDelegate<GroupTargetOrderByIdPrototype>();
            this._ForGroup = this.Get("ForGroup").ToDelegate<ForGroupPrototype>();
            this._FirstOfGroup = this.Get("FirstOfGroup").ToDelegate<FirstOfGroupPrototype>();
            this._CreateForce = this.Get("CreateForce").ToDelegate<CreateForcePrototype>();
            this._DestroyForce = this.Get("DestroyForce").ToDelegate<DestroyForcePrototype>();
            this._ForceAddPlayer = this.Get("ForceAddPlayer").ToDelegate<ForceAddPlayerPrototype>();
            this._ForceRemovePlayer = this.Get("ForceRemovePlayer").ToDelegate<ForceRemovePlayerPrototype>();
            this._ForceClear = this.Get("ForceClear").ToDelegate<ForceClearPrototype>();
            this._ForceEnumPlayers = this.Get("ForceEnumPlayers").ToDelegate<ForceEnumPlayersPrototype>();
            this._ForceEnumPlayersCounted = this.Get("ForceEnumPlayersCounted").ToDelegate<ForceEnumPlayersCountedPrototype>();
            this._ForceEnumAllies = this.Get("ForceEnumAllies").ToDelegate<ForceEnumAlliesPrototype>();
            this._ForceEnumEnemies = this.Get("ForceEnumEnemies").ToDelegate<ForceEnumEnemiesPrototype>();
            this._ForForce = this.Get("ForForce").ToDelegate<ForForcePrototype>();
            this._Rect = this.Get("Rect").ToDelegate<RectPrototype>();
            this._RectFromLoc = this.Get("RectFromLoc").ToDelegate<RectFromLocPrototype>();
            this._RemoveRect = this.Get("RemoveRect").ToDelegate<RemoveRectPrototype>();
            this._SetRect = this.Get("SetRect").ToDelegate<SetRectPrototype>();
            this._SetRectFromLoc = this.Get("SetRectFromLoc").ToDelegate<SetRectFromLocPrototype>();
            this._MoveRectTo = this.Get("MoveRectTo").ToDelegate<MoveRectToPrototype>();
            this._MoveRectToLoc = this.Get("MoveRectToLoc").ToDelegate<MoveRectToLocPrototype>();
            this._GetRectCenterX = this.Get("GetRectCenterX").ToDelegate<GetRectCenterXPrototype>();
            this._GetRectCenterY = this.Get("GetRectCenterY").ToDelegate<GetRectCenterYPrototype>();
            this._GetRectMinX = this.Get("GetRectMinX").ToDelegate<GetRectMinXPrototype>();
            this._GetRectMinY = this.Get("GetRectMinY").ToDelegate<GetRectMinYPrototype>();
            this._GetRectMaxX = this.Get("GetRectMaxX").ToDelegate<GetRectMaxXPrototype>();
            this._GetRectMaxY = this.Get("GetRectMaxY").ToDelegate<GetRectMaxYPrototype>();
            this._CreateRegion = this.Get("CreateRegion").ToDelegate<CreateRegionPrototype>();
            this._RemoveRegion = this.Get("RemoveRegion").ToDelegate<RemoveRegionPrototype>();
            this._RegionAddRect = this.Get("RegionAddRect").ToDelegate<RegionAddRectPrototype>();
            this._RegionClearRect = this.Get("RegionClearRect").ToDelegate<RegionClearRectPrototype>();
            this._RegionAddCell = this.Get("RegionAddCell").ToDelegate<RegionAddCellPrototype>();
            this._RegionAddCellAtLoc = this.Get("RegionAddCellAtLoc").ToDelegate<RegionAddCellAtLocPrototype>();
            this._RegionClearCell = this.Get("RegionClearCell").ToDelegate<RegionClearCellPrototype>();
            this._RegionClearCellAtLoc = this.Get("RegionClearCellAtLoc").ToDelegate<RegionClearCellAtLocPrototype>();
            this._Location = this.Get("Location").ToDelegate<LocationPrototype>();
            this._RemoveLocation = this.Get("RemoveLocation").ToDelegate<RemoveLocationPrototype>();
            this._MoveLocation = this.Get("MoveLocation").ToDelegate<MoveLocationPrototype>();
            this._GetLocationX = this.Get("GetLocationX").ToDelegate<GetLocationXPrototype>();
            this._GetLocationY = this.Get("GetLocationY").ToDelegate<GetLocationYPrototype>();
            this._GetLocationZ = this.Get("GetLocationZ").ToDelegate<GetLocationZPrototype>();
            this._IsUnitInRegion = this.Get("IsUnitInRegion").ToDelegate<IsUnitInRegionPrototype>();
            this._IsPointInRegion = this.Get("IsPointInRegion").ToDelegate<IsPointInRegionPrototype>();
            this._IsLocationInRegion = this.Get("IsLocationInRegion").ToDelegate<IsLocationInRegionPrototype>();
            this._GetWorldBounds = this.Get("GetWorldBounds").ToDelegate<GetWorldBoundsPrototype>();
            this._CreateTrigger = this.Get("CreateTrigger").ToDelegate<CreateTriggerPrototype>();
            this._DestroyTrigger = this.Get("DestroyTrigger").ToDelegate<DestroyTriggerPrototype>();
            this._ResetTrigger = this.Get("ResetTrigger").ToDelegate<ResetTriggerPrototype>();
            this._EnableTrigger = this.Get("EnableTrigger").ToDelegate<EnableTriggerPrototype>();
            this._DisableTrigger = this.Get("DisableTrigger").ToDelegate<DisableTriggerPrototype>();
            this._IsTriggerEnabled = this.Get("IsTriggerEnabled").ToDelegate<IsTriggerEnabledPrototype>();
            this._TriggerWaitOnSleeps = this.Get("TriggerWaitOnSleeps").ToDelegate<TriggerWaitOnSleepsPrototype>();
            this._IsTriggerWaitOnSleeps = this.Get("IsTriggerWaitOnSleeps").ToDelegate<IsTriggerWaitOnSleepsPrototype>();
            this._GetFilterUnit = this.Get("GetFilterUnit").ToDelegate<GetFilterUnitPrototype>();
            this._GetEnumUnit = this.Get("GetEnumUnit").ToDelegate<GetEnumUnitPrototype>();
            this._GetFilterDestructable = this.Get("GetFilterDestructable").ToDelegate<GetFilterDestructablePrototype>();
            this._GetEnumDestructable = this.Get("GetEnumDestructable").ToDelegate<GetEnumDestructablePrototype>();
            this._GetFilterItem = this.Get("GetFilterItem").ToDelegate<GetFilterItemPrototype>();
            this._GetEnumItem = this.Get("GetEnumItem").ToDelegate<GetEnumItemPrototype>();
            this._GetFilterPlayer = this.Get("GetFilterPlayer").ToDelegate<GetFilterPlayerPrototype>();
            this._GetEnumPlayer = this.Get("GetEnumPlayer").ToDelegate<GetEnumPlayerPrototype>();
            this._GetTriggeringTrigger = this.Get("GetTriggeringTrigger").ToDelegate<GetTriggeringTriggerPrototype>();
            this._GetTriggerEventId = this.Get("GetTriggerEventId").ToDelegate<GetTriggerEventIdPrototype>();
            this._GetTriggerEvalCount = this.Get("GetTriggerEvalCount").ToDelegate<GetTriggerEvalCountPrototype>();
            this._GetTriggerExecCount = this.Get("GetTriggerExecCount").ToDelegate<GetTriggerExecCountPrototype>();
            this._ExecuteFunc = this.Get("ExecuteFunc").ToDelegate<ExecuteFuncPrototype>();
            this._And = this.Get("And").ToDelegate<AndPrototype>();
            this._Or = this.Get("Or").ToDelegate<OrPrototype>();
            this._Not = this.Get("Not").ToDelegate<NotPrototype>();
            this._Condition = this.Get("Condition").ToDelegate<ConditionPrototype>();
            this._DestroyCondition = this.Get("DestroyCondition").ToDelegate<DestroyConditionPrototype>();
            this._Filter = this.Get("Filter").ToDelegate<FilterPrototype>();
            this._DestroyFilter = this.Get("DestroyFilter").ToDelegate<DestroyFilterPrototype>();
            this._DestroyBoolExpr = this.Get("DestroyBoolExpr").ToDelegate<DestroyBoolExprPrototype>();
            this._TriggerRegisterVariableEvent = this.Get("TriggerRegisterVariableEvent").ToDelegate<TriggerRegisterVariableEventPrototype>();
            this._TriggerRegisterTimerEvent = this.Get("TriggerRegisterTimerEvent").ToDelegate<TriggerRegisterTimerEventPrototype>();
            this._TriggerRegisterTimerExpireEvent = this.Get("TriggerRegisterTimerExpireEvent").ToDelegate<TriggerRegisterTimerExpireEventPrototype>();
            this._TriggerRegisterGameStateEvent = this.Get("TriggerRegisterGameStateEvent").ToDelegate<TriggerRegisterGameStateEventPrototype>();
            this._TriggerRegisterDialogEvent = this.Get("TriggerRegisterDialogEvent").ToDelegate<TriggerRegisterDialogEventPrototype>();
            this._TriggerRegisterDialogButtonEvent = this.Get("TriggerRegisterDialogButtonEvent").ToDelegate<TriggerRegisterDialogButtonEventPrototype>();
            this._GetEventGameState = this.Get("GetEventGameState").ToDelegate<GetEventGameStatePrototype>();
            this._TriggerRegisterGameEvent = this.Get("TriggerRegisterGameEvent").ToDelegate<TriggerRegisterGameEventPrototype>();
            this._GetWinningPlayer = this.Get("GetWinningPlayer").ToDelegate<GetWinningPlayerPrototype>();
            this._TriggerRegisterEnterRegion = this.Get("TriggerRegisterEnterRegion").ToDelegate<TriggerRegisterEnterRegionPrototype>();
            this._GetTriggeringRegion = this.Get("GetTriggeringRegion").ToDelegate<GetTriggeringRegionPrototype>();
            this._GetEnteringUnit = this.Get("GetEnteringUnit").ToDelegate<GetEnteringUnitPrototype>();
            this._TriggerRegisterLeaveRegion = this.Get("TriggerRegisterLeaveRegion").ToDelegate<TriggerRegisterLeaveRegionPrototype>();
            this._GetLeavingUnit = this.Get("GetLeavingUnit").ToDelegate<GetLeavingUnitPrototype>();
            this._TriggerRegisterTrackableHitEvent = this.Get("TriggerRegisterTrackableHitEvent").ToDelegate<TriggerRegisterTrackableHitEventPrototype>();
            this._TriggerRegisterTrackableTrackEvent = this.Get("TriggerRegisterTrackableTrackEvent").ToDelegate<TriggerRegisterTrackableTrackEventPrototype>();
            this._GetTriggeringTrackable = this.Get("GetTriggeringTrackable").ToDelegate<GetTriggeringTrackablePrototype>();
            this._GetClickedButton = this.Get("GetClickedButton").ToDelegate<GetClickedButtonPrototype>();
            this._GetClickedDialog = this.Get("GetClickedDialog").ToDelegate<GetClickedDialogPrototype>();
            this._GetTournamentFinishSoonTimeRemaining = this.Get("GetTournamentFinishSoonTimeRemaining").ToDelegate<GetTournamentFinishSoonTimeRemainingPrototype>();
            this._GetTournamentFinishNowRule = this.Get("GetTournamentFinishNowRule").ToDelegate<GetTournamentFinishNowRulePrototype>();
            this._GetTournamentFinishNowPlayer = this.Get("GetTournamentFinishNowPlayer").ToDelegate<GetTournamentFinishNowPlayerPrototype>();
            this._GetTournamentScore = this.Get("GetTournamentScore").ToDelegate<GetTournamentScorePrototype>();
            this._GetSaveBasicFilename = this.Get("GetSaveBasicFilename").ToDelegate<GetSaveBasicFilenamePrototype>();
            this._TriggerRegisterPlayerEvent = this.Get("TriggerRegisterPlayerEvent").ToDelegate<TriggerRegisterPlayerEventPrototype>();
            this._GetTriggerPlayer = this.Get("GetTriggerPlayer").ToDelegate<GetTriggerPlayerPrototype>();
            this._TriggerRegisterPlayerUnitEvent = this.Get("TriggerRegisterPlayerUnitEvent").ToDelegate<TriggerRegisterPlayerUnitEventPrototype>();
            this._GetLevelingUnit = this.Get("GetLevelingUnit").ToDelegate<GetLevelingUnitPrototype>();
            this._GetLearningUnit = this.Get("GetLearningUnit").ToDelegate<GetLearningUnitPrototype>();
            this._GetLearnedSkill = this.Get("GetLearnedSkill").ToDelegate<GetLearnedSkillPrototype>();
            this._GetLearnedSkillLevel = this.Get("GetLearnedSkillLevel").ToDelegate<GetLearnedSkillLevelPrototype>();
            this._GetRevivableUnit = this.Get("GetRevivableUnit").ToDelegate<GetRevivableUnitPrototype>();
            this._GetRevivingUnit = this.Get("GetRevivingUnit").ToDelegate<GetRevivingUnitPrototype>();
            this._GetAttacker = this.Get("GetAttacker").ToDelegate<GetAttackerPrototype>();
            this._GetRescuer = this.Get("GetRescuer").ToDelegate<GetRescuerPrototype>();
            this._GetDyingUnit = this.Get("GetDyingUnit").ToDelegate<GetDyingUnitPrototype>();
            this._GetKillingUnit = this.Get("GetKillingUnit").ToDelegate<GetKillingUnitPrototype>();
            this._GetDecayingUnit = this.Get("GetDecayingUnit").ToDelegate<GetDecayingUnitPrototype>();
            this._GetConstructingStructure = this.Get("GetConstructingStructure").ToDelegate<GetConstructingStructurePrototype>();
            this._GetCancelledStructure = this.Get("GetCancelledStructure").ToDelegate<GetCancelledStructurePrototype>();
            this._GetConstructedStructure = this.Get("GetConstructedStructure").ToDelegate<GetConstructedStructurePrototype>();
            this._GetResearchingUnit = this.Get("GetResearchingUnit").ToDelegate<GetResearchingUnitPrototype>();
            this._GetResearched = this.Get("GetResearched").ToDelegate<GetResearchedPrototype>();
            this._GetTrainedUnitType = this.Get("GetTrainedUnitType").ToDelegate<GetTrainedUnitTypePrototype>();
            this._GetTrainedUnit = this.Get("GetTrainedUnit").ToDelegate<GetTrainedUnitPrototype>();
            this._GetDetectedUnit = this.Get("GetDetectedUnit").ToDelegate<GetDetectedUnitPrototype>();
            this._GetSummoningUnit = this.Get("GetSummoningUnit").ToDelegate<GetSummoningUnitPrototype>();
            this._GetSummonedUnit = this.Get("GetSummonedUnit").ToDelegate<GetSummonedUnitPrototype>();
            this._GetTransportUnit = this.Get("GetTransportUnit").ToDelegate<GetTransportUnitPrototype>();
            this._GetLoadedUnit = this.Get("GetLoadedUnit").ToDelegate<GetLoadedUnitPrototype>();
            this._GetSellingUnit = this.Get("GetSellingUnit").ToDelegate<GetSellingUnitPrototype>();
            this._GetSoldUnit = this.Get("GetSoldUnit").ToDelegate<GetSoldUnitPrototype>();
            this._GetBuyingUnit = this.Get("GetBuyingUnit").ToDelegate<GetBuyingUnitPrototype>();
            this._GetSoldItem = this.Get("GetSoldItem").ToDelegate<GetSoldItemPrototype>();
            this._GetChangingUnit = this.Get("GetChangingUnit").ToDelegate<GetChangingUnitPrototype>();
            this._GetChangingUnitPrevOwner = this.Get("GetChangingUnitPrevOwner").ToDelegate<GetChangingUnitPrevOwnerPrototype>();
            this._GetManipulatingUnit = this.Get("GetManipulatingUnit").ToDelegate<GetManipulatingUnitPrototype>();
            this._GetManipulatedItem = this.Get("GetManipulatedItem").ToDelegate<GetManipulatedItemPrototype>();
            this._GetOrderedUnit = this.Get("GetOrderedUnit").ToDelegate<GetOrderedUnitPrototype>();
            this._GetIssuedOrderId = this.Get("GetIssuedOrderId").ToDelegate<GetIssuedOrderIdPrototype>();
            this._GetOrderPointX = this.Get("GetOrderPointX").ToDelegate<GetOrderPointXPrototype>();
            this._GetOrderPointY = this.Get("GetOrderPointY").ToDelegate<GetOrderPointYPrototype>();
            this._GetOrderPointLoc = this.Get("GetOrderPointLoc").ToDelegate<GetOrderPointLocPrototype>();
            this._GetOrderTarget = this.Get("GetOrderTarget").ToDelegate<GetOrderTargetPrototype>();
            this._GetOrderTargetDestructable = this.Get("GetOrderTargetDestructable").ToDelegate<GetOrderTargetDestructablePrototype>();
            this._GetOrderTargetItem = this.Get("GetOrderTargetItem").ToDelegate<GetOrderTargetItemPrototype>();
            this._GetOrderTargetUnit = this.Get("GetOrderTargetUnit").ToDelegate<GetOrderTargetUnitPrototype>();
            this._GetSpellAbilityUnit = this.Get("GetSpellAbilityUnit").ToDelegate<GetSpellAbilityUnitPrototype>();
            this._GetSpellAbilityId = this.Get("GetSpellAbilityId").ToDelegate<GetSpellAbilityIdPrototype>();
            this._GetSpellAbility = this.Get("GetSpellAbility").ToDelegate<GetSpellAbilityPrototype>();
            this._GetSpellTargetLoc = this.Get("GetSpellTargetLoc").ToDelegate<GetSpellTargetLocPrototype>();
            this._GetSpellTargetX = this.Get("GetSpellTargetX").ToDelegate<GetSpellTargetXPrototype>();
            this._GetSpellTargetY = this.Get("GetSpellTargetY").ToDelegate<GetSpellTargetYPrototype>();
            this._GetSpellTargetDestructable = this.Get("GetSpellTargetDestructable").ToDelegate<GetSpellTargetDestructablePrototype>();
            this._GetSpellTargetItem = this.Get("GetSpellTargetItem").ToDelegate<GetSpellTargetItemPrototype>();
            this._GetSpellTargetUnit = this.Get("GetSpellTargetUnit").ToDelegate<GetSpellTargetUnitPrototype>();
            this._TriggerRegisterPlayerAllianceChange = this.Get("TriggerRegisterPlayerAllianceChange").ToDelegate<TriggerRegisterPlayerAllianceChangePrototype>();
            this._TriggerRegisterPlayerStateEvent = this.Get("TriggerRegisterPlayerStateEvent").ToDelegate<TriggerRegisterPlayerStateEventPrototype>();
            this._GetEventPlayerState = this.Get("GetEventPlayerState").ToDelegate<GetEventPlayerStatePrototype>();
            this._TriggerRegisterPlayerChatEvent = this.Get("TriggerRegisterPlayerChatEvent").ToDelegate<TriggerRegisterPlayerChatEventPrototype>();
            this._GetEventPlayerChatString = this.Get("GetEventPlayerChatString").ToDelegate<GetEventPlayerChatStringPrototype>();
            this._GetEventPlayerChatStringMatched = this.Get("GetEventPlayerChatStringMatched").ToDelegate<GetEventPlayerChatStringMatchedPrototype>();
            this._TriggerRegisterDeathEvent = this.Get("TriggerRegisterDeathEvent").ToDelegate<TriggerRegisterDeathEventPrototype>();
            this._GetTriggerUnit = this.Get("GetTriggerUnit").ToDelegate<GetTriggerUnitPrototype>();
            this._TriggerRegisterUnitStateEvent = this.Get("TriggerRegisterUnitStateEvent").ToDelegate<TriggerRegisterUnitStateEventPrototype>();
            this._GetEventUnitState = this.Get("GetEventUnitState").ToDelegate<GetEventUnitStatePrototype>();
            this._TriggerRegisterUnitEvent = this.Get("TriggerRegisterUnitEvent").ToDelegate<TriggerRegisterUnitEventPrototype>();
            this._GetEventDamage = this.Get("GetEventDamage").ToDelegate<GetEventDamagePrototype>();
            this._GetEventDamageSource = this.Get("GetEventDamageSource").ToDelegate<GetEventDamageSourcePrototype>();
            this._GetEventDetectingPlayer = this.Get("GetEventDetectingPlayer").ToDelegate<GetEventDetectingPlayerPrototype>();
            this._TriggerRegisterFilterUnitEvent = this.Get("TriggerRegisterFilterUnitEvent").ToDelegate<TriggerRegisterFilterUnitEventPrototype>();
            this._GetEventTargetUnit = this.Get("GetEventTargetUnit").ToDelegate<GetEventTargetUnitPrototype>();
            this._TriggerRegisterUnitInRange = this.Get("TriggerRegisterUnitInRange").ToDelegate<TriggerRegisterUnitInRangePrototype>();
            this._TriggerAddCondition = this.Get("TriggerAddCondition").ToDelegate<TriggerAddConditionPrototype>();
            this._TriggerRemoveCondition = this.Get("TriggerRemoveCondition").ToDelegate<TriggerRemoveConditionPrototype>();
            this._TriggerClearConditions = this.Get("TriggerClearConditions").ToDelegate<TriggerClearConditionsPrototype>();
            this._TriggerAddAction = this.Get("TriggerAddAction").ToDelegate<TriggerAddActionPrototype>();
            this._TriggerRemoveAction = this.Get("TriggerRemoveAction").ToDelegate<TriggerRemoveActionPrototype>();
            this._TriggerClearActions = this.Get("TriggerClearActions").ToDelegate<TriggerClearActionsPrototype>();
            this._TriggerSleepAction = this.Get("TriggerSleepAction").ToDelegate<TriggerSleepActionPrototype>();
            this._TriggerWaitForSound = this.Get("TriggerWaitForSound").ToDelegate<TriggerWaitForSoundPrototype>();
            this._TriggerEvaluate = this.Get("TriggerEvaluate").ToDelegate<TriggerEvaluatePrototype>();
            this._TriggerExecute = this.Get("TriggerExecute").ToDelegate<TriggerExecutePrototype>();
            this._TriggerExecuteWait = this.Get("TriggerExecuteWait").ToDelegate<TriggerExecuteWaitPrototype>();
            this._TriggerSyncStart = this.Get("TriggerSyncStart").ToDelegate<TriggerSyncStartPrototype>();
            this._TriggerSyncReady = this.Get("TriggerSyncReady").ToDelegate<TriggerSyncReadyPrototype>();
            this._GetWidgetLife = this.Get("GetWidgetLife").ToDelegate<GetWidgetLifePrototype>();
            this._SetWidgetLife = this.Get("SetWidgetLife").ToDelegate<SetWidgetLifePrototype>();
            this._GetWidgetX = this.Get("GetWidgetX").ToDelegate<GetWidgetXPrototype>();
            this._GetWidgetY = this.Get("GetWidgetY").ToDelegate<GetWidgetYPrototype>();
            this._GetTriggerWidget = this.Get("GetTriggerWidget").ToDelegate<GetTriggerWidgetPrototype>();
            this._CreateDestructable = this.Get("CreateDestructable").ToDelegate<CreateDestructablePrototype>();
            this._CreateDestructableZ = this.Get("CreateDestructableZ").ToDelegate<CreateDestructableZPrototype>();
            this._CreateDeadDestructable = this.Get("CreateDeadDestructable").ToDelegate<CreateDeadDestructablePrototype>();
            this._CreateDeadDestructableZ = this.Get("CreateDeadDestructableZ").ToDelegate<CreateDeadDestructableZPrototype>();
            this._RemoveDestructable = this.Get("RemoveDestructable").ToDelegate<RemoveDestructablePrototype>();
            this._KillDestructable = this.Get("KillDestructable").ToDelegate<KillDestructablePrototype>();
            this._SetDestructableInvulnerable = this.Get("SetDestructableInvulnerable").ToDelegate<SetDestructableInvulnerablePrototype>();
            this._IsDestructableInvulnerable = this.Get("IsDestructableInvulnerable").ToDelegate<IsDestructableInvulnerablePrototype>();
            this._EnumDestructablesInRect = this.Get("EnumDestructablesInRect").ToDelegate<EnumDestructablesInRectPrototype>();
            this._GetDestructableTypeId = this.Get("GetDestructableTypeId").ToDelegate<GetDestructableTypeIdPrototype>();
            this._GetDestructableX = this.Get("GetDestructableX").ToDelegate<GetDestructableXPrototype>();
            this._GetDestructableY = this.Get("GetDestructableY").ToDelegate<GetDestructableYPrototype>();
            this._SetDestructableLife = this.Get("SetDestructableLife").ToDelegate<SetDestructableLifePrototype>();
            this._GetDestructableLife = this.Get("GetDestructableLife").ToDelegate<GetDestructableLifePrototype>();
            this._SetDestructableMaxLife = this.Get("SetDestructableMaxLife").ToDelegate<SetDestructableMaxLifePrototype>();
            this._GetDestructableMaxLife = this.Get("GetDestructableMaxLife").ToDelegate<GetDestructableMaxLifePrototype>();
            this._DestructableRestoreLife = this.Get("DestructableRestoreLife").ToDelegate<DestructableRestoreLifePrototype>();
            this._QueueDestructableAnimation = this.Get("QueueDestructableAnimation").ToDelegate<QueueDestructableAnimationPrototype>();
            this._SetDestructableAnimation = this.Get("SetDestructableAnimation").ToDelegate<SetDestructableAnimationPrototype>();
            this._SetDestructableAnimationSpeed = this.Get("SetDestructableAnimationSpeed").ToDelegate<SetDestructableAnimationSpeedPrototype>();
            this._ShowDestructable = this.Get("ShowDestructable").ToDelegate<ShowDestructablePrototype>();
            this._GetDestructableOccluderHeight = this.Get("GetDestructableOccluderHeight").ToDelegate<GetDestructableOccluderHeightPrototype>();
            this._SetDestructableOccluderHeight = this.Get("SetDestructableOccluderHeight").ToDelegate<SetDestructableOccluderHeightPrototype>();
            this._GetDestructableName = this.Get("GetDestructableName").ToDelegate<GetDestructableNamePrototype>();
            this._GetTriggerDestructable = this.Get("GetTriggerDestructable").ToDelegate<GetTriggerDestructablePrototype>();
            this._CreateItem = this.Get("CreateItem").ToDelegate<CreateItemPrototype>();
            this._RemoveItem = this.Get("RemoveItem").ToDelegate<RemoveItemPrototype>();
            this._GetItemPlayer = this.Get("GetItemPlayer").ToDelegate<GetItemPlayerPrototype>();
            this._GetItemTypeId = this.Get("GetItemTypeId").ToDelegate<GetItemTypeIdPrototype>();
            this._GetItemX = this.Get("GetItemX").ToDelegate<GetItemXPrototype>();
            this._GetItemY = this.Get("GetItemY").ToDelegate<GetItemYPrototype>();
            this._SetItemPosition = this.Get("SetItemPosition").ToDelegate<SetItemPositionPrototype>();
            this._SetItemDropOnDeath = this.Get("SetItemDropOnDeath").ToDelegate<SetItemDropOnDeathPrototype>();
            this._SetItemDroppable = this.Get("SetItemDroppable").ToDelegate<SetItemDroppablePrototype>();
            this._SetItemPawnable = this.Get("SetItemPawnable").ToDelegate<SetItemPawnablePrototype>();
            this._SetItemPlayer = this.Get("SetItemPlayer").ToDelegate<SetItemPlayerPrototype>();
            this._SetItemInvulnerable = this.Get("SetItemInvulnerable").ToDelegate<SetItemInvulnerablePrototype>();
            this._IsItemInvulnerable = this.Get("IsItemInvulnerable").ToDelegate<IsItemInvulnerablePrototype>();
            this._SetItemVisible = this.Get("SetItemVisible").ToDelegate<SetItemVisiblePrototype>();
            this._IsItemVisible = this.Get("IsItemVisible").ToDelegate<IsItemVisiblePrototype>();
            this._IsItemOwned = this.Get("IsItemOwned").ToDelegate<IsItemOwnedPrototype>();
            this._IsItemPowerup = this.Get("IsItemPowerup").ToDelegate<IsItemPowerupPrototype>();
            this._IsItemSellable = this.Get("IsItemSellable").ToDelegate<IsItemSellablePrototype>();
            this._IsItemPawnable = this.Get("IsItemPawnable").ToDelegate<IsItemPawnablePrototype>();
            this._IsItemIdPowerup = this.Get("IsItemIdPowerup").ToDelegate<IsItemIdPowerupPrototype>();
            this._IsItemIdSellable = this.Get("IsItemIdSellable").ToDelegate<IsItemIdSellablePrototype>();
            this._IsItemIdPawnable = this.Get("IsItemIdPawnable").ToDelegate<IsItemIdPawnablePrototype>();
            this._EnumItemsInRect = this.Get("EnumItemsInRect").ToDelegate<EnumItemsInRectPrototype>();
            this._GetItemLevel = this.Get("GetItemLevel").ToDelegate<GetItemLevelPrototype>();
            this._GetItemType = this.Get("GetItemType").ToDelegate<GetItemTypePrototype>();
            this._SetItemDropID = this.Get("SetItemDropID").ToDelegate<SetItemDropIDPrototype>();
            this._GetItemName = this.Get("GetItemName").ToDelegate<GetItemNamePrototype>();
            this._GetItemCharges = this.Get("GetItemCharges").ToDelegate<GetItemChargesPrototype>();
            this._SetItemCharges = this.Get("SetItemCharges").ToDelegate<SetItemChargesPrototype>();
            this._GetItemUserData = this.Get("GetItemUserData").ToDelegate<GetItemUserDataPrototype>();
            this._SetItemUserData = this.Get("SetItemUserData").ToDelegate<SetItemUserDataPrototype>();
            this._CreateUnit = this.Get("CreateUnit").ToDelegate<CreateUnitPrototype>();
            this._CreateUnitByName = this.Get("CreateUnitByName").ToDelegate<CreateUnitByNamePrototype>();
            this._CreateUnitAtLoc = this.Get("CreateUnitAtLoc").ToDelegate<CreateUnitAtLocPrototype>();
            this._CreateUnitAtLocByName = this.Get("CreateUnitAtLocByName").ToDelegate<CreateUnitAtLocByNamePrototype>();
            this._CreateCorpse = this.Get("CreateCorpse").ToDelegate<CreateCorpsePrototype>();
            this._KillUnit = this.Get("KillUnit").ToDelegate<KillUnitPrototype>();
            this._RemoveUnit = this.Get("RemoveUnit").ToDelegate<RemoveUnitPrototype>();
            this._ShowUnit = this.Get("ShowUnit").ToDelegate<ShowUnitPrototype>();
            this._SetUnitState = this.Get("SetUnitState").ToDelegate<SetUnitStatePrototype>();
            this._SetUnitX = this.Get("SetUnitX").ToDelegate<SetUnitXPrototype>();
            this._SetUnitY = this.Get("SetUnitY").ToDelegate<SetUnitYPrototype>();
            this._SetUnitPosition = this.Get("SetUnitPosition").ToDelegate<SetUnitPositionPrototype>();
            this._SetUnitPositionLoc = this.Get("SetUnitPositionLoc").ToDelegate<SetUnitPositionLocPrototype>();
            this._SetUnitFacing = this.Get("SetUnitFacing").ToDelegate<SetUnitFacingPrototype>();
            this._SetUnitFacingTimed = this.Get("SetUnitFacingTimed").ToDelegate<SetUnitFacingTimedPrototype>();
            this._SetUnitMoveSpeed = this.Get("SetUnitMoveSpeed").ToDelegate<SetUnitMoveSpeedPrototype>();
            this._SetUnitFlyHeight = this.Get("SetUnitFlyHeight").ToDelegate<SetUnitFlyHeightPrototype>();
            this._SetUnitTurnSpeed = this.Get("SetUnitTurnSpeed").ToDelegate<SetUnitTurnSpeedPrototype>();
            this._SetUnitPropWindow = this.Get("SetUnitPropWindow").ToDelegate<SetUnitPropWindowPrototype>();
            this._SetUnitAcquireRange = this.Get("SetUnitAcquireRange").ToDelegate<SetUnitAcquireRangePrototype>();
            this._SetUnitCreepGuard = this.Get("SetUnitCreepGuard").ToDelegate<SetUnitCreepGuardPrototype>();
            this._GetUnitAcquireRange = this.Get("GetUnitAcquireRange").ToDelegate<GetUnitAcquireRangePrototype>();
            this._GetUnitTurnSpeed = this.Get("GetUnitTurnSpeed").ToDelegate<GetUnitTurnSpeedPrototype>();
            this._GetUnitPropWindow = this.Get("GetUnitPropWindow").ToDelegate<GetUnitPropWindowPrototype>();
            this._GetUnitFlyHeight = this.Get("GetUnitFlyHeight").ToDelegate<GetUnitFlyHeightPrototype>();
            this._GetUnitDefaultAcquireRange = this.Get("GetUnitDefaultAcquireRange").ToDelegate<GetUnitDefaultAcquireRangePrototype>();
            this._GetUnitDefaultTurnSpeed = this.Get("GetUnitDefaultTurnSpeed").ToDelegate<GetUnitDefaultTurnSpeedPrototype>();
            this._GetUnitDefaultPropWindow = this.Get("GetUnitDefaultPropWindow").ToDelegate<GetUnitDefaultPropWindowPrototype>();
            this._GetUnitDefaultFlyHeight = this.Get("GetUnitDefaultFlyHeight").ToDelegate<GetUnitDefaultFlyHeightPrototype>();
            this._SetUnitOwner = this.Get("SetUnitOwner").ToDelegate<SetUnitOwnerPrototype>();
            this._SetUnitColor = this.Get("SetUnitColor").ToDelegate<SetUnitColorPrototype>();
            this._SetUnitScale = this.Get("SetUnitScale").ToDelegate<SetUnitScalePrototype>();
            this._SetUnitTimeScale = this.Get("SetUnitTimeScale").ToDelegate<SetUnitTimeScalePrototype>();
            this._SetUnitBlendTime = this.Get("SetUnitBlendTime").ToDelegate<SetUnitBlendTimePrototype>();
            this._SetUnitVertexColor = this.Get("SetUnitVertexColor").ToDelegate<SetUnitVertexColorPrototype>();
            this._QueueUnitAnimation = this.Get("QueueUnitAnimation").ToDelegate<QueueUnitAnimationPrototype>();
            this._SetUnitAnimation = this.Get("SetUnitAnimation").ToDelegate<SetUnitAnimationPrototype>();
            this._SetUnitAnimationByIndex = this.Get("SetUnitAnimationByIndex").ToDelegate<SetUnitAnimationByIndexPrototype>();
            this._SetUnitAnimationWithRarity = this.Get("SetUnitAnimationWithRarity").ToDelegate<SetUnitAnimationWithRarityPrototype>();
            this._AddUnitAnimationProperties = this.Get("AddUnitAnimationProperties").ToDelegate<AddUnitAnimationPropertiesPrototype>();
            this._SetUnitLookAt = this.Get("SetUnitLookAt").ToDelegate<SetUnitLookAtPrototype>();
            this._ResetUnitLookAt = this.Get("ResetUnitLookAt").ToDelegate<ResetUnitLookAtPrototype>();
            this._SetUnitRescuable = this.Get("SetUnitRescuable").ToDelegate<SetUnitRescuablePrototype>();
            this._SetUnitRescueRange = this.Get("SetUnitRescueRange").ToDelegate<SetUnitRescueRangePrototype>();
            this._SetHeroStr = this.Get("SetHeroStr").ToDelegate<SetHeroStrPrototype>();
            this._SetHeroAgi = this.Get("SetHeroAgi").ToDelegate<SetHeroAgiPrototype>();
            this._SetHeroInt = this.Get("SetHeroInt").ToDelegate<SetHeroIntPrototype>();
            this._GetHeroStr = this.Get("GetHeroStr").ToDelegate<GetHeroStrPrototype>();
            this._GetHeroAgi = this.Get("GetHeroAgi").ToDelegate<GetHeroAgiPrototype>();
            this._GetHeroInt = this.Get("GetHeroInt").ToDelegate<GetHeroIntPrototype>();
            this._UnitStripHeroLevel = this.Get("UnitStripHeroLevel").ToDelegate<UnitStripHeroLevelPrototype>();
            this._GetHeroXP = this.Get("GetHeroXP").ToDelegate<GetHeroXPPrototype>();
            this._SetHeroXP = this.Get("SetHeroXP").ToDelegate<SetHeroXPPrototype>();
            this._GetHeroSkillPoints = this.Get("GetHeroSkillPoints").ToDelegate<GetHeroSkillPointsPrototype>();
            this._UnitModifySkillPoints = this.Get("UnitModifySkillPoints").ToDelegate<UnitModifySkillPointsPrototype>();
            this._AddHeroXP = this.Get("AddHeroXP").ToDelegate<AddHeroXPPrototype>();
            this._SetHeroLevel = this.Get("SetHeroLevel").ToDelegate<SetHeroLevelPrototype>();
            this._GetHeroLevel = this.Get("GetHeroLevel").ToDelegate<GetHeroLevelPrototype>();
            this._GetUnitLevel = this.Get("GetUnitLevel").ToDelegate<GetUnitLevelPrototype>();
            this._GetHeroProperName = this.Get("GetHeroProperName").ToDelegate<GetHeroProperNamePrototype>();
            this._SuspendHeroXP = this.Get("SuspendHeroXP").ToDelegate<SuspendHeroXPPrototype>();
            this._IsSuspendedXP = this.Get("IsSuspendedXP").ToDelegate<IsSuspendedXPPrototype>();
            this._SelectHeroSkill = this.Get("SelectHeroSkill").ToDelegate<SelectHeroSkillPrototype>();
            this._GetUnitAbilityLevel = this.Get("GetUnitAbilityLevel").ToDelegate<GetUnitAbilityLevelPrototype>();
            this._DecUnitAbilityLevel = this.Get("DecUnitAbilityLevel").ToDelegate<DecUnitAbilityLevelPrototype>();
            this._IncUnitAbilityLevel = this.Get("IncUnitAbilityLevel").ToDelegate<IncUnitAbilityLevelPrototype>();
            this._SetUnitAbilityLevel = this.Get("SetUnitAbilityLevel").ToDelegate<SetUnitAbilityLevelPrototype>();
            this._ReviveHero = this.Get("ReviveHero").ToDelegate<ReviveHeroPrototype>();
            this._ReviveHeroLoc = this.Get("ReviveHeroLoc").ToDelegate<ReviveHeroLocPrototype>();
            this._SetUnitExploded = this.Get("SetUnitExploded").ToDelegate<SetUnitExplodedPrototype>();
            this._SetUnitInvulnerable = this.Get("SetUnitInvulnerable").ToDelegate<SetUnitInvulnerablePrototype>();
            this._PauseUnit = this.Get("PauseUnit").ToDelegate<PauseUnitPrototype>();
            this._IsUnitPaused = this.Get("IsUnitPaused").ToDelegate<IsUnitPausedPrototype>();
            this._SetUnitPathing = this.Get("SetUnitPathing").ToDelegate<SetUnitPathingPrototype>();
            this._ClearSelection = this.Get("ClearSelection").ToDelegate<ClearSelectionPrototype>();
            this._SelectUnit = this.Get("SelectUnit").ToDelegate<SelectUnitPrototype>();
            this._GetUnitPointValue = this.Get("GetUnitPointValue").ToDelegate<GetUnitPointValuePrototype>();
            this._GetUnitPointValueByType = this.Get("GetUnitPointValueByType").ToDelegate<GetUnitPointValueByTypePrototype>();
            this._UnitAddItem = this.Get("UnitAddItem").ToDelegate<UnitAddItemPrototype>();
            this._UnitAddItemById = this.Get("UnitAddItemById").ToDelegate<UnitAddItemByIdPrototype>();
            this._UnitAddItemToSlotById = this.Get("UnitAddItemToSlotById").ToDelegate<UnitAddItemToSlotByIdPrototype>();
            this._UnitRemoveItem = this.Get("UnitRemoveItem").ToDelegate<UnitRemoveItemPrototype>();
            this._UnitRemoveItemFromSlot = this.Get("UnitRemoveItemFromSlot").ToDelegate<UnitRemoveItemFromSlotPrototype>();
            this._UnitHasItem = this.Get("UnitHasItem").ToDelegate<UnitHasItemPrototype>();
            this._UnitItemInSlot = this.Get("UnitItemInSlot").ToDelegate<UnitItemInSlotPrototype>();
            this._UnitInventorySize = this.Get("UnitInventorySize").ToDelegate<UnitInventorySizePrototype>();
            this._UnitDropItemPoint = this.Get("UnitDropItemPoint").ToDelegate<UnitDropItemPointPrototype>();
            this._UnitDropItemSlot = this.Get("UnitDropItemSlot").ToDelegate<UnitDropItemSlotPrototype>();
            this._UnitDropItemTarget = this.Get("UnitDropItemTarget").ToDelegate<UnitDropItemTargetPrototype>();
            this._UnitUseItem = this.Get("UnitUseItem").ToDelegate<UnitUseItemPrototype>();
            this._UnitUseItemPoint = this.Get("UnitUseItemPoint").ToDelegate<UnitUseItemPointPrototype>();
            this._UnitUseItemTarget = this.Get("UnitUseItemTarget").ToDelegate<UnitUseItemTargetPrototype>();
            this._GetUnitX = this.Get("GetUnitX").ToDelegate<GetUnitXPrototype>();
            this._GetUnitY = this.Get("GetUnitY").ToDelegate<GetUnitYPrototype>();
            this._GetUnitLoc = this.Get("GetUnitLoc").ToDelegate<GetUnitLocPrototype>();
            this._GetUnitFacing = this.Get("GetUnitFacing").ToDelegate<GetUnitFacingPrototype>();
            this._GetUnitMoveSpeed = this.Get("GetUnitMoveSpeed").ToDelegate<GetUnitMoveSpeedPrototype>();
            this._GetUnitDefaultMoveSpeed = this.Get("GetUnitDefaultMoveSpeed").ToDelegate<GetUnitDefaultMoveSpeedPrototype>();
            this._GetUnitState = this.Get("GetUnitState").ToDelegate<GetUnitStatePrototype>();
            this._GetOwningPlayer = this.Get("GetOwningPlayer").ToDelegate<GetOwningPlayerPrototype>();
            this._GetUnitTypeId = this.Get("GetUnitTypeId").ToDelegate<GetUnitTypeIdPrototype>();
            this._GetUnitRace = this.Get("GetUnitRace").ToDelegate<GetUnitRacePrototype>();
            this._GetUnitName = this.Get("GetUnitName").ToDelegate<GetUnitNamePrototype>();
            this._GetUnitFoodUsed = this.Get("GetUnitFoodUsed").ToDelegate<GetUnitFoodUsedPrototype>();
            this._GetUnitFoodMade = this.Get("GetUnitFoodMade").ToDelegate<GetUnitFoodMadePrototype>();
            this._GetFoodMade = this.Get("GetFoodMade").ToDelegate<GetFoodMadePrototype>();
            this._GetFoodUsed = this.Get("GetFoodUsed").ToDelegate<GetFoodUsedPrototype>();
            this._SetUnitUseFood = this.Get("SetUnitUseFood").ToDelegate<SetUnitUseFoodPrototype>();
            this._GetUnitRallyPoint = this.Get("GetUnitRallyPoint").ToDelegate<GetUnitRallyPointPrototype>();
            this._GetUnitRallyUnit = this.Get("GetUnitRallyUnit").ToDelegate<GetUnitRallyUnitPrototype>();
            this._GetUnitRallyDestructable = this.Get("GetUnitRallyDestructable").ToDelegate<GetUnitRallyDestructablePrototype>();
            this._IsUnitInGroup = this.Get("IsUnitInGroup").ToDelegate<IsUnitInGroupPrototype>();
            this._IsUnitInForce = this.Get("IsUnitInForce").ToDelegate<IsUnitInForcePrototype>();
            this._IsUnitOwnedByPlayer = this.Get("IsUnitOwnedByPlayer").ToDelegate<IsUnitOwnedByPlayerPrototype>();
            this._IsUnitAlly = this.Get("IsUnitAlly").ToDelegate<IsUnitAllyPrototype>();
            this._IsUnitEnemy = this.Get("IsUnitEnemy").ToDelegate<IsUnitEnemyPrototype>();
            this._IsUnitVisible = this.Get("IsUnitVisible").ToDelegate<IsUnitVisiblePrototype>();
            this._IsUnitDetected = this.Get("IsUnitDetected").ToDelegate<IsUnitDetectedPrototype>();
            this._IsUnitInvisible = this.Get("IsUnitInvisible").ToDelegate<IsUnitInvisiblePrototype>();
            this._IsUnitFogged = this.Get("IsUnitFogged").ToDelegate<IsUnitFoggedPrototype>();
            this._IsUnitMasked = this.Get("IsUnitMasked").ToDelegate<IsUnitMaskedPrototype>();
            this._IsUnitSelected = this.Get("IsUnitSelected").ToDelegate<IsUnitSelectedPrototype>();
            this._IsUnitRace = this.Get("IsUnitRace").ToDelegate<IsUnitRacePrototype>();
            this._IsUnitType = this.Get("IsUnitType").ToDelegate<IsUnitTypePrototype>();
            this._IsUnit = this.Get("IsUnit").ToDelegate<IsUnitPrototype>();
            this._IsUnitInRange = this.Get("IsUnitInRange").ToDelegate<IsUnitInRangePrototype>();
            this._IsUnitInRangeXY = this.Get("IsUnitInRangeXY").ToDelegate<IsUnitInRangeXYPrototype>();
            this._IsUnitInRangeLoc = this.Get("IsUnitInRangeLoc").ToDelegate<IsUnitInRangeLocPrototype>();
            this._IsUnitHidden = this.Get("IsUnitHidden").ToDelegate<IsUnitHiddenPrototype>();
            this._IsUnitIllusion = this.Get("IsUnitIllusion").ToDelegate<IsUnitIllusionPrototype>();
            this._IsUnitInTransport = this.Get("IsUnitInTransport").ToDelegate<IsUnitInTransportPrototype>();
            this._IsUnitLoaded = this.Get("IsUnitLoaded").ToDelegate<IsUnitLoadedPrototype>();
            this._IsHeroUnitId = this.Get("IsHeroUnitId").ToDelegate<IsHeroUnitIdPrototype>();
            this._IsUnitIdType = this.Get("IsUnitIdType").ToDelegate<IsUnitIdTypePrototype>();
            this._UnitShareVision = this.Get("UnitShareVision").ToDelegate<UnitShareVisionPrototype>();
            this._UnitSuspendDecay = this.Get("UnitSuspendDecay").ToDelegate<UnitSuspendDecayPrototype>();
            this._UnitAddType = this.Get("UnitAddType").ToDelegate<UnitAddTypePrototype>();
            this._UnitRemoveType = this.Get("UnitRemoveType").ToDelegate<UnitRemoveTypePrototype>();
            this._UnitAddAbility = this.Get("UnitAddAbility").ToDelegate<UnitAddAbilityPrototype>();
            this._UnitRemoveAbility = this.Get("UnitRemoveAbility").ToDelegate<UnitRemoveAbilityPrototype>();
            this._UnitMakeAbilityPermanent = this.Get("UnitMakeAbilityPermanent").ToDelegate<UnitMakeAbilityPermanentPrototype>();
            this._UnitRemoveBuffs = this.Get("UnitRemoveBuffs").ToDelegate<UnitRemoveBuffsPrototype>();
            this._UnitRemoveBuffsEx = this.Get("UnitRemoveBuffsEx").ToDelegate<UnitRemoveBuffsExPrototype>();
            this._UnitHasBuffsEx = this.Get("UnitHasBuffsEx").ToDelegate<UnitHasBuffsExPrototype>();
            this._UnitCountBuffsEx = this.Get("UnitCountBuffsEx").ToDelegate<UnitCountBuffsExPrototype>();
            this._UnitAddSleep = this.Get("UnitAddSleep").ToDelegate<UnitAddSleepPrototype>();
            this._UnitCanSleep = this.Get("UnitCanSleep").ToDelegate<UnitCanSleepPrototype>();
            this._UnitAddSleepPerm = this.Get("UnitAddSleepPerm").ToDelegate<UnitAddSleepPermPrototype>();
            this._UnitCanSleepPerm = this.Get("UnitCanSleepPerm").ToDelegate<UnitCanSleepPermPrototype>();
            this._UnitIsSleeping = this.Get("UnitIsSleeping").ToDelegate<UnitIsSleepingPrototype>();
            this._UnitWakeUp = this.Get("UnitWakeUp").ToDelegate<UnitWakeUpPrototype>();
            this._UnitApplyTimedLife = this.Get("UnitApplyTimedLife").ToDelegate<UnitApplyTimedLifePrototype>();
            this._UnitIgnoreAlarm = this.Get("UnitIgnoreAlarm").ToDelegate<UnitIgnoreAlarmPrototype>();
            this._UnitIgnoreAlarmToggled = this.Get("UnitIgnoreAlarmToggled").ToDelegate<UnitIgnoreAlarmToggledPrototype>();
            this._UnitResetCooldown = this.Get("UnitResetCooldown").ToDelegate<UnitResetCooldownPrototype>();
            this._UnitSetConstructionProgress = this.Get("UnitSetConstructionProgress").ToDelegate<UnitSetConstructionProgressPrototype>();
            this._UnitSetUpgradeProgress = this.Get("UnitSetUpgradeProgress").ToDelegate<UnitSetUpgradeProgressPrototype>();
            this._UnitPauseTimedLife = this.Get("UnitPauseTimedLife").ToDelegate<UnitPauseTimedLifePrototype>();
            this._UnitSetUsesAltIcon = this.Get("UnitSetUsesAltIcon").ToDelegate<UnitSetUsesAltIconPrototype>();
            this._UnitDamagePoint = this.Get("UnitDamagePoint").ToDelegate<UnitDamagePointPrototype>();
            this._UnitDamageTarget = this.Get("UnitDamageTarget").ToDelegate<UnitDamageTargetPrototype>();
            this._IssueImmediateOrder = this.Get("IssueImmediateOrder").ToDelegate<IssueImmediateOrderPrototype>();
            this._IssueImmediateOrderById = this.Get("IssueImmediateOrderById").ToDelegate<IssueImmediateOrderByIdPrototype>();
            this._IssuePointOrder = this.Get("IssuePointOrder").ToDelegate<IssuePointOrderPrototype>();
            this._IssuePointOrderLoc = this.Get("IssuePointOrderLoc").ToDelegate<IssuePointOrderLocPrototype>();
            this._IssuePointOrderById = this.Get("IssuePointOrderById").ToDelegate<IssuePointOrderByIdPrototype>();
            this._IssuePointOrderByIdLoc = this.Get("IssuePointOrderByIdLoc").ToDelegate<IssuePointOrderByIdLocPrototype>();
            this._IssueTargetOrder = this.Get("IssueTargetOrder").ToDelegate<IssueTargetOrderPrototype>();
            this._IssueTargetOrderById = this.Get("IssueTargetOrderById").ToDelegate<IssueTargetOrderByIdPrototype>();
            this._IssueInstantPointOrder = this.Get("IssueInstantPointOrder").ToDelegate<IssueInstantPointOrderPrototype>();
            this._IssueInstantPointOrderById = this.Get("IssueInstantPointOrderById").ToDelegate<IssueInstantPointOrderByIdPrototype>();
            this._IssueInstantTargetOrder = this.Get("IssueInstantTargetOrder").ToDelegate<IssueInstantTargetOrderPrototype>();
            this._IssueInstantTargetOrderById = this.Get("IssueInstantTargetOrderById").ToDelegate<IssueInstantTargetOrderByIdPrototype>();
            this._IssueBuildOrder = this.Get("IssueBuildOrder").ToDelegate<IssueBuildOrderPrototype>();
            this._IssueBuildOrderById = this.Get("IssueBuildOrderById").ToDelegate<IssueBuildOrderByIdPrototype>();
            this._IssueNeutralImmediateOrder = this.Get("IssueNeutralImmediateOrder").ToDelegate<IssueNeutralImmediateOrderPrototype>();
            this._IssueNeutralImmediateOrderById = this.Get("IssueNeutralImmediateOrderById").ToDelegate<IssueNeutralImmediateOrderByIdPrototype>();
            this._IssueNeutralPointOrder = this.Get("IssueNeutralPointOrder").ToDelegate<IssueNeutralPointOrderPrototype>();
            this._IssueNeutralPointOrderById = this.Get("IssueNeutralPointOrderById").ToDelegate<IssueNeutralPointOrderByIdPrototype>();
            this._IssueNeutralTargetOrder = this.Get("IssueNeutralTargetOrder").ToDelegate<IssueNeutralTargetOrderPrototype>();
            this._IssueNeutralTargetOrderById = this.Get("IssueNeutralTargetOrderById").ToDelegate<IssueNeutralTargetOrderByIdPrototype>();
            this._GetUnitCurrentOrder = this.Get("GetUnitCurrentOrder").ToDelegate<GetUnitCurrentOrderPrototype>();
            this._SetResourceAmount = this.Get("SetResourceAmount").ToDelegate<SetResourceAmountPrototype>();
            this._AddResourceAmount = this.Get("AddResourceAmount").ToDelegate<AddResourceAmountPrototype>();
            this._GetResourceAmount = this.Get("GetResourceAmount").ToDelegate<GetResourceAmountPrototype>();
            this._WaygateGetDestinationX = this.Get("WaygateGetDestinationX").ToDelegate<WaygateGetDestinationXPrototype>();
            this._WaygateGetDestinationY = this.Get("WaygateGetDestinationY").ToDelegate<WaygateGetDestinationYPrototype>();
            this._WaygateSetDestination = this.Get("WaygateSetDestination").ToDelegate<WaygateSetDestinationPrototype>();
            this._WaygateActivate = this.Get("WaygateActivate").ToDelegate<WaygateActivatePrototype>();
            this._WaygateIsActive = this.Get("WaygateIsActive").ToDelegate<WaygateIsActivePrototype>();
            this._AddItemToAllStock = this.Get("AddItemToAllStock").ToDelegate<AddItemToAllStockPrototype>();
            this._AddItemToStock = this.Get("AddItemToStock").ToDelegate<AddItemToStockPrototype>();
            this._AddUnitToAllStock = this.Get("AddUnitToAllStock").ToDelegate<AddUnitToAllStockPrototype>();
            this._AddUnitToStock = this.Get("AddUnitToStock").ToDelegate<AddUnitToStockPrototype>();
            this._RemoveItemFromAllStock = this.Get("RemoveItemFromAllStock").ToDelegate<RemoveItemFromAllStockPrototype>();
            this._RemoveItemFromStock = this.Get("RemoveItemFromStock").ToDelegate<RemoveItemFromStockPrototype>();
            this._RemoveUnitFromAllStock = this.Get("RemoveUnitFromAllStock").ToDelegate<RemoveUnitFromAllStockPrototype>();
            this._RemoveUnitFromStock = this.Get("RemoveUnitFromStock").ToDelegate<RemoveUnitFromStockPrototype>();
            this._SetAllItemTypeSlots = this.Get("SetAllItemTypeSlots").ToDelegate<SetAllItemTypeSlotsPrototype>();
            this._SetAllUnitTypeSlots = this.Get("SetAllUnitTypeSlots").ToDelegate<SetAllUnitTypeSlotsPrototype>();
            this._SetItemTypeSlots = this.Get("SetItemTypeSlots").ToDelegate<SetItemTypeSlotsPrototype>();
            this._SetUnitTypeSlots = this.Get("SetUnitTypeSlots").ToDelegate<SetUnitTypeSlotsPrototype>();
            this._GetUnitUserData = this.Get("GetUnitUserData").ToDelegate<GetUnitUserDataPrototype>();
            this._SetUnitUserData = this.Get("SetUnitUserData").ToDelegate<SetUnitUserDataPrototype>();
            this._Player = this.Get("Player").ToDelegate<PlayerPrototype>();
            this._GetLocalPlayer = this.Get("GetLocalPlayer").ToDelegate<GetLocalPlayerPrototype>();
            this._IsPlayerAlly = this.Get("IsPlayerAlly").ToDelegate<IsPlayerAllyPrototype>();
            this._IsPlayerEnemy = this.Get("IsPlayerEnemy").ToDelegate<IsPlayerEnemyPrototype>();
            this._IsPlayerInForce = this.Get("IsPlayerInForce").ToDelegate<IsPlayerInForcePrototype>();
            this._IsPlayerObserver = this.Get("IsPlayerObserver").ToDelegate<IsPlayerObserverPrototype>();
            this._IsVisibleToPlayer = this.Get("IsVisibleToPlayer").ToDelegate<IsVisibleToPlayerPrototype>();
            this._IsLocationVisibleToPlayer = this.Get("IsLocationVisibleToPlayer").ToDelegate<IsLocationVisibleToPlayerPrototype>();
            this._IsFoggedToPlayer = this.Get("IsFoggedToPlayer").ToDelegate<IsFoggedToPlayerPrototype>();
            this._IsLocationFoggedToPlayer = this.Get("IsLocationFoggedToPlayer").ToDelegate<IsLocationFoggedToPlayerPrototype>();
            this._IsMaskedToPlayer = this.Get("IsMaskedToPlayer").ToDelegate<IsMaskedToPlayerPrototype>();
            this._IsLocationMaskedToPlayer = this.Get("IsLocationMaskedToPlayer").ToDelegate<IsLocationMaskedToPlayerPrototype>();
            this._GetPlayerRace = this.Get("GetPlayerRace").ToDelegate<GetPlayerRacePrototype>();
            this._GetPlayerId = this.Get("GetPlayerId").ToDelegate<GetPlayerIdPrototype>();
            this._GetPlayerUnitCount = this.Get("GetPlayerUnitCount").ToDelegate<GetPlayerUnitCountPrototype>();
            this._GetPlayerTypedUnitCount = this.Get("GetPlayerTypedUnitCount").ToDelegate<GetPlayerTypedUnitCountPrototype>();
            this._GetPlayerStructureCount = this.Get("GetPlayerStructureCount").ToDelegate<GetPlayerStructureCountPrototype>();
            this._GetPlayerState = this.Get("GetPlayerState").ToDelegate<GetPlayerStatePrototype>();
            this._GetPlayerScore = this.Get("GetPlayerScore").ToDelegate<GetPlayerScorePrototype>();
            this._GetPlayerAlliance = this.Get("GetPlayerAlliance").ToDelegate<GetPlayerAlliancePrototype>();
            this._GetPlayerHandicap = this.Get("GetPlayerHandicap").ToDelegate<GetPlayerHandicapPrototype>();
            this._GetPlayerHandicapXP = this.Get("GetPlayerHandicapXP").ToDelegate<GetPlayerHandicapXPPrototype>();
            this._SetPlayerHandicap = this.Get("SetPlayerHandicap").ToDelegate<SetPlayerHandicapPrototype>();
            this._SetPlayerHandicapXP = this.Get("SetPlayerHandicapXP").ToDelegate<SetPlayerHandicapXPPrototype>();
            this._SetPlayerTechMaxAllowed = this.Get("SetPlayerTechMaxAllowed").ToDelegate<SetPlayerTechMaxAllowedPrototype>();
            this._GetPlayerTechMaxAllowed = this.Get("GetPlayerTechMaxAllowed").ToDelegate<GetPlayerTechMaxAllowedPrototype>();
            this._AddPlayerTechResearched = this.Get("AddPlayerTechResearched").ToDelegate<AddPlayerTechResearchedPrototype>();
            this._SetPlayerTechResearched = this.Get("SetPlayerTechResearched").ToDelegate<SetPlayerTechResearchedPrototype>();
            this._GetPlayerTechResearched = this.Get("GetPlayerTechResearched").ToDelegate<GetPlayerTechResearchedPrototype>();
            this._GetPlayerTechCount = this.Get("GetPlayerTechCount").ToDelegate<GetPlayerTechCountPrototype>();
            this._SetPlayerUnitsOwner = this.Get("SetPlayerUnitsOwner").ToDelegate<SetPlayerUnitsOwnerPrototype>();
            this._CripplePlayer = this.Get("CripplePlayer").ToDelegate<CripplePlayerPrototype>();
            this._SetPlayerAbilityAvailable = this.Get("SetPlayerAbilityAvailable").ToDelegate<SetPlayerAbilityAvailablePrototype>();
            this._SetPlayerState = this.Get("SetPlayerState").ToDelegate<SetPlayerStatePrototype>();
            this._RemovePlayer = this.Get("RemovePlayer").ToDelegate<RemovePlayerPrototype>();
            this._CachePlayerHeroData = this.Get("CachePlayerHeroData").ToDelegate<CachePlayerHeroDataPrototype>();
            this._SetFogStateRect = this.Get("SetFogStateRect").ToDelegate<SetFogStateRectPrototype>();
            this._SetFogStateRadius = this.Get("SetFogStateRadius").ToDelegate<SetFogStateRadiusPrototype>();
            this._SetFogStateRadiusLoc = this.Get("SetFogStateRadiusLoc").ToDelegate<SetFogStateRadiusLocPrototype>();
            this._FogMaskEnable = this.Get("FogMaskEnable").ToDelegate<FogMaskEnablePrototype>();
            this._IsFogMaskEnabled = this.Get("IsFogMaskEnabled").ToDelegate<IsFogMaskEnabledPrototype>();
            this._FogEnable = this.Get("FogEnable").ToDelegate<FogEnablePrototype>();
            this._IsFogEnabled = this.Get("IsFogEnabled").ToDelegate<IsFogEnabledPrototype>();
            this._CreateFogModifierRect = this.Get("CreateFogModifierRect").ToDelegate<CreateFogModifierRectPrototype>();
            this._CreateFogModifierRadius = this.Get("CreateFogModifierRadius").ToDelegate<CreateFogModifierRadiusPrototype>();
            this._CreateFogModifierRadiusLoc = this.Get("CreateFogModifierRadiusLoc").ToDelegate<CreateFogModifierRadiusLocPrototype>();
            this._DestroyFogModifier = this.Get("DestroyFogModifier").ToDelegate<DestroyFogModifierPrototype>();
            this._FogModifierStart = this.Get("FogModifierStart").ToDelegate<FogModifierStartPrototype>();
            this._FogModifierStop = this.Get("FogModifierStop").ToDelegate<FogModifierStopPrototype>();
            this._VersionGet = this.Get("VersionGet").ToDelegate<VersionGetPrototype>();
            this._VersionCompatible = this.Get("VersionCompatible").ToDelegate<VersionCompatiblePrototype>();
            this._VersionSupported = this.Get("VersionSupported").ToDelegate<VersionSupportedPrototype>();
            this._EndGame = this.Get("EndGame").ToDelegate<EndGamePrototype>();
            this._ChangeLevel = this.Get("ChangeLevel").ToDelegate<ChangeLevelPrototype>();
            this._RestartGame = this.Get("RestartGame").ToDelegate<RestartGamePrototype>();
            this._ReloadGame = this.Get("ReloadGame").ToDelegate<ReloadGamePrototype>();
            this._SetCampaignMenuRace = this.Get("SetCampaignMenuRace").ToDelegate<SetCampaignMenuRacePrototype>();
            this._SetCampaignMenuRaceEx = this.Get("SetCampaignMenuRaceEx").ToDelegate<SetCampaignMenuRaceExPrototype>();
            this._ForceCampaignSelectScreen = this.Get("ForceCampaignSelectScreen").ToDelegate<ForceCampaignSelectScreenPrototype>();
            this._LoadGame = this.Get("LoadGame").ToDelegate<LoadGamePrototype>();
            this._SaveGame = this.Get("SaveGame").ToDelegate<SaveGamePrototype>();
            this._RenameSaveDirectory = this.Get("RenameSaveDirectory").ToDelegate<RenameSaveDirectoryPrototype>();
            this._RemoveSaveDirectory = this.Get("RemoveSaveDirectory").ToDelegate<RemoveSaveDirectoryPrototype>();
            this._CopySaveGame = this.Get("CopySaveGame").ToDelegate<CopySaveGamePrototype>();
            this._SaveGameExists = this.Get("SaveGameExists").ToDelegate<SaveGameExistsPrototype>();
            this._SyncSelections = this.Get("SyncSelections").ToDelegate<SyncSelectionsPrototype>();
            this._SetFloatGameState = this.Get("SetFloatGameState").ToDelegate<SetFloatGameStatePrototype>();
            this._GetFloatGameState = this.Get("GetFloatGameState").ToDelegate<GetFloatGameStatePrototype>();
            this._SetIntegerGameState = this.Get("SetIntegerGameState").ToDelegate<SetIntegerGameStatePrototype>();
            this._GetIntegerGameState = this.Get("GetIntegerGameState").ToDelegate<GetIntegerGameStatePrototype>();
            this._SetTutorialCleared = this.Get("SetTutorialCleared").ToDelegate<SetTutorialClearedPrototype>();
            this._SetMissionAvailable = this.Get("SetMissionAvailable").ToDelegate<SetMissionAvailablePrototype>();
            this._SetCampaignAvailable = this.Get("SetCampaignAvailable").ToDelegate<SetCampaignAvailablePrototype>();
            this._SetOpCinematicAvailable = this.Get("SetOpCinematicAvailable").ToDelegate<SetOpCinematicAvailablePrototype>();
            this._SetEdCinematicAvailable = this.Get("SetEdCinematicAvailable").ToDelegate<SetEdCinematicAvailablePrototype>();
            this._GetDefaultDifficulty = this.Get("GetDefaultDifficulty").ToDelegate<GetDefaultDifficultyPrototype>();
            this._SetDefaultDifficulty = this.Get("SetDefaultDifficulty").ToDelegate<SetDefaultDifficultyPrototype>();
            this._SetCustomCampaignButtonVisible = this.Get("SetCustomCampaignButtonVisible").ToDelegate<SetCustomCampaignButtonVisiblePrototype>();
            this._GetCustomCampaignButtonVisible = this.Get("GetCustomCampaignButtonVisible").ToDelegate<GetCustomCampaignButtonVisiblePrototype>();
            this._DoNotSaveReplay = this.Get("DoNotSaveReplay").ToDelegate<DoNotSaveReplayPrototype>();
            this._DialogCreate = this.Get("DialogCreate").ToDelegate<DialogCreatePrototype>();
            this._DialogDestroy = this.Get("DialogDestroy").ToDelegate<DialogDestroyPrototype>();
            this._DialogClear = this.Get("DialogClear").ToDelegate<DialogClearPrototype>();
            this._DialogSetMessage = this.Get("DialogSetMessage").ToDelegate<DialogSetMessagePrototype>();
            this._DialogAddButton = this.Get("DialogAddButton").ToDelegate<DialogAddButtonPrototype>();
            this._DialogAddQuitButton = this.Get("DialogAddQuitButton").ToDelegate<DialogAddQuitButtonPrototype>();
            this._DialogDisplay = this.Get("DialogDisplay").ToDelegate<DialogDisplayPrototype>();
            this._ReloadGameCachesFromDisk = this.Get("ReloadGameCachesFromDisk").ToDelegate<ReloadGameCachesFromDiskPrototype>();
            this._InitGameCache = this.Get("InitGameCache").ToDelegate<InitGameCachePrototype>();
            this._SaveGameCache = this.Get("SaveGameCache").ToDelegate<SaveGameCachePrototype>();
            this._StoreInteger = this.Get("StoreInteger").ToDelegate<StoreIntegerPrototype>();
            this._StoreReal = this.Get("StoreReal").ToDelegate<StoreRealPrototype>();
            this._StoreBoolean = this.Get("StoreBoolean").ToDelegate<StoreBooleanPrototype>();
            this._StoreUnit = this.Get("StoreUnit").ToDelegate<StoreUnitPrototype>();
            this._StoreString = this.Get("StoreString").ToDelegate<StoreStringPrototype>();
            this._SyncStoredInteger = this.Get("SyncStoredInteger").ToDelegate<SyncStoredIntegerPrototype>();
            this._SyncStoredReal = this.Get("SyncStoredReal").ToDelegate<SyncStoredRealPrototype>();
            this._SyncStoredBoolean = this.Get("SyncStoredBoolean").ToDelegate<SyncStoredBooleanPrototype>();
            this._SyncStoredUnit = this.Get("SyncStoredUnit").ToDelegate<SyncStoredUnitPrototype>();
            this._SyncStoredString = this.Get("SyncStoredString").ToDelegate<SyncStoredStringPrototype>();
            this._HaveStoredInteger = this.Get("HaveStoredInteger").ToDelegate<HaveStoredIntegerPrototype>();
            this._HaveStoredReal = this.Get("HaveStoredReal").ToDelegate<HaveStoredRealPrototype>();
            this._HaveStoredBoolean = this.Get("HaveStoredBoolean").ToDelegate<HaveStoredBooleanPrototype>();
            this._HaveStoredUnit = this.Get("HaveStoredUnit").ToDelegate<HaveStoredUnitPrototype>();
            this._HaveStoredString = this.Get("HaveStoredString").ToDelegate<HaveStoredStringPrototype>();
            this._FlushGameCache = this.Get("FlushGameCache").ToDelegate<FlushGameCachePrototype>();
            this._FlushStoredMission = this.Get("FlushStoredMission").ToDelegate<FlushStoredMissionPrototype>();
            this._FlushStoredInteger = this.Get("FlushStoredInteger").ToDelegate<FlushStoredIntegerPrototype>();
            this._FlushStoredReal = this.Get("FlushStoredReal").ToDelegate<FlushStoredRealPrototype>();
            this._FlushStoredBoolean = this.Get("FlushStoredBoolean").ToDelegate<FlushStoredBooleanPrototype>();
            this._FlushStoredUnit = this.Get("FlushStoredUnit").ToDelegate<FlushStoredUnitPrototype>();
            this._FlushStoredString = this.Get("FlushStoredString").ToDelegate<FlushStoredStringPrototype>();
            this._GetStoredInteger = this.Get("GetStoredInteger").ToDelegate<GetStoredIntegerPrototype>();
            this._GetStoredReal = this.Get("GetStoredReal").ToDelegate<GetStoredRealPrototype>();
            this._GetStoredBoolean = this.Get("GetStoredBoolean").ToDelegate<GetStoredBooleanPrototype>();
            this._GetStoredString = this.Get("GetStoredString").ToDelegate<GetStoredStringPrototype>();
            this._RestoreUnit = this.Get("RestoreUnit").ToDelegate<RestoreUnitPrototype>();
            this._InitHashtable = this.Get("InitHashtable").ToDelegate<InitHashtablePrototype>();
            this._SaveInteger = this.Get("SaveInteger").ToDelegate<SaveIntegerPrototype>();
            this._SaveReal = this.Get("SaveReal").ToDelegate<SaveRealPrototype>();
            this._SaveBoolean = this.Get("SaveBoolean").ToDelegate<SaveBooleanPrototype>();
            this._SaveStr = this.Get("SaveStr").ToDelegate<SaveStrPrototype>();
            this._SavePlayerHandle = this.Get("SavePlayerHandle").ToDelegate<SavePlayerHandlePrototype>();
            this._SaveWidgetHandle = this.Get("SaveWidgetHandle").ToDelegate<SaveWidgetHandlePrototype>();
            this._SaveDestructableHandle = this.Get("SaveDestructableHandle").ToDelegate<SaveDestructableHandlePrototype>();
            this._SaveItemHandle = this.Get("SaveItemHandle").ToDelegate<SaveItemHandlePrototype>();
            this._SaveUnitHandle = this.Get("SaveUnitHandle").ToDelegate<SaveUnitHandlePrototype>();
            this._SaveAbilityHandle = this.Get("SaveAbilityHandle").ToDelegate<SaveAbilityHandlePrototype>();
            this._SaveTimerHandle = this.Get("SaveTimerHandle").ToDelegate<SaveTimerHandlePrototype>();
            this._SaveTriggerHandle = this.Get("SaveTriggerHandle").ToDelegate<SaveTriggerHandlePrototype>();
            this._SaveTriggerConditionHandle = this.Get("SaveTriggerConditionHandle").ToDelegate<SaveTriggerConditionHandlePrototype>();
            this._SaveTriggerActionHandle = this.Get("SaveTriggerActionHandle").ToDelegate<SaveTriggerActionHandlePrototype>();
            this._SaveTriggerEventHandle = this.Get("SaveTriggerEventHandle").ToDelegate<SaveTriggerEventHandlePrototype>();
            this._SaveForceHandle = this.Get("SaveForceHandle").ToDelegate<SaveForceHandlePrototype>();
            this._SaveGroupHandle = this.Get("SaveGroupHandle").ToDelegate<SaveGroupHandlePrototype>();
            this._SaveLocationHandle = this.Get("SaveLocationHandle").ToDelegate<SaveLocationHandlePrototype>();
            this._SaveRectHandle = this.Get("SaveRectHandle").ToDelegate<SaveRectHandlePrototype>();
            this._SaveBooleanExprHandle = this.Get("SaveBooleanExprHandle").ToDelegate<SaveBooleanExprHandlePrototype>();
            this._SaveSoundHandle = this.Get("SaveSoundHandle").ToDelegate<SaveSoundHandlePrototype>();
            this._SaveEffectHandle = this.Get("SaveEffectHandle").ToDelegate<SaveEffectHandlePrototype>();
            this._SaveUnitPoolHandle = this.Get("SaveUnitPoolHandle").ToDelegate<SaveUnitPoolHandlePrototype>();
            this._SaveItemPoolHandle = this.Get("SaveItemPoolHandle").ToDelegate<SaveItemPoolHandlePrototype>();
            this._SaveQuestHandle = this.Get("SaveQuestHandle").ToDelegate<SaveQuestHandlePrototype>();
            this._SaveQuestItemHandle = this.Get("SaveQuestItemHandle").ToDelegate<SaveQuestItemHandlePrototype>();
            this._SaveDefeatConditionHandle = this.Get("SaveDefeatConditionHandle").ToDelegate<SaveDefeatConditionHandlePrototype>();
            this._SaveTimerDialogHandle = this.Get("SaveTimerDialogHandle").ToDelegate<SaveTimerDialogHandlePrototype>();
            this._SaveLeaderboardHandle = this.Get("SaveLeaderboardHandle").ToDelegate<SaveLeaderboardHandlePrototype>();
            this._SaveMultiboardHandle = this.Get("SaveMultiboardHandle").ToDelegate<SaveMultiboardHandlePrototype>();
            this._SaveMultiboardItemHandle = this.Get("SaveMultiboardItemHandle").ToDelegate<SaveMultiboardItemHandlePrototype>();
            this._SaveTrackableHandle = this.Get("SaveTrackableHandle").ToDelegate<SaveTrackableHandlePrototype>();
            this._SaveDialogHandle = this.Get("SaveDialogHandle").ToDelegate<SaveDialogHandlePrototype>();
            this._SaveButtonHandle = this.Get("SaveButtonHandle").ToDelegate<SaveButtonHandlePrototype>();
            this._SaveTextTagHandle = this.Get("SaveTextTagHandle").ToDelegate<SaveTextTagHandlePrototype>();
            this._SaveLightningHandle = this.Get("SaveLightningHandle").ToDelegate<SaveLightningHandlePrototype>();
            this._SaveImageHandle = this.Get("SaveImageHandle").ToDelegate<SaveImageHandlePrototype>();
            this._SaveUbersplatHandle = this.Get("SaveUbersplatHandle").ToDelegate<SaveUbersplatHandlePrototype>();
            this._SaveRegionHandle = this.Get("SaveRegionHandle").ToDelegate<SaveRegionHandlePrototype>();
            this._SaveFogStateHandle = this.Get("SaveFogStateHandle").ToDelegate<SaveFogStateHandlePrototype>();
            this._SaveFogModifierHandle = this.Get("SaveFogModifierHandle").ToDelegate<SaveFogModifierHandlePrototype>();
            this._SaveAgentHandle = this.Get("SaveAgentHandle").ToDelegate<SaveAgentHandlePrototype>();
            this._SaveHashtableHandle = this.Get("SaveHashtableHandle").ToDelegate<SaveHashtableHandlePrototype>();
            this._LoadInteger = this.Get("LoadInteger").ToDelegate<LoadIntegerPrototype>();
            this._LoadReal = this.Get("LoadReal").ToDelegate<LoadRealPrototype>();
            this._LoadBoolean = this.Get("LoadBoolean").ToDelegate<LoadBooleanPrototype>();
            this._LoadStr = this.Get("LoadStr").ToDelegate<LoadStrPrototype>();
            this._LoadPlayerHandle = this.Get("LoadPlayerHandle").ToDelegate<LoadPlayerHandlePrototype>();
            this._LoadWidgetHandle = this.Get("LoadWidgetHandle").ToDelegate<LoadWidgetHandlePrototype>();
            this._LoadDestructableHandle = this.Get("LoadDestructableHandle").ToDelegate<LoadDestructableHandlePrototype>();
            this._LoadItemHandle = this.Get("LoadItemHandle").ToDelegate<LoadItemHandlePrototype>();
            this._LoadUnitHandle = this.Get("LoadUnitHandle").ToDelegate<LoadUnitHandlePrototype>();
            this._LoadAbilityHandle = this.Get("LoadAbilityHandle").ToDelegate<LoadAbilityHandlePrototype>();
            this._LoadTimerHandle = this.Get("LoadTimerHandle").ToDelegate<LoadTimerHandlePrototype>();
            this._LoadTriggerHandle = this.Get("LoadTriggerHandle").ToDelegate<LoadTriggerHandlePrototype>();
            this._LoadTriggerConditionHandle = this.Get("LoadTriggerConditionHandle").ToDelegate<LoadTriggerConditionHandlePrototype>();
            this._LoadTriggerActionHandle = this.Get("LoadTriggerActionHandle").ToDelegate<LoadTriggerActionHandlePrototype>();
            this._LoadTriggerEventHandle = this.Get("LoadTriggerEventHandle").ToDelegate<LoadTriggerEventHandlePrototype>();
            this._LoadForceHandle = this.Get("LoadForceHandle").ToDelegate<LoadForceHandlePrototype>();
            this._LoadGroupHandle = this.Get("LoadGroupHandle").ToDelegate<LoadGroupHandlePrototype>();
            this._LoadLocationHandle = this.Get("LoadLocationHandle").ToDelegate<LoadLocationHandlePrototype>();
            this._LoadRectHandle = this.Get("LoadRectHandle").ToDelegate<LoadRectHandlePrototype>();
            this._LoadBooleanExprHandle = this.Get("LoadBooleanExprHandle").ToDelegate<LoadBooleanExprHandlePrototype>();
            this._LoadSoundHandle = this.Get("LoadSoundHandle").ToDelegate<LoadSoundHandlePrototype>();
            this._LoadEffectHandle = this.Get("LoadEffectHandle").ToDelegate<LoadEffectHandlePrototype>();
            this._LoadUnitPoolHandle = this.Get("LoadUnitPoolHandle").ToDelegate<LoadUnitPoolHandlePrototype>();
            this._LoadItemPoolHandle = this.Get("LoadItemPoolHandle").ToDelegate<LoadItemPoolHandlePrototype>();
            this._LoadQuestHandle = this.Get("LoadQuestHandle").ToDelegate<LoadQuestHandlePrototype>();
            this._LoadQuestItemHandle = this.Get("LoadQuestItemHandle").ToDelegate<LoadQuestItemHandlePrototype>();
            this._LoadDefeatConditionHandle = this.Get("LoadDefeatConditionHandle").ToDelegate<LoadDefeatConditionHandlePrototype>();
            this._LoadTimerDialogHandle = this.Get("LoadTimerDialogHandle").ToDelegate<LoadTimerDialogHandlePrototype>();
            this._LoadLeaderboardHandle = this.Get("LoadLeaderboardHandle").ToDelegate<LoadLeaderboardHandlePrototype>();
            this._LoadMultiboardHandle = this.Get("LoadMultiboardHandle").ToDelegate<LoadMultiboardHandlePrototype>();
            this._LoadMultiboardItemHandle = this.Get("LoadMultiboardItemHandle").ToDelegate<LoadMultiboardItemHandlePrototype>();
            this._LoadTrackableHandle = this.Get("LoadTrackableHandle").ToDelegate<LoadTrackableHandlePrototype>();
            this._LoadDialogHandle = this.Get("LoadDialogHandle").ToDelegate<LoadDialogHandlePrototype>();
            this._LoadButtonHandle = this.Get("LoadButtonHandle").ToDelegate<LoadButtonHandlePrototype>();
            this._LoadTextTagHandle = this.Get("LoadTextTagHandle").ToDelegate<LoadTextTagHandlePrototype>();
            this._LoadLightningHandle = this.Get("LoadLightningHandle").ToDelegate<LoadLightningHandlePrototype>();
            this._LoadImageHandle = this.Get("LoadImageHandle").ToDelegate<LoadImageHandlePrototype>();
            this._LoadUbersplatHandle = this.Get("LoadUbersplatHandle").ToDelegate<LoadUbersplatHandlePrototype>();
            this._LoadRegionHandle = this.Get("LoadRegionHandle").ToDelegate<LoadRegionHandlePrototype>();
            this._LoadFogStateHandle = this.Get("LoadFogStateHandle").ToDelegate<LoadFogStateHandlePrototype>();
            this._LoadFogModifierHandle = this.Get("LoadFogModifierHandle").ToDelegate<LoadFogModifierHandlePrototype>();
            this._LoadHashtableHandle = this.Get("LoadHashtableHandle").ToDelegate<LoadHashtableHandlePrototype>();
            this._HaveSavedInteger = this.Get("HaveSavedInteger").ToDelegate<HaveSavedIntegerPrototype>();
            this._HaveSavedReal = this.Get("HaveSavedReal").ToDelegate<HaveSavedRealPrototype>();
            this._HaveSavedBoolean = this.Get("HaveSavedBoolean").ToDelegate<HaveSavedBooleanPrototype>();
            this._HaveSavedString = this.Get("HaveSavedString").ToDelegate<HaveSavedStringPrototype>();
            this._HaveSavedHandle = this.Get("HaveSavedHandle").ToDelegate<HaveSavedHandlePrototype>();
            this._RemoveSavedInteger = this.Get("RemoveSavedInteger").ToDelegate<RemoveSavedIntegerPrototype>();
            this._RemoveSavedReal = this.Get("RemoveSavedReal").ToDelegate<RemoveSavedRealPrototype>();
            this._RemoveSavedBoolean = this.Get("RemoveSavedBoolean").ToDelegate<RemoveSavedBooleanPrototype>();
            this._RemoveSavedString = this.Get("RemoveSavedString").ToDelegate<RemoveSavedStringPrototype>();
            this._RemoveSavedHandle = this.Get("RemoveSavedHandle").ToDelegate<RemoveSavedHandlePrototype>();
            this._FlushParentHashtable = this.Get("FlushParentHashtable").ToDelegate<FlushParentHashtablePrototype>();
            this._FlushChildHashtable = this.Get("FlushChildHashtable").ToDelegate<FlushChildHashtablePrototype>();
            this._GetRandomInt = this.Get("GetRandomInt").ToDelegate<GetRandomIntPrototype>();
            this._GetRandomReal = this.Get("GetRandomReal").ToDelegate<GetRandomRealPrototype>();
            this._CreateUnitPool = this.Get("CreateUnitPool").ToDelegate<CreateUnitPoolPrototype>();
            this._DestroyUnitPool = this.Get("DestroyUnitPool").ToDelegate<DestroyUnitPoolPrototype>();
            this._UnitPoolAddUnitType = this.Get("UnitPoolAddUnitType").ToDelegate<UnitPoolAddUnitTypePrototype>();
            this._UnitPoolRemoveUnitType = this.Get("UnitPoolRemoveUnitType").ToDelegate<UnitPoolRemoveUnitTypePrototype>();
            this._PlaceRandomUnit = this.Get("PlaceRandomUnit").ToDelegate<PlaceRandomUnitPrototype>();
            this._CreateItemPool = this.Get("CreateItemPool").ToDelegate<CreateItemPoolPrototype>();
            this._DestroyItemPool = this.Get("DestroyItemPool").ToDelegate<DestroyItemPoolPrototype>();
            this._ItemPoolAddItemType = this.Get("ItemPoolAddItemType").ToDelegate<ItemPoolAddItemTypePrototype>();
            this._ItemPoolRemoveItemType = this.Get("ItemPoolRemoveItemType").ToDelegate<ItemPoolRemoveItemTypePrototype>();
            this._PlaceRandomItem = this.Get("PlaceRandomItem").ToDelegate<PlaceRandomItemPrototype>();
            this._ChooseRandomCreep = this.Get("ChooseRandomCreep").ToDelegate<ChooseRandomCreepPrototype>();
            this._ChooseRandomNPBuilding = this.Get("ChooseRandomNPBuilding").ToDelegate<ChooseRandomNPBuildingPrototype>();
            this._ChooseRandomItem = this.Get("ChooseRandomItem").ToDelegate<ChooseRandomItemPrototype>();
            this._ChooseRandomItemEx = this.Get("ChooseRandomItemEx").ToDelegate<ChooseRandomItemExPrototype>();
            this._SetRandomSeed = this.Get("SetRandomSeed").ToDelegate<SetRandomSeedPrototype>();
            this._SetTerrainFog = this.Get("SetTerrainFog").ToDelegate<SetTerrainFogPrototype>();
            this._ResetTerrainFog = this.Get("ResetTerrainFog").ToDelegate<ResetTerrainFogPrototype>();
            this._SetUnitFog = this.Get("SetUnitFog").ToDelegate<SetUnitFogPrototype>();
            this._SetTerrainFogEx = this.Get("SetTerrainFogEx").ToDelegate<SetTerrainFogExPrototype>();
            this._DisplayTextToPlayer = this.Get("DisplayTextToPlayer").ToDelegate<DisplayTextToPlayerPrototype>();
            this._DisplayTimedTextToPlayer = this.Get("DisplayTimedTextToPlayer").ToDelegate<DisplayTimedTextToPlayerPrototype>();
            this._DisplayTimedTextFromPlayer = this.Get("DisplayTimedTextFromPlayer").ToDelegate<DisplayTimedTextFromPlayerPrototype>();
            this._ClearTextMessages = this.Get("ClearTextMessages").ToDelegate<ClearTextMessagesPrototype>();
            this._SetDayNightModels = this.Get("SetDayNightModels").ToDelegate<SetDayNightModelsPrototype>();
            this._SetSkyModel = this.Get("SetSkyModel").ToDelegate<SetSkyModelPrototype>();
            this._EnableUserControl = this.Get("EnableUserControl").ToDelegate<EnableUserControlPrototype>();
            this._EnableUserUI = this.Get("EnableUserUI").ToDelegate<EnableUserUIPrototype>();
            this._SuspendTimeOfDay = this.Get("SuspendTimeOfDay").ToDelegate<SuspendTimeOfDayPrototype>();
            this._SetTimeOfDayScale = this.Get("SetTimeOfDayScale").ToDelegate<SetTimeOfDayScalePrototype>();
            this._GetTimeOfDayScale = this.Get("GetTimeOfDayScale").ToDelegate<GetTimeOfDayScalePrototype>();
            this._ShowInterface = this.Get("ShowInterface").ToDelegate<ShowInterfacePrototype>();
            this._PauseGame = this.Get("PauseGame").ToDelegate<PauseGamePrototype>();
            this._UnitAddIndicator = this.Get("UnitAddIndicator").ToDelegate<UnitAddIndicatorPrototype>();
            this._AddIndicator = this.Get("AddIndicator").ToDelegate<AddIndicatorPrototype>();
            this._PingMinimap = this.Get("PingMinimap").ToDelegate<PingMinimapPrototype>();
            this._PingMinimapEx = this.Get("PingMinimapEx").ToDelegate<PingMinimapExPrototype>();
            this._EnableOcclusion = this.Get("EnableOcclusion").ToDelegate<EnableOcclusionPrototype>();
            this._SetIntroShotText = this.Get("SetIntroShotText").ToDelegate<SetIntroShotTextPrototype>();
            this._SetIntroShotModel = this.Get("SetIntroShotModel").ToDelegate<SetIntroShotModelPrototype>();
            this._EnableWorldFogBoundary = this.Get("EnableWorldFogBoundary").ToDelegate<EnableWorldFogBoundaryPrototype>();
            this._PlayModelCinematic = this.Get("PlayModelCinematic").ToDelegate<PlayModelCinematicPrototype>();
            this._PlayCinematic = this.Get("PlayCinematic").ToDelegate<PlayCinematicPrototype>();
            this._ForceUIKey = this.Get("ForceUIKey").ToDelegate<ForceUIKeyPrototype>();
            this._ForceUICancel = this.Get("ForceUICancel").ToDelegate<ForceUICancelPrototype>();
            this._DisplayLoadDialog = this.Get("DisplayLoadDialog").ToDelegate<DisplayLoadDialogPrototype>();
            this._SetAltMinimapIcon = this.Get("SetAltMinimapIcon").ToDelegate<SetAltMinimapIconPrototype>();
            this._DisableRestartMission = this.Get("DisableRestartMission").ToDelegate<DisableRestartMissionPrototype>();
            this._CreateTextTag = this.Get("CreateTextTag").ToDelegate<CreateTextTagPrototype>();
            this._DestroyTextTag = this.Get("DestroyTextTag").ToDelegate<DestroyTextTagPrototype>();
            this._SetTextTagText = this.Get("SetTextTagText").ToDelegate<SetTextTagTextPrototype>();
            this._SetTextTagPos = this.Get("SetTextTagPos").ToDelegate<SetTextTagPosPrototype>();
            this._SetTextTagPosUnit = this.Get("SetTextTagPosUnit").ToDelegate<SetTextTagPosUnitPrototype>();
            this._SetTextTagColor = this.Get("SetTextTagColor").ToDelegate<SetTextTagColorPrototype>();
            this._SetTextTagVelocity = this.Get("SetTextTagVelocity").ToDelegate<SetTextTagVelocityPrototype>();
            this._SetTextTagVisibility = this.Get("SetTextTagVisibility").ToDelegate<SetTextTagVisibilityPrototype>();
            this._SetTextTagSuspended = this.Get("SetTextTagSuspended").ToDelegate<SetTextTagSuspendedPrototype>();
            this._SetTextTagPermanent = this.Get("SetTextTagPermanent").ToDelegate<SetTextTagPermanentPrototype>();
            this._SetTextTagAge = this.Get("SetTextTagAge").ToDelegate<SetTextTagAgePrototype>();
            this._SetTextTagLifespan = this.Get("SetTextTagLifespan").ToDelegate<SetTextTagLifespanPrototype>();
            this._SetTextTagFadepoint = this.Get("SetTextTagFadepoint").ToDelegate<SetTextTagFadepointPrototype>();
            this._SetReservedLocalHeroButtons = this.Get("SetReservedLocalHeroButtons").ToDelegate<SetReservedLocalHeroButtonsPrototype>();
            this._GetAllyColorFilterState = this.Get("GetAllyColorFilterState").ToDelegate<GetAllyColorFilterStatePrototype>();
            this._SetAllyColorFilterState = this.Get("SetAllyColorFilterState").ToDelegate<SetAllyColorFilterStatePrototype>();
            this._GetCreepCampFilterState = this.Get("GetCreepCampFilterState").ToDelegate<GetCreepCampFilterStatePrototype>();
            this._SetCreepCampFilterState = this.Get("SetCreepCampFilterState").ToDelegate<SetCreepCampFilterStatePrototype>();
            this._EnableMinimapFilterButtons = this.Get("EnableMinimapFilterButtons").ToDelegate<EnableMinimapFilterButtonsPrototype>();
            this._EnableDragSelect = this.Get("EnableDragSelect").ToDelegate<EnableDragSelectPrototype>();
            this._EnablePreSelect = this.Get("EnablePreSelect").ToDelegate<EnablePreSelectPrototype>();
            this._EnableSelect = this.Get("EnableSelect").ToDelegate<EnableSelectPrototype>();
            this._CreateTrackable = this.Get("CreateTrackable").ToDelegate<CreateTrackablePrototype>();
            this._CreateQuest = this.Get("CreateQuest").ToDelegate<CreateQuestPrototype>();
            this._DestroyQuest = this.Get("DestroyQuest").ToDelegate<DestroyQuestPrototype>();
            this._QuestSetTitle = this.Get("QuestSetTitle").ToDelegate<QuestSetTitlePrototype>();
            this._QuestSetDescription = this.Get("QuestSetDescription").ToDelegate<QuestSetDescriptionPrototype>();
            this._QuestSetIconPath = this.Get("QuestSetIconPath").ToDelegate<QuestSetIconPathPrototype>();
            this._QuestSetRequired = this.Get("QuestSetRequired").ToDelegate<QuestSetRequiredPrototype>();
            this._QuestSetCompleted = this.Get("QuestSetCompleted").ToDelegate<QuestSetCompletedPrototype>();
            this._QuestSetDiscovered = this.Get("QuestSetDiscovered").ToDelegate<QuestSetDiscoveredPrototype>();
            this._QuestSetFailed = this.Get("QuestSetFailed").ToDelegate<QuestSetFailedPrototype>();
            this._QuestSetEnabled = this.Get("QuestSetEnabled").ToDelegate<QuestSetEnabledPrototype>();
            this._IsQuestRequired = this.Get("IsQuestRequired").ToDelegate<IsQuestRequiredPrototype>();
            this._IsQuestCompleted = this.Get("IsQuestCompleted").ToDelegate<IsQuestCompletedPrototype>();
            this._IsQuestDiscovered = this.Get("IsQuestDiscovered").ToDelegate<IsQuestDiscoveredPrototype>();
            this._IsQuestFailed = this.Get("IsQuestFailed").ToDelegate<IsQuestFailedPrototype>();
            this._IsQuestEnabled = this.Get("IsQuestEnabled").ToDelegate<IsQuestEnabledPrototype>();
            this._QuestCreateItem = this.Get("QuestCreateItem").ToDelegate<QuestCreateItemPrototype>();
            this._QuestItemSetDescription = this.Get("QuestItemSetDescription").ToDelegate<QuestItemSetDescriptionPrototype>();
            this._QuestItemSetCompleted = this.Get("QuestItemSetCompleted").ToDelegate<QuestItemSetCompletedPrototype>();
            this._IsQuestItemCompleted = this.Get("IsQuestItemCompleted").ToDelegate<IsQuestItemCompletedPrototype>();
            this._CreateDefeatCondition = this.Get("CreateDefeatCondition").ToDelegate<CreateDefeatConditionPrototype>();
            this._DestroyDefeatCondition = this.Get("DestroyDefeatCondition").ToDelegate<DestroyDefeatConditionPrototype>();
            this._DefeatConditionSetDescription = this.Get("DefeatConditionSetDescription").ToDelegate<DefeatConditionSetDescriptionPrototype>();
            this._FlashQuestDialogButton = this.Get("FlashQuestDialogButton").ToDelegate<FlashQuestDialogButtonPrototype>();
            this._ForceQuestDialogUpdate = this.Get("ForceQuestDialogUpdate").ToDelegate<ForceQuestDialogUpdatePrototype>();
            this._CreateTimerDialog = this.Get("CreateTimerDialog").ToDelegate<CreateTimerDialogPrototype>();
            this._DestroyTimerDialog = this.Get("DestroyTimerDialog").ToDelegate<DestroyTimerDialogPrototype>();
            this._TimerDialogSetTitle = this.Get("TimerDialogSetTitle").ToDelegate<TimerDialogSetTitlePrototype>();
            this._TimerDialogSetTitleColor = this.Get("TimerDialogSetTitleColor").ToDelegate<TimerDialogSetTitleColorPrototype>();
            this._TimerDialogSetTimeColor = this.Get("TimerDialogSetTimeColor").ToDelegate<TimerDialogSetTimeColorPrototype>();
            this._TimerDialogSetSpeed = this.Get("TimerDialogSetSpeed").ToDelegate<TimerDialogSetSpeedPrototype>();
            this._TimerDialogDisplay = this.Get("TimerDialogDisplay").ToDelegate<TimerDialogDisplayPrototype>();
            this._IsTimerDialogDisplayed = this.Get("IsTimerDialogDisplayed").ToDelegate<IsTimerDialogDisplayedPrototype>();
            this._TimerDialogSetRealTimeRemaining = this.Get("TimerDialogSetRealTimeRemaining").ToDelegate<TimerDialogSetRealTimeRemainingPrototype>();
            this._CreateLeaderboard = this.Get("CreateLeaderboard").ToDelegate<CreateLeaderboardPrototype>();
            this._DestroyLeaderboard = this.Get("DestroyLeaderboard").ToDelegate<DestroyLeaderboardPrototype>();
            this._LeaderboardDisplay = this.Get("LeaderboardDisplay").ToDelegate<LeaderboardDisplayPrototype>();
            this._IsLeaderboardDisplayed = this.Get("IsLeaderboardDisplayed").ToDelegate<IsLeaderboardDisplayedPrototype>();
            this._LeaderboardGetItemCount = this.Get("LeaderboardGetItemCount").ToDelegate<LeaderboardGetItemCountPrototype>();
            this._LeaderboardSetSizeByItemCount = this.Get("LeaderboardSetSizeByItemCount").ToDelegate<LeaderboardSetSizeByItemCountPrototype>();
            this._LeaderboardAddItem = this.Get("LeaderboardAddItem").ToDelegate<LeaderboardAddItemPrototype>();
            this._LeaderboardRemoveItem = this.Get("LeaderboardRemoveItem").ToDelegate<LeaderboardRemoveItemPrototype>();
            this._LeaderboardRemovePlayerItem = this.Get("LeaderboardRemovePlayerItem").ToDelegate<LeaderboardRemovePlayerItemPrototype>();
            this._LeaderboardClear = this.Get("LeaderboardClear").ToDelegate<LeaderboardClearPrototype>();
            this._LeaderboardSortItemsByValue = this.Get("LeaderboardSortItemsByValue").ToDelegate<LeaderboardSortItemsByValuePrototype>();
            this._LeaderboardSortItemsByPlayer = this.Get("LeaderboardSortItemsByPlayer").ToDelegate<LeaderboardSortItemsByPlayerPrototype>();
            this._LeaderboardSortItemsByLabel = this.Get("LeaderboardSortItemsByLabel").ToDelegate<LeaderboardSortItemsByLabelPrototype>();
            this._LeaderboardHasPlayerItem = this.Get("LeaderboardHasPlayerItem").ToDelegate<LeaderboardHasPlayerItemPrototype>();
            this._LeaderboardGetPlayerIndex = this.Get("LeaderboardGetPlayerIndex").ToDelegate<LeaderboardGetPlayerIndexPrototype>();
            this._LeaderboardSetLabel = this.Get("LeaderboardSetLabel").ToDelegate<LeaderboardSetLabelPrototype>();
            this._LeaderboardGetLabelText = this.Get("LeaderboardGetLabelText").ToDelegate<LeaderboardGetLabelTextPrototype>();
            this._PlayerSetLeaderboard = this.Get("PlayerSetLeaderboard").ToDelegate<PlayerSetLeaderboardPrototype>();
            this._PlayerGetLeaderboard = this.Get("PlayerGetLeaderboard").ToDelegate<PlayerGetLeaderboardPrototype>();
            this._LeaderboardSetLabelColor = this.Get("LeaderboardSetLabelColor").ToDelegate<LeaderboardSetLabelColorPrototype>();
            this._LeaderboardSetValueColor = this.Get("LeaderboardSetValueColor").ToDelegate<LeaderboardSetValueColorPrototype>();
            this._LeaderboardSetStyle = this.Get("LeaderboardSetStyle").ToDelegate<LeaderboardSetStylePrototype>();
            this._LeaderboardSetItemValue = this.Get("LeaderboardSetItemValue").ToDelegate<LeaderboardSetItemValuePrototype>();
            this._LeaderboardSetItemLabel = this.Get("LeaderboardSetItemLabel").ToDelegate<LeaderboardSetItemLabelPrototype>();
            this._LeaderboardSetItemStyle = this.Get("LeaderboardSetItemStyle").ToDelegate<LeaderboardSetItemStylePrototype>();
            this._LeaderboardSetItemLabelColor = this.Get("LeaderboardSetItemLabelColor").ToDelegate<LeaderboardSetItemLabelColorPrototype>();
            this._LeaderboardSetItemValueColor = this.Get("LeaderboardSetItemValueColor").ToDelegate<LeaderboardSetItemValueColorPrototype>();
            this._CreateMultiboard = this.Get("CreateMultiboard").ToDelegate<CreateMultiboardPrototype>();
            this._DestroyMultiboard = this.Get("DestroyMultiboard").ToDelegate<DestroyMultiboardPrototype>();
            this._MultiboardDisplay = this.Get("MultiboardDisplay").ToDelegate<MultiboardDisplayPrototype>();
            this._IsMultiboardDisplayed = this.Get("IsMultiboardDisplayed").ToDelegate<IsMultiboardDisplayedPrototype>();
            this._MultiboardMinimize = this.Get("MultiboardMinimize").ToDelegate<MultiboardMinimizePrototype>();
            this._IsMultiboardMinimized = this.Get("IsMultiboardMinimized").ToDelegate<IsMultiboardMinimizedPrototype>();
            this._MultiboardClear = this.Get("MultiboardClear").ToDelegate<MultiboardClearPrototype>();
            this._MultiboardSetTitleText = this.Get("MultiboardSetTitleText").ToDelegate<MultiboardSetTitleTextPrototype>();
            this._MultiboardGetTitleText = this.Get("MultiboardGetTitleText").ToDelegate<MultiboardGetTitleTextPrototype>();
            this._MultiboardSetTitleTextColor = this.Get("MultiboardSetTitleTextColor").ToDelegate<MultiboardSetTitleTextColorPrototype>();
            this._MultiboardGetRowCount = this.Get("MultiboardGetRowCount").ToDelegate<MultiboardGetRowCountPrototype>();
            this._MultiboardGetColumnCount = this.Get("MultiboardGetColumnCount").ToDelegate<MultiboardGetColumnCountPrototype>();
            this._MultiboardSetColumnCount = this.Get("MultiboardSetColumnCount").ToDelegate<MultiboardSetColumnCountPrototype>();
            this._MultiboardSetRowCount = this.Get("MultiboardSetRowCount").ToDelegate<MultiboardSetRowCountPrototype>();
            this._MultiboardSetItemsStyle = this.Get("MultiboardSetItemsStyle").ToDelegate<MultiboardSetItemsStylePrototype>();
            this._MultiboardSetItemsValue = this.Get("MultiboardSetItemsValue").ToDelegate<MultiboardSetItemsValuePrototype>();
            this._MultiboardSetItemsValueColor = this.Get("MultiboardSetItemsValueColor").ToDelegate<MultiboardSetItemsValueColorPrototype>();
            this._MultiboardSetItemsWidth = this.Get("MultiboardSetItemsWidth").ToDelegate<MultiboardSetItemsWidthPrototype>();
            this._MultiboardSetItemsIcon = this.Get("MultiboardSetItemsIcon").ToDelegate<MultiboardSetItemsIconPrototype>();
            this._MultiboardGetItem = this.Get("MultiboardGetItem").ToDelegate<MultiboardGetItemPrototype>();
            this._MultiboardReleaseItem = this.Get("MultiboardReleaseItem").ToDelegate<MultiboardReleaseItemPrototype>();
            this._MultiboardSetItemStyle = this.Get("MultiboardSetItemStyle").ToDelegate<MultiboardSetItemStylePrototype>();
            this._MultiboardSetItemValue = this.Get("MultiboardSetItemValue").ToDelegate<MultiboardSetItemValuePrototype>();
            this._MultiboardSetItemValueColor = this.Get("MultiboardSetItemValueColor").ToDelegate<MultiboardSetItemValueColorPrototype>();
            this._MultiboardSetItemWidth = this.Get("MultiboardSetItemWidth").ToDelegate<MultiboardSetItemWidthPrototype>();
            this._MultiboardSetItemIcon = this.Get("MultiboardSetItemIcon").ToDelegate<MultiboardSetItemIconPrototype>();
            this._MultiboardSuppressDisplay = this.Get("MultiboardSuppressDisplay").ToDelegate<MultiboardSuppressDisplayPrototype>();
            this._SetCameraPosition = this.Get("SetCameraPosition").ToDelegate<SetCameraPositionPrototype>();
            this._SetCameraQuickPosition = this.Get("SetCameraQuickPosition").ToDelegate<SetCameraQuickPositionPrototype>();
            this._SetCameraBounds = this.Get("SetCameraBounds").ToDelegate<SetCameraBoundsPrototype>();
            this._StopCamera = this.Get("StopCamera").ToDelegate<StopCameraPrototype>();
            this._ResetToGameCamera = this.Get("ResetToGameCamera").ToDelegate<ResetToGameCameraPrototype>();
            this._PanCameraTo = this.Get("PanCameraTo").ToDelegate<PanCameraToPrototype>();
            this._PanCameraToTimed = this.Get("PanCameraToTimed").ToDelegate<PanCameraToTimedPrototype>();
            this._PanCameraToWithZ = this.Get("PanCameraToWithZ").ToDelegate<PanCameraToWithZPrototype>();
            this._PanCameraToTimedWithZ = this.Get("PanCameraToTimedWithZ").ToDelegate<PanCameraToTimedWithZPrototype>();
            this._SetCinematicCamera = this.Get("SetCinematicCamera").ToDelegate<SetCinematicCameraPrototype>();
            this._SetCameraRotateMode = this.Get("SetCameraRotateMode").ToDelegate<SetCameraRotateModePrototype>();
            this._SetCameraField = this.Get("SetCameraField").ToDelegate<SetCameraFieldPrototype>();
            this._AdjustCameraField = this.Get("AdjustCameraField").ToDelegate<AdjustCameraFieldPrototype>();
            this._SetCameraTargetController = this.Get("SetCameraTargetController").ToDelegate<SetCameraTargetControllerPrototype>();
            this._SetCameraOrientController = this.Get("SetCameraOrientController").ToDelegate<SetCameraOrientControllerPrototype>();
            this._CreateCameraSetup = this.Get("CreateCameraSetup").ToDelegate<CreateCameraSetupPrototype>();
            this._CameraSetupSetField = this.Get("CameraSetupSetField").ToDelegate<CameraSetupSetFieldPrototype>();
            this._CameraSetupGetField = this.Get("CameraSetupGetField").ToDelegate<CameraSetupGetFieldPrototype>();
            this._CameraSetupSetDestPosition = this.Get("CameraSetupSetDestPosition").ToDelegate<CameraSetupSetDestPositionPrototype>();
            this._CameraSetupGetDestPositionLoc = this.Get("CameraSetupGetDestPositionLoc").ToDelegate<CameraSetupGetDestPositionLocPrototype>();
            this._CameraSetupGetDestPositionX = this.Get("CameraSetupGetDestPositionX").ToDelegate<CameraSetupGetDestPositionXPrototype>();
            this._CameraSetupGetDestPositionY = this.Get("CameraSetupGetDestPositionY").ToDelegate<CameraSetupGetDestPositionYPrototype>();
            this._CameraSetupApply = this.Get("CameraSetupApply").ToDelegate<CameraSetupApplyPrototype>();
            this._CameraSetupApplyWithZ = this.Get("CameraSetupApplyWithZ").ToDelegate<CameraSetupApplyWithZPrototype>();
            this._CameraSetupApplyForceDuration = this.Get("CameraSetupApplyForceDuration").ToDelegate<CameraSetupApplyForceDurationPrototype>();
            this._CameraSetupApplyForceDurationWithZ = this.Get("CameraSetupApplyForceDurationWithZ").ToDelegate<CameraSetupApplyForceDurationWithZPrototype>();
            this._CameraSetTargetNoise = this.Get("CameraSetTargetNoise").ToDelegate<CameraSetTargetNoisePrototype>();
            this._CameraSetSourceNoise = this.Get("CameraSetSourceNoise").ToDelegate<CameraSetSourceNoisePrototype>();
            this._CameraSetTargetNoiseEx = this.Get("CameraSetTargetNoiseEx").ToDelegate<CameraSetTargetNoiseExPrototype>();
            this._CameraSetSourceNoiseEx = this.Get("CameraSetSourceNoiseEx").ToDelegate<CameraSetSourceNoiseExPrototype>();
            this._CameraSetSmoothingFactor = this.Get("CameraSetSmoothingFactor").ToDelegate<CameraSetSmoothingFactorPrototype>();
            this._SetCineFilterTexture = this.Get("SetCineFilterTexture").ToDelegate<SetCineFilterTexturePrototype>();
            this._SetCineFilterBlendMode = this.Get("SetCineFilterBlendMode").ToDelegate<SetCineFilterBlendModePrototype>();
            this._SetCineFilterTexMapFlags = this.Get("SetCineFilterTexMapFlags").ToDelegate<SetCineFilterTexMapFlagsPrototype>();
            this._SetCineFilterStartUV = this.Get("SetCineFilterStartUV").ToDelegate<SetCineFilterStartUVPrototype>();
            this._SetCineFilterEndUV = this.Get("SetCineFilterEndUV").ToDelegate<SetCineFilterEndUVPrototype>();
            this._SetCineFilterStartColor = this.Get("SetCineFilterStartColor").ToDelegate<SetCineFilterStartColorPrototype>();
            this._SetCineFilterEndColor = this.Get("SetCineFilterEndColor").ToDelegate<SetCineFilterEndColorPrototype>();
            this._SetCineFilterDuration = this.Get("SetCineFilterDuration").ToDelegate<SetCineFilterDurationPrototype>();
            this._DisplayCineFilter = this.Get("DisplayCineFilter").ToDelegate<DisplayCineFilterPrototype>();
            this._IsCineFilterDisplayed = this.Get("IsCineFilterDisplayed").ToDelegate<IsCineFilterDisplayedPrototype>();
            this._SetCinematicScene = this.Get("SetCinematicScene").ToDelegate<SetCinematicScenePrototype>();
            this._EndCinematicScene = this.Get("EndCinematicScene").ToDelegate<EndCinematicScenePrototype>();
            this._ForceCinematicSubtitles = this.Get("ForceCinematicSubtitles").ToDelegate<ForceCinematicSubtitlesPrototype>();
            this._GetCameraMargin = this.Get("GetCameraMargin").ToDelegate<GetCameraMarginPrototype>();
            this._GetCameraBoundMinX = this.Get("GetCameraBoundMinX").ToDelegate<GetCameraBoundMinXPrototype>();
            this._GetCameraBoundMinY = this.Get("GetCameraBoundMinY").ToDelegate<GetCameraBoundMinYPrototype>();
            this._GetCameraBoundMaxX = this.Get("GetCameraBoundMaxX").ToDelegate<GetCameraBoundMaxXPrototype>();
            this._GetCameraBoundMaxY = this.Get("GetCameraBoundMaxY").ToDelegate<GetCameraBoundMaxYPrototype>();
            this._GetCameraField = this.Get("GetCameraField").ToDelegate<GetCameraFieldPrototype>();
            this._GetCameraTargetPositionX = this.Get("GetCameraTargetPositionX").ToDelegate<GetCameraTargetPositionXPrototype>();
            this._GetCameraTargetPositionY = this.Get("GetCameraTargetPositionY").ToDelegate<GetCameraTargetPositionYPrototype>();
            this._GetCameraTargetPositionZ = this.Get("GetCameraTargetPositionZ").ToDelegate<GetCameraTargetPositionZPrototype>();
            this._GetCameraTargetPositionLoc = this.Get("GetCameraTargetPositionLoc").ToDelegate<GetCameraTargetPositionLocPrototype>();
            this._GetCameraEyePositionX = this.Get("GetCameraEyePositionX").ToDelegate<GetCameraEyePositionXPrototype>();
            this._GetCameraEyePositionY = this.Get("GetCameraEyePositionY").ToDelegate<GetCameraEyePositionYPrototype>();
            this._GetCameraEyePositionZ = this.Get("GetCameraEyePositionZ").ToDelegate<GetCameraEyePositionZPrototype>();
            this._GetCameraEyePositionLoc = this.Get("GetCameraEyePositionLoc").ToDelegate<GetCameraEyePositionLocPrototype>();
            this._NewSoundEnvironment = this.Get("NewSoundEnvironment").ToDelegate<NewSoundEnvironmentPrototype>();
            this._CreateSound = this.Get("CreateSound").ToDelegate<CreateSoundPrototype>();
            this._CreateSoundFilenameWithLabel = this.Get("CreateSoundFilenameWithLabel").ToDelegate<CreateSoundFilenameWithLabelPrototype>();
            this._CreateSoundFromLabel = this.Get("CreateSoundFromLabel").ToDelegate<CreateSoundFromLabelPrototype>();
            this._CreateMIDISound = this.Get("CreateMIDISound").ToDelegate<CreateMIDISoundPrototype>();
            this._SetSoundParamsFromLabel = this.Get("SetSoundParamsFromLabel").ToDelegate<SetSoundParamsFromLabelPrototype>();
            this._SetSoundDistanceCutoff = this.Get("SetSoundDistanceCutoff").ToDelegate<SetSoundDistanceCutoffPrototype>();
            this._SetSoundChannel = this.Get("SetSoundChannel").ToDelegate<SetSoundChannelPrototype>();
            this._SetSoundVolume = this.Get("SetSoundVolume").ToDelegate<SetSoundVolumePrototype>();
            this._SetSoundPitch = this.Get("SetSoundPitch").ToDelegate<SetSoundPitchPrototype>();
            this._SetSoundPlayPosition = this.Get("SetSoundPlayPosition").ToDelegate<SetSoundPlayPositionPrototype>();
            this._SetSoundDistances = this.Get("SetSoundDistances").ToDelegate<SetSoundDistancesPrototype>();
            this._SetSoundConeAngles = this.Get("SetSoundConeAngles").ToDelegate<SetSoundConeAnglesPrototype>();
            this._SetSoundConeOrientation = this.Get("SetSoundConeOrientation").ToDelegate<SetSoundConeOrientationPrototype>();
            this._SetSoundPosition = this.Get("SetSoundPosition").ToDelegate<SetSoundPositionPrototype>();
            this._SetSoundVelocity = this.Get("SetSoundVelocity").ToDelegate<SetSoundVelocityPrototype>();
            this._AttachSoundToUnit = this.Get("AttachSoundToUnit").ToDelegate<AttachSoundToUnitPrototype>();
            this._StartSound = this.Get("StartSound").ToDelegate<StartSoundPrototype>();
            this._StopSound = this.Get("StopSound").ToDelegate<StopSoundPrototype>();
            this._KillSoundWhenDone = this.Get("KillSoundWhenDone").ToDelegate<KillSoundWhenDonePrototype>();
            this._SetMapMusic = this.Get("SetMapMusic").ToDelegate<SetMapMusicPrototype>();
            this._ClearMapMusic = this.Get("ClearMapMusic").ToDelegate<ClearMapMusicPrototype>();
            this._PlayMusic = this.Get("PlayMusic").ToDelegate<PlayMusicPrototype>();
            this._PlayMusicEx = this.Get("PlayMusicEx").ToDelegate<PlayMusicExPrototype>();
            this._StopMusic = this.Get("StopMusic").ToDelegate<StopMusicPrototype>();
            this._ResumeMusic = this.Get("ResumeMusic").ToDelegate<ResumeMusicPrototype>();
            this._PlayThematicMusic = this.Get("PlayThematicMusic").ToDelegate<PlayThematicMusicPrototype>();
            this._PlayThematicMusicEx = this.Get("PlayThematicMusicEx").ToDelegate<PlayThematicMusicExPrototype>();
            this._EndThematicMusic = this.Get("EndThematicMusic").ToDelegate<EndThematicMusicPrototype>();
            this._SetMusicVolume = this.Get("SetMusicVolume").ToDelegate<SetMusicVolumePrototype>();
            this._SetMusicPlayPosition = this.Get("SetMusicPlayPosition").ToDelegate<SetMusicPlayPositionPrototype>();
            this._SetThematicMusicPlayPosition = this.Get("SetThematicMusicPlayPosition").ToDelegate<SetThematicMusicPlayPositionPrototype>();
            this._SetSoundDuration = this.Get("SetSoundDuration").ToDelegate<SetSoundDurationPrototype>();
            this._GetSoundDuration = this.Get("GetSoundDuration").ToDelegate<GetSoundDurationPrototype>();
            this._GetSoundFileDuration = this.Get("GetSoundFileDuration").ToDelegate<GetSoundFileDurationPrototype>();
            this._VolumeGroupSetVolume = this.Get("VolumeGroupSetVolume").ToDelegate<VolumeGroupSetVolumePrototype>();
            this._VolumeGroupReset = this.Get("VolumeGroupReset").ToDelegate<VolumeGroupResetPrototype>();
            this._GetSoundIsPlaying = this.Get("GetSoundIsPlaying").ToDelegate<GetSoundIsPlayingPrototype>();
            this._GetSoundIsLoading = this.Get("GetSoundIsLoading").ToDelegate<GetSoundIsLoadingPrototype>();
            this._RegisterStackedSound = this.Get("RegisterStackedSound").ToDelegate<RegisterStackedSoundPrototype>();
            this._UnregisterStackedSound = this.Get("UnregisterStackedSound").ToDelegate<UnregisterStackedSoundPrototype>();
            this._AddWeatherEffect = this.Get("AddWeatherEffect").ToDelegate<AddWeatherEffectPrototype>();
            this._RemoveWeatherEffect = this.Get("RemoveWeatherEffect").ToDelegate<RemoveWeatherEffectPrototype>();
            this._EnableWeatherEffect = this.Get("EnableWeatherEffect").ToDelegate<EnableWeatherEffectPrototype>();
            this._TerrainDeformCrater = this.Get("TerrainDeformCrater").ToDelegate<TerrainDeformCraterPrototype>();
            this._TerrainDeformRipple = this.Get("TerrainDeformRipple").ToDelegate<TerrainDeformRipplePrototype>();
            this._TerrainDeformWave = this.Get("TerrainDeformWave").ToDelegate<TerrainDeformWavePrototype>();
            this._TerrainDeformRandom = this.Get("TerrainDeformRandom").ToDelegate<TerrainDeformRandomPrototype>();
            this._TerrainDeformStop = this.Get("TerrainDeformStop").ToDelegate<TerrainDeformStopPrototype>();
            this._TerrainDeformStopAll = this.Get("TerrainDeformStopAll").ToDelegate<TerrainDeformStopAllPrototype>();
            this._AddSpecialEffect = this.Get("AddSpecialEffect").ToDelegate<AddSpecialEffectPrototype>();
            this._AddSpecialEffectLoc = this.Get("AddSpecialEffectLoc").ToDelegate<AddSpecialEffectLocPrototype>();
            this._AddSpecialEffectTarget = this.Get("AddSpecialEffectTarget").ToDelegate<AddSpecialEffectTargetPrototype>();
            this._DestroyEffect = this.Get("DestroyEffect").ToDelegate<DestroyEffectPrototype>();
            this._AddSpellEffect = this.Get("AddSpellEffect").ToDelegate<AddSpellEffectPrototype>();
            this._AddSpellEffectLoc = this.Get("AddSpellEffectLoc").ToDelegate<AddSpellEffectLocPrototype>();
            this._AddSpellEffectById = this.Get("AddSpellEffectById").ToDelegate<AddSpellEffectByIdPrototype>();
            this._AddSpellEffectByIdLoc = this.Get("AddSpellEffectByIdLoc").ToDelegate<AddSpellEffectByIdLocPrototype>();
            this._AddSpellEffectTarget = this.Get("AddSpellEffectTarget").ToDelegate<AddSpellEffectTargetPrototype>();
            this._AddSpellEffectTargetById = this.Get("AddSpellEffectTargetById").ToDelegate<AddSpellEffectTargetByIdPrototype>();
            this._AddLightning = this.Get("AddLightning").ToDelegate<AddLightningPrototype>();
            this._AddLightningEx = this.Get("AddLightningEx").ToDelegate<AddLightningExPrototype>();
            this._DestroyLightning = this.Get("DestroyLightning").ToDelegate<DestroyLightningPrototype>();
            this._MoveLightning = this.Get("MoveLightning").ToDelegate<MoveLightningPrototype>();
            this._MoveLightningEx = this.Get("MoveLightningEx").ToDelegate<MoveLightningExPrototype>();
            this._GetLightningColorA = this.Get("GetLightningColorA").ToDelegate<GetLightningColorAPrototype>();
            this._GetLightningColorR = this.Get("GetLightningColorR").ToDelegate<GetLightningColorRPrototype>();
            this._GetLightningColorG = this.Get("GetLightningColorG").ToDelegate<GetLightningColorGPrototype>();
            this._GetLightningColorB = this.Get("GetLightningColorB").ToDelegate<GetLightningColorBPrototype>();
            this._SetLightningColor = this.Get("SetLightningColor").ToDelegate<SetLightningColorPrototype>();
            this._GetAbilityEffect = this.Get("GetAbilityEffect").ToDelegate<GetAbilityEffectPrototype>();
            this._GetAbilityEffectById = this.Get("GetAbilityEffectById").ToDelegate<GetAbilityEffectByIdPrototype>();
            this._GetAbilitySound = this.Get("GetAbilitySound").ToDelegate<GetAbilitySoundPrototype>();
            this._GetAbilitySoundById = this.Get("GetAbilitySoundById").ToDelegate<GetAbilitySoundByIdPrototype>();
            this._GetTerrainCliffLevel = this.Get("GetTerrainCliffLevel").ToDelegate<GetTerrainCliffLevelPrototype>();
            this._SetWaterBaseColor = this.Get("SetWaterBaseColor").ToDelegate<SetWaterBaseColorPrototype>();
            this._SetWaterDeforms = this.Get("SetWaterDeforms").ToDelegate<SetWaterDeformsPrototype>();
            this._GetTerrainType = this.Get("GetTerrainType").ToDelegate<GetTerrainTypePrototype>();
            this._GetTerrainVariance = this.Get("GetTerrainVariance").ToDelegate<GetTerrainVariancePrototype>();
            this._SetTerrainType = this.Get("SetTerrainType").ToDelegate<SetTerrainTypePrototype>();
            this._IsTerrainPathable = this.Get("IsTerrainPathable").ToDelegate<IsTerrainPathablePrototype>();
            this._SetTerrainPathable = this.Get("SetTerrainPathable").ToDelegate<SetTerrainPathablePrototype>();
            this._CreateImage = this.Get("CreateImage").ToDelegate<CreateImagePrototype>();
            this._DestroyImage = this.Get("DestroyImage").ToDelegate<DestroyImagePrototype>();
            this._ShowImage = this.Get("ShowImage").ToDelegate<ShowImagePrototype>();
            this._SetImageConstantHeight = this.Get("SetImageConstantHeight").ToDelegate<SetImageConstantHeightPrototype>();
            this._SetImagePosition = this.Get("SetImagePosition").ToDelegate<SetImagePositionPrototype>();
            this._SetImageColor = this.Get("SetImageColor").ToDelegate<SetImageColorPrototype>();
            this._SetImageRender = this.Get("SetImageRender").ToDelegate<SetImageRenderPrototype>();
            this._SetImageRenderAlways = this.Get("SetImageRenderAlways").ToDelegate<SetImageRenderAlwaysPrototype>();
            this._SetImageAboveWater = this.Get("SetImageAboveWater").ToDelegate<SetImageAboveWaterPrototype>();
            this._SetImageType = this.Get("SetImageType").ToDelegate<SetImageTypePrototype>();
            this._CreateUbersplat = this.Get("CreateUbersplat").ToDelegate<CreateUbersplatPrototype>();
            this._DestroyUbersplat = this.Get("DestroyUbersplat").ToDelegate<DestroyUbersplatPrototype>();
            this._ResetUbersplat = this.Get("ResetUbersplat").ToDelegate<ResetUbersplatPrototype>();
            this._FinishUbersplat = this.Get("FinishUbersplat").ToDelegate<FinishUbersplatPrototype>();
            this._ShowUbersplat = this.Get("ShowUbersplat").ToDelegate<ShowUbersplatPrototype>();
            this._SetUbersplatRender = this.Get("SetUbersplatRender").ToDelegate<SetUbersplatRenderPrototype>();
            this._SetUbersplatRenderAlways = this.Get("SetUbersplatRenderAlways").ToDelegate<SetUbersplatRenderAlwaysPrototype>();
            this._SetBlight = this.Get("SetBlight").ToDelegate<SetBlightPrototype>();
            this._SetBlightRect = this.Get("SetBlightRect").ToDelegate<SetBlightRectPrototype>();
            this._SetBlightPoint = this.Get("SetBlightPoint").ToDelegate<SetBlightPointPrototype>();
            this._SetBlightLoc = this.Get("SetBlightLoc").ToDelegate<SetBlightLocPrototype>();
            this._CreateBlightedGoldmine = this.Get("CreateBlightedGoldmine").ToDelegate<CreateBlightedGoldminePrototype>();
            this._IsPointBlighted = this.Get("IsPointBlighted").ToDelegate<IsPointBlightedPrototype>();
            this._SetDoodadAnimation = this.Get("SetDoodadAnimation").ToDelegate<SetDoodadAnimationPrototype>();
            this._SetDoodadAnimationRect = this.Get("SetDoodadAnimationRect").ToDelegate<SetDoodadAnimationRectPrototype>();
            this._StartMeleeAI = this.Get("StartMeleeAI").ToDelegate<StartMeleeAIPrototype>();
            this._StartCampaignAI = this.Get("StartCampaignAI").ToDelegate<StartCampaignAIPrototype>();
            this._CommandAI = this.Get("CommandAI").ToDelegate<CommandAIPrototype>();
            this._PauseCompAI = this.Get("PauseCompAI").ToDelegate<PauseCompAIPrototype>();
            this._GetAIDifficulty = this.Get("GetAIDifficulty").ToDelegate<GetAIDifficultyPrototype>();
            this._RemoveGuardPosition = this.Get("RemoveGuardPosition").ToDelegate<RemoveGuardPositionPrototype>();
            this._RecycleGuardPosition = this.Get("RecycleGuardPosition").ToDelegate<RecycleGuardPositionPrototype>();
            this._RemoveAllGuardPositions = this.Get("RemoveAllGuardPositions").ToDelegate<RemoveAllGuardPositionsPrototype>();
            this._Cheat = this.Get("Cheat").ToDelegate<CheatPrototype>();
            this._IsNoVictoryCheat = this.Get("IsNoVictoryCheat").ToDelegate<IsNoVictoryCheatPrototype>();
            this._IsNoDefeatCheat = this.Get("IsNoDefeatCheat").ToDelegate<IsNoDefeatCheatPrototype>();
            this._Preload = this.Get("Preload").ToDelegate<PreloadPrototype>();
            this._PreloadEnd = this.Get("PreloadEnd").ToDelegate<PreloadEndPrototype>();
            this._PreloadStart = this.Get("PreloadStart").ToDelegate<PreloadStartPrototype>();
            this._PreloadRefresh = this.Get("PreloadRefresh").ToDelegate<PreloadRefreshPrototype>();
            this._PreloadEndEx = this.Get("PreloadEndEx").ToDelegate<PreloadEndExPrototype>();
            this._PreloadGenClear = this.Get("PreloadGenClear").ToDelegate<PreloadGenClearPrototype>();
            this._PreloadGenStart = this.Get("PreloadGenStart").ToDelegate<PreloadGenStartPrototype>();
            this._PreloadGenEnd = this.Get("PreloadGenEnd").ToDelegate<PreloadGenEndPrototype>();
            this._Preloader = this.Get("Preloader").ToDelegate<PreloaderPrototype>();
        }
    }
}
