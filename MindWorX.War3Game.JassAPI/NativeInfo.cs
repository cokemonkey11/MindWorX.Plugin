﻿using System;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged;

namespace MindWorX.War3Game.JassAPI
{
    public class NativeInfo
    {
        public NativeInfo(IntPtr function, string name, string prototype)
        {
            this.FunctionPtr = function;
            this.Name = name;
            this.Prototype = prototype;

            this.NamePtr = Memory.AllocString(this.Name);
            this.PrototypePtr = Memory.AllocString(this.Prototype);
        }

        public NativeInfo(Delegate function, string name, string prototype)
            : this(
                Marshal.GetFunctionPointerForDelegate(function),
                name,
                prototype)
        { }

        public NativeInfo(IntPtr address)
        {
            this.PrototypePtr = Memory.Read<IntPtr>(address, 0x01);
            this.NamePtr = Memory.Read<IntPtr>(address, 0x06);
            this.FunctionPtr = Memory.Read<IntPtr>(address, 0x0B);

            this.Prototype = Memory.ReadString(this.PrototypePtr);
            this.Name = Memory.ReadString(this.NamePtr);
        }

        public IntPtr FunctionPtr { get; }

        public IntPtr NamePtr { get; }

        public IntPtr PrototypePtr { get; }

        public string Name { get; }

        public string Prototype { get; }

        public T ToDelegate<T>() => Marshal.GetDelegateForFunctionPointer<T>(this.FunctionPtr);
    }
}
