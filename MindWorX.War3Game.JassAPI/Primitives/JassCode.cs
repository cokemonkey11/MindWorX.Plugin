﻿using System;

namespace MindWorX.War3Game.JassAPI.Primitives
{
    [JassType("C")]
    [Serializable]
    public struct JassCode
    {
        public static JassCode None { get; } = new JassCode(IntPtr.Zero);


        public readonly IntPtr Handle;

        public JassCode(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
