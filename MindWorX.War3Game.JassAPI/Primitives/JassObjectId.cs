﻿using System;
using MindWorX.Blizzard;

namespace MindWorX.War3Game.JassAPI.Primitives
{
    /// <summary>
    /// JassObjectId is a wrapper for the four byte literal syntax in JASS.
    /// </summary>
    /// <remarks>Used with units, items, abilities, destructables, upgrades and doodads.</remarks>
    [JassType("I")]
    [Serializable]
    public struct JassObjectId
    {
        private readonly int value;

        public JassObjectId(int value)
        {
            this.value = value;
        }

        public JassObjectId(string objectId)
        {
            if (objectId.Length != 4)
                throw new ArgumentOutOfRangeException(nameof(objectId), "Invalid objectId. id must be 4 characters long");
            this.value =
                (objectId[0] << 24) |
                (objectId[1] << 16) |
                (objectId[2] << 8) |
                (objectId[3]);
        }

        // Implicit conversion from JassObjectId to String
        public static implicit operator string(JassObjectId from)
        {
            return new string(new[] {
                (char)((from.value & 0xFF000000) >> 24),
                (char)((from.value & 0x00FF0000) >> 16),
                (char)((from.value & 0x0000FF00) >> 8),
                (char)((from.value & 0x000000FF)) });
        }

        // Explicit conversion from String to JassObjectId
        public static explicit operator JassObjectId(string from)
        {
            // This has to be explicit because general C# guidelines says the following about implicit:
            // [...] implicit conversion operators should never throw exceptions and never lose information  [...]
            // http://msdn.microsoft.com/en-us/library/z5z9kes2%28v=vs.110%29.aspx

            return new JassObjectId(from);
        }

        // Implicit conversion from JassObjectId to Int32
        public static implicit operator int(JassObjectId from)
        {
            return from.value;
        }

        // Implicit conversion from Int32 to JassObjectId
        public static implicit operator JassObjectId(int from)
        {
            return new JassObjectId(from);
        }

        // Implicit conversion from JassObjectId to ObjectIdL
        public static implicit operator ObjectIdL(JassObjectId from)
        {
            return new ObjectIdL(from.value);
        }

        // Implicit conversion from JassObjectId to ObjectIdB
        public static implicit operator ObjectIdB(JassObjectId from)
        {
            return (ObjectIdB)(new ObjectIdL(from.value));
        }
    }
}
