﻿using System;
using MindWorX.War3Game.WarAPI;

namespace MindWorX.War3Game.JassAPI.Primitives
{
    [JassType("S")]
    [Serializable]
    public struct JassStringRet
    {
        public readonly int Reference;

        public JassStringRet(int reference)
        {
            this.Reference = reference;
        }

        // Implicit conversion from JassStringRet to String
        public static implicit operator string(JassStringRet from)
        {
            return Game.Instance.JassStringHandleToString(Game.Instance.JassStringIndexToJassStringHandle(from.Reference));
        }

        // Implicit conversion from String to JassStringRet
        public static implicit operator JassStringRet(string from)
        {
            return new JassStringRet(Game.Instance.StringToJassStringIndex(from));
        }
    }

    [JassType("S")]
    [Serializable]
    public struct JassStringArg
    {
        public readonly IntPtr Handle;

        public JassStringArg(IntPtr handle)
        {
            this.Handle = handle;
        }

        // Implicit conversion from JassStringArg to String
        public static implicit operator string(JassStringArg from)
        {
            return Game.Instance.JassStringHandleToString(from.Handle);
        }

        // Implicit conversion from String to JassStringArg
        public static implicit operator JassStringArg(string from)
        {
            return new JassStringArg(Game.Instance.JassStringIndexToJassStringHandle(Game.Instance.StringToJassStringIndex(from)));
        }
    }
}
