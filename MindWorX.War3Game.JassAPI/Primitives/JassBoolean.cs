﻿using System;

namespace MindWorX.War3Game.JassAPI.Primitives
{
    [JassType("B")]
    [Serializable]
    public struct JassBoolean
    {
        public readonly int Value;

        private JassBoolean(int value)
        {
            this.Value = value;
        }

        // Implicit conversion from JassBoolean to Boolean
        public static implicit operator bool(JassBoolean from) => from.Value != 0;

        // Implicit conversion from Boolean to JassBoolean
        public static implicit operator JassBoolean(bool from) => new JassBoolean(from ? 1 : 0);
    }
}
