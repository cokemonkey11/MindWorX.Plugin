﻿using System;

namespace MindWorX.War3Game.JassAPI.Primitives
{
    [JassType("H")]
    [Serializable]
    public struct JassHandle
    {
        public static JassHandle None { get; } = new JassHandle(IntPtr.Zero);


        public readonly IntPtr Handle;

        public JassHandle(IntPtr handle)
        {
            this.Handle = handle;
        }
    }
}
