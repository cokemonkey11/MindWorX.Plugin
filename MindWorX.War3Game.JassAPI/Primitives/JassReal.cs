﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace MindWorX.War3Game.JassAPI.Primitives
{
    [JassType("R")]
    [Serializable]
    public struct JassRealArg
    {
        private readonly IntPtr valueAsPtr;

        private JassRealArg(IntPtr valueAsPtr)
        {
            this.valueAsPtr = valueAsPtr;
        }

        // Implicit conversion from JassRealArg to Single
        public static implicit operator float(JassRealArg from)
        {
            //TODO: Fix obvious memory leak and perhaps optimize this a bit?
            var data = new byte[4];
            Marshal.Copy(from.valueAsPtr, data, 0, 4);
            return BitConverter.ToSingle(data, 0);
        }

        // Implicit conversion from Single to JassRealArg
        public static implicit operator JassRealArg(float from)
        {
            //TODO: Fix obvious memory leak and perhaps optimize this a bit?
            var ptr = Marshal.AllocHGlobal(sizeof(float));
            Marshal.Copy(BitConverter.GetBytes(from), 0, ptr, sizeof(float));
            return new JassRealArg(ptr);
        }

        // Implicit conversion from JassRealArg to JassRealRet
        public static implicit operator JassRealRet(JassRealArg from)
        {
            return (float)from;
        }
    }

    [JassType("R")]
    [Serializable]
    public struct JassRealRet
    {
        private readonly int valueAsInt32;

        private JassRealRet(int valueAsInt32)
        {
            this.valueAsInt32 = valueAsInt32;
        }

        // Implicit conversion from JassRealRet to Single
        public static implicit operator float(JassRealRet from)
        {
            //TODO: Fix obvious memory leak and perhaps optimize this a bit?
            var stream = new MemoryStream(4);
            var writer = new BinaryWriter(stream);
            writer.Write(from.valueAsInt32);

            return BitConverter.ToSingle(stream.ToArray(), 0);
        }

        // Implicit conversion from Single to JassRealRet
        public static implicit operator JassRealRet(float from)
        {
            //TODO: Fix obvious memory leak and perhaps optimize this a bit?
            var stream = new MemoryStream(4);
            var writer = new BinaryWriter(stream);
            writer.Write(from);
            return new JassRealRet(BitConverter.ToInt32(stream.ToArray(), 0));
        }

        // Implicit conversion from JassRealRet to JassRealArg
        public static implicit operator JassRealArg(JassRealRet from)
        {
            return (float)from;
        }
    }
}
