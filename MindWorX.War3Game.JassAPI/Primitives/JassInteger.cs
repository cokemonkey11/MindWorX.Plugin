﻿using System;

namespace MindWorX.War3Game.JassAPI.Primitives
{
    [JassType("I")]
    [Serializable]
    public struct JassInteger
    {
        public readonly int Value;

        private JassInteger(int value)
        {
            this.Value = value;
        }

        // Implicit conversion from JassInt to Int32
        public static implicit operator int(JassInteger from) => from.Value;

        // Implicit conversion from Int32 to JassInt
        public static implicit operator JassInteger(int from) => new JassInteger(from);
    }
}
