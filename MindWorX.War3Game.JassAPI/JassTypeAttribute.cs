﻿using System;

namespace MindWorX.War3Game.JassAPI
{
    [AttributeUsage(AttributeTargets.All)]
    public sealed class JassTypeAttribute : Attribute
    {
        public JassTypeAttribute(string typeString)
        {
            this.TypeString = typeString;
        }

        public string TypeString { get; }
    }
}
