﻿using System.Diagnostics;
using System.Runtime.InteropServices;
using MindWorX.Unmanaged.Windows;
using SharpCraft;
using MindWorX.GenericAddressManager;
using MindWorX.GenericLibraryInjectionSystem;
using System.ComponentModel.Composition;
using System.IO;
using Serilog;

namespace MindWorX.War3Game.InfoInjection
{
    internal class InfoInjectionPlugin : IPlugin
    {
        [Import]
        private Profile profile;

        [Import]
        private IAddressManager addressManager;

        [Import]
        private ILibraryInjectionSystem libraryInjectionSystem;

        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo(
            typeof(InfoInjectionPlugin).FullName,
            "A basic plugin that injects the current SharpCraft version to the games main menu.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIGame)
                return;

            this.libraryInjectionSystem.AddLibraryLoadedEventHandler("Game.dll", this.GameLibraryLoaded);

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        private void GameLibraryLoaded(object sender, LibraryLoadedEventArgs libraryLoadedEventArgs)
        {
            var address = this.addressManager.Current["MenuVersionStringValue"];

            if (address == default(int))
            {
                Log.Logger?.Information("Unable to inject SharpCraft version to main menu, the address is missing.");
                return;
            }

            var version = "unknown";

            var filePath = typeof(IPlugin).Assembly.Location;
            if (filePath != null && File.Exists(filePath))
                version = FileVersionInfo.GetVersionInfo(filePath).FileVersion;

            Log.Logger?.Information("Injecting SharpCraft version to main menu . . .");
            Page old;
            var versionFormatPtr = libraryLoadedEventArgs.Handle + address;
            Kernel32.VirtualProtect(versionFormatPtr, 4, Page.ExecuteReadWrite, out old);
            Marshal.WriteIntPtr(versionFormatPtr, Marshal.StringToHGlobalAnsi(
                $"|cffffdeadSharpCraft {version}|r - |cff87ceebWarcraft %d.%d.%d.%d (%s %s)|r"));
            Kernel32.VirtualProtect(versionFormatPtr, 4, old, out old); // Restore
            Log.Logger?.Information(". . . done!");

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }
    }
}
