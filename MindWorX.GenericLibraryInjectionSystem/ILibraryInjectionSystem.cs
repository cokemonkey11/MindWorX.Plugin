﻿using System;

namespace MindWorX.GenericLibraryInjectionSystem
{
    public interface ILibraryInjectionSystem
    {
        /// <summary>
        /// Add a handler for all dll libraries. This can easily affect performance or make the game unloadable.
        /// </summary>
        /// <param name="handler"></param>
        void AddLibraryLoadingEventHandler(EventHandler<LibraryLoadingEventArgs> handler);

        /// <summary>
        /// Removes a previously added handler.
        /// </summary>
        /// <param name="handler"></param>
        /// <returns>True if the handler was removed. False if the handler was never added before.</returns>
        bool RemoveLibraryLoadingEventHandler(EventHandler<LibraryLoadingEventArgs> handler);

        /// <summary>
        /// Add a handler for a specific dll library.
        /// </summary>
        /// <param name="fileName">Case-sensitive library name.</param>
        /// <param name="handler"></param>
        void AddLibraryLoadingEventHandler(string fileName, EventHandler<LibraryLoadingEventArgs> handler);

        /// <summary>
        /// Removes a previously added handler.
        /// </summary>
        /// <param name="fileName">Case-sensitive library name.</param>
        /// <param name="handler"></param>
        /// <returns>True if the handler was removed. False if the handler was never added before.</returns>
        bool RemoveLibraryLoadingEventHandler(string fileName, EventHandler<LibraryLoadingEventArgs> handler);

        /// <summary>
        /// Add a handler for all dll libraries. This can easily affect performance or make the game unloadable.
        /// </summary>
        /// <param name="handler"></param>
        void AddLibraryLoadedEventHandler(EventHandler<LibraryLoadedEventArgs> handler);

        /// <summary>
        /// Removes a previously added handler.
        /// </summary>
        /// <param name="handler"></param>
        /// <returns>True if the handler was removed. False if the handler was never added before.</returns>
        bool RemoveLibraryLoadedEventHandler(EventHandler<LibraryLoadedEventArgs> handler);

        /// <summary>
        /// Add a handler for a specific dll library.
        /// </summary>
        /// <param name="fileName">Case-sensitive library name.</param>
        /// <param name="handler"></param>
        void AddLibraryLoadedEventHandler(string fileName, EventHandler<LibraryLoadedEventArgs> handler);

        /// <summary>
        /// Removes a previously added handler.
        /// </summary>
        /// <param name="fileName">Case-sensitive library name.</param>
        /// <param name="handler"></param>
        /// <returns>True if the handler was removed. False if the handler was never added before.</returns>
        bool RemoveLibraryLoadedEventHandler(string fileName, EventHandler<LibraryLoadedEventArgs> handler);
    }
}
