﻿using System;

namespace MindWorX.GenericLibraryInjectionSystem
{
    public class LibraryLoadedEventArgs : EventArgs
    {
        public LibraryLoadedEventArgs(string fileName, IntPtr handle, bool isReplaced)
        {
            this.FileName = fileName;
            this.Handle = handle;
            this.IsReplaced = isReplaced;
        }

        /// <summary>
        /// The name of the library being loaded.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// The handle of the library being loaded.
        /// </summary>
        public IntPtr Handle { get; }

        /// <summary>
        /// This is true if the library has been replaced by another plugin.
        /// </summary>
        public bool IsReplaced { get; }
    }
}
