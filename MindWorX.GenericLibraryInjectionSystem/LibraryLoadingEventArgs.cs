﻿using System;

namespace MindWorX.GenericLibraryInjectionSystem
{
    public class LibraryLoadingEventArgs : EventArgs
    {
        public LibraryLoadingEventArgs(string fileName)
        {
            this.FileName = fileName;
        }

        /// <summary>
        /// The name of the library being loaded. This can be modified to load another library instead. 
        /// If set to null, the library does not get loaded at all. Probably not very safe to do.
        /// </summary>
        public string FileName { get; set; }
    }
}
