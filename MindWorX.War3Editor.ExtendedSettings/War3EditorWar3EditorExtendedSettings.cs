﻿using System;
using System.ComponentModel.Composition;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using EasyHook;
using MindWorX.GenericAddressManager;
using MindWorX.Unmanaged.Windows;
using MindWorX.War3Editor.ExtendedSettings.Dialogs;
using MindWorX.War3Editor.Hooks;
using Serilog;
using SharpCraft;

namespace MindWorX.War3Editor.ExtendedSettings
{
    [Export(typeof(IWar3EditorExtendedSettings))]
    internal class War3EditorWar3EditorExtendedSettings : IWar3EditorExtendedSettings
    {
        // ReSharper disable InconsistentNaming
        #region bool __thiscall CheckObjectLimitsWarning(int this, int a2)

        private LocalHook CheckObjectLimitsWarningLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        private delegate bool CheckObjectLimitsWarningPrototype(IntPtr @this, bool a2);

        private CheckObjectLimitsWarningPrototype CheckObjectLimitsWarning;

        #endregion

        #region bool __thiscall CheckObjectLimitsError(int this, int a2)

        private LocalHook CheckObjectLimitsErrorLocalHook;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)]
        private delegate bool CheckObjectLimitsErrorPrototype(IntPtr @this, bool a2);

        private CheckObjectLimitsErrorPrototype CheckObjectLimitsError;

        #endregion
        // ReSharper restore InconsistentNaming

        [Import]
        private Profile profile;

        [Import]
        private IAddressManager addressManager;

        [Import]
        private IWar3EditorHooks worldEditHooks;

        private ObjectIdDialog objectIdDialog;

        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo(
            typeof(War3EditorWar3EditorExtendedSettings).FullName,
            "A plugin that adds some additional editor settings, like nolimits.",
            "1.0.0.0", "MindWorX");

        public bool IsReady { get; private set; }

        public bool IsNoLimitEnabled { get; set; } = true;

        public void Initialize()
        {
            if (this.profile.Type != ProfileType.BlizzardWarcraftIIIEditor)
                return;

            var handle = Kernel32.GetModuleHandle("worldedit.exe");

            if (this.addressManager.Current["WE_CheckObjectLimitsWarning"] != 0)
            {
                var checkObjectLimitsWarningPtr = handle + this.addressManager.Current["WE_CheckObjectLimitsWarning"];
                Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.CheckObjectLimitsWarningHook), (uint)checkObjectLimitsWarningPtr);
                this.CheckObjectLimitsWarning = Marshal.GetDelegateForFunctionPointer<CheckObjectLimitsWarningPrototype>(checkObjectLimitsWarningPtr);
                this.CheckObjectLimitsWarningLocalHook = LocalHook.Create(checkObjectLimitsWarningPtr, new CheckObjectLimitsWarningPrototype(this.CheckObjectLimitsWarningHook), null);
                this.CheckObjectLimitsWarningLocalHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WE_CheckObjectLimitsWarning");
            }

            if (this.addressManager.Current["WE_CheckObjectLimitsError"] != 0)
            {
                var checkObjectLimitsErrorPtr = handle + this.addressManager.Current["WE_CheckObjectLimitsError"];
                Log.Logger?.Information("Injecting {functionName} @ 0x{address:X8}.", nameof(this.CheckObjectLimitsErrorHook), (uint)checkObjectLimitsErrorPtr);
                this.CheckObjectLimitsError = Marshal.GetDelegateForFunctionPointer<CheckObjectLimitsErrorPrototype>(checkObjectLimitsErrorPtr);
                this.CheckObjectLimitsErrorLocalHook = LocalHook.Create(checkObjectLimitsErrorPtr, new CheckObjectLimitsErrorPrototype(this.CheckObjectLimitsErrorHook), null);
                this.CheckObjectLimitsErrorLocalHook.ThreadACL.SetExclusiveACL(new[] { 0 });
            }
            else
            {
                Log.Logger?.Error("Unable to locate address for {addressName}", "WE_CheckObjectLimitsError");
            }

            Log.Logger?.Information("Installing Object Id Dialog");
            this.objectIdDialog = new ObjectIdDialog();
            this.worldEditHooks.GetNextObjectId += this.WorldEditHooksOnGetNextObjectId;

            this.IsReady = true;
            this.Ready?.Invoke(this);
        }

        private bool CheckObjectLimitsWarningHook(IntPtr @this, bool a2)
        {
            Log.Logger?.Information("+CheckObjectLimitsWarning({this:X8}, {a2})", (uint)@this, a2);

            if (this.IsNoLimitEnabled)
                return true;

            var result = this.CheckObjectLimitsWarning(@this, a2);
            Log.Logger?.Information("-CheckObjectLimitsWarning({this:X8}, {a2}) = {result}", (uint)@this, a2, result);
            return result;
        }

        private bool CheckObjectLimitsErrorHook(IntPtr @this, bool a2)
        {
            Log.Logger?.Information("+CheckObjectLimitsError({this:X8}, {a2})", (uint)@this, a2);

            if (this.IsNoLimitEnabled)
                return true;

            var result = this.CheckObjectLimitsError(@this, a2);
            Log.Logger?.Information("-CheckObjectLimitsError({this:X8}, {a2}) = {result}", (uint)@this, a2, result);
            return result;
        }

        private void WorldEditHooksOnGetNextObjectId(object sender, ObjectIdEventArgs e)
        {
            this.objectIdDialog.ObjectId = e.ObjectId;
            if (this.objectIdDialog.ShowDialog() == DialogResult.OK)
                e.ObjectId = this.objectIdDialog.ObjectId;
        }
    }
}
