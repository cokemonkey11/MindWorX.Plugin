﻿using System;
using System.Windows.Forms;
using MindWorX.Blizzard;

namespace MindWorX.War3Editor.ExtendedSettings.Dialogs
{
    public partial class ObjectIdDialog : Form
    {
        private ObjectIdB objectId;

        public ObjectIdDialog()
        {
            this.InitializeComponent();
        }

        public ObjectIdB ObjectId
        {
            get { return this.objectId; }
            set
            {
                this.objectId = value;
                this.ObjectIdChanged();
            }
        }

        private void ObjectIdChanged()
        {
            this.objectIdTextBox.Text = this.objectId.ToString();
        }

        private void objectIdTextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.objectId = new ObjectIdB(this.objectIdTextBox.Text);
                this.okButton.Enabled = true;
            }
            catch (ArgumentOutOfRangeException)
            {
                this.okButton.Enabled = false;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void objectIdTextBox_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.okButton.Enabled)
                this.DialogResult = DialogResult.OK;
        }
    }
}
