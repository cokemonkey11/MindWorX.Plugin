﻿using SharpCraft;

namespace MindWorX.War3Editor.ExtendedSettings
{
    public interface IWar3EditorExtendedSettings : IPlugin
    {
        bool IsNoLimitEnabled { get; set; }
    }
}
