﻿using SharpCraft;

namespace MindWorX.War3Editor.Grimex
{
    internal class War3EditorGrimex : IPlugin
    {
        public event PluginReadyEventHandler Ready;

        public PluginInfo Info { get; } = new PluginInfo(
            "Grim Extension Pack", "",
            "1.0b", "PitzerMike");

        public bool IsReady { get; private set; }

        public void Initialize()
        {
            this.IsReady = true;
            this.Ready?.Invoke(this);
        }
    }
}
